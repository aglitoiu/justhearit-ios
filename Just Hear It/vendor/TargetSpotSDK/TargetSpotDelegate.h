//
//  TargetSpotDelegate.h
//  libTargetSpot
//
//  Created by Kirby Turner on 5/14/10.
//
//  Copyright 2010 TargetSpot, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TargetSpot.h"


@protocol TargetSpotDelegate <NSObject>
@optional

/////////////////
// Control settings for the ad view.

- (CGFloat)targetSpotAlphaForBackground;  // Defaults to 0.50.
- (float)targetSpotVolume;                // Defaults to 1.0.


/////////////////
// TargetSpot OnDemand web service call parameters.

- (NSString *)targetSpotStation;
- (TSContentSize)targetSpotContentSize;   // Defaults to TSContentSizeLarge.
- (TSAdType)targetSpotAdType;             // Defaults to TSAdTypeAudio.
- (NSInteger)targetSpotAdPlaybackLength;  // Defaults to 15.
- (TSGender)targetSpotListenersGender;    // Defaults to TSGenderUnknown
- (NSInteger)targetSpotListenersAge;      // Defaults to 0.
- (NSInteger)targetSpotMaxNumAds;         // defaults to -1
- (NSString *)targetSpotListenersPostalCode; // Defaults to zero-length string.
- (TSLocationCoordinate)targetSpotLocationCoordinate; // Defaults to (0, 0).
- (BOOL)targetSpotTestMode; // Defaults to false
- (BOOL)targetSpotManagedMode; // Defaults to true
- (BOOL)targetSpotBackToBackMode; // Defaults to false
- (TSAudioFormat)targetSpotAudioFormat; // Defaults to TSAudioMP3
- (TSAudioBitrate)targetSpotAudioBitrate; // Defaults to TSAudioBitrate64kbps


/////////////////
// Notification messages sent to TargetSpot delegate.

- (void)targetSpotAdViewDidLoadAd:(TargetSpotAdView *)adView;
- (void)targetSpotAdViewDidFinishAction:(TargetSpotAdView *)adView;
- (BOOL)targetSpotAdViewWillBeginAction:(TargetSpotAdView *)adView willLeaveApplication:(BOOL)willLeave;
- (void)targetSpotAdViewDidFailToLoadAdWithError:(NSError *)error;
- (void)targetSpotAdViewWillDisplayAd:(TargetSpotAdView *)adView;
- (void)targetSpotAdViewDidDisplayAd:(TargetSpotAdView *)adView;
- (void)targetSpotAdViewWillHideAd:(TargetSpotAdView *)adView;
- (void)targetSpotAdViewDidHideAd:(TargetSpotAdView *)adView;

@end

TargetSpot iOS SDK
==================


SDK Integration Instructions
============================

1. Add the TargetSpot library code and headers to your project.
   The files are: TargetSpot.h, TargetSpotDelegate.h and libTargetSpot.a.

2. Add the following frameworks to your project. These frameworks are required
   to compile:
   - AVFoundation.framework
   - SystemConfiguration.framework

3. Add the flag -ObjC to the Other Linker Flags in the target's build settings.


Displaying an Ad
================

1. Add the TargetSpotAdViewDelegate protocol to the interface declaration of
   your view controller.

   Example:

   #import "TargetSpotDelegate.h"
   @interface MyViewController : UIViewController <TargetSpotDelegate> { ... }

2. Create an instance of the TargetSpotAdView by calling the convenience method
   [TargetSpotAdView adViewWithDelegate:].

   A good place to do this is in the viewDidLoad method of your view controller.

   Example:

   - (void)viewDidLoad
   {
      [super viewDidLoad];
      TargetSpotAdView *adView = [TargetSpotAdView adView];
      [adView setDelegate:self];
      [adView setDesiredPlaybackLengths:[NSArray arrayWithObject:[NSNumber numberWithInt:15]]];
   }

3. Add the TargetSpotAdView instance to the view hierarchy. The viewDidLoad
   in the view controller is a good place to do this.

   Example:

   - (void)viewDidLoad
   {
      [super viewDidLoad];
      TargetSpotAdView *adView = [TargetSpotAdView adView];
      [adView setDelegate:self];
      [adView setDesiredPlaybackLengths:[NSArray arrayWithObject:[NSNumber numberWithInt:15]]];
      [[self view] addSubview:adView];
   }

4. Implement the required delegate method targetSpotStation.

   This method must return the unique station identifier assigned to you
   by TargetSpot.

   Example:

   - (NSString *)targetSpotStation
   {
      return @"STATION ID HERE";
   }

5. Implement code in your application to display the ad. Be sure to create an ivar reference to the TargetSpot Ad View.

   Example:

   - (void)displayAd
   {
      [adView showAnimated:YES];
   }

6. Implement the optional delegate method targetSpotAdViewDidFinishAction:.

   This is the ideal place to hide the ad view.

   Example:

   - (void)targetSpotAdViewDidFinishAction:(TargetSpotAdView *)adView
   {
      [adView hideWithAnimation:YES];
   }

7. Start requesting ads by calling the startAdSession on the TargetSpotAdView instance.

   The ideal place to call this method is immediately after adding the ad view
   to the view hierarchy.

   Example:

   - (void)viewDidLoad
   {
      [super viewDidLoad];
      TargetSpotAdView *adView = [TargetSpotAdView adView];
      [adView setDelegate:self];
      [adView setDesiredPlaybackLengths:[NSArray arrayWithObject:[NSNumber numberWithInt:15]]];
      [[self view] addSubview:adView];
      [adView startAdSession];
   }

8. Display a pre-roll ad by calling showPrerollAd on the TargetSpotAdView instance.

   Example:

   - (void)viewDidLoad
   {
      [super viewDidLoad];
      TargetSpotAdView *adView = [TargetSpotAdView adView];
      [adView setDelegate:self];
      [adView setDesiredPlaybackLengths:[NSArray arrayWithObject:[NSNumber numberWithInt:15]]];
      [[self view] addSubview:adView];
      [adView startAdSession];
      
      [adView showPrerollAd];
   }

9. Build and run your application. Your app will now display and play
   TargetSpot ads.


Display Ad History
==================

The TargetSpot Ad History is intended to be displayed at the top or bottom on the application screen. This requires you to set the origin of the ad history view frame prior to adding the view to the view hierarchy.

The following is an example of adding the ad history view to the view hierarchy. This code is typically implemented in the viewDidLoad method of the view controller.

- (void)viewDidLoad
{
   [super viewDidLoad];

   CGRect bounds = [[self view] bounds];
   int y = 0;  // Display on top.
   y = bounds.size.height - 80;  // Display on bottom.
   TargetSpotAdHistoryView *adHistoryView = [TargetSpotAdHistoryView adHistoryViewWithOrigin:CGPointMake(0, y)];
   [[self view] addSubview:adHistoryView];
}

A controller built into the ad history view will manage any and all user interaction with the ad history view.

TargetSpot SDK API Class Reference
==================================

For the most up-to-date document, view the header files TargetSpot.h and TargetSpotDelegate.h.

Class Name: TargetSpotAdView
----------------------------

Class Methods:

   /**
    * Creates and returns an auto-released reference to an instance of
    * the TargetSpotAdView class.
    */
   + (TargetSpotAdView *)adViewWithDelegate:(id<TargetSpotDelegate>)delegate;

Instance Methods:

   /**
    * Shows the TargetSpot ad.
    */
   - (void)showWithAnimation:(BOOL)animate;

   /**
    * Hides the TargetSpot ad.
    */
   - (void)hideWithAnimation:(BOOL)animate;

   /**
    * Tells the TargetSpot SDK to retrieve a new ad.
    */
   - (void)requestAd;

   /**
    * Tells the TargetSpot SDK to retrieve a new ad after n seconds have elapsed.
    * Set repeats flag to YES to retrieve a new ad every n seconds.
    * Seconds must be greater than 0 to activate the internal timer.
    * Note the elapse timer starts immediately after the call and after each ad has completed playing when the repeats flag is YES.
    */
   - (void)requestAdWithInterval:(NSInteger)seconds repeats:(BOOL)repeats;

   /**
    * Tells the TargetSpot SDK to retrieve a new preroll ad.
    */
   - (void)requestPrerollAd;

   /**
    * Plays pending ads.
    */
   - (void)playAd;

Class Name: TargetSpotAdHistoryView
-----------------------------------

Class Method:
   /**
    * Creates and returns an auto-released reference to an instance of
    * the TargetSpotAdView class.
    */
   + (TargetSpotAdHistoryView *)adHistoryViewWithOrigin:(CGPoint)origin;

Type Name: TSGender
-------------------
   /**
    *  TSGender
    *  
    *  Discussion:
    *    Type used to represent the gender of the listener.
    *
    */
   typedef enum
   {
      TSGenderUnknown = 0,
      TSGenderMale,
      TSGenderFemale,
   } TSGender;

Type Name: TSContentSize
------------------------

   /**
    *  TSContentSize
    *  
    *  Discussion:
    *    Type used to represent the content size.
    *
    */
   typedef enum
   {
      TSContentSizeSmall = 0,
      TSContentSizeLarge,
      TSContentSizeUnknown
   } TSContentSize;

Type Name: TSAdType
-------------------
   /**
    *  TSAdType
    *  
    *  Discussion:
    *    Type used to represent the type of ad.
    *
    */
   typedef enum
   {
      TSAdTypeAudio = 0,   // Default
      TSAdTypeVideo,
      TSAdTypeText,
      TSAdTypeGraphic,
   } TSAdType;

Type Name: TSLocationDegrees
----------------------------

   /**
    *  TSLocationDegrees
    *  
    *  Discussion:
    *    Type used to represent a latitude or longitude coordinate in degrees under the WGS 84 reference
    *    frame. The degree can be positive (North and East) or negative (South and West).  
    */
   typedef double TSLocationDegrees;

Type Name: TSLocationCoordinate
-------------------------------

   /**
    *  TSLocationCoordinate
    *  
    *  Discussion:
    *    A structure that contains a geographical coordinate.
    *
    *  Fields:
    *    latitude:
    *      The latitude in degrees.
    *    longitude:
    *      The longitude in degrees.
    */
   typedef struct {
   	TSLocationDegrees latitude;
   	TSLocationDegrees longitude;
   } TSLocationCoordinate;


Protocol: TargetSpotDelegate
----------------------------

Required methods:
   - (NSString *)targetSpotStation;

Optional methods:

   /////////////////
   // Control settings for the ad view.

   - (CGFloat)targetSpotAlphaForBackground;  // Defaults to 0.50.
   - (float)targetSpotVolume;                // Defaults to 1.0.


   /////////////////
   // TargetSpot OnDemand web service call parameters.

   - (TSContentSize)targetSpotContentSize;   // Defaults to TSContentSizeLarge.
   - (TSAdType)targetSpotAdType;             // Defaults to TSAdTypeAudio.
   - (NSInteger)targetSpotAdPlaybackLength;  // Defaults to 15.
   - (TSGender)targetSpotListenersGender;    // Defaults to TSGenderUnknown
   - (NSInteger)targetSpotListenersAge;      // Defaults to 0.
   - (NSString *)targetSpotListenersPostalCode; // Defaults to zero-length string.
   - (NSString *)targetSpotListenersIncome;  // Defaults to zero-length string.
   - (TSLocationCoordinate)targetSpotLocationCoordinate; // Defaults to (0, 0).


   /////////////////
   // Notification messages sent to TargetSpot delegate.

   - (void)targetSpotAdViewDidLoadAd:(TargetSpotAdView *)adView;
   - (void)targetSpotAdViewDidFinishAction:(TargetSpotAdView *)adView;
   - (BOOL)targetSpotAdViewWillBeginAction:(TargetSpotAdView *)adView willLeaveApplication:(BOOL)willLeave;
   - (void)targetSpotAdViewDidFailToLoadAdWithError:(NSError *)error;

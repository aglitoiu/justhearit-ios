//
//  NWSlider.h
//  Just Hear It
//
//  Created by Goiceanu Ciprian on 12/11/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NWSlider : UISlider

@end

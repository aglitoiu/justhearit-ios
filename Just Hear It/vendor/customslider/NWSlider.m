//
//  NWSlider.m
//  Just Hear It
//
//  Created by Goiceanu Ciprian on 12/11/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "NWSlider.h"

#define SIZE_EXTENSION_Y -20
#define SIZE_EXTENSION_X 0

#define THUMB_SIZE 50
#define EFFECTIVE_THUMB_SIZE 50

@implementation NWSlider

- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent*)event {
    
    //Log(@"pointSide");
    
    CGRect bounds = self.bounds;
    bounds = CGRectInset(bounds, SIZE_EXTENSION_X, SIZE_EXTENSION_Y);
    return CGRectContainsPoint(bounds, point);
    
}

- (BOOL) beginTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event {
    
    CGRect bounds = self.bounds;
    float thumbPercent = (self.value - self.minimumValue) / (self.maximumValue - self.minimumValue);
    float thumbPos = THUMB_SIZE + (thumbPercent * (bounds.size.width - (2 * THUMB_SIZE)));
    CGPoint touchPoint = [touch locationInView:self];
    return (touchPoint.x >= (thumbPos - EFFECTIVE_THUMB_SIZE) && touchPoint.x <= (thumbPos + EFFECTIVE_THUMB_SIZE));
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

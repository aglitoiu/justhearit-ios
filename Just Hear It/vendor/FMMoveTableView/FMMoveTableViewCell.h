@protocol FMMoveTableViewCell
@optional

- (void)prepareForMove;
- (void)takeShotIn:(UIView *) snapshotImageView;

@end
//
//  FMMoveTableView.h
//  FMFramework
//
//  Created by Florian Mielke.
//  Copyright 2012 Florian Mielke. All rights reserved.
//


#import <QuartzCore/QuartzCore.h>


@class FMMoveTableView;

@interface FMSnapShotImageView : UIView

- (void)moveByOffset:(CGPoint)offset;

@end





@protocol FMMoveTableViewDelegate <NSObject, UITableViewDelegate>

@optional

// Allows customization of the target row for a particular row as it is being moved
- (NSIndexPath *)moveTableView:(FMMoveTableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath;

// Called before the particular row is about to change to a moving state
- (void) moveTableView:(FMMoveTableView *)tableView willMoveRowAtIndexPath:(NSIndexPath *)indexPath;

- (void) moveTableView:(FMMoveTableView *)tableView startedDraggingWithGesture:(UILongPressGestureRecognizer *)gesture;
- (void) moveTableView:(FMMoveTableView *)tableView draggingWithGesture:(UILongPressGestureRecognizer *)gesture withInitialIndexPath:(NSIndexPath *)movingIndex;

- (void)moveTableViewDidEndDragging:(FMMoveTableView *)tableView withGesture:(UILongPressGestureRecognizer *)gesture withInitialIndexPath:(NSIndexPath *)indexPath;

- (void) moveTableViewFinished:(FMMoveTableView *)tableView;

@end



@protocol FMMoveTableViewDataSource <NSObject, UITableViewDataSource>

// Called after the particular row is being dropped to it's new index path
- (void)moveTableView:(FMMoveTableView *)tableView moveRowFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath;

@optional

// Allows to reorder a particular row
- (BOOL)moveTableView:(FMMoveTableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath;

@end



@interface FMMoveTableView : UITableView <UIGestureRecognizerDelegate>

@property (nonatomic, weak) id <FMMoveTableViewDataSource> dataSource;
@property (nonatomic, weak) id <FMMoveTableViewDelegate> delegate;
@property (nonatomic, weak) id <FMMoveTableViewDelegate> placeholderDelegate;
@property (nonatomic, strong) NSIndexPath *movingIndexPath;
@property (nonatomic, strong) NSIndexPath *initialIndexPathForMovingRow;
@property (nonatomic, strong) FMSnapShotImageView *snapShotImageView;
@property (nonatomic, strong) FMSnapShotImageView *snapShotPlaceholder;
@property (nonatomic, assign) BOOL stopAutoscroll;
@property (nonatomic, assign) BOOL finishAnimated;

- (BOOL)indexPathIsMovingIndexPath:(NSIndexPath *)indexPath;
- (NSIndexPath *)adaptedIndexPathForRowAtIndexPath:(NSIndexPath *)indexPath;

@end

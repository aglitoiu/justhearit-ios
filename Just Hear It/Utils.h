//
//  Utils.h
//  Just Hear It
//
//  Created by Andrei S on 9/7/12.
//  Copyright (c) 2012 Hearit Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PlaylistSong;
@class Playlist;
#define STATUS_BAR_HEIGHT 20

@interface Utils : NSObject

//returns true if screen is in portrait mode (up or down)
+(BOOL) isPortrait;

+(BOOL) isPortrait:(UIDeviceOrientation) orientation;
+(BOOL) IS_IPHONE_X;
+ (void)alert:(NSString *)message;

//returns the screen width bounds regardless of orientation
+(int) screenWidth;

//returns screen height bounds regardless of orientation
+(int) screenHeight;

+(int) currentWidth:(UIDeviceOrientation) orientation;

+(int) currentHeight:(UIDeviceOrientation) orientation;

//orientation aware + statusbar aware width
+(int) currentWidth;

//orientation aware + statusbar aware height
+(int) currentHeight;

//status bar visible?
+(BOOL) statusBar;

+ (UIImage *)imageFromColor:(UIColor *)color withWidth:(int)width andHeight:(int)height;


+ (UIImage *) imageFromColor:(UIColor *)color;

+ (void) makeSearchStyleTextField:(UITextField *) textField;

+ (void) queryBlock:(id (^)(void))block inBackgroundQueue:(NSString *) backgroundQueue completion:(void (^)(id))completion;

+ (NSString *) uuid;

+ (UIImage *)UIImageFromView:(UIView*) view;

+ (UIImageView *)UIImageViewFromView:(UIView *)view;

+ (UIImage *)image:(UIImage *)image byApplyingAlpha:(CGFloat)alpha;

+ (void) applyMaskToView:(UIView *)view;

+ (UIViewController *)sharingViewControllerForSong:(PlaylistSong *)song;

+ (UIViewController *)sharingViewControllerForPlaylist:(Playlist *)playlist;

+ (uint64_t)getFreeDiskspace;

+ (BOOL)isReachable;

+ (BOOL)isReachableOverWifi;

+ (BOOL)isReachableOverCellular;

+ (BOOL)applicationInBackground;

+ (BOOL)isLandscape;

+ (BOOL)isVersionLessThan_8_0;

+ (BOOL)isVersionMoreThan_8_0;
@end

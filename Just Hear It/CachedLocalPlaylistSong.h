//
//  CachedPlaylistSong.h
//  Just Hear It
//
//  Created by andrei st on 1/16/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CachedLocalPlaylist;

@interface CachedLocalPlaylistSong : NSManagedObject

@property (nonatomic, retain) NSString * playlistId;
@property (nonatomic, retain) NSString * album;
@property (nonatomic, retain) NSString * artist;
@property (nonatomic, retain) NSNumber * fileSize;
@property (nonatomic, retain) NSDate * lastListenedAt;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * songId;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * downloaded;
@property (nonatomic, retain) NSSet *cachedPlaylists;
@property (nonatomic, retain) NSSet *cachedLocalPlaylists;
@property (nonatomic, retain) NSNumber *nodownload;
@property (nonatomic, retain) NSString * isNewPlaylistSong;
@property (nonatomic, assign) NSNumber * order;
@property (nonatomic, assign) NSNumber * position;
@property (nonatomic, retain) NSString * playlistSongId;
@property (nonatomic, retain) NSString * shareUrl;
@property (nonatomic, retain) NSString * token;

@end

@interface CachedLocalPlaylistSong (CoreDataGeneratedAccessors)

- (void)addCachedLocalPlaylistsObject:(CachedLocalPlaylist *)value;
- (void)removeCachedLocalPlaylistsObject:(CachedLocalPlaylist *)value;

- (void)addCachedLocalPlaylists:(NSSet *)values;
- (void)removeCachedLocalPlaylists:(NSSet *)values;

@end

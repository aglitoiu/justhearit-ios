//
//  CachedPlaylist.m
//  Just Hear It
//
//  Created by andrei st on 1/16/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "CachedPlaylist.h"
#import "CachedPlaylistSong.h"


@implementation CachedPlaylist

@dynamic playlistId;
@dynamic playlistType;
@dynamic name;
@dynamic cachedPlaylistSongs;
@dynamic position;

@end

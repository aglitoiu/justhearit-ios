#import <QuartzCore/QuartzCore.h>
#import "HandleView.h"

@implementation HandleView

@synthesize handleDraggingView, handleNormalView, headerHeight;

- (void) initializeWithHeaderHeight:(NSUInteger)height {
    headerHeight = height;
    handleDraggingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [Utils currentWidth], headerHeight)];
    handleNormalView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [Utils currentWidth], headerHeight)];
    
    handleNormalView.autoresizingMask = UIViewAutoresizingAll;
    handleDraggingView.autoresizingMask = UIViewAutoresizingAll;

    [self addSubview:handleDraggingView];
    [self addSubview:handleNormalView];
    self.clipsToBounds = NO;
}

- (void) setHandleProperties:(UIImageView *)imageView {
    imageView.contentMode = UIViewContentModeScaleToFill;
    imageView.backgroundColor = [UIColor clearColor];
    imageView.opaque = NO;
    imageView.frame = CGRectMake(0, 0, [Utils currentWidth], headerHeight);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
}

- (void) showNormalView {
    [self fadeOut:handleDraggingView];
    [self fadeIn:handleNormalView];
}

- (void) showDraggingView {
    [self fadeOut:handleNormalView];
    [self fadeIn:handleDraggingView];
}

- (void) fadeOut:(UIView *)view {
    view.userInteractionEnabled = NO;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.toValue = [NSNumber numberWithFloat:0.0f];
    animation.duration = 0.2;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    [view.layer addAnimation:animation forKey:@"opacity"];
}

- (void) fadeIn:(UIView *)view {
    view.userInteractionEnabled = YES;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.toValue = [NSNumber numberWithFloat:1.0f];
    animation.duration = 0.2;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    [view.layer addAnimation:animation forKey:@"opacity"];
}

@end
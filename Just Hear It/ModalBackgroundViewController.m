#import "ModalBackgroundViewController.h"

#define CLOSE_BUTTON_SIZE 60


@implementation ModalBackgroundViewController

@synthesize parentImageView, closeButton,closeButtonw;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //add the background first, push it bottom on the layer stack
    
    //CGRect screenFrame = [[UIScreen mainScreen] bounds];

    
    /*if(IS_DEVICE == IS_IPAD) {
     screenFrame = CGRectMake([[UIScreen mainScreen] bounds].origin.x, [[UIScreen mainScreen] bounds].origin.y, [Utils currentWidth], [Utils currentHeight]);
     }*/
        CGRect screenFrame = CGRectMake(0, 0, [Utils currentWidth], [Utils currentHeight]);
    closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(screenFrame.size.width - CLOSE_BUTTON_SIZE, 0, CLOSE_BUTTON_SIZE, CLOSE_BUTTON_SIZE);
    [closeButton setImage:[UIImage imageNamed:@"icon_close.png"] forState:UIControlStateNormal];
    closeButton.contentMode = UIViewContentModeCenter;
    [self.view addSubview:closeButton];
    [closeButton addTarget:self action:@selector(dismissViewController:) forControlEvents:UIControlEventTouchUpInside];


    //take a shot of what was in the parent view controller
    parentImageView = [self _parentImageView];
    
    //make the background and the view transparent (we only need what's on the VC in a shot)
    self.view.opaque = NO;
    
    //take a shot of the current view controller
    UIImageView *selfImageView = [Utils UIImageViewFromView:self.view];
    selfImageView.frame = screenFrame;
    
    //make a view which we'll push down under the screen (vertically)
    CGRect animationViewFrame = screenFrame;
    animationViewFrame.origin.y = animationViewFrame.size.height;
    UIView *animationView = [[UIView alloc] initWithFrame:animationViewFrame];
    
    //make a background image, push it in the animation view, and put it above it (currently where the screen should be)
   // UIImageView *backgroundImageView = [UIImageView imageNamed:@"background.png"];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[self _blurryShot]];
    CGRect backgroundImageViewFrame = screenFrame;
    backgroundImageViewFrame.origin.y = -backgroundImageViewFrame.size.height;
    backgroundImageView.frame = backgroundImageViewFrame;
    
  //  [animationView addSubview:backgroundImageView];
   // [animationView addSubview:selfImageView];
    
    //otherwise the background that's above it will show up on the screen
    animationView.clipsToBounds = YES;
    
    //[self.view addSubview:parentImageView];
   //[self.view addSubview:animationView];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         animationView.frame = screenFrame;
                         backgroundImageView.frame = screenFrame;
                     } completion:^(BOOL finished) {
                         [self.background setImage:backgroundImageView.image];
                         [parentImageView removeFromSuperview];
                         [animationView removeFromSuperview];
                     }];
}

- (UIImage*) _blurryShot {
    UIView *blurryBackground = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self _parentImageView];
    FXBlurViewScaled *blurView = [[FXBlurViewScaled alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
    blurView.dynamic = NO;
    blurView.blurRadius = 30;
    blurView.tintColor = HexColor(0x000000);
    [blurryBackground addSubview:parentImageView];
    [blurryBackground addSubview:blurView];
    return [Utils UIImageFromView:blurryBackground];
}


- (UIImageView*) _parentImageView {
    if (parentImageView) return parentImageView;
    
    parentImageView = [Utils UIImageViewFromView:self.presentingViewController.view];
    parentImageView.frame = [[UIScreen mainScreen] bounds];
    return parentImageView;
}

@end

#import "LocalPlaylist.h"
#import "Playlists.h"
#import "SearchParams.h"
#import "Pagination.h"

@implementation LocalPlaylist

- (id) init {
    if(self=[super init]) {
        self.localPlaylist = YES;
        self.searchParams = [[SearchParams alloc] init];
        _searchParams.pagination = self.pagination;
    }
    return self;
}

+ (LocalPlaylist *) localSearchPlaylistWithQuery:(NSString *) query {
    LocalPlaylist *playlist = [[LocalPlaylist alloc] init];
    playlist.playlistId = [NSString stringWithFormat:@"search_%@", query];
    playlist.localPlaylistType = localPlaylistSearch;
    playlist.name = query;
    playlist.searchParams.query = query;
    playlist.playlistGroupObject = [[Playlists sharedInstance] defaultPlaylistsGroup];
    return playlist;
}

+ (LocalPlaylist *) localPlaylistWithSongToken:(NSString *) token {
    LocalPlaylist *playlist = [[LocalPlaylist alloc] init];
    playlist.playlistId = [NSString stringWithFormat:@"song_%@", token];
    playlist.localPlaylistType = localPlaylistSongToken;
    playlist.name = token;
    playlist.playlistGroupObject = [[Playlists sharedInstance] defaultPlaylistsGroup];
    return playlist;
}

+ (LocalPlaylist *) localSearchPlaylistsWithQuery:(NSString *) query {
    LocalPlaylist *playlist = [[LocalPlaylist alloc] init];
    playlist.playlistId = [NSString stringWithFormat:@"search_playlists_%@", query];
    playlist.localPlaylistType = localPlaylistSearchPlaylists;
    playlist.name = query;
    playlist.searchParams.query = query;
    playlist.playlistGroupObject = [[Playlists sharedInstance] defaultPlaylistsGroup];
    return playlist;
}

+ (LocalPlaylist *)localSharedPlaylistWithPlaylist:(Playlist *)p {
    LocalPlaylist *playlist = [[LocalPlaylist alloc] init];
    playlist.playlistId = p.playlistId;
    playlist.localPlaylistType = localPlaylistShared;
    playlist.name = p.name;
    playlist.userId = p.userId;
    playlist.playlistGroupObject = [[Playlists sharedInstance] defaultPlaylistsGroup];
    return playlist;
}


@end
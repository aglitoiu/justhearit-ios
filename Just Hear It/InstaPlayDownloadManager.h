#import <Foundation/Foundation.h>
#import "SongCacheDownloadManager.h"

@class PlaylistSong;

@interface InstaPlayDownloadManager : SongCacheDownloadManager
- (void)pushSong:(PlaylistSong *)song;

- (void)downloadSong:(PlaylistSong *)song;

- (void)downloadNextSongIfPossible;
@end
//
//  CachedLocalPlaylist.h
//  Just Hear It
//
//  Created by ciprian st on 11/05/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CachedLocalPlaylistSong;

@interface CachedLocalPlaylist : NSManagedObject

@property (nonatomic, retain) NSString * playlistId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSString * playlistType;
@property (nonatomic, retain) NSString * playlistGroup;
@property (nonatomic, retain) NSString * position;
@property (nonatomic, retain) NSNumber * isDefault;
@property (nonatomic, retain) NSNumber * currentPage;
@property (nonatomic, retain) NSNumber * totalPages;
@property (nonatomic, retain) NSNumber * erasableContent;
@property (nonatomic, retain) NSSet *cachedLocalPlaylistSongs;
@property (nonatomic, retain) NSString * shareUrl;


@end

@interface CachedLocalPlaylist (CoreDataGeneratedAccessors)

- (void)addCachedLocalPlaylistSongsObject:(CachedLocalPlaylistSong *)value;
- (void)removeCachedLocalPlaylistSongsObject:(CachedLocalPlaylistSong *)value;
- (void)addCachedLocalPlaylistSongs:(NSSet *)values;
- (void)removeCachedLocalPlaylistSongs:(NSSet *)values;

@end

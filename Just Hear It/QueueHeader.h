#import <UIKit/UIKit.h>

@interface QueueHeader : NSObject

+ (UIView *) initWithTitle:(NSString *) title;

@end
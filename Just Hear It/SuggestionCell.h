#import <Foundation/Foundation.h>

@interface SuggestionCell : UITableViewCell

@property(nonatomic, strong) UILabel *label;
@property(nonatomic, strong) UIImageView *icon;

@end
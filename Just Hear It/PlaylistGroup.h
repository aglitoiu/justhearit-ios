#import <Foundation/Foundation.h>

@interface PlaylistGroup : NSObject

@property (nonatomic, assign) BOOL collapsed;
@property (nonatomic, strong) NSMutableArray *collection;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *groupId;


+ (PlaylistGroup *)initWithKey:(NSDictionary *)key andCollection:(NSMutableArray *)collection;

+ (NSArray *)PlaylistGroups;

- (BOOL)isDefaultPlaylistsGroup;

- (BOOL)isMyPlaylistsGroup;

- (BOOL)isFollowingPlaylistsGroup;


@end

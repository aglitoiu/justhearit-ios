#import "NSArray+OCTotallyLazy.h"
#import "MainViewController.h"
#import "Pagination.h"
#import "DidYouMeanCell.h"
#import "LocalPlaylist.h"
#import "MasterViewController.h"
#import "FrostyControlsView.h"
#import "Player.h"
#import "PlaylistsViewController.h"
#import "PlaylistSongMove.h"
#import "PlaylistPreviewTitleCell.h"
#import "PlaylistPreviewSongCell.h"
#import "AudioPlayer.h"
#import "SongCache.h"
#import "InstaPlayDownloadManager.h"
#import "MusicFeedCell.h"
#import "NSDate+TimeAgo.h"
#import "PlaylistPreviewFooterCell.h"
#import "PlaylistPreview.h"
#import "User.h"
#import "CachedPlaylist.h"
#import "NSSet+OCTotallyLazy.h"
#import "PlaylistBadgeChangeNotification.h"
#import "CachedPlaylist.h"
#import "CachedPlaylist+Utils.h"
#import "CachedLocalPlaylist.h"
#import "CachedLocalPlaylist+Utils.h"
#import "CachedPlaylistSong.h"
#import "CachedLocalPlaylistSong.h"
#import "NWReachabilityManger.h"
#import "Bluuur.h"
#import "SongCacheDownloadManager.h"
#import "MPCoachMarks.h"
#import "EchoprintViewController.h"

#define CLOSE_BUTTON_SIZE 60
#define kDestinationURL @"https://justhearit.com:8000/fileUpload.php"

@interface MainViewController () <UIViewControllerPreviewingDelegate>
@end
@implementation MainViewController
int testnr=0;
int MMARE=0;
NSString *PHPResponseSTR;
NSArray *coachMarks;
UIAlertView *progressAlert;
@synthesize playlistsDrawerIsOpenFromDragging, masterVC, navigationBar, playerView, songsTable, songList, spinner, spinnerFooter, playlists, isLoadingSongs, playlistsList, navigationBarHeight, closeButton, welcomeView, playerStarted, delegate, welcomeMessage, welcomeTitle, isTableResized, hasNewSongs, nSongsButton, scrollOffsetY, topFrame, timer, songListTmp,OfflineBtn,isOffline, menuBtn,audioRecorder, audioFilePath, defaults;

+ (id)sharedInstance {
    static MainViewController *__sharedInstance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[MainViewController alloc] init];
    });
    
    return __sharedInstance;
}
- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {
    
    // check if we're not already displaying a preview controller
    
   /* UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *previewController = [storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
    [NSThread detachNewThreadSelector:NSSelectorFromString(@"suspend") toTarget:[UIApplication sharedApplication] withObject:nil];*/
    return nil;
}
- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    
   
    
    // alternatively, use the view controller that's being provided here (viewControllerToCommit)
}


- (void)viewDidLoad {
    [self prepareFingerprint];
    [self setupWelcomeView];
    //hasNewSongs = YES;
    if(IS_DEVICE == IS_IPAD) { [navigationBar setHidden:YES]; }
    
    [super viewDidLoad];
    
    songListTmp = [[NSMutableArray alloc] init];
    
    isTableResized = NO;
    
    playlists = [Playlists sharedInstance];
    
    songList = playlists.openedPlaylist.songs;
    
    playlistsList = playlists.openedPlaylist.playlists;

    navigationBarHeight = navigationBar.frame.size.height;

    playlistsDrawerIsOpenFromDragging = NO;
    
    self.JAHorizontalSlide = YES;
    if(IS_DEVICE == IS_IPAD) {
        spinner = [Spinner initWithParentView:self.view withFixedWidth:[Utils currentWidth]-LEFTPANEL_WIDTH_IPAD withHeight:[Utils currentHeight]];
    }else {
        spinner = [Spinner initWithParentView:self.view];
    }
    
    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [Utils currentWidth], PLAYLISTS_CELL_HEIGHT)];
    songsTable.tableFooterView = tableFooterView;
    if(IS_DEVICE == IS_IPAD) {
        spinnerFooter = [Spinner initWithParentView:tableFooterView withFixedWidth:[Utils currentWidth] - LEFTPANEL_WIDTH_IPAD withHeight:PLAYLISTS_CELL_HEIGHT];
    }else {
        spinnerFooter = [Spinner initWithParentView:tableFooterView];
    }
    
    if(IS_DEVICE != IS_IPAD) {
        
        int width = [Utils currentWidth];
        int height = [Utils currentHeight];
        
        [playerView initialize];
        playerView.delegate = self;
        playerView.frostyControlsView.mainViewController = self;
        [playerView initPositions:width :height];
        
        
        [self hidePlayer];
        
    }else {
        
        nSongsButton.frame = CGRectMake(([Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - nSongsButton.frame.size.width)/2, nSongsButton.frame.origin.y, nSongsButton.frame.size.width, nSongsButton.frame.size.height);
        
    }
    
    
    UIImage *buttonImage = [UIImage imageNamed:@"new_songs_button.png"];
    [nSongsButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    //nSongsButton.imageView.image = [UIImage imageNamed:@"new_songs_button.png"];
    
    topFrame = nSongsButton.frame;
    
    songsTable.backgroundColor = [UIColor clearColor];

    masterVC = (MasterViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];

    songsTable.placeholderDelegate = masterVC.view;
    
    [self refreshNavigationBar];
    
    if(SETTINGS.OfflineOnly==NO&&[NWReachabilityManger isReachable]&&SETTINGS.isLoggedIn==YES)
    {
    [spinner show:YES];
    }
    [songsTable reloadData];
    
    if ( playlists.openedPlaylist.isMusicFeed && playlists.openedPlaylist.isInCache) {
        if( !SETTINGS.welcomeMessageDisabled.boolValue ) {
            [welcomeView setHidden:NO];
        }else {
            [welcomeView setHidden:YES];
        }
    }else {
        // ??
        if(IS_DEVICE == IS_IPHONE) {
            //[spinner show:YES];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(songStartedPlay:) name:Ev.songStartedBuffering object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(songStartedPlay:) name:Ev.songStartedPlay object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshNavigationBar) name:Ev.navigationBarShouldReload object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(askForOffline) name:Ev.gotOffline object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setOnline) name:Ev.gotOnline object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadPlaylistSongsFromUrlPath:) name:Ev.loadSongsFromUrl object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOr:) name:@"searchclosed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(spinnerHide:) name:Ev.shouldHideSpinner object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openDiscoverIpad:) name:@"shouldLaunchSegue" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CoachPH2:) name:@"CoachPH2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CoachPH5:) name:@"CoachPH5" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startTUT:) name:@"startTUT" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveopenmenu:) name:@"openmenunot" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menudstarted:) name:@"MENUDstarted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menudended:) name:@"MENUDended" object:nil];


   
  
        [self registerForPreviewingWithDelegate:(id)self sourceView:self.view];
    
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mhandlePlaylistNotification:) name:Ev.shouldUpdateNewSongs object:nil];
    
    if ([[Player sharedInstance] currentPlayingSong]) {
        if(IS_DEVICE == IS_IPAD) {
            [masterVC showPlayer];
        }else {
            [self showPlayer];
        }
    }
    

    
}
- (void) prepareFingerprint{
   /* audioFilePath = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:@"reportAudio.m4a"];
    NSURL *outputFileURL = [NSURL fileURLWithPath:audioFilePath];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    NSError *error = nil;
    audioRecorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:&error];
    audioRecorder.delegate = self;
    audioRecorder.meteringEnabled = YES;
    
    // Deal with any errors
    if (error)
    NSLog(@"error: %@", [error localizedDescription]);
    else
    [audioRecorder prepareToRecord];
    
    // Check to see if we have permission to use the microphone.
    if ([session respondsToSelector:@selector(requestRecordPermission:)]) {
        [session performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
            if (granted) {
                NSLog(@"Mic access granted");
            }
            else {
                NSLog(@"Mic access NOT granted");
            }
        }];
    }*/
}
- (void) menudstarted:(NSNotification *) notification{
    songsTable.scrollEnabled=NO;
    
}
- (void) menudended:(NSNotification *) notification{
    songsTable.scrollEnabled=YES;
}
- (void) recordSongForFingerprinting{
    NSLog(@"Started Recording");
    if (!audioRecorder.recording)
    {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryRecord error:nil];
        [session setActive:YES error:nil];
        
        // Start recording
        [audioRecorder record];
        [self performSelector:@selector(stopFingerprintrecord) withObject:nil afterDelay:5.0 ];
    }
}
    -(void) stopFingerprintrecord{
        NSLog(@"Stop Recording");
        if ([audioRecorder isRecording])
        {
            [audioRecorder stop];
            AVAudioSession *audioSession = [AVAudioSession sharedInstance];
            [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
            [audioSession setActive:NO error:nil];
            [self sendFingerprintToApi];
        }
        else if (audioPlayer.playing)
        [audioPlayer stop];
    }
- (void) sendFingerprintToApi{
    // Define the Paths
    
    //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    // Define the Paths
    NSString *urlWithParams=[NSString stringWithFormat:kDestinationURL@"?recname=%f",audioRecorder.deviceCurrentTime];
    
    NSURL *icyURL = [NSURL URLWithString:urlWithParams];
    //NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    // Create the Request
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:icyURL];
    [request setHTTPMethod:@"POST"];
    //[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    // [request setHTTPBody:postData];
    // Configure the NSURL Session
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"com.upload"];
    sessionConfig.HTTPMaximumConnectionsPerHost = 1;
    NSURLSession *upLoadSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    
    // Define the Upload task
    NSURLSessionUploadTask *uploadTask = [upLoadSession uploadTaskWithRequest:request fromFile:audioRecorder.url];
    
    // Run it!
    [uploadTask resume];

}
-(NSURL *)applicationDocumentsDirectory {
        return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                       inDomains:NSUserDomainMask] lastObject];
}
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
        didReceiveData:(NSData *)data {
   
        PHPResponseSTR = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    dispatch_async(dispatch_get_main_queue(), ^{
        [spinner show:NO];
        [playlists handleOpenUrl:PHPResponseSTR];
        
        
      
    });
    
    
    
    
        //[self performSegueWithIdentifier:@"echoprintSegue" sender:self];
    
}
  
- (void) showAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Song Name"
                                                    message:PHPResponseSTR
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}
- (void) openEchoprint:(id)sender{
    //LoginViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"echoprintVC"];
    [self recordSongForFingerprinting];
    [spinner show:YES];
    
    
    
 
    
    [(MasterViewController*)self.sidePanelController playlistsChanged];
   
    
    // [self presentViewController:modalVC animated:YES completion:nil];
    
    
    
    
    
    
    
    
    
    
  
    
    //[self presentViewController:evc animated:YES completion:nil];
}
- (void) startTUT:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if(IS_DEVICE==IS_IPHONE)
    {
        CGRect coachmark1;
        if([Utils IS_IPHONE_X])
        {
           coachmark1 = CGRectMake(7, 30, 40, 40);
        }
        else{
           coachmark1 = CGRectMake(5, 5, 40, 40);
        }
        
        

    
    // Setup coach marks
    coachMarks = @[
                   @{
                       @"rect": [NSValue valueWithCGRect:coachmark1],
                       @"caption": @"tap here or slide anywhere to open the playlists menu",
                       @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                       @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                       @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                       @"showArrow":[NSNumber numberWithBool:YES]
                       }
                   
                   ];
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
   // [self.view addSubview:coachMarksView];
        coachMarksView.TUTnmbr=1;
    //[coachMarksView start];
        
    
    }
   
    
    
    
}
- (void) CoachPH2:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
  
    
    [self.sidePanelController showLeftPanelAnimated:YES];
       
    
       
        
    
    
}
-(void) CoachPH5:(NSNotification *) notification{
    [playerView setStateOpenAnimated ];
}
- (void) openDiscoverIpad:(NSNotification *) notification
{
   
    PlaylistSong *song= [[notification userInfo] valueForKey:@"Discover"];

    if(song)
    {
    [self performSegueWithIdentifier:@"discoverSegue" sender:song];
        
        
        
    }
   song= [[notification userInfo] valueForKey:@"AddTo"];
    if(song)
    {
    [self addToWithPlaylistSongs:@[song]];
    }
    
    song= [[notification userInfo] valueForKey:@"Share"];
    if(song)
    {
        UIViewController *shr =[Utils sharingViewControllerForSong:song];
        if(IS_DEVICE==IS_IPAD)
        {
            shr.popoverPresentationController.sourceView = self.view;
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:shr];
                     // popover.delegate = self;
    
            CGRect fr=CGRectMake(0, [Utils screenHeight], [Utils screenWidth], 100);
          
            [popover presentPopoverFromRect:fr inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
            //[self presentViewController:shr animated:YES completion:nil];
            
        }
        else
        {
            [self presentViewController:shr animated:YES completion:nil];
            
        }    }
    
    //[self performSegueWithIdentifier:@"discoverSegue" sender:song];
    
}

- (void) spinnerHide:(NSNotification*)notification {
    [spinner show:NO];
}
- (void)refreshOr:(NSNotification *)notification{
    //[self xOnRotation:[Utils currentWidth] :[Utils currentHeight]];
    [self repositionBackground ];
}
- (void) refreshNavigationBar {
  
    // Log(@"isReachable: %d", [NWReachabilityManger isReachable] && !playlists.offlineMode);
    if(SETTINGS.OfflineOnly==YES){
      
            [navigationBar showRedHandle];
        [navigationBar showTitle:@"Off the grid" :@"Tap to go online"];
        isOffline=3;
        
        
            
        
        
    }
    else if(SETTINGS.OfflineOnly==NO&&isOffline==3)
    {
        [navigationBar showNormalHandle];
        [navigationBar showLogo];
        isOffline=0;
    }
    else if(isOffline==0)
    {
        [navigationBar showNormalHandle];
        [navigationBar showLogo];
    }
    else if(isOffline==1&&SETTINGS.OfflineOnly==NO)
    {
        [navigationBar showHalfRedHandle];
        [navigationBar showTitle:@"Connection lost" :@"Tap to go off the grid"];
    }
    else if(isOffline==2)
    {
        [navigationBar showRedHandle];
        [navigationBar showTitle:@"Off the grid" :@"Tap to go online"];
    }
        
        
    }
        
    


-(void) askForOffline{
    if(IS_DEVICE==IS_IPHONE)
    {
    if(isOffline==0)
    {
    [navigationBar showHalfRedHandle];
        [navigationBar showTitle:@"Connection lost" :@"Tap to go off the grid"];
    isOffline=1;
    }
    else if(isOffline==4)
    {
        [navigationBar showHalfRedHandle];
        [navigationBar showTitle:@"Connection lost" :@"Tap to go off the grid"];

    }
    }
    
}
-(void) setOnline
{
    if(IS_DEVICE==IS_IPHONE)
    {
    if(isOffline==1)
    {
        
        [navigationBar showNormalHandle];
        [navigationBar showLogo];
        isOffline=0;
        
    }
    else if(isOffline==4)
    {
        [navigationBar showNormalHandle];
        [navigationBar showLogo];
        isOffline=0;
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
    }
    }
}
-(void) checkForOnline{
    if(IS_DEVICE==IS_IPHONE)
    {
if(isOffline==2)
{
    
    [navigationBar showTitle:@"Refreshing" :@"Please wait"];
    if([NWReachabilityManger isReachable])
    {
        isOffline=0;
        [navigationBar showNormalHandle];
        
        //
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
        [navigationBar showLogo];
        
    }
    else
    {
        isOffline=4;
        [self askForOffline];
    }
}
    }

}

-(BOOL) alreadyOffline{
    if (isOffline!=0) {
        return YES;
        
    }
    else
    {
        return NO;
    }
    
}
- (void) showPlayer {
    
    [playerView closeView];
    [self setSongsTablePlayerScreen];
    
}

- (void) hidePlayer {
    playerStarted=NO;
    [playerView hideView];
    [self setSongsTableFullScreen];
}

- (void)centerViewControllerOpened {
    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
    if (playlistsViewController.playlistsMode == playlistsViewControllerAddTo) [playlistsViewController setNormalMode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)pullableView:(PullableView *)pView movingToYOffset:(CGFloat)offset {
    [playerView offsetBackground:offset];
}

- (void)pullableView:(PullableView *)pView didChangeState:(int)state {
   
    
       

    
}

- (void)pullableView:(PullableView *)pView willChangeStateFromState:(int)fromState toState:(int)toState {
    
    if(fromState==STATE_OPEN&&toState==STATE_CLOSED&&!SETTINGS.notfirstSongDrag)
    {
        CGRect coachmark2;
        if([Utils IS_IPHONE_X])
        {
           
            coachmark2 = CGRectMake(0, 230 , [Utils screenWidth], 46);
        }
        
        else
        {
            coachmark2 = CGRectMake(0, 210 , [Utils screenWidth], 46);
         
        }
        coachMarks = @[
                       @{
                           @"rect": [NSValue valueWithCGRect:coachmark2],
                           @"caption": @"slide to the left to access the song menu",
                           @"shape": [NSNumber numberWithInteger:DEFAULT],
                           @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                           @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                           @"showArrow":[NSNumber numberWithBool:YES]
                           }
                       ];
        MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        //[self.view addSubview:coachMarksView];
        coachMarksView.TUTnmbr=4;
       // [coachMarksView start];
        [SETTINGS setnotfirstSongDrag];
        
    }
    if (toState == STATE_HIDDEN) {
        

        Log(@"player state hidden");
        [UIView animateWithDuration:.2 animations:^{
            [self setSongsTableFullScreen];
        }];
    } else if (toState == STATE_CLOSED && fromState == STATE_HIDDEN) {
        Log(@"player state closed and hidden");


        [UIView animateWithDuration:.2 animations:^{
            [self setSongsTablePlayerScreen];
        }];
    }

    if (toState == STATE_OPEN) {
        
        [playerView.frostyControlsView showAllButtonsAnimated:YES];
        [masterVC disableLeftRight];
    } else {
        [masterVC enableLeftRight];
        [playerView.frostyControlsView showThreeButtonsAnimated:YES];
    }
}

- (void) setSongsTableFullScreen {
    
    playlists = [Playlists sharedInstance];
    
    //Log(@"Set songs table full: %@", playlists.openedPlaylist.playlistType);

    //Log(@"Is music feed: %d", (playlists.openedPlaylist.isMusicFeed && (!SETTINGS.welcomeMessageDisabled.boolValue) && welcomeView.hidden == NO));
    
    BOOL withWelcomeMessage;
    CGFloat offsetY;

    if(playlists.openedPlaylist.isMusicFeed && (!SETTINGS.welcomeMessageDisabled.boolValue) && welcomeView.hidden == NO) {
        withWelcomeMessage = YES;
        offsetY = welcomeView.frame.size.height;
    }else {
        withWelcomeMessage = NO;
        offsetY = 0.0f;
    }
    
    CGFloat width;
    
    if (IS_DEVICE == IS_IPAD) {
        width = [Utils currentWidth] - self.sidePanelController.centerPanelContainer.frame.origin.x;
    }else {
        width = [Utils currentWidth];
    }
    //Log(@"offsetY: %f", offsetY);
    //Log(@"table height: %f", [Utils currentHeight] - navigationBarHeight - offsetY);
    
    songsTable.frame = CGRectMake(0, offsetY+navigationBarHeight, width, [Utils currentHeight] - navigationBarHeight  - offsetY);
   // [songsTable setContentInset:UIEdgeInsetsMake(navigationBarHeight,0,0,0)];
}

- (void) setSongsTablePlayerScreen {
    
    Log(@"Set table player");
    
    BOOL withWelcomeMessage;
    CGFloat offsetY;
    
    if(playlists.openedPlaylist.isMusicFeed && (!SETTINGS.welcomeMessageDisabled.boolValue) && welcomeView.hidden == NO) {
        withWelcomeMessage = YES;
        offsetY = welcomeView.frame.size.height;
    }else {
        withWelcomeMessage = NO;
        offsetY = 0.0f;
    }
    
    CGFloat width;
    
    if (IS_DEVICE == IS_IPAD) {
        width = [Utils currentWidth] - self.sidePanelController.centerPanelContainer.frame.origin.x;
    }else {
        width = [Utils currentWidth];
    }
    if([Utils IS_IPHONE_X])
    {
    songsTable.frame = CGRectMake(0, navigationBarHeight + offsetY, width, [Utils currentHeight] - navigationBarHeight - PLAYER_DRAWER_HEIGHT_X - offsetY);
    }
    else{
    songsTable.frame = CGRectMake(0, navigationBarHeight + offsetY, width, [Utils currentHeight] - navigationBarHeight - PLAYER_DRAWER_HEIGHT - offsetY);
    }
    
}

- (void)pullableViewStartedDrag:(PullableView *)pView fromState:(int)state {
    [Analytics event:@"player_view":@"drag"];
}

- (IBAction) closeWelcomeMsg:(id) sender {
    
    SETTINGS.welcomeMessageDisabled = [NSNumber numberWithInt:1];

    [UIView animateWithDuration:.6 animations:^{
        welcomeView.alpha = 0.0f;
    }
    completion:^(BOOL finished) {
        [welcomeView setHidden:YES];
    }
    ];
    
    [UIView animateWithDuration:.3
                          delay:0.3f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         songsTable.frame = CGRectMake(songsTable.frame.origin.x, songsTable.frame.origin.y - welcomeView.frame.size.height, songsTable.frame.size.width, [Utils currentHeight] - navigationBarHeight);
                     }
                     completion:^(BOOL finished) {}
    ];
}

- (void) setupWelcomeView {
    
    /*if(!SETTINGS.welcomeMessageDisabled.boolValue) {
        [welcomeView setHidden:NO];
    }*/
    
        
    if(IS_DEVICE == IS_IPAD) {
        
        welcomeView.autoresizesSubviews = YES;
        welcomeView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        welcomeMessage.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        CGFloat fixWidth;
        CGFloat fixHeight;
        if([Utils isLandscape]) {
            if(![Utils isVersionLessThan_8_0]) {
                fixWidth = 0.0f;
            }else {
                fixWidth = 250.0f;
            }
            fixHeight = 15.0f;
        }else {
            fixWidth = 0.0f;
            fixHeight = 0.0f;
        }
        
        welcomeView.frame = CGRectMake(welcomeView.frame.origin.x, welcomeView.frame.origin.y, [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD, welcomeView.frame.size.height);
        
        if ([Utils isLandscape] && ![Utils isVersionLessThan_8_0]) {
            welcomeMessage.frame = CGRectMake(welcomeMessage.frame.origin.x, welcomeTitle.frame.origin.y + 30.0f, welcomeView.frame.size.width - welcomeMessage.frame.origin.x * 2 - fixWidth, welcomeMessage.frame.size.height);
        }else {
            welcomeMessage.frame = CGRectMake(welcomeMessage.frame.origin.x, welcomeMessage.frame.origin.y, welcomeView.frame.size.width - welcomeMessage.frame.origin.x * 2 - fixWidth, welcomeMessage.frame.size.height);
        }
        
    }

    closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake( welcomeView.frame.size.width - CLOSE_BUTTON_SIZE, 0, CLOSE_BUTTON_SIZE, CLOSE_BUTTON_SIZE );
    [closeButton setImage:[UIImage imageNamed:@"icon_close.png"] forState:UIControlStateNormal];
    closeButton.contentMode = UIViewContentModeCenter;
    
    [welcomeView addSubview:closeButton];
    
    [closeButton addTarget:self action:@selector(closeWelcomeMsg:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void) mhandlePlaylistNotification:(NSNotification *)notification {
    Log(@"notif: %@", notification);
    
    PlaylistBadgeChangeNotification *badgeChangeNotification = (PlaylistBadgeChangeNotification *)notification.userInfo;
    
    
    NSDictionary *dictionary = @{
                                 @"playlistId" : badgeChangeNotification.playlistId,
                                 @"song_ids" : badgeChangeNotification.song_ids
                                 };
    
    NSString *playlistid = [dictionary objectForKey:@"playlistId"];
    
    if([playlistid isEqualToString:playlists.openedPlaylist.playlistId] || playlists.openedPlaylist.isMusicFeed) {
    
    Playlist *playlist = [[Playlist alloc] init];
    playlist.playlistId = playlistid;
    
        [[JusthearitClient sharedInstance] loadNextPageFromNormalPlaylistUpdates:playlist andIds:[dictionary objectForKey:@"song_ids"] withSuccessBlock:^(NSArray *array) {

            Playlist *loadedPlaylistWithSongs = _.first(array);
            NSMutableArray *newSongList = loadedPlaylistWithSongs.songs;
            
            if((loadedPlaylistWithSongs.playlistData.playlistType == [NSNumber numberWithInt:2] && (!playlists.openedPlaylist.isMyUploads || playlists.openedPlaylist.isMusicFeed)) || (loadedPlaylistWithSongs.playlistData.playlistType == [NSNumber numberWithInt:3] && (!playlists.openedPlaylist.isRecentlyPlayed || playlists.openedPlaylist.isMusicFeed) )) return;
            
            if(loadedPlaylistWithSongs.playlistData.playlistType != [NSNumber numberWithInt:2] && loadedPlaylistWithSongs.playlistData.playlistType != [NSNumber numberWithInt:3])
                [loadedPlaylistWithSongs cacheSongsNoDownloadMusicFeed];
                
            
            NSMutableArray *arrayOfIndexPaths = [[NSMutableArray alloc] init];
            NSIndexPath *path;
            if(playlists.openedPlaylist.isMusicFeed || playlists.openedPlaylist.isMyUploads || playlists.openedPlaylist.isRecentlyPlayed || playlists.openedPlaylist.isSharedSearchPlaylist) {
                path = [NSIndexPath indexPathForRow:0 inSection:0];
            }else if(( playlists.openedPlaylist.isNormalPlaylist && ( playlists.openedPlaylist.pagination.totalPages.intValue == 1 || ( playlists.openedPlaylist.pagination.totalPages.intValue > 1 && playlists.openedPlaylist.pagination.currentPage.intValue == playlists.openedPlaylist.pagination.totalPages.intValue ) ) ) || ( !playlists.openedPlaylist.isNormalPlaylist && playlists.openedPlaylist.pagination.totalPages.intValue >= 1 )) {
                path = [NSIndexPath indexPathForRow:songList.count inSection:0];
            }else {
                path = [NSIndexPath indexPathForRow:0 inSection:0];
            }
            
            [arrayOfIndexPaths addObject:path];
            
            if ( newSongList.count > 0 ) {
                hasNewSongs = YES;
            }

            if ( ( playlists.openedPlaylist.isNormalPlaylist && ( playlists.openedPlaylist.pagination.totalPages.intValue == 1 || ( playlists.openedPlaylist.pagination.totalPages.intValue > 1 && playlists.openedPlaylist.pagination.currentPage.intValue == playlists.openedPlaylist.pagination.totalPages.intValue ) ) ) || ( !playlists.openedPlaylist.isNormalPlaylist && playlists.openedPlaylist.pagination.totalPages.intValue >= 1 ) ) {
                
                //Log(@"update row");
                
                CGFloat offsetY;
                for(PlaylistSong *playlistNewSong in newSongList) {
                    
                    playlistNewSong.playlistOrigin = loadedPlaylistWithSongs.playlistData;
                    if(playlists.openedPlaylist.isMusicFeed || playlists.openedPlaylist.isMyUploads || playlists.openedPlaylist.isRecentlyPlayed) {
                        [songList insertObject:playlistNewSong atIndex:0];
                    }else if(playlists.openedPlaylist.isNormalPlaylist && ( playlists.openedPlaylist.pagination.totalPages.intValue == 1 || ( playlists.openedPlaylist.pagination.totalPages.intValue > 1 && playlists.openedPlaylist.pagination.currentPage.intValue == playlists.openedPlaylist.pagination.totalPages.intValue ) ) ) {
                        
                        [songList insertObject:playlistNewSong atIndex:songList.count];
                        
                    }else {
                        
                        [songList insertObject:playlistNewSong atIndex:0];
                        
                    }
                    
                    //Log(@"is normal playlist? %hhd", playlists.openedPlaylist.isNormalPlaylist);
                    if(playlists.openedPlaylist.isNormalPlaylist && playlists.openedPlaylist.pagination.totalPages.intValue == 1)
                        [loadedPlaylistWithSongs cacheSongsNoDownloadNormalPlaylist:playlistNewSong];
                    
                    if(playlists.openedPlaylist.isRecentlyPlayed)
                        [loadedPlaylistWithSongs cacheSongsNoDownloadRecentlyPlayed:playlistNewSong];
                    
                    //Log(@"is shearch playlist? %hhd", playlists.openedPlaylist.isSharedSearchPlaylist);
                    if(playlists.openedPlaylist.isSharedSearchPlaylist)
                        [loadedPlaylistWithSongs cacheSongsNoDownloadSearchPlaylist:playlistNewSong];
                    
                    if(playlists.openedPlaylist.isMyUploads)
                        [loadedPlaylistWithSongs cacheSongsNoDownloadMyUploadsPlaylist:playlistNewSong];
                    
                    [[Player sharedInstance] addQueueSong:playlistNewSong];
                    
                    offsetY = songsTable.contentOffset.y;

                    if(offsetY >= SONG_CELL_HEIGHT) [UIView setAnimationsEnabled:NO];
                
                    [songsTable beginUpdates];
                    
                    [songsTable insertRowsAtIndexPaths:arrayOfIndexPaths withRowAnimation:UITableViewRowAnimationRight];
                    
                    if(offsetY >= SONG_CELL_HEIGHT) {
                        
                        CGPoint point = CGPointMake(0,offsetY + SONG_CELL_HEIGHT);
                        if(playlists.openedPlaylist.isMusicFeed || playlists.openedPlaylist.isMyUploads || playlists.openedPlaylist.isRecentlyPlayed) {
                            point.y = offsetY + SONG_CELL_HEIGHT;
                        }else if( playlists.openedPlaylist.isNormalPlaylist && ( playlists.openedPlaylist.pagination.totalPages.intValue == 1 || ( playlists.openedPlaylist.pagination.totalPages.intValue > 1 && playlists.openedPlaylist.pagination.currentPage.intValue == playlists.openedPlaylist.pagination.totalPages.intValue ) ) ) {
                            
                            point.y = offsetY;
                            
                        }else {
                            
                            point.y = offsetY + SONG_CELL_HEIGHT;
                            
                        }
                        
                        [songsTable setContentOffset:point animated:NO];
                        
                    }
                
                    [songsTable endUpdates];
                    
                    if(offsetY >= SONG_CELL_HEIGHT) [UIView setAnimationsEnabled:YES];
                    
                    if(playlists.openedPlaylist.pagination.currentPage.intValue == playlists.openedPlaylist.pagination.totalPages.intValue && !playlists.openedPlaylist.isMyUploads && !playlists.openedPlaylist.isMusicFeed && !playlists.openedPlaylist.isRecentlyPlayed && !playlists.openedPlaylist.isSharedSearchPlaylist) {
                        
                        NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:[songList count]-1 inSection:0];
                        [songsTable scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionTop animated:YES];
                    }
                    
                    if(loadedPlaylistWithSongs.playlistData.playlistType == [NSNumber numberWithInt:3]) {
                        NSIndexPath *lastPath = [NSIndexPath indexPathForRow:[songList count] - 1 inSection:0];
                        [songList removeObject:[songList objectAtIndex:[songList count] - 1]];
                        
                        [songsTable beginUpdates];
                        
                        [songsTable deleteRowsAtIndexPaths:@[lastPath] withRowAnimation:UITableViewRowAnimationRight];
                        
                        [songsTable endUpdates];
                    }
                    
                    
                }
                
            }
            
            //Log(@"current page: %d, total pages: %d", playlists.openedPlaylist.pagination.currentPage.intValue, playlists.openedPlaylist.pagination.totalPages.intValue);
            
            if (hasNewSongs && ![playlists.openedPlaylist.pagination.currentPage isEqualToNumber: playlists.openedPlaylist.pagination.totalPages]) {
                nSongsButton.hidden = NO;
                [self showNewSongsButton];
                
            }else {
                nSongsButton.hidden = YES;
            }
            
            if(playlists.openedPlaylist.isSharedSearchPlaylist || playlists.openedPlaylist.isNormalPlaylist) {
                playlists.openedPlaylist.badgeCount = @0;
                PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
                [playlistsViewController.playlistsTable reloadData];
            }
            
            playlists.openedPlaylist.hasNotification = NO;
            
        } withErrorBlock:^(NSError *error) {}];
    }else {
        
        Playlist *playlist = [[Playlist alloc] init];
        playlist.playlistId = playlistid;
        
        [[JusthearitClient sharedInstance] loadNextPageFromNormalPlaylistUpdates:playlist andIds:[dictionary objectForKey:@"song_ids"] withSuccessBlock:^(NSArray *array) {
            
            Playlist *loadedPlaylistWithSongs = _.first(array);
            
            if(loadedPlaylistWithSongs.playlistData.playlistType != [NSNumber numberWithInt:2] && loadedPlaylistWithSongs.playlistData.playlistType != [NSNumber numberWithInt:3])
                [loadedPlaylistWithSongs cacheSongsNoDownloadMusicFeed];
            
        } withErrorBlock:^(NSError *error) {}];
    }
    
}

- (void) showNewSongsButton {
    
    if( ( ( songList.count > 14 && IS_DEVICE == IS_IPAD ) || ( songList.count > 10 && IS_DEVICE == IS_IPHONE ) ) && playlists.openedPlaylist.isNormalPlaylist && ( playlists.openedPlaylist.pagination.totalPages.intValue >= 1)) {
        
        CGRect frame;
        if(IS_DEVICE == IS_IPAD) {
            frame = CGRectMake( ([Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - nSongsButton.frame.size.width) / 2, ( [Utils currentHeight] - 64.0f ), nSongsButton.frame.size.width, nSongsButton.frame.size.height  );
        }else {
            frame = CGRectMake( ([Utils currentWidth] - nSongsButton.frame.size.width) / 2, ( [Utils currentHeight] - 64.0f ), nSongsButton.frame.size.width, nSongsButton.frame.size.height  );
        }
        
        if(playerStarted && nSongsButton.frame.origin.y <= ( [Utils currentHeight] - 64.0f ) ) {
            frame.origin.y = frame.origin.y - PLAYER_DRAWER_HEIGHT;
            //nSongsButton.frame = frame;
        }
        nSongsButton.frame = frame;
        UIImage *buttonImage = [UIImage imageNamed:@"new_songs-button-down.png"];
        [nSongsButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        
    }else {
        
        UIImage *buttonImage = [UIImage imageNamed:@"new_songs_button.png"];
        [nSongsButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        
    }
   
    if ( scrollOffsetY > SONG_CELL_HEIGHT || ( ( ( songList.count > 14 && IS_DEVICE == IS_IPAD ) || ( songList.count > 10 && IS_DEVICE == IS_IPHONE ) ) && playlists.openedPlaylist.isNormalPlaylist && playlists.openedPlaylist.pagination.totalPages.intValue >= 1 )) {
        
        [UIView animateWithDuration:0.2
                              delay:0.2
                            options:UIViewAnimationOptionCurveLinear
                         animations:^ {
                             nSongsButton.alpha = 0.8;
                         }
                         completion:^(BOOL finished) {}];
        
    }else {
        hasNewSongs = NO;
    }
    
}

- (void) hideNewSongsButton {

   // if ( scrollOffsetY <= 0.0f ) {
        
    [UIView animateWithDuration:0.2
                          delay:0.2
                        options:UIViewAnimationOptionCurveLinear
                     animations:^ {
                         nSongsButton.alpha = 0;
                     }
                     completion:^(BOOL finished) {}];


    playlists.openedPlaylist.badgeCount = @0;
    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
    [playlistsViewController.playlistsTable reloadData];
    
   // }
    
}

- (void) scrollToBottom {
    Log(@"scroll to bottom");
    NSIndexPath*ip = [NSIndexPath indexPathForRow:songList.count-1 inSection:0];
    [songsTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
        timer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                             target:self
                                           selector:@selector(loadAuto)
                                           userInfo:nil
                                            repeats:YES];
}

- (IBAction) scrollToTop:(id) sender {
    
    if(playlists.openedPlaylist.isNormalPlaylist && playlists.openedPlaylist.pagination.totalPages.intValue == 1) {
        NSIndexPath* ip = [NSIndexPath indexPathForRow:songList.count-1 inSection:0];
        [songsTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if ( playlists.openedPlaylist.isNormalPlaylist && playlists.openedPlaylist.pagination.totalPages.intValue > 1 )
    {
       
        NSIndexPath*ip = [NSIndexPath indexPathForRow:songList.count-1 inSection:0];
        [songsTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                 target:self
                                               selector:@selector(loadAuto)
                                               userInfo:nil
                                                repeats:YES];
        
    }
    else if ([self numberOfSectionsInTableView:songsTable] > 0)
    {
        NSIndexPath* top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
        [songsTable scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
}

- (void) loadAuto {
    
    if (!isLoadingSongs && playlists.openedPlaylist.pagination.hasNextPage) {
        
        Log(@"next page 5");
        [self loadNextPageOfPlaylist:playlists.openedPlaylist andIsAuto:YES];
        isLoadingSongs = YES;
        
        
    }else if (!isLoadingSongs && !playlists.openedPlaylist.pagination.hasNextPage){
        
        [timer invalidate];
        timer = nil;
        
    }
    
    NSIndexPath* ip;
    
    ip = [NSIndexPath indexPathForRow:songList.count-1 inSection:0];
    [songsTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [self showFooter];
    
    if (playlists.openedPlaylist.pagination.currentPage.intValue == playlists.openedPlaylist.pagination.totalPages.intValue) {
        
        [self hideFooter];
        [self hideNewSongsButton];
        
        playlists.openedPlaylist.hasNotification = NO;
        
        hasNewSongs = NO;
        
    }
}

-(void) removeRowsWhenPlaylistDelete:(NSArray *)songs andPlaylist:(Playlist *)playlist{
    //NSArray *cells = [songsTable visibleCells];
    //SongCell *cellfordel = [[SongCell alloc] init];
    //NSInteger i;
    //Log(@"songs to check: %@, %d", songs, [songs count]);
    NSIndexPath *indexPath;
    NSInteger i;
    
    Log(@"song list count: %lu", (unsigned long)[songList count]);
    //Log(@"song list tmp count: %d", [songListTmp count]);
    
    for (PlaylistSong *song in songs) {
        //i = [songList indexOfObject:song];
        
        
        for (i = 0; i < [songList count]; ++i)
        {
            if([[songList objectAtIndex:i] isEqual:song]) {
                indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                break;
            }
        }
        
        Log(@"index : %ld", (long)i);
        
        //[songListTmp removeObject:song];
        [songList removeObjectAtIndex:i-1];

        //Log(@"index of object: %d",[songList indexOfObject:songs]);
        
        //[songsTable beginUpdates];
        //[songsTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        //[songsTable endUpdates];
        
        //Log(@"cell for delete: %@", indexPath);
    }
    
    Log(@"song list count: %lu", (unsigned long)[songList count]);
    //Log(@"song list tmp count: %d", [songListTmp count]);
    
    [songsTable reloadData];
    
}

#pragma mark FMMoveTableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (playlists.openedPlaylist.isLocalSearchPlaylistsWith) {
        return playlists.openedPlaylist.playlists.count;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(FMMoveTableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (playerStarted && playlists.openedPlaylist.isMusicFeed == 0) {
        [self setSongsTablePlayerScreen];
    }else if(!playerStarted && playlists.openedPlaylist.isMusicFeed == 0) {
        [self setSongsTableFullScreen];
    }
    
    //Log(@"number of songs: %d", [songList count]);
    
    if (playlists.openedPlaylist.isLocalSearchPlaylistsWith) {
        Log(@"XXX 1");
        return 2 + [[playlists.openedPlaylist.playlists[section] songs] count];
    } else if (playlists.openedPlaylist.showEmptyResults) {
        Log(@"XXX 2");
        return 1;
    } else if (playlists.openedPlaylist.showSuggestionResults) {
        Log(@"XXX 3");
        return playlists.openedPlaylist.suggestions.count + 1;
    } else if (playlists.openedPlaylist.isLocalSearchPlaylistsWith) {
        Log(@"XXX 4");
        return 0;
    } else {
        Log(@"XXX 5");
        return [songList count];
    }
}

- (UITableViewCell *)tableView:(FMMoveTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Log(@"operation queue %lu", (unsigned long)[[RKObjectManager sharedManager].operationQueue operationCount]);
    
    if([[RKObjectManager sharedManager].operationQueue operationCount] > 0 && !isLoadingSongs && !playlists.openedPlaylist.hasSongsInCache && [songList count] == 0) {
        
        [spinner show:YES];
        
        static NSString *cellIdentifier = @"songCell";
        SongCell *cell = (SongCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        cell.artistLabel.text = @"";
        cell.songNameLabel.text = @"";
        cell.cellBorder.borderBottom.alpha = 0;
        
        UIView *bgColorView = [[UIView alloc] init];
        [bgColorView setBackgroundColor:[UIColor clearColor]];
        [cell setSelectedBackgroundView:bgColorView];
        
        //if (playlists.openedPlaylist.isSharedPlaylist || playlists.openedPlaylist.isSharedSearchPlaylist || playlists.openedPlaylist.isMusicFeed) {
            [cell setUnseen:YES];
        //}
        
        [cell setBackgroundColor:[UIColor clearColor]];
        
        return cell;
    }else if (playlists.openedPlaylist.isLocalSearchPlaylistsWith) {

        PlaylistPreview *playlistPreview = playlists.openedPlaylist.playlists[indexPath.section];
        if (indexPath.row == 0) {
            //first row
            static NSString *playlistsPreviewTitleCellIdentifier = @"playlistPreviewTitleCell";
            PlaylistPreviewTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:playlistsPreviewTitleCellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.shouldIndentWhileEditing = NO;
            cell.showsReorderControl = NO;
            cell.titleLabel.text = playlistPreview.playlist.name;
            [cell.icon setImage:[UIImage imageNamed:@"icon_normal_playlist.png"]];
            [cell setBackgroundColor:[UIColor clearColor]];
            return cell;
        } else if (indexPath.row == [self tableView:songsTable numberOfRowsInSection:indexPath.section] - 1) {
            //last row
            static NSString *playlistsPreviewFooterCellIdentifier = @"playlistPreviewFooterCell";
            PlaylistPreviewFooterCell *cell = [tableView dequeueReusableCellWithIdentifier:playlistsPreviewFooterCellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.shouldIndentWhileEditing = NO;
            cell.showsReorderControl = NO;

            NSString *fullName;

            if (playlistPreview.owner.firstName && playlistPreview.owner.lastName) {
                fullName = [NSString stringWithFormat:@"%@ %@", playlistPreview.owner.firstName, playlistPreview.owner.lastName];
            } else {
                fullName = playlistPreview.owner.toLabel;
            }
            cell.userLabel.text = fullName;

            cell.genreLabel.text = playlistPreview.genresAsString;

            [cell adjustPlayCount:[NSString stringWithFormat:@"%d", playlistPreview.playlist.playCount.intValue]
                     andSongCount:[NSString stringWithFormat:@"%d", playlistPreview.playlist.playlistSongsCount.intValue]];

            [cell setBackgroundColor:[UIColor clearColor]];
            return cell;
        } else {
            //mid row
            static NSString *playlistsPreviewSongCellIdentifier = @"playlistPreviewSongCell";
            PlaylistPreviewSongCell *cell = [tableView dequeueReusableCellWithIdentifier:playlistsPreviewSongCellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.shouldIndentWhileEditing = NO;
            cell.showsReorderControl = NO;
            PlaylistSong *song = [playlists.openedPlaylist.playlists[indexPath.section] songs][indexPath.row-1];
            cell.songNameLabel.text = [NSString stringWithFormat:@"%@ - %@", song.artist, song.name];
            [cell setBackgroundColor:[UIColor clearColor]];
            return cell;
        }

    } else if (playlists.openedPlaylist.shouldShowEmptyResults) {
        Log(@"empty results");
        if (indexPath.row == 0) {
            static NSString *sorryCellIdentifier = @"sorryCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sorryCellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.shouldIndentWhileEditing = NO;
            cell.showsReorderControl = NO;
            [cell setBackgroundColor:[UIColor clearColor]];
            return cell;
        } else {
            static NSString *didYouMeanCellIdentifier = @"didYouMeanCell";
            DidYouMeanCell *cell = (DidYouMeanCell *)[tableView dequeueReusableCellWithIdentifier:didYouMeanCellIdentifier];
            [cell.icon setImage:[UIImage imageNamed:@"icon_magnifier36.png"]];
            cell.titleLabel.text = playlists.openedPlaylist.suggestions[indexPath.row-1];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.shouldIndentWhileEditing = NO;
            cell.showsReorderControl = NO;
            [cell setBackgroundColor:[UIColor clearColor]];
            return cell;
        }
    } else if (playlists.openedPlaylist.isMusicFeed) {
        //NSLog(@"<<<<<<<<<<< MUSIC FEED >>>>>>>>>>>");
        /*if(indexPath.row == 0 && SETTINGS.welcomeMessage) {
            
            static NSString *welcomeCellIdentifier = @"welcomeCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:welcomeCellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.shouldIndentWhileEditing = NO;
            cell.showsReorderControl = NO;
            
            CGRect screenFrame = [[UIScreen mainScreen] bounds];
            
            closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            closeButton.frame = CGRectMake( screenFrame.size.width - CLOSE_BUTTON_SIZE, 0, CLOSE_BUTTON_SIZE, CLOSE_BUTTON_SIZE );
            [closeButton setImage:[UIImage imageNamed:@"icon_close.png"] forState:UIControlStateNormal];
            closeButton.contentMode = UIViewContentModeCenter;
            [cell addSubview:closeButton];
            
            [closeButton addTarget:self action:@selector(closeWelcomeMsg:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        }else {*/
            
            static NSString *cellIdentifier = @"musicFeedCell";
            MusicFeedCell *cell = (MusicFeedCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

            if ([tableView indexPathIsMovingIndexPath:indexPath]) {
                [cell prepareForMove];
            } else {

                if (tableView.movingIndexPath != nil) {
                    indexPath = [tableView adaptedIndexPathForRowAtIndexPath:indexPath];
                }

                PlaylistSong *playlistSong = songList[indexPath.row];

                cell.artistAndSongNameLabel.text = [NSString stringWithFormat:@"%@ - %@", playlistSong.artist, playlistSong.name];

                if (playlistSong.playlistOrigin.isNormalPlaylist) {
                    cell.playlistIcon.image = [UIImage imageNamed:@"icon_playlist_small.png"];
                } else {
                    cell.playlistIcon.image = [UIImage imageNamed:@"icon_search_small.png"];
                }

                cell.playlistName.text = playlistSong.playlistOrigin.name;
                cell.timestamp.text = playlistSong.createdAt.timeAgo;

                cell.cellBorder.borderBottom.alpha = 1;
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.shouldIndentWhileEditing = NO;
                cell.showsReorderControl = NO;
                cell.song = playlistSong;
                cell.slideDelegate = self;
                cell.playlistIcon.alpha = 0.6;

                if (playlistSong.playlist.isOfflinePlaylist) {
                    [cell setOfflineSongInteractions];
                } else {
                    [cell setOnlineSongInteractions];
                }

                UIView *bgColorView = [[UIView alloc] init];
                [bgColorView setBackgroundColor:RGBA(0, 0, 0, 0.4)];
                [cell setSelectedBackgroundView:bgColorView];

                [cell markPlaying:playlistSong.isPlayingOrPaused];
                [cell markSelected:playlistSong.isPlayingOrPaused];

                [cell setUnseen:playlistSong.isNew];
                cell.canRemove = NO;
            }
        
            if(IS_DEVICE == IS_IPAD) {
                cell.songNameLabel.frame = CGRectMake(cell.songNameLabel.frame.origin.x, cell.songNameLabel.frame.origin.y, cell.frame.size.width - cell.songNameLabel.frame.origin.x*2, cell.songNameLabel.frame.size.height);
                cell.artistLabel.frame = CGRectMake(cell.artistLabel.frame.origin.x, cell.artistLabel.frame.origin.y, cell.frame.size.width - cell.artistLabel.frame.origin.x*2, cell.artistLabel.frame.size.height);
            }
        
            [cell setBackgroundColor:[UIColor clearColor]];
        
            return cell;
       /* } */
    } else {
        static NSString *cellIdentifier = @"songCell";
        SongCell *cell = (SongCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

        if ([tableView indexPathIsMovingIndexPath:indexPath]) {
            [cell prepareForMove];
        } else {

            if (tableView.movingIndexPath != nil) {
                indexPath = [tableView adaptedIndexPathForRowAtIndexPath:indexPath];
            }
            Log(@"%@", [NSString stringWithFormat:@"%ld", (long)indexPath.row]);
            PlaylistSong *playlistSong = songList[indexPath.row];

            cell.artistLabel.text = playlistSong.artist;
            cell.songNameLabel.text = playlistSong.name;
            
            cell.cellBorder.borderBottom.alpha = 1;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.shouldIndentWhileEditing = NO;
            cell.showsReorderControl = NO;
            cell.song = playlistSong;
            cell.slideDelegate = self;
           

            if (playlistSong.playlist.isOfflinePlaylist||SETTINGS.OfflineOnly==YES) {
                [cell setOfflineSongInteractions];
            } else {
                [cell setOnlineSongInteractions];
            }

            UIView *bgColorView = [[UIView alloc] init];
            [bgColorView setBackgroundColor:RGBA(0, 0, 0, 0.4)];
            [cell setSelectedBackgroundView:bgColorView];

            [cell markPlaying:playlistSong.isPlayingOrPaused];
            [cell markSelected:playlistSong.isPlayingOrPaused];
            cell.canRemove = playlistSong.playlist.erasableContent.boolValue;
            if(playlists.openedPlaylist.isSharedPlaylist||playlists.openedPlaylist.playlistType.intValue==playlistTypeShared||playlists.openedPlaylist.playlistType.intValue==playlistTypeSearch )
            {
            [cell setUnseen:playlistSong.isNew];
            }
            [cell setBackgroundColor:[UIColor clearColor]];

         
             
        }
       
        return cell;
    }
}

- (void)moveTableViewDidEndDragging:(FMMoveTableView *)tableView withGesture:(UILongPressGestureRecognizer *)gesture withInitialIndexPath:(NSIndexPath *)indexPath {
    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;

    if (playlistsDrawerIsOpenFromDragging) {
        tableView.finishAnimated = NO;
        [playlistsViewController droppedSongOverWithGesture:gesture andContainer:tableView.snapShotPlaceholder];
    }

    playlistsDrawerIsOpenFromDragging = NO;
    [playlistsViewController draggingSongOverStoppedWithGesture:gesture andContainer:tableView.snapShotPlaceholder];
    
    if(IS_DEVICE == IS_IPAD) {
        [masterVC.playerView songStoppedDraggingWithGesture:gesture];
        
        if (CGRectContainsPoint(masterVC.playerView.frame, [gesture locationInView:self.view])){
            [[Player sharedInstance] addUpNextSong:songList[indexPath.row]];
        }
    }else {
        [playerView songStoppedDraggingWithGesture:gesture];

        if (CGRectContainsPoint(playerView.frame, [gesture locationInView:self.view])){
            [[Player sharedInstance] addUpNextSong:songList[indexPath.row]];
        }
    }
}

- (void)moveTableViewFinished:(FMMoveTableView *)tableView {
    [tableView reloadData];
}

- (void)moveTableView:(FMMoveTableView *)tableView startedDraggingWithGesture:(UILongPressGestureRecognizer *)gesture {
    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
    [playlistsViewController draggingSongOverStartedWithGesture:gesture andContainer:tableView.snapShotPlaceholder];
    if(IS_DEVICE == IS_IPAD) {
        if([[Player sharedInstance] currentPlayingSong]) {
            [masterVC.playerView songStartedDraggingWithGesture:gesture];
        }
    }else {
        [playerView songStartedDraggingWithGesture:gesture];
    }
}

- (void)moveTableView:(FMMoveTableView *)tableView draggingWithGesture:(UILongPressGestureRecognizer *)gesture withInitialIndexPath:(NSIndexPath *)indexPath{
    CGPoint touchPoint = [gesture locationInView:masterVC.view];

    if(IS_DEVICE == IS_IPAD) {
        
        if([[Player sharedInstance] currentPlayingSong]) {
            [masterVC.playerView songDraggingWithGesture:gesture andContainer:tableView.snapShotPlaceholder];
        }
        
        if (CGRectContainsPoint(masterVC.playerView.frame, [gesture locationInView:self.view])) {
            tableView.finishAnimated = NO;
            tableView.stopAutoscroll = YES;
        } else {
            tableView.finishAnimated = YES;
            tableView.stopAutoscroll = NO;
        }
        
    }else {
        
        if([[Player sharedInstance] currentPlayingSong]) {

            [playerView songDraggingWithGesture:gesture andContainer:tableView.snapShotPlaceholder];
        }
        
        if (CGRectContainsPoint(playerView.frame, [gesture locationInView:self.view])) {
            tableView.finishAnimated = NO;
            tableView.stopAutoscroll = YES;
        } else {
            tableView.finishAnimated = YES;
            tableView.stopAutoscroll = NO;
        }
        
    }

    CGFloat width;
    CGFloat bazingaX;
    CGFloat bazingaW;
    if(IS_DEVICE == IS_IPAD) {
        width = [Utils currentWidth];
        bazingaX = LEFTPANEL_WIDTH_IPAD;
        bazingaW = bazingaX;
    }else {
        width = [Utils screenWidth];
        bazingaX = 40;
        bazingaW = width - bazingaX;
    }
    
    if (SETTINGS.isLoggedIn) {
        if (!playlistsDrawerIsOpenFromDragging && touchPoint.x < bazingaX && touchPoint.x > 0) {

            playlistsDrawerIsOpenFromDragging = YES;
            [UIView animateWithDuration:.3 animations:^{
                tableView.snapShotPlaceholder.alpha = 0.3;
            }];
            [self addToWithPlaylistSongs:@[songList[indexPath.row]]];

            Log(@"song name: %@", [songList[indexPath.row] name]);
            tableView.stopAutoscroll = YES;

        } else if (playlistsDrawerIsOpenFromDragging) {
            
            if (touchPoint.x > bazingaW && touchPoint.x < width) {
                [UIView animateWithDuration:.3 animations:^{
                    tableView.snapShotPlaceholder.alpha = 1;
                }];
                
                playlistsDrawerIsOpenFromDragging = NO;
                
                if(IS_DEVICE == IS_IPAD) {
                    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
                    if (playlistsViewController.playlistsMode == playlistsViewControllerAddTo) [playlistsViewController setNormalMode];
                }else {
                    [self.sidePanelController showCenterPanelAnimated:YES];
                }
                
                tableView.stopAutoscroll = NO;
                
            } else {
                
                PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
                [playlistsViewController draggingSongOverWithGesture:gesture andContainer:tableView.snapShotPlaceholder];
                
                tableView.stopAutoscroll = YES;
            }
        }
    }
}

- (void)moveTableView:(FMMoveTableView *)tableView willMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    [Analytics event:@"main_view_controller":@"drag_song"];
    //VIBR
    AudioServicesPlaySystemSound(1519);
    UISelectionFeedbackGenerator *selfeedbacGenerator = [[UISelectionFeedbackGenerator alloc] init];
    [selfeedbacGenerator prepare];
    [selfeedbacGenerator selectionChanged];
    
}

- (BOOL)moveTableView:(FMMoveTableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return !(playlists.openedPlaylist.shouldShowEmptyResults || playlists.openedPlaylist.isLocalSearchPlaylistsWith);
}

- (void)moveTableView:(FMMoveTableView *)tableView moveRowFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    if (tableView.stopAutoscroll) {
        [tableView reloadData];
        return;
    }
    //VIBR
    //AudioServicesPlaySystemSound(1519);
    //UISelectionFeedbackGenerator *selfeedbacGenerator = [[UISelectionFeedbackGenerator alloc] init];
    //[selfeedbacGenerator prepare];
    //[selfeedbacGenerator selectionChanged];
    PlaylistSong *playlistSong = songList[fromIndexPath.row];

    [songList removeObjectAtIndex:fromIndexPath.row];
    [songList insertObject:playlistSong atIndex:toIndexPath.row];

    [[Player sharedInstance] refreshNextSongsInPlaylist];

    if(toIndexPath.row != fromIndexPath.row) {
        if (playlistSong.isReorderable) {
            PlaylistSongMove *playlistMove = [[PlaylistSongMove alloc] init];
            playlistMove.playlistSongId = playlistSong.playlistSongId;
            playlistMove.playlistId = playlistSong.playlistId;
            playlistMove.position = [NSNumber numberWithLong:toIndexPath.row];

            if (SETTINGS.isLoggedIn) {
                [[JusthearitClient sharedInstance] movePlaylistSong:playlistMove withSuccessBlock:^(NSArray *array) {} withErrorBlock:^(NSError *error) {}];
            }
        }
    }
}

#pragma mark -
#pragma mark Table view delegate

- (CGFloat)tableView:(FMMoveTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (playlists.openedPlaylist.isLocalSearchPlaylistsWith) {
        if (indexPath.row == 0) {
            return 44;
        } else if (indexPath.row == [self tableView:songsTable numberOfRowsInSection:indexPath.section] - 1) {
            return 64;
        } else {
            return PLAYLISTS_PREVIEW_SONG_CELL_HEIGHT;
        }
    } if (playlists.openedPlaylist.shouldShowEmptyResults) {
        if (playlists.openedPlaylist.showEmptyResults) {
            if (indexPath.row == 0) {
                return 85;
            } else return 44;
        } else if (playlists.openedPlaylist.showSuggestionResults) {
            if (indexPath.row == 0) {
                return 145;
            } else return 44;
        }
    }

    return SONG_CELL_HEIGHT;
}


- (NSIndexPath *)moveTableView:(FMMoveTableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    return proposedDestinationIndexPath;
}
/* - (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(playlists.openedPlaylist.isMusicFeed == 1 && (unsigned long)songList.count == 0) {
        return nil;
    }
    return indexPath;
}*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
    
    if(playlistsViewController.playlistsMode == playlistsViewControllerAddTo) {
            [playlistsViewController setNormalMode];
    }
    
    if(!songsTable.isDecelerating) {
    
        if (playlists.openedPlaylist.isLocalSearchPlaylistsWith) {
            PlaylistPreview *playlistPreview = playlists.openedPlaylist.playlists[indexPath.section];

            [Analytics event:@"main_view_controller":@"hit_playlist_from_playlists_with"];

            id playlist = [LocalPlaylist localSharedPlaylistWithPlaylist:playlistPreview.playlist];
            playlist = [[Playlists sharedInstance] addLocalPlaylist:playlist];
            [playlists openPlaylist:playlist];
            [(MasterViewController*)self.sidePanelController playlistsChanged];
            [self loadPlaylistSongs:playlist fromScratch:YES];

        } else if (playlists.openedPlaylist.showSuggestionResults) {
            //check if the playlist is already shared
            if (indexPath.row > 0) {
                
                if(IS_DEVICE == IS_IPAD) {
                    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
                    if(playerStarted == NO) {
                        [playlistsViewController repositionBottomButtons];
                    }
                }
                
                [Analytics event:@"main_view_controller":@"hit_suggestion"];
                NSString *query = playlists.openedPlaylist.suggestions[indexPath.row-1];
                id playlist = [LocalPlaylist localSearchPlaylistWithQuery:query];
                playlist = [[Playlists sharedInstance] addLocalPlaylist:playlist];
                [playlists openPlaylist:playlist];
                [(MasterViewController*)self.sidePanelController playlistsChanged];
                [self loadPlaylistSongs:playlist fromScratch:YES];
            }
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        } else {
            
            if(IS_DEVICE == IS_IPAD) {
                PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
                if(playerStarted == NO) {
                    [playlistsViewController repositionBottomButtons];
                }
            }
            
            if(IS_DEVICE == IS_IPAD) {
                playerStarted = YES;
                [masterVC testOpenPlayer:(int)indexPath.row];
            }else {
             
                if(!SETTINGS.notfirstPlay)
                {
                    CGRect coachmark1,coachmark2;
                    if([Utils IS_IPHONE_X])
                    {
                    coachmark1 = CGRectMake(2,[Utils screenHeight]-80 , [Utils screenWidth]-2, 78);
                    coachmark2 = CGRectMake(0, 200 , [Utils screenWidth], 30);
                    }
                    
                    else
                    {
                    coachmark2 = CGRectMake(0, 200 , [Utils screenWidth], 64);
                    coachmark1 = CGRectMake(2,[Utils screenHeight]-60 , [Utils screenWidth]-2, 58);
                    }
                    coachMarks = @[
                                   @{
                                       @"rect": [NSValue valueWithCGRect:coachmark1],
                                       @"caption": @"drag to show full player",
                                       @"shape": [NSNumber numberWithInteger:DEFAULT],
                                       @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                       @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                                       @"showArrow":[NSNumber numberWithBool:YES]
                                       }
                                   ];
                    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
                   // [self.view addSubview:coachMarksView];
                    coachMarksView.TUTnmbr=3;
                    //[coachMarksView start];
                    [SETTINGS setnotfirstPlay];
                }
                playerStarted = YES;
                [Analytics event:@"main_view_controller":@"hit_play_song"];
                [playerView setStateClosedAnimated];
                [[Player sharedInstance] playSong:songList[indexPath.row] fromPlaylist:playlists.openedPlaylist];
                PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
                [playlistsViewController.playlistsTable reloadData];

                [songsTable reloadData];
            }
            
        }
        
        CGRect frame;
        
        frame = CGRectMake( ([Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - nSongsButton.frame.size.width) / 2, ( [Utils currentHeight] - 64.0f ), nSongsButton.frame.size.width, nSongsButton.frame.size.height  );
        
        if(playerStarted && nSongsButton.frame.origin.y <= ( [Utils currentHeight] - 64.0f ) ) {
            
            frame.origin.y = frame.origin.y - PLAYER_DRAWER_HEIGHT;
            nSongsButton.frame = frame;
            
        }
        
        nSongsButton.frame = frame;
        
    }
    
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
}
- (IBAction)StartOffline:(id)sender {


    if(IS_DEVICE==IS_IPHONE){
    if(isOffline==1)
    {
        [navigationBar showRedHandle];
        [navigationBar showTitle:@"Off the grid" :@"Tap to go online"];

        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
        
        
        isOffline=2;
    }
    else if(isOffline==2)
    {
        
        [self checkForOnline];
    }
    else if(isOffline==3)
    {
        
        if([NWReachabilityManger isReachable])
        {
        [SETTINGS setOfflineOnly:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
     
        [navigationBar showNormalHandle];
        [navigationBar showLogo];
        isOffline=0;
        [[SongCacheDownloadManager sharedInstance]StartDownloadingAfterOffline ];
            
        }
    }
    }
    
      
}

-(void)openSide{


    
}
- (IBAction) openSidebar:(id)sender {
    
    
  /*  AudioServicesPlaySystemSound(1519);
    UISelectionFeedbackGenerator *selfeedbacGenerator = [[UISelectionFeedbackGenerator alloc] init];
    [selfeedbacGenerator prepare];
    [selfeedbacGenerator selectionChanged];
    //VIBR*/
    [self.sidePanelController showLeftPanelAnimated:YES];
    [Analytics event:@"main_view_controller":@"hit_sidebar_toggle_button"];
}

- (void) loadPlaylistSongs:(Playlist *)playlist fromScratch:(BOOL)fromScratch {


        
    if(!playlist.isMusicFeed) {
        [welcomeView setHidden:YES];
    }else {
        if(!SETTINGS.welcomeMessageDisabled.boolValue) {
            [welcomeView setHidden:NO];
        }
    }
    
    if(playlist.hasNotification && playlist.isInCache && playlist.hasSongsInCache) {
        
        CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist findByPlaylistId:playlist.playlistId];
        [existingPlaylist removeCachedLocalPlaylistSongs:existingPlaylist.cachedLocalPlaylistSongs];
        playlists.openedPlaylist.pagination.currentPage = [NSNumber numberWithInt:1];
    }
    
    if (!playlist) {
        Log(@"no playlist");
        songList = [[NSMutableArray alloc] init];
        playlistsList = [[NSMutableArray alloc] init];
        
        if(!SETTINGS.isLoggedIn) {
            [spinner show:NO];
        }
        //[songsTable reloadData];
        //[self loadComplete:NO andIsAuto:NO];
        
    } else if (playlist.isPlayingOrPaused){
       
        
        songList = [[Player sharedInstance] queue];

        [songsTable reloadData];
        NSIndexPath *initial = [NSIndexPath indexPathForRow:[songList indexOfObject:[[Player sharedInstance] currentPlayingSong]] inSection:0];
        [songsTable scrollToRowAtIndexPath:initial atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        
        if(!SETTINGS.welcomeMessageDisabled.boolValue && playlist.isMusicFeed) {
            
            if(playerStarted) {
                [self setSongsTablePlayerScreen];
            }else {
                [self setSongsTableFullScreen];
            }
        }
        
    } else if (playlist.isOfflinePlaylist) {
        
        NSSortDescriptor *orderDescriptor;
        
        
        if([playlist.playlistType intValue]==playlistTypeMyUploads){
  
            orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:NO];
        }
        else
        {
            orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        }
    
    
        playlist.songs = [SongCache cachedPlaylistSongsAsPlaylistSongs:[playlist.cachedPlaylist.cachedPlaylistSongs.asArray sortedArrayUsingDescriptors:@[orderDescriptor]]];
    
        [playlist.songs foreach:^(PlaylistSong *playlistSong) {
            playlistSong.playlist = playlist;
        }];
        
        //playlist.songs = [SongCache cachedPlaylistSongsAsPlaylistSongsNoDownload:[existingPlaylist.cachedLocalPlaylistSongs.asArray sortedArrayUsingDescriptors:@[orderDescriptor]]];

        songList = playlist.songs;
        [songsTable reloadData];
        
        if (playlists.offlineMode) {
            Log(@"reset top");
            [songsTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
        }
        
    }/* else if (playlist.isInCache && playlist.hasSongsInCache) {
        
        if (fromScratch) [playlist reset];
        
        
        [songsTable  scrollRectToVisible:CGRectMake(0.0, 0.0, songsTable.frame.size.width, songsTable.frame.size.height) animated:NO];
        
        isLoadingSongs = NO;
        playlists = [Playlists sharedInstance];

        songList = [[NSMutableArray alloc] init];
        playlistsList = [[NSMutableArray alloc] init];
        [songsTable reloadData];
        
        CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist findByPlaylistId:playlist.playlistId];
        
        Log(@"number of songs in cache: %lu", (unsigned long)[existingPlaylist.cachedLocalPlaylistSongs count]);
        
        [spinner show:YES];

        Log(@"current cache page: %@, current opened playlist page: %@", existingPlaylist.currentPage, playlists.openedPlaylist.pagination.currentPage);
        
        //[playlists.openedPlaylist.pagination restoreCurrentPage];
        
        /*if([playlists.openedPlaylist.pagination.totalPages isEqualToNumber:[NSNumber numberWithInt:1]]) {
            
            //[playlists.openedPlaylist.pagination setCurrentPage:[NSNumber numberWithInt:0]];
            
        }else {
        
            [playlists.openedPlaylist.pagination restoreCurrentPage];
            
        }*/
        
      /*  NSSortDescriptor *orderDescriptor;

        if ([existingPlaylist.playlistType  isEqual: @"5"]) {
            orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdAt" ascending:NO];
        }else if( [existingPlaylist.playlistType  isEqual: @"4"] ){
            orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:YES];
        }else if( [existingPlaylist.playlistType  isEqual: @"2"] ){
            orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:NO];
        } else {
            orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        }
        
        Log(@"playlist songs: %lu", (unsigned long)[existingPlaylist.cachedLocalPlaylistSongs.asArray count]);
        
        playlist.songs = [SongCache cachedPlaylistSongsAsPlaylistSongsNoDownload:[existingPlaylist.cachedLocalPlaylistSongs.asArray sortedArrayUsingDescriptors:@[orderDescriptor]]];
        
        [playlist.songs foreach:^(PlaylistSong *playlistSong) {
            playlistSong.playlistOrigin = [playlists findByPlaylistId:playlistSong.playlistId];
            playlistSong.playlist = playlist;
        }];
        
        songList = playlist.songs;
        
        Log(@"condition 1: %@", playlists.openedPlaylist.pagination.currentPage);
        Log(@"condition 2: %@", playlists.openedPlaylist.pagination.totalPages);
        //[playlists.openedPlaylist.pagination restoreCurrentPage];
        if((playlists.openedPlaylist.pagination.currentPage == [NSNumber numberWithInt:0] || playlists.openedPlaylist.pagination.currentPage > [NSNumber numberWithInt:1]) && playlists.openedPlaylist.pagination.totalPages > [NSNumber numberWithInt:1]) {
        //if(playlists.openedPlaylist.pagination.currentPage == [NSNumber numberWithInt:0]) {
            [playlists.openedPlaylist.pagination restoreCurrentPage];
        }
        
        if (!existingPlaylist || [existingPlaylist.cachedLocalPlaylistSongs.asArray count] == 0) {
            Log(@"load next page 1");
            [self loadNextPageOfPlaylist:playlist andIsAuto:NO];
        } else {
            [songsTable reloadData];
            [self loadComplete:NO andIsAuto:NO];
        }
        
        if(!SETTINGS.welcomeMessageDisabled.boolValue && [existingPlaylist.playlistType  isEqual: @"5"]) {
            
            if(playerStarted) {
                [self setSongsTablePlayerScreen];
            }else {
                [self setSongsTableFullScreen];
            }
        }
        
    } */else {
        if (fromScratch) [playlist reset];

        songList = [[NSMutableArray alloc] init];
        playlistsList = [[NSMutableArray alloc] init];
        
        [songsTable reloadData];
        
        songList = playlist.songs;
        
        
        //playlists.openedPlaylist.songs;
        [spinner show:YES];

        if (playlist.pagination.hasNextPage) {
            
            Log(@"next page 2");
            [self loadNextPageOfPlaylist:playlist andIsAuto:NO];
            
        } else {
            [songsTable reloadData];
            [self loadComplete:NO andIsAuto:NO];
        }
        
        if( playlists.openedPlaylist.isNormalPlaylist && playlists.openedPlaylist.pagination.totalPages.intValue == 1 ) {
            
            CGRect frame;
            
            frame = CGRectMake( nSongsButton.frame.origin.x, ( [Utils currentHeight] - 64.0f ), nSongsButton.frame.size.width, nSongsButton.frame.size.height  );
            
            if(playerStarted) {
                
                frame.origin.y = frame.origin.y - PLAYER_DRAWER_HEIGHT;
                
            }
            
            nSongsButton.frame = frame;
            
        }else {
            
            nSongsButton.frame = topFrame;
            
        }
        
        nSongsButton.alpha = 0;
        
        if(!SETTINGS.welcomeMessageDisabled.boolValue && playlist.isMusicFeed) {
            
            if(playerStarted) {
                [self setSongsTablePlayerScreen];
            }else {
                [self setSongsTableFullScreen];
            }
        }
    }
    
    songListTmp = [songList mutableCopy];
}

- (void) savePlaylistToCache: (Playlist*) playlist {

    // wait for 3 seconds before starting the thread, you don't have to do that. This is just an example how to stop the NSThread for some time
    [NSThread sleepForTimeInterval:3];
    [playlist cacheNoDownload];

}

- (void) cacheSongs: (Playlist*) playlist {
    
}

- (void) loadPlaylistSongsFromUrlPath:(NSNotification *)notification {
    
    [spinner show:YES];
    
    Playlist *added = (Playlist *) notification.userInfo;
 
   
    [added.pagination reset];
    [welcomeView setHidden:YES];
    songList = [[NSMutableArray alloc] init];
    playlistsList = [[NSMutableArray alloc] init];
    [songsTable reloadData];
    [playlists openPlaylist:added];
    
    songList = playlists.openedPlaylist.songs;

    __weak typeof(self) weakSelf = self;
    __weak typeof(added) weakAdded = added;

    [[JusthearitClient sharedInstance] loadNextPageFromPlaylist:added withSuccessBlock:^(NSArray *array) {
        Log(@"load next page");
        
        Playlist *loadedPlaylistWithSongs = _.first(array);
        //Log(@"loaded playlist with songs: %@", loadedPlaylistWithSongs.songs);
        [weakAdded mergeWithPlaylist:loadedPlaylistWithSongs];
        [weakSelf loadComplete:NO andIsAuto:NO];
        [weakSelf.songsTable reloadData];
        weakAdded.name = loadedPlaylistWithSongs.playlistData.name;
        weakAdded.userId = loadedPlaylistWithSongs.playlistData.userId;
        
        [(MasterViewController*)self.sidePanelController playlistsChanged];

    } withErrorBlock:^(NSError *error) {
        [weakSelf loadComplete:NO andIsAuto:NO];
    }];
    
    [(MasterViewController*)self.sidePanelController playlistsChanged];
}

- (void)addToWithPlaylistSongs:(NSArray *)songs {
    if (!SETTINGS.isLoggedIn) {
        PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
        [playlistsViewController openLoginViewController];
    } else if (songs) {
        PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
        [playlistsViewController setAddToModeWithSongs:songs];
        
        if(IS_DEVICE == IS_IPAD) {
            if([[Player sharedInstance] currentPlayingSong]) {
                [masterVC.playerView setState:STATE_CLOSED animated:YES];
            }
        }else {
            [self.sidePanelController showLeftPanelAnimated:YES];
        }
    }
}
- (void)setNormalMode {
    
    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
    [playlistsViewController setNormalMode];
    [self.sidePanelController showLeftPanelAnimated:YES];
    
}
- (void) loadNextPageOfPlaylist:(Playlist*)playlist andIsAuto:(BOOL)isauto {
    __weak typeof(self) weakSelf = self;
    __weak typeof(playlist) weakPlaylist = playlist;
    
    [[JusthearitClient sharedInstance] loadNextPageFromPlaylist:playlist withSuccessBlock:^(NSArray *array) {
        Playlist *loadedPlaylistWithSongs = _.first(array);
        
        [weakPlaylist mergeWithPlaylist:loadedPlaylistWithSongs];
        
        if(songList.count == 0) songList = weakPlaylist.songs;
        
        [weakSelf.songsTable reloadData];
        
        if (isauto) {
             [weakSelf loadComplete:YES andIsAuto:YES];
        }else {
             [weakSelf loadComplete:YES andIsAuto:NO];
        }
       
        
    } withErrorBlock:^(NSError *error) {
        
        [weakSelf loadComplete:NO andIsAuto:NO];
        
    }];
}

- (void) loadComplete:(BOOL)isNextPage andIsAuto:(BOOL)isauto {
    
    //Log(@"is music feed 2: %@", playlists.openedPlaylist.playlistType);
    if(playlists.openedPlaylist.isMusicFeed && (!SETTINGS.welcomeMessageDisabled.boolValue)) {

        /*if(!isTableResized && isNextPage) {
            songsTable.frame = CGRectMake(songsTable.frame.origin.x, 55 + welcomeView.frame.size.height, songsTable.frame.size.width, songsTable.frame.size.height - welcomeView.frame.size.height);
            isTableResized = YES;
        }else if(!isNextPage) {
            songsTable.frame = CGRectMake(songsTable.frame.origin.x, 55 + welcomeView.frame.size.height, songsTable.frame.size.width, songsTable.frame.size.height - welcomeView.frame.size.height);
            isTableResized = YES;
        }*/
        
        if(playlists.openedPlaylist.pagination.currentPage == [NSNumber numberWithInt:1] && !playlists.openedPlaylist.isInCache) {
            songsTable.frame = CGRectMake(songsTable.frame.origin.x, 55 + welcomeView.frame.size.height, songsTable.frame.size.width, songsTable.frame.size.height - welcomeView.frame.size.height);
            isTableResized = YES;
        }
        
        
    }else {
        
        [welcomeView setHidden:YES];
        
        if(playerStarted) {
            [self setSongsTablePlayerScreen];
        }else {
            [self setSongsTableFullScreen];
        }
        
    }
    
    playlists.openedPlaylist.loaded = YES;
    
    if (isauto) {
        
        NSIndexPath* ip = [NSIndexPath indexPathForRow:songList.count-1 inSection:0];
        [songsTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
    }
    
    if(playlists.openedPlaylist.shouldShowEmptyResults) {
        [spinner show:NO];
    }
    
    [self hideFooter];
    
    isLoadingSongs = NO;
    [spinner show:NO];
    
    /* if([playlists.openedPlaylist.songs count] > 0) {
        isLoadingSongs = NO;
        [spinner show:NO];
    } */
    
    
    Log(@"current page of opened playlist: %d", playlists.openedPlaylist.pagination.currentPage.intValue);
    if(playlists.openedPlaylist.isInCache && playlists.openedPlaylist.pagination.currentPage.intValue == 1 && !playlists.openedPlaylist.hasSongsInCache && !playlists.openedPlaylist.isLocalPlaylist) {
     
        [playlists.openedPlaylist cacheNoDownload];
    }
    
    if (playlists.openedPlaylist.shouldShowEmptyResults || playlists.openedPlaylist.pagination.currentPage.intValue == 0) {
        [songsTable reloadData];
    }
    
    if (playlists.openedPlaylist.hasNotification && !playlists.openedPlaylist.isMusicFeed && !playlists.openedPlaylist.isRecentlyPlayed && !playlists.openedPlaylist.isMyUploads && !playlists.openedPlaylist.isSharedSearchPlaylist) {
        [self scrollToBottom];
        playlists.openedPlaylist.hasNotification = NO;
    }else if(playlists.openedPlaylist.isMyUploads) {
        playlists.openedPlaylist.hasNotification = NO;
    }
    
}

#pragma mark -
#pragma mark SongCell Delegate

- (void)cellDidSelectAddTo:(SongCell *)cell {
    [self addToWithPlaylistSongs:@[cell.song]];
}

- (void)cellDidSelectDiscover:(SongCell *)cell {
    [self performSegueWithIdentifier:@"discoverSegue" sender:cell.song];
}

- (void)cellDidSelectShare:(SongCell *)cell {
    UIViewController *shr =[Utils sharingViewControllerForSong:cell.song];
    if(IS_DEVICE==IS_IPAD)
    {
        shr.popoverPresentationController.sourceView = self.view;
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:shr];
        // popover.delegate = self;
        
        CGRect fr=CGRectMake(0, [Utils screenHeight], [Utils screenWidth], 100);
        
        [popover presentPopoverFromRect:fr inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        //[self presentViewController:shr animated:YES completion:nil];
        
    }
    else
    {
        [self presentViewController:shr animated:YES completion:nil];
        
    }
    }
- (void) receiveopenmenu:(NSNotification *) notification
{
    [self.sidePanelController showLeftPanelAnimated:YES];
    [Analytics event:@"main_view_controller":@"hit_sidebar_toggle_button"];
}

- (void)cellDidSelectRemove:(SongCell *)cell {
    
    PlaylistSong *song=cell.song;
    if(song.isPlayingOrPaused)
    {
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.nextButtonHit object:nil userInfo:nil];
        [self performSelector:@selector(delafterDelay:) withObject:cell afterDelay:0.7 ];
    }
    else{
        [self delafterDelay:cell];
    }
    
}
-(void) delafterDelay:(SongCell *) cell{
    PlaylistSong *song = cell.song;
    
    [song.playlist.songs removeObject:song];
    [songListTmp removeObject:song];
    
    
    /* if (song.isPlayingOrPaused) {
     //[self stopMusic];
     songList = songListTmp;
     playlists.openedPlaylist.songs = songListTmp;
     }*/
    
    NSIndexPath *indexPath = [songsTable indexPathForCell:cell];
    
    
    //[songList removeObjectAtIndex:indexPath.row];
    [[JusthearitClient sharedInstance] deletePlaylistSong:song withSuccessBlock:^(NSArray *array) {
    }                                      withErrorBlock:^(NSError *error) {
    }];
    
    //NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"(playlistSongId = %d)", song.playlistSongId, song.songId];
    
    //CachedPlaylistSong *existingSong = [CachedPlaylistSong MR_findFirstWithPredicate:predicate1];
    
    //[songList removeObjectAtIndex:indexPath.row];
    
    CachedLocalPlaylistSong *existingSong = [CachedLocalPlaylistSong MR_findFirstByAttribute:@"playlistSongId" withValue:song.playlistSongId];
    
    if(existingSong) {
        [SongCache deleteSongFromCoreDataNoDownload:existingSong];
    }
    
    //[songsTable reloadData];
    [songsTable beginUpdates];
    [songsTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [songsTable endUpdates];
    
    /* if (song.isPlayingOrPaused) {
     [self stopMusic];
     
     song.playlist.playState = playStateStopped;
     
     PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
     [playlistsViewController.playlistsTable reloadData];
     
     playerStarted = NO;
     }
     */
    
    if (song.playlist.isPlayingOrPaused&&song.isPlayingOrPaused==NO) {
        
        [[Player sharedInstance] refreshNextSongsInPlaylist];
    }

}

- (void)cellStartedRevealing:(SongCell *)cell {



    [songsTable.visibleCells foreach:^(SongCell *vCell) {
        if (vCell != cell) {
            if([vCell respondsToSelector:@selector(enclosingTableViewDidScroll)]) {
                [vCell enclosingTableViewDidScroll];
                 //[self performSelector:@selector(VOIDvibrate) withObject:self afterDelay:.2 ];
            }
        }
    }];
}
-(void) VOIDvibrate{
    UISelectionFeedbackGenerator *selfeedbacGenerator = [[UISelectionFeedbackGenerator alloc] init];
    [selfeedbacGenerator prepare];
    [selfeedbacGenerator selectionChanged];
    AudioServicesPlaySystemSound(1519);
}
- (void)cellWasTapped:(SongCell *)cell {
    
    if(cell.scrollViewSelf.contentOffset.x == 0) {
        NSIndexPath *indexPath = [songsTable indexPathForCell:cell];
        [self tableView:songsTable didSelectRowAtIndexPath:indexPath];
    }

}

#pragma mark -
#pragma mark Scroll View Delegate


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (playlists.openedPlaylist.isLocalSearchPlaylistsWith) {
        //Log(@"%f of %f (frame height: %f)", songsTable.contentOffset.y, songsTable.contentSize.height, songsTable.frame.size.height);
        CGFloat frameHeight = songsTable.frame.size.height;
        CGFloat offsetY = songsTable.contentOffset.y;
        CGFloat sizeY = songsTable.contentSize.height;

        CGFloat distanceFromBottom = sizeY - offsetY;

        BOOL limitTouched = distanceFromBottom < frameHeight *3;

        BOOL hasNextPage = playlists.openedPlaylist.pagination.totalPages.intValue >= playlists.openedPlaylist.pagination.currentPage.intValue;

        if (limitTouched && !isLoadingSongs && hasNextPage){
            [self showFooter];
            //Log(@"next page 3");
            [self loadNextPageOfPlaylist:playlists.openedPlaylist andIsAuto:NO];
            isLoadingSongs = YES;
        }
    } else if (songList.count > 0) {
        
        if (Utils.isReachable && !playlists.offlineMode) {
        
            int percentage = (int)(100*indexPath.row/songList.count);
            //Log(@"percentage: %d, %d, %d", percentage, indexPath.row, songList.count);
            //Log(@"is loading songs: is not loading %d, has next page %hhd, total pages %@, current page %@", (!isLoadingSongs), playlists.openedPlaylist.pagination.hasNextPage, playlists.openedPlaylist.pagination.totalPages, playlists.openedPlaylist.pagination.currentPage);
            if (percentage > 90 && !isLoadingSongs && playlists.openedPlaylist.pagination.hasNextPage){
                [self showFooter];
                //Log(@"next page 4");
                [self loadNextPageOfPlaylist:playlists.openedPlaylist andIsAuto:NO];
                isLoadingSongs = YES;
            }
            
        }
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [[NSNotificationCenter defaultCenter] postNotificationName:SongCellEnclosingTableViewDidBeginScrollingNotification object:scrollView];
    
    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
    
    if(playlistsViewController.playlistsMode == playlistsViewControllerAddTo) {
        [playlistsViewController setNormalMode];
    }
}

//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
//    _.arrayEach(songsTable.visibleCells, ^(id cell){
//        if ([cell isKindOfClass:[SongCell class]]) {
//            PlaylistSong *song = [(SongCell*)cell song];
//            [[InstaPlayDownloadManager sharedInstance] pushSong:song];
//        }
//    });
//}
-(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
     playlists.openedPlaylist.badgeCount = @0;
     playlists.openedPlaylist.hasNotification = NO;
     PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
     [playlistsViewController.playlistsTable reloadData];
    
}
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;

    if(offset.y+[Utils screenHeight]>size.height||offset.y<0){
        
        if(MMARE==0)
        {
            //VIBR
            UIImpactFeedbackGenerator *myGen = [[UIImpactFeedbackGenerator alloc] init];
            [myGen initWithStyle:(UIImpactFeedbackStyleLight)];
            [myGen impactOccurred];
            myGen = NULL;
        AudioServicesPlaySystemSound(1521);
        }
        MMARE=1;
    }
    else{
        MMARE=0;
    }
    scrollOffsetY = offset.y;
    
    if(offset.y > SONG_CELL_HEIGHT && hasNewSongs && (playlists.openedPlaylist.isNormalPlaylist && playlists.openedPlaylist.pagination.totalPages.intValue > 1) && y <= h) {
        [self showNewSongsButton];
    }else if(offset.y <= 0.0f ){
        [self hideNewSongsButton];
        hasNewSongs = NO;
    }else if(y >= h) {
        [self hideNewSongsButton];
        hasNewSongs = NO;
    }
}

- (void) hideFooter {
    songsTable.tableFooterView.hidden = YES;
    [songsTable setContentInset:(UIEdgeInsetsMake(0, 0, - PLAYLISTS_CELL_HEIGHT, 0))];
    [spinnerFooter stop];
}

- (void) showFooter {
    songsTable.tableFooterView.hidden = NO;
    [songsTable setContentInset:(UIEdgeInsetsMake(0, 0, 0, 0))];
    [spinnerFooter start];
}

#pragma mark -
#pragma mark Notifications

- (void) songStartedPlay:(NSNotification *)notification {
    [songsTable reloadData];
}

#pragma mark -
#pragma mark General segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"discoverSegue"]) {
   
     
         DiscoverViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DiscoverViewC"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.delegate = self;
        vc.song = sender;
        vc.view.frame = self.view.bounds;
        vc.view.backgroundColor = [UIColor clearColor];
        
        
        

        [self presentViewController:vc animated:YES completion:nil];
      

        
        
    } else if ([segue.identifier isEqualToString:@"shareSegue"]) {
        ShareViewController * vc = (ShareViewController*) segue.destinationViewController;
        
        vc.delegate = self;
    } else if([segue.identifier isEqualToString:@"echoprintSegue"]){
        EchoprintViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"echoprintVC"];
        evc.songname=PHPResponseSTR;
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        [self presentViewController:evc animated:YES completion:nil];
    }
    
}


#pragma mark -
#pragma mark ShareViewControllerDelegate

- (void)shareViewControllerDidFinish:(ShareViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark -
#pragma mark DiscoverViewControllerDelegate

- (void)discoverViewController:(DiscoverViewController *)controller didFinishSearchWithQuery:(NSString *)query forPlaylists:(BOOL)forPlaylists {
    [self makeSearchPlaylistWithQuery:query forPlaylists:forPlaylists];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void) makeSearchPlaylistWithQuery:(NSString *)query forPlaylists:(BOOL) forPlaylists {
    if (query) {
        id playlist;

        if (forPlaylists) {
            playlist = [LocalPlaylist localSearchPlaylistsWithQuery:query];
        } else {
            playlist = [LocalPlaylist localSearchPlaylistWithQuery:query];
        }

        playlist = [[Playlists sharedInstance] addLocalPlaylist:playlist];
        [[Playlists sharedInstance] openPlaylist:playlist];
        [(MasterViewController*)self.sidePanelController playlistsChanged];
        [self loadPlaylistSongs:playlist fromScratch:YES];

        if (playerView.viewState == STATE_OPEN) [playerView closeView];
    }
}

- (void)stopMusic {
    [[Player sharedInstance] stopMusic];
    [[AudioPlayer sharedInstance] stopMusic];
    playerStarted=NO;
    //[self setSongsTableFullScreen];
    if(IS_DEVICE==IS_IPHONE)
    {
    [self hidePlayer];
    }
    else
    {
        
       [masterVC hidePlayer];
        PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
        
        [playlistsViewController repositionNegativeBottomButtons];
        
    }
}



- (void)centerViewControllerMovedXOffset:(CGFloat)offset {
    [super centerViewControllerMovedXOffset:offset];
}

-(void) testOpenPlayer:(int)row {
    [delegate testOpenPlayer:row];
}
- (void) repositionBackground {
    /*[self.background removeFromSuperview];
    self.background = [UIImageView imageNamed:@"background_landscape_ipad.png"];
    self.background.frame = CGRectMake(0, 0, [Utils currentWidth], [Utils currentHeight]);
    [self.view addSubview:self.background];
    [self.view sendSubviewToBack:self.background];
    [self centerViewControllerMovedXOffset:self.sidePanelController.centerPanelContainer.frame.origin.x];*/

}
- (void) repositionOnRotation:(float)width :(float)height {
    
    closeButton.frame = CGRectMake( [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - CLOSE_BUTTON_SIZE, 0, CLOSE_BUTTON_SIZE, CLOSE_BUTTON_SIZE );
    if([Utils isLandscape]) {
        //welcomeMessage.frame = CGRectMake(welcomeMessage.frame.origin.x, welcomeMessage.frame.origin.y, welcomeMessage.frame.size.width, welcomeMessage.frame.size.height + 100.0f);
    }else {
        //welcomeMessage.frame = CGRectMake(welcomeMessage.frame.origin.x, welcomeMessage.frame.origin.y, welcomeMessage.frame.size.width, welcomeMessage.frame.size.height - 100.0f);
    }
    
    spinner.activityImageView.frame = CGRectMake(( [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - spinner.activityImageView.frame.size.width ) / 2,( [Utils currentHeight] - spinner.activityImageView.frame.size.height ) / 2, spinner.activityImageView.frame.size.width, spinner.activityImageView.frame.size.height);
    
    spinnerFooter.activityImageView.frame = CGRectMake(( [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - spinnerFooter.activityImageView.frame.size.width ) / 2,spinnerFooter.activityImageView.frame.origin.y, spinnerFooter.activityImageView.frame.size.width, spinnerFooter.activityImageView.frame.size.height);
    
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, width, height);
    
    self.background.frame = CGRectMake(0, 0, [Utils currentWidth], [Utils currentHeight]);
    [self centerViewControllerMovedXOffset:self.sidePanelController.centerPanelContainer.frame.origin.x];

    if(playerStarted) {
        [self setSongsTablePlayerScreen];
    }else {
        [self setSongsTableFullScreen];
    }
    
    CGRect frame;
    
    frame = CGRectMake( ([Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - nSongsButton.frame.size.width) / 2, ( [Utils currentHeight] - 64.0f ), nSongsButton.frame.size.width, nSongsButton.frame.size.height  );
    
    if(playerStarted && nSongsButton.frame.origin.y <= ( [Utils currentHeight] - 64.0f ) ) {
        
        frame.origin.y = frame.origin.y - PLAYER_DRAWER_HEIGHT;
        nSongsButton.frame = frame;
        
    }
    
    if(![Utils isVersionLessThan_8_0]) {
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, [Utils currentWidth], [Utils currentHeight]) cornerRadius:0.0f];
    
        self.sidePanelController.centerPanelContainer.layer.shadowPath = shadowPath.CGPath;
    }
    
    nSongsButton.frame = frame;
    
    [songsTable reloadData];
    
}
/*- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    NSLog(@"current width: %d", [Utils currentWidth]);
    NSLog(@"current height: %d", [Utils currentHeight]);
    if(playerStarted) {
        [self setSongsTablePlayerScreen];
    }else {
        [self setSongsTableFullScreen];
    }
    [songsTable reloadData];
}*/
/*-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    NSLog(@"current width: %d", [Utils currentWidth]);
    NSLog(@"current height: %d", [Utils currentHeight]);
    if(playerStarted) {
        [self setSongsTablePlayerScreen];
    }else {
        [self setSongsTableFullScreen];
    }
    [songsTable reloadData];
}*/

@end

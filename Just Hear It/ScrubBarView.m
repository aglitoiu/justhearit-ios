#import "ScrubBarView.h"

@implementation ScrubBarView

float plm;

@synthesize slider, time, audioLoaded, isDragging, songTime, delegate, isCache, lastKnownThumbRect;

- (void) setup {
    
    
    slider = [[OBSlider alloc] init];
    [slider addTarget:self action:@selector(sliderPositionChanged:) forControlEvents:UIControlEventValueChanged];
    [slider addTarget:self action:@selector(sliderEndDrag:) forControlEvents:UIControlEventTouchUpInside];

    
    [slider addTarget:self action:@selector(sliderEndDrag:) forControlEvents:UIControlEventTouchUpOutside];
    [slider addTarget:self action:@selector(sliderStartDrag:) forControlEvents:UIControlEventTouchDown];

   
    CGRect sliderFrame;
    
  
        sliderFrame = CGRectMake(([Utils currentWidth] - 320) / 2, 0, 320, 15);
    
    
    slider.frame = sliderFrame;
    slider.maximumValue = 1;
    
    CGFloat inset = 7+17.5f;
    
    UIImage *trackSlider = [Utils image:[[UIImage imageNamed:@"scrub_bar_background.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, inset, 0, inset)] byApplyingAlpha:0.4f];
    UIImage *backgroundSlider = [Utils image:[[UIImage imageNamed:@"scrub_bar_background.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, inset, 0, inset)] byApplyingAlpha:0.2f];
    
    audioLoaded = [[AKProgressView alloc] initWithFrame:sliderFrame];
    
    NSLog(@"<<<<<<<<< PROGRESS VIEW 2 >>>>>>>>>");
    
    //[audioLoaded set]
    
    [audioLoaded setProgressImage:backgroundSlider];
    [audioLoaded setTrackImage:trackSlider];
    audioLoaded.frame = sliderFrame;
    CGRect audioLoadedFrame = audioLoaded.frame;
    audioLoadedFrame.size.width -= (35.5f);
    audioLoaded.frame = audioLoadedFrame;
    audioLoaded.center = slider.center;
    
    audioLoadedFrame = audioLoaded.frame;
    audioLoadedFrame.origin.x += 2;
    audioLoaded.frame = audioLoadedFrame;
    
    audioLoaded.autoresizingMask = slider.autoresizingMask;
    
    [slider setMinimumTrackImage:[[UIImage imageNamed:@"scrub_progress_gradient.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 7, 0, 7)] forState:UIControlStateNormal];
    [slider setMaximumTrackImage:[Utils imageFromColor:[UIColor clearColor] withWidth:1 andHeight:15] forState:UIControlStateNormal];
    [slider setThumbImage:[Utils imageFromColor:[UIColor clearColor] withWidth:30 andHeight:50] forState:UIControlStateNormal];
    
    time = [[UILabel alloc] init];
    CGRect timeFrame = sliderFrame;
    timeFrame.size.width -= 22;
    time.frame = timeFrame;
    time.textColor = RGBA(255,255,255,.6);
    time.font = [UIFont boldSystemFontOfSize:14];
    
    time.textAlignment = NSTextAlignmentRight;
    time.text = @"00:00";
    
    [self addSubview:audioLoaded];
    [self addSubview:slider];
    [self addSubview:time];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSongProgressChanged:)      name:Ev.songProgressChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAudioPercentageAvailable:) name:Ev.audioPercentageAvailable object:nil];
}



- (void)panning:(id)panning {
    
}

- (void) handleAudioPercentageAvailable:(NSNotification *) notification {
    NSNumber *percentage = notification.userInfo;
    audioLoaded.progress = [percentage floatValue];
}

- (void) sliderStartDrag:(NSNotification *)notification {
    if ([delegate respondsToSelector:@selector(scrubBarDragStarted)]) {
        [delegate scrubBarDragStarted];
           }
    
    // set your message properties
       [[NSNotificationCenter defaultCenter] postNotificationName:Ev.isScrubbing object:nil];
}






- (void) sliderEndDrag:(id)sender {
    isDragging = NO;
   [slider setThumbImage:[Utils imageFromColor:[UIColor clearColor] withWidth:30 andHeight:50] forState:UIControlStateNormal];

    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songProgressManuallyChanged object:nil userInfo:songTime];
    
    if ([delegate respondsToSelector:@selector(scrubBarDragEnded)]) {
        [delegate scrubBarDragEnded];
    }
}

- (void)sliderPositionChanged:(id) sender {
    isDragging = YES;
     [slider setThumbImage:[UIImage imageNamed:@"scrub_handle.png"] forState:UIControlStateNormal];
    Float64 scrolledToSecond = [songTime secondsFromPercentageDone:slider.value];
    
    songTime.currentSeconds = scrolledToSecond;
    songTime.remainingSeconds = songTime.totalSeconds - scrolledToSecond;
    songTime.elapsed = [songTime humanTimeElapsedWithSeconds:songTime.currentSeconds];
    songTime.remaining = [songTime humanTimeElapsedWithSeconds:songTime.remainingSeconds];
    
    [self updateSliderLabels];
    
    if ([delegate respondsToSelector:@selector(scrubBarSpeedChanged:)]) {
        [delegate scrubBarSpeedChanged:slider.scrubbingSpeed];
    }
    
}

- (void) handleSongProgressChanged:(NSNotification *)notification {
    if (!isDragging) {
        songTime = (SongTime *)notification.userInfo;
        slider.value = songTime.percentageDone;
        [self updateSliderLabels];
    }
}

- (void)updateSliderLabels {
    time.text = songTime.elapsed;
}

@end

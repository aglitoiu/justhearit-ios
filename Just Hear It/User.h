#import <Foundation/Foundation.h>


@interface User : NSObject

@property(nonatomic, strong) NSString *userId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *firstName;
@property(nonatomic, strong) NSString *lastName;
@property(nonatomic, strong) NSString *toLabel;
@property(nonatomic, strong) NSString *fullName;

@end
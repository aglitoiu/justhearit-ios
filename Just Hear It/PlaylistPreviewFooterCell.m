#import "FontAwesomeKit/FAKFontAwesome.h"
#import "PlaylistPreviewFooterCell.h"
#import "StandardCellBorder.h"

@implementation PlaylistPreviewFooterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUserIcon];
    [self setupGenreIcon];
    [self setupUserLabel];
    [self setupGenreLabel];

    [self setupPlayCountIcon];
    [self setupPlayCountLabel];

    [self setupSongCountIcon];
    [self setupSongCountLabel];

    [self addBorder];
    
}

- (void) addBorder {
    UIImageView *borderBottom = [[UIImageView alloc] initWithImage:[Utils imageFromColor:RGBA(255,255,255,.1)]];
    borderBottom.frame = CGRectMake(0, 63, self.contentView.frame.size.width, 1);
    borderBottom.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    [self.contentView addSubview:borderBottom];
}

- (void) adjustPlayCount:(NSString *)playCount andSongCount:(NSString *)songCount {
    _playCountLabel.text = playCount;
    _songCountLabel.text = songCount;

    //setup play count label
    CGRect playCountFrame = _playCountLabel.frame;
    playCountFrame.size.width = [playCount sizeWithAttributes:@{NSFontAttributeName: _playCountLabel.font}].width;
    playCountFrame.origin.x = _playCountIcon.frame.origin.x - playCountFrame.size.width - 5;
    _playCountLabel.frame = playCountFrame;

    //setup song count icon
    CGRect songCountIconFrame = _songCountIcon.frame;
    songCountIconFrame.origin.x = _playCountLabel.frame.origin.x - 10 - songCountIconFrame.size.width;
    _songCountIcon.frame = songCountIconFrame;

    //setup song count label
    CGRect songCountLabelFrame = _songCountLabel.frame;
    songCountLabelFrame.size.width = [songCount sizeWithAttributes:@{NSFontAttributeName: _songCountLabel.font}].width;
    songCountLabelFrame.origin.x = songCountIconFrame.origin.x - songCountLabelFrame.size.width - 5;
    _songCountLabel.frame = songCountLabelFrame;

    CGRect userLabelFrame = _userLabel.frame;
    userLabelFrame.size.width = songCountLabelFrame.origin.x - 10;
    _userLabel.frame = userLabelFrame;
}

- (void) setupPlayCountLabel {
    //we dont have x or width here
    _playCountLabel = [[UILabel alloc] init];
    _playCountLabel.frame = CGRectMake([Utils screenWidth], _playCountIcon.frame.origin.y, 0, _playCountIcon.frame.size.height);
    _playCountLabel.adjustsFontSizeToFitWidth = NO;
    _playCountLabel.font = [UIFont boldSystemFontOfSize:12];
    _playCountLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_playCountLabel];
}

- (void) setupSongCountLabel {
    //we dont have x or width here
    _songCountLabel = [[UILabel alloc] init];
    _songCountLabel.frame = CGRectMake([Utils screenWidth], _playCountIcon.frame.origin.y, 0, _playCountIcon.frame.size.height);
    _songCountLabel.adjustsFontSizeToFitWidth = NO;
    _songCountLabel.font = [UIFont boldSystemFontOfSize:12];
    _songCountLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_songCountLabel];
}

- (void) setupPlayCountIcon {
    _playCountIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_play_count.png"]];
    _playCountIcon.contentMode = UIViewContentModeCenter;
    CGFloat x;
    
    if(IS_DEVICE == IS_IPAD) {
        x = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - _playCountIcon.image.size.width - 10;
    }else {
        x = [Utils screenWidth] - _playCountIcon.image.size.width - 10;
    }
    //Log(@"play icon x: %f", x);
    //CGFloat x = [Utils screenWidth] - _playCountIcon.image.size.width - 10;

    _playCountIcon.alpha = 0.6;
    _playCountIcon.frame = CGRectMake(x, 15, _userIcon.image.size.width, _userIcon.image.size.height);
    [self.contentView addSubview:_playCountIcon];
}

- (void) setupSongCountIcon {
    //we dont have x here
    _songCountIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_my_uploads.png"]];
    _songCountIcon.contentMode = UIViewContentModeCenter;
    _songCountIcon.frame = _playCountIcon.frame;
    _songCountIcon.alpha = 0.6;
    [self.contentView addSubview:_songCountIcon];
}

- (void) setupUserLabel {
    CGFloat x = _userIcon.frame.size.width + _userIcon.frame.origin.x + 5;
    _userLabel = [[UILabel alloc] init];
    _userLabel.frame = CGRectMake(x, _userIcon.frame.origin.y, [Utils screenWidth]/2 - x - 5, _userIcon.frame.size.height);
    _userLabel.adjustsFontSizeToFitWidth = NO;
    _userLabel.font = [UIFont boldSystemFontOfSize:12];
    _userLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_userLabel];
}

- (void) setupGenreLabel {
    CGFloat x = _genreIcon.frame.size.width + _genreIcon.frame.origin.x + 5;
    _genreLabel = [[UILabel alloc] init];
    _genreLabel.frame = CGRectMake(x, _genreIcon.frame.origin.y, [Utils screenWidth] - x - 10, _genreIcon.frame.size.height);
    _genreLabel.adjustsFontSizeToFitWidth = NO;
    _genreLabel.font = [UIFont boldSystemFontOfSize:12];
    _genreLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_genreLabel];
}

- (void)setupUserIcon {
    CGFloat x = PLAYLISTS_PREVIEW_ICON_PADDING_LEFT*2 + PLAYLISTS_PREVIEW_ICON_WIDTH;
    _userIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_user.png"]];
    _userIcon.alpha = 0.6;
    _userIcon.frame = CGRectMake(x, 15, _userIcon.image.size.width, _userIcon.image.size.height);
    [self.contentView addSubview:_userIcon];
}

- (void) setupGenreIcon {
    FAKFontAwesome *tagIcon = [FAKFontAwesome tagIconWithSize:16];
    [tagIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    tagIcon.iconFontSize = 11;

    UIImage *source = [tagIcon imageWithSize:CGSizeMake(16, 16)];
    UIImage *tagIconImage = [UIImage imageWithCGImage:source.CGImage scale:source.scale orientation:UIImageOrientationUpMirrored];

    CGFloat x = PLAYLISTS_PREVIEW_ICON_PADDING_LEFT*2 + PLAYLISTS_PREVIEW_ICON_WIDTH;

    _genreIcon = [[UIImageView alloc] initWithImage:tagIconImage];
    _genreIcon.alpha = 0.6;
    _genreIcon.frame = CGRectMake(x, _userIcon.frame.origin.y + _userIcon.frame.size.height + 5, _genreIcon.image.size.width, _genreIcon.image.size.height);
    [self.contentView addSubview:_genreIcon];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat x;
    if(IS_DEVICE == IS_IPAD) {
        x = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - _playCountIcon.image.size.width - 10;
    }else {
        x = [Utils screenWidth] - _playCountIcon.image.size.width - 10;
    }
    
    CGRect frame;
    frame = _playCountIcon.frame;
    frame.origin.x = x;
    
    _playCountIcon.frame = frame;
    
}
- (void)takeShotIn:(UIView *)snapshotImageView { }
@end

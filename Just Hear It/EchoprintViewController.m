//
//  EchoprintViewController.m
//  Just Hear It
//
//  Created by Aglitoiu Marius on 14/02/2018.
//  Copyright © 2018 HearIt Inc. All rights reserved.
//

#define kDestinationURL @"http://justhearit.com:8000/fileUpload.php"

#import "EchoprintViewController.h"

@interface EchoprintViewController ()

@end

@implementation EchoprintViewController;
@synthesize songname;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.songnamelbl setText:songname];

    
}
//##################################################################################################//
#pragma mark Record and Playback Methods
//##################################################################################################//

//##################################################################################################//
//##################################################################################################//

//##################################################################################################//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


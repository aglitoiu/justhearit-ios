#import "ModalBackgroundViewController.h"

@class TourViewController;

@protocol TourViewControllerDelegate <NSObject>

- (void) tourViewControllerDidFinish:(TourViewController*)controller;

@end

@interface TourViewController : ModalBackgroundViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (weak) id <TourViewControllerDelegate> delegate;
@property (strong, nonatomic) NSArray *pageDescriptions;
@property (strong, nonatomic) NSArray *pageImages;

- (void)dismissViewController:(id)sender;

@end
#import "DiscoverViewController.h"
#import "PlaylistSong.h"
#import "Bluuur.h"

@implementation DiscoverViewController

@synthesize delegate, song, rows, discoverTable, titleLabel;

enum DiscoverProperties {
    DiscoverPropertyPlaylistsWith = 1,
    DiscoverPropertySongsFrom = 2,
    DiscoverPropertySongsBy = 3,
    DiscoverPropertyVersionsOf = 4,
};

- (void)viewDidLoad {
    [super viewDidLoad];

    rows = [[NSMutableArray alloc] init];
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
    MLWBluuurView *beView = [[MLWBluuurView alloc] initWithEffect:blurEffect];
    beView.blurRadius=10;
    beView.frame = self.view.bounds;
    
   
    [self.view insertSubview:beView atIndex:0];
    
    if (song) {
        if ([song hasProperty:song.name])
            [self addRowWithProperty:DiscoverPropertyPlaylistsWith andLabel:[NSString stringWithFormat:@"Playlists with '%@'", song.name]];
        if ([song hasProperty:song.album])
            [self addRowWithProperty:DiscoverPropertySongsFrom andLabel:[NSString stringWithFormat:@"Songs from '%@'", song.album]];
        if ([song hasProperty:song.artist])
            [self addRowWithProperty:DiscoverPropertySongsBy andLabel:[NSString stringWithFormat:@"Songs by '%@'", song.artist]];
        if ([song hasProperty:song.name])
            [self addRowWithProperty:DiscoverPropertyVersionsOf andLabel:[NSString stringWithFormat:@"Versions of '%@'", song.name]];
    }
    if( IS_DEVICE == IS_IPAD ) {
        titleLabel.frame = CGRectMake(([Utils currentWidth] - titleLabel.frame.size.width) / 2, titleLabel.frame.origin.y, titleLabel.frame.size.width, titleLabel.frame.size.height);
    }
    else if([Utils IS_IPHONE_X])
    {

        CGRect frm=titleLabel.frame;
        frm.origin.y+=18;
        titleLabel.frame=frm;
    }
            
   
    
    //discoverTable.separatorColor = [UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1];

    discoverTable.separatorEffect=[UIVibrancyEffect effectForBlurEffect:blurEffect];
    [discoverTable reloadData];


}

- (void) addRowWithProperty:(NSInteger)property andLabel:(NSString *)label {
    [rows addObject:@{
            @"property": [NSNumber numberWithInt:(int)property],
            @"label": label
    }];
}

- (void)dismissViewController:(id)sender {
    
    [delegate discoverViewController:self didFinishSearchWithQuery:nil forPlaylists:NO];
}

#pragma mark -
#pragma mark TableView delegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    int prop = [[[rows objectAtIndex:indexPath.row] objectForKey:@"property"] intValue];
    NSString *query;
    BOOL forPlaylists = NO;

    if (prop == DiscoverPropertyPlaylistsWith) {
        query = song.name;
        forPlaylists = YES;
    } else if (prop == DiscoverPropertySongsFrom) {
        query = song.album;
    } else if (prop == DiscoverPropertySongsBy) {
        query = song.artist;
    } else if (prop == DiscoverPropertyVersionsOf) {
        query = song.name;
    }

    [delegate discoverViewController:self didFinishSearchWithQuery:query forPlaylists:forPlaylists];
}

#pragma mark -
#pragma mark TableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return rows.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"discoverCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    NSDictionary *cellProperty = [rows objectAtIndex:indexPath.row];

    cell.textLabel.text = [cellProperty objectForKey:@"label"];

    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.shouldIndentWhileEditing = NO;
    cell.showsReorderControl = NO;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    cell.textLabel.adjustsFontSizeToFitWidth = NO;

    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:RGBA(0, 0, 0, 0.4)];
    [cell setSelectedBackgroundView:bgColorView];
    
    //cell.layer.borderColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.1]CGColor];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;

}


@end

#import <Foundation/Foundation.h>

@interface Session : NSObject

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *fbAuthToken;
@property (nonatomic, strong) NSString *validToken;
@property (nonatomic, strong) NSString *fullName;

+ (Session *) initWithLogin:(NSString *)login andPassword:(NSString *)password;
+ (Session *) initWithRegisters:(NSString *)login andPassword:(NSString *)password;
+ (Session *) initWithfbAuthToken:(NSString *)fbAuthToken;

@end
#import "SearchViewController.h"
#import "LocalPlaylist.h"
#import "MasterViewController.h"
#import "PlaylistsViewController.h"
#import "LatestSearch.h"

@implementation SearchViewController

@synthesize textInput, closeButton, suggestionsTable, navigationBar, results,mainV, autocompleteCache, delegate,playerV;

- (void)viewDidLoad {
    [super viewDidLoad];
    results = [[NSMutableArray alloc] init];
    autocompleteCache = [[AutocompleteCache alloc] init];

    self.JAHorizontalSlide = YES;
    [textInput becomeFirstResponder];

    self.background.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.background addGestureRecognizer:tap];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    [textInput addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    if ([LatestSearch sharedInstance].query.length > 0) {
        textInput.text = [LatestSearch sharedInstance].query;
        [self loadAutocompleteResults];
    }
    if([Utils IS_IPHONE_X])
    {

        CGRect sgX=suggestionsTable.frame;
        sgX.origin.y+=20;
        suggestionsTable.frame=sgX;
    }
}
- (void)orientationChanged:(NSNotification *)notification{
    if(IS_DEVICE==IS_IPAD)
    {
      [delegate searchViewControllerDidFinish:self];
    }
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    
    navigationBar.logoView.center = CGPointMake([Utils currentWidth]/2, navigationBar.logoView.center.y);
    [mainV repositionOnRotation:[Utils currentWidth] :[Utils currentHeight]];
    
    
    //[self.sidePanelController styleContainer:self.sidePanelController.centerPanel.view animate:NO duration:0.0f];
    [playerV repositionOnRotation:[Utils currentWidth] :[Utils currentHeight]];
    
    if([[Player sharedInstance] currentPlayingSong]) {
        //[playerView closeView];
        [playerV setState:STATE_CLOSED animated:YES];
    }else {
        //[playerView hideView];
        [playerV setState:STATE_HIDDEN animated:YES];
    }
}
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [mainV repositionBackground];
}

- (void)textFieldDidChange:(NSNotification *)notification {
    [self loadAutocompleteResults];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self keyboardDidShow:notification];
}

- (void)keyboardDidShow:(NSNotification *)notification {
    if(IS_DEVICE != IS_IPAD) {
        CGRect keyboardFrame = [[notification userInfo][UIKeyboardFrameEndUserInfoKey] CGRectValue];
        CGRect suggestionsFrame = suggestionsTable.frame;
        suggestionsFrame.size.height = keyboardFrame.origin.y - suggestionsFrame.origin.y;
        suggestionsTable.frame = suggestionsFrame;
    }
}

- (void) dismissKeyboard:(UITapGestureRecognizer *)gesture {
    [textInput resignFirstResponder];
}

- (void) loadAutocompleteResults {
    if (textInput.text.length == 0) {
        [self clearResults];
    } else {
        [autocompleteCache autocomplete:textInput.text withSuccessBlock:^(Autocomplete *autocomplete) {
            [results removeAllObjects];
            [results addObjectsFromArray:_.head(autocomplete.suggestions, 5)];
            [suggestionsTable reloadData];
        } withErrorBlock:^(NSError *error) {
            [results removeAllObjects];
            [suggestionsTable reloadData];
        }];
    }
}

- (void)centerViewControllerMovedXOffset:(CGFloat)offset {
    [super centerViewControllerMovedXOffset:offset];
    if (offset <= 0) {
        [textInput becomeFirstResponder];
    } else {
        [textInput resignFirstResponder];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textInput resignFirstResponder];
    [self returnToMainViewControllerWithQuery:textField.text];
    return YES;
}

- (void)returnToMainViewControllerWithSearchPlaylists {
    //TODO: refactor this, it looks alot like the other one below
    MainViewController* mainViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
    __weak typeof(self) weakSelf = self;
    if (textInput.text.length > 0) {
        [LatestSearch sharedInstance].query = textInput.text;
        id playlist = [LocalPlaylist localSearchPlaylistsWithQuery:textInput.text]; //diff
        playlist = [[Playlists sharedInstance] addLocalPlaylist:playlist];
        [[Playlists sharedInstance] openPlaylist:playlist];
        
        if(IS_DEVICE == IS_IPAD) {
            
            [self playlistsChanged];
            [self openAfterSearch:mainViewController andPlaylist:playlist];
            [weakSelf.self dismissViewController:self];
            
        }else {
            
            [self playlistsChanged];
            [self openAfterSearch:mainViewController andPlaylist:playlist];
            [weakSelf.self dismissViewController:self];
            
            /*[(MasterViewController*)self.sidePanelController playlistsChanged];
            self.sidePanelController.delegate = mainViewController;
            [self.sidePanelController setCenterPanel:mainViewController];
            [mainViewController loadPlaylistSongs:playlist fromScratch:YES];*/
            
        }
    } else {
        
        if(IS_DEVICE == IS_IPAD) {
            
            [weakSelf.self dismissViewController:self];
            
        }else {
            
           // self.sidePanelController.delegate = mainViewController;
           // [self.sidePanelController setCenterPanel:mainViewController];
            [weakSelf.self dismissViewController:self];
        }
        
    }


}

- (void)returnToMainViewControllerWithQuery:(NSString *)query {
    MainViewController* mainViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
    __weak typeof(self) weakSelf = self;
    
    if (query && query.length > 0) {
        
        //Log(@"should show search");
        
        [LatestSearch sharedInstance].query = query;
        id playlist = [LocalPlaylist localSearchPlaylistWithQuery:query];
        playlist = [[Playlists sharedInstance] addLocalPlaylist:playlist];
        [[Playlists sharedInstance] openPlaylist:playlist];
        
        if(IS_DEVICE == IS_IPAD) {
            
            [self playlistsChanged];
            [self openAfterSearch:mainViewController andPlaylist:playlist];
            [weakSelf.self dismissViewController:self];
            
        }else {
            [self playlistsChanged];
            [self openAfterSearch:mainViewController andPlaylist:playlist];
            [weakSelf.self dismissViewController:self];
            /*[(MasterViewController*)self.sidePanelController playlistsChanged];
            
            self.sidePanelController.delegate = mainViewController;
            [self.sidePanelController setCenterPanel:mainViewController];
            
            [mainViewController loadPlaylistSongs:playlist fromScratch:YES];*/
        }
        
    } else {
        
        if(IS_DEVICE == IS_IPAD) {
            
            [weakSelf.self dismissViewController:self];
            
        }else {
            [weakSelf.self dismissViewController:self];
            /*self.sidePanelController.delegate = mainViewController;
            [self.sidePanelController setCenterPanel:mainViewController];*/
            
            //[self.sidePanelController showCenterPanelAnimated:NO];
            
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.shouldHideSpinner object:nil userInfo:nil];
    }
}

- (void) clearResults {
    [results removeAllObjects];
    [suggestionsTable reloadData];
}

- (IBAction)closeAction {
    if (textInput.text.length > 0) {
        textInput.text = @"";
        [self clearResults];
    } else {
        [self returnToMainViewControllerWithQuery:nil];
    }
}

- (void)dismissViewController:(id)sender {
    [delegate searchViewControllerDidFinish:self];
}

- (void)playlistsChanged {
    [delegate playlistsChanged];
}

- (void)openAfterSearch:(MainViewController*)mainVC andPlaylist:(id)playlist{
    [delegate openAfterSearch:mainVC andPlaylist:playlist];
}
#pragma mark -
#pragma mark UITableView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [textInput resignFirstResponder];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (results.count > 0 && results.count == indexPath.row) {
        //we hit the cell after the results (playlists with)
        [self returnToMainViewControllerWithSearchPlaylists];
    } else {
        //we hit one of the results
        [self returnToMainViewControllerWithQuery:results[indexPath.row]];
    }
}

#pragma mark -
#pragma mark UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (results.count > 0) {
        return results.count + 1;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"suggestionCell";
    SuggestionCell *cell = (SuggestionCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];


    if (results.count == indexPath.row) {
        [cell.icon setImage:[UIImage imageNamed:@"icon_normal_playlist.png"]];
        cell.label.text = [NSString stringWithFormat:@"Playlists with '%@'", textInput.text];
    } else {
        [cell.icon setImage:nil];
        cell.label.text = results[indexPath.row];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.shouldIndentWhileEditing = NO;
    cell.showsReorderControl = NO;

    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:RGBA(0, 0, 0, 0.4)];
    [cell setSelectedBackgroundView:bgColorView];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}

@end

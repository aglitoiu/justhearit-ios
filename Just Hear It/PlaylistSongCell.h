#import <Foundation/Foundation.h>
#import "SongCell.h"

@interface PlaylistSongCell : SongCell

@property (nonatomic, strong) UILabel *artistAndSongNameLabel;
@property (nonatomic, strong) UILabel *timestamp;
@property (nonatomic, strong) UIImageView *playlistIcon;
@property (nonatomic, strong) UILabel *playlistName;

@end

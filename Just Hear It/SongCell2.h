#import "FMMoveTableViewCell.h"

@class StandardCellBorder;
@class SongCell2;
@class PlaylistSong;

@protocol SongCellSlideDelegate <NSObject>

- (void) cellDidSelectShare:(SongCell2 *)cell;
- (void) cellDidSelectAddTo:(SongCell2 *)cell;
- (void) cellDidSelectDiscover:(SongCell2 *)cell;
- (void) cellDidSelectRemove:(SongCell2 *)cell;
- (void) cellStartedRevealing:(SongCell2 *)cell;
- (void) cellWasTapped:(SongCell2 *)cell;

@end

extern NSString *const SongCellEnclosingTableViewDidBeginScrollingNotification;

@interface SongCell2 : UITableViewCell <FMMoveTableViewCell>

@property (nonatomic, weak) UIScrollView *scrollViewSelf;

@property (nonatomic, strong) IBOutlet UILabel* artistLabel;
@property (nonatomic, strong) IBOutlet UILabel* songNameLabel;

@property (nonatomic, strong) PlaylistSong *song;

@property (nonatomic, strong) UIView *frontView;

@property (nonatomic, strong) StandardCellBorder *cellBorder;
@property (nonatomic, strong) UIImageView *icon;

@property (nonatomic, strong) UIView *slideButtonsView;

@property(nonatomic, strong) UIButton *shareButton;
@property(nonatomic, strong) UIButton *addToButton;
@property(nonatomic, strong) UIButton *discoverButton;
@property(nonatomic, strong) UIButton *removeButton;

@property(nonatomic, assign) BOOL canShare;
@property(nonatomic, assign) BOOL canAddTo;
@property(nonatomic, assign) BOOL canDiscover;
@property(nonatomic, assign) BOOL canRemove;


@property (nonatomic, weak) id<SongCellSlideDelegate> slideDelegate;

- (void)setOnlineSongInteractions;

- (void)setOfflineSongInteractions;

- (void)enclosingTableViewDidScroll;

- (void) prepareForMove;

- (void) markSelected:(BOOL)state;
- (void) markPlaying:(BOOL)state;
- (void) addIcon:(UIImage *) iconImage;

- (void)removeIcon;

- (void)setUnseen:(BOOL)unseen;
@end

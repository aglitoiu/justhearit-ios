#import "PullableView.h"
#import "ControlsView.h"
#import "Player.h"
#import "SongCell.h"


@class FrostyControlsView;

@interface PlayerView : PullableView <SongCellSlideDelegate, UITableViewDelegate, UITableViewDataSource, PlayerQueueDelegate>

@property(nonatomic, strong) IBOutlet UIImageView *background;

@property(nonatomic, strong) IBOutlet UITableView *queueTable;
@property(nonatomic, strong) FrostyControlsView *frostyControlsView;

@property(nonatomic, strong) NSMutableArray *nextSongsInPlaylist;

- (void)initialize;

- (void)initPositions:(float)width :(float)height;

- (void)repositionOnRotation:(float)width :(float)height;

- (void)offsetBackground:(CGFloat)offset;

- (void)songStartedDraggingWithGesture:(UILongPressGestureRecognizer *)recognizer;

- (void)songDraggingWithGesture:(UILongPressGestureRecognizer *)recognizer andContainer:(UIView *)container;

- (void)songStoppedDraggingWithGesture:(UILongPressGestureRecognizer *)recognizer;

@end

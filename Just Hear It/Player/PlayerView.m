#import "PlayerView.h"
#import "SongCell.h"
#import "ControlsView.h"
#import "FrostyControlsView.h"
#import "QueueHeader.h"
#import "PlaylistSong.h"
#import "StandardCellBorder.h"
#import "NSArray+OCTotallyLazy.h"
#import "MainViewController.h"
#import <AudioToolbox/AudioToolbox.h>
@implementation PlayerView

@synthesize background, frostyControlsView, handleView, queueTable, nextSongsInPlaylist;

- (void)initialize {
    
    handleView = [[HandleView alloc] init];
    
    [self addSubview:self.handleView];
    if (IS_DEVICE==IS_IPHONE) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
    [self initDragWithHeaderHeight:FROSTY_PLAYER_HEIGHT_X];
        }
        else
        {
     [self initDragWithHeaderHeight:FROSTY_PLAYER_HEIGHT];
        }
    }
    else{
     [self initDragWithHeaderHeight:FROSTY_PLAYER_HEIGHT];
    }
    self.clipsToBounds = YES;
    
    [[Player sharedInstance] setQueueDelegate:self];
    
    nextSongsInPlaylist = [[Player sharedInstance] nextSongsInPlaylist];
    
    //setup controls view
    frostyControlsView = [[FrostyControlsView alloc] init];
    [frostyControlsView setup];
    [handleView addSubview:frostyControlsView];
    
    //setup queueTable
    queueTable.frame = CGRectMake(0, frostyControlsView.frame.size.height, [Utils currentWidth], self.frame.size.height - frostyControlsView.frame.size.height);
    [queueTable reloadData];
   
}
-(void) reld{[queueTable reloadData];}
- (void) initPositions:(float)width :(float)height {
    
    self.handleView.frame = CGRectMake(0, 0, width, headerHeight);
    openedCenter = CGPointMake(width/2,  height/2);
    if (IS_DEVICE==IS_IPHONE) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
    closedCenter = CGPointMake(width/2, height + height/2 - PLAYER_DRAWER_HEIGHT_X);
        }
        else
        {
     closedCenter = CGPointMake(width/2, height + height/2 - PLAYER_DRAWER_HEIGHT);
        }
    }
    else{
         closedCenter = CGPointMake(width/2, height + height/2 - PLAYER_DRAWER_HEIGHT);
    }
        
    
    if(IS_DEVICE == IS_IPAD && [Utils isLandscape]) {
        hiddenCenter = CGPointMake(width/2 - 134.0f, height + height/2 + 2*64.0f);
    }else {
        hiddenCenter = CGPointMake(width/2, height + height/2);
    }
    
}

- (void) offsetBackground:(CGFloat) offset {
    
    CGRect frame = background.frame;
    frame.origin.y = - offset;
    background.frame = frame;
    
}

- (void)repositionOnRotation:(float)width :(float)height {
    [frostyControlsView reposition];
    //[self initPositions:[Utils currentWidth] :[Utils currentHeight]];
    
    self.handleView.frame = CGRectMake(0, 0, width, headerHeight);
    openedCenter = CGPointMake(width/2,  height/2);
    closedCenter = CGPointMake(width/2, height + height/2 - PLAYER_DRAWER_HEIGHT);
    
    if(IS_DEVICE == IS_IPAD && [Utils isLandscape]) {
        hiddenCenter = CGPointMake(width/2 - 134.0f, height + height/2 + 2*64.0f);
    }else {
        hiddenCenter = CGPointMake(width/2, height + height/2);
    }
    
    
    //[self hideView];
}
#pragma mark -
#pragma mark SongCell Delegate

- (void)cellDidSelectAddTo:(SongCell *)cell {
    [(MainViewController *)delegate cellDidSelectAddTo:cell];
}

- (void)cellDidSelectDiscover:(SongCell *)cell {
    [(MainViewController *)delegate cellDidSelectDiscover:cell];
}

- (void)cellDidSelectShare:(SongCell *)cell {
    [(MainViewController *)delegate cellDidSelectShare:cell];
}

- (void)cellDidSelectRemove:(SongCell *)cell {
    
    if ([[Player sharedInstance] hasUpNextSongs]) {
        NSIndexPath *indexPath = [queueTable indexPathForCell:cell];
        
        if (indexPath.section == 0) {
            [[[Player sharedInstance] upNextQueue] removeObject:cell.song];
            [queueTable beginUpdates];
            [queueTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [queueTable endUpdates];
        }
    }
}

- (void)cellStartedRevealing:(SongCell *)cell {
    [queueTable.visibleCells foreach:^(SongCell *vCell) {
        if (vCell != cell) {
            if([vCell respondsToSelector:@selector(enclosingTableViewDidScroll)]) {
                [vCell enclosingTableViewDidScroll];
            }
        }
    }];
}

- (void)cellWasTapped:(SongCell *)cell {
    if(cell.scrollViewSelf.contentOffset.x == 0) {
        NSIndexPath *indexPath = [queueTable indexPathForCell:cell];
        [self tableView:queueTable didSelectRowAtIndexPath:indexPath];
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [[NSNotificationCenter defaultCenter] postNotificationName:SongCellEnclosingTableViewDidBeginScrollingNotification object:scrollView];
}

#pragma mark -
#pragma mark QueueTable delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(!queueTable.isDecelerating) {
        
        if ([[Player sharedInstance] hasUpNextSongs]) {
            if (indexPath.section == 0) {
                [[Player sharedInstance] playSong:[[Player sharedInstance] upNextQueue][indexPath.row] fromPlaylist:nil];
            } else if (indexPath.section == 1) {
                [[Player sharedInstance] playSong:nextSongsInPlaylist[indexPath.row] fromPlaylist:nil];
            }
        } else {
            [[Player sharedInstance] playSong:nextSongsInPlaylist[indexPath.row] fromPlaylist:nil];
        }
        
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SONG_CELL_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return QUEUE_HEADER_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if ([[Player sharedInstance] hasUpNextSongs]) {
        if (section == 0) return [QueueHeader initWithTitle:@"Up Next"];
        else if (section == 1) return [QueueHeader initWithTitle:[NSString stringWithFormat:@"Back to '%@'", [[[Player sharedInstance] currentPlayingPlaylist] name]]];
    }
    return [QueueHeader initWithTitle:@"Up Next"];
}

#pragma mark -
#pragma mark QueueTable datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([[Player sharedInstance] hasUpNextSongs]) return 2;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[Player sharedInstance] hasUpNextSongs]) {
        if (section == 0) return [[[Player sharedInstance] upNextQueue] count];
        else if (section == 1) return nextSongsInPlaylist.count;
    }
    CGRect frame = queueTable.frame;
    
    frame.size.width = [Utils currentWidth];
    
    queueTable.frame = frame;
    return nextSongsInPlaylist.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"songCell";
    SongCell *cell = (SongCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    PlaylistSong *playlistSong = nil;
    
    BOOL canRemove = YES;
    
    if ([[Player sharedInstance] hasUpNextSongs]){
        if (indexPath.section == 0) {
            playlistSong = [[Player sharedInstance] upNextQueue][indexPath.row];
        } else if (indexPath.section == 1) {
            playlistSong = nextSongsInPlaylist[indexPath.row];
            canRemove = NO;
        }
    } else {
        canRemove = NO;
        playlistSong = nextSongsInPlaylist[indexPath.row];
    }
    
    
    cell.artistLabel.text = playlistSong.artist;
    cell.songNameLabel.text = playlistSong.name;
    
    cell.cellBorder.borderBottom.alpha = 1;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.shouldIndentWhileEditing = NO;
    cell.showsReorderControl = NO;
    cell.song = playlistSong;
    cell.slideDelegate = self;
    
    
    if (playlistSong.playlist.isOfflinePlaylist) {
        [cell setOfflineSongInteractions];
    } else {
        [cell setOnlineSongInteractions];
        cell.canRemove = canRemove;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:RGBA(0, 0, 0, 0.4)];
    [cell setSelectedBackgroundView:bgColorView];
    
    [cell markPlaying:NO];
    [cell markSelected:NO];
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}

#pragma mark -
#pragma mark PlayerQueueDelegate

- (void)nextPlaylistSongsChanged:(NSArray *)songs {
    [queueTable reloadData];
}

- (void)songStartedDraggingWithGesture:(UILongPressGestureRecognizer *)recognizer{
    if([[[Player sharedInstance] upNextQueue] count] > 0) {
        for(UILabel *aView in frostyControlsView.dropToPlayNext.subviews){
            if([aView isKindOfClass:[UILabel class]]){
                aView.text = @"Drop here to queue";
            }
        }
        //frostyControlsView.dropToPlayNext;
    }else {
        for(UILabel *aView in frostyControlsView.dropToPlayNext.subviews){
            if([aView isKindOfClass:[UILabel class]]){
                aView.text = @"Drop here to play next";
            }
        }
    }
    [frostyControlsView showDropHereToPlayNext];
}

- (void)songDraggingWithGesture:(UILongPressGestureRecognizer *)recognizer andContainer:(UIView *)container{
    CGPoint touchPoint = [recognizer locationInView:frostyControlsView];
    if (CGRectContainsPoint(frostyControlsView.frame, touchPoint)) {
        [frostyControlsView highlightDropHereToPlayNext:YES];
        [UIView animateWithDuration:0.3 animations:^{
            container.alpha = 0.3;
        }];
    } else {
        [frostyControlsView highlightDropHereToPlayNext:NO];
        [UIView animateWithDuration:0.3 animations:^{
            container.alpha = 1;
        }];
    }
}

- (void)songStoppedDraggingWithGesture:(UILongPressGestureRecognizer *)recognizer {
    //VIBR
    UISelectionFeedbackGenerator *selfeedbacGenerator = [[UISelectionFeedbackGenerator alloc] init];
    [selfeedbacGenerator prepare];
    [selfeedbacGenerator selectionChanged];
    AudioServicesPlaySystemSound(1519);
    
    [frostyControlsView showControls];
}

@end

//
//  BackgroundViewController.h
//  Just Hear It
//
//  Created by andrei st on 10/17/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"

@interface BackgroundViewController : UIViewController <JASidePanelControllerDelegate>

@property (nonatomic, strong) UIImageView *background;
@property (nonatomic, assign) BOOL JAHorizontalSlide;

@end

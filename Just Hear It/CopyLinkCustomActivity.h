//
// Created by andrei st on 1/28/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface CopyLinkCustomActivity : UIActivity

@property(nonatomic, strong) NSString *link;

@end
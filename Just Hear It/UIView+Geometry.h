//
// Created by andreis on 10/9/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@interface UIView (Geometry)

- (void)paddingLeft:(int)left;

- (void)paddingRight:(int)right;

- (void)width:(int)w;

- (void)marginLeft:(int)left;

- (void)top:(int)top;


@end
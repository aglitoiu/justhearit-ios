//
// Created by andreis on 4/26/13.
//
// To change the template use AppCode | Preferences | File Templates.
//
#import <GoogleAnalytics-iOS-SDK/GAITracker.h>
#import <GoogleAnalytics-iOS-SDK/GAI.h>

#import <Foundation/Foundation.h>


@interface Analytics : NSObject

+ (void) event:(NSString *) action :(NSString *) label;
+ (void) event:(NSString *) action;

@end
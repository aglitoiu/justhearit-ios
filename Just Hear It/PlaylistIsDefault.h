//
//  PlaylistIsDefault.h
//  Just Hear It
//
//  Created by Goiceanu Ciprian on 14/12/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "Playlist.h"

@interface PlaylistIsDefault : Playlist

@property(nonatomic, strong) NSNumber *isDefault;

@end

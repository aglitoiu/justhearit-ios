#import <UIKit/UIKit.h>

@interface PlaylistPreviewFooterCell : UITableViewCell

@property (nonatomic, strong) UIImageView *userIcon;
@property (nonatomic, strong) UIImageView *songCountIcon;
@property (nonatomic, strong) UIImageView *playCountIcon;
@property (nonatomic, strong) UIImageView *genreIcon;

@property (nonatomic, strong) UILabel* songNameLabel;
@property (nonatomic, strong) UILabel* genreLabel;
@property (nonatomic, strong) UILabel *userLabel;
@property (nonatomic, strong) UILabel *songCountLabel;
@property (nonatomic, strong) UILabel *playCountLabel;

- (void)adjustPlayCount:(NSString *)playCount andSongCount:(NSString *)songCount;
@end

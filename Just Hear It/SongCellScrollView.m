#import "SongCellScrollView.h"
#import "Utils.h"

@implementation SongCellScrollView

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event {
    if (!self.dragging) {
        if([Utils isVersionLessThan_8_0]) {
            [self.superview.superview.superview touchesEnded: touches withEvent:event];
        }else {
            [self.superview.superview touchesEnded: touches withEvent:event];
        }
    } else {
        [super touchesEnded: touches withEvent: event];

    }
}

@end

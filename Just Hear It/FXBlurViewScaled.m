//
//  FXBlurViewScaled.m
//  Just Hear It
//
//  Created by andrei st on 10/17/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import "FXBlurViewScaled.h"

@interface FXBlurViewScaled ()

@property (nonatomic, strong) NSDate *lastUpdate;

@end

@implementation FXBlurViewScaled

- (UIImage *)snapshotOfSuperview:(UIView *)superview
{
    self.lastUpdate = [NSDate date];
    CGFloat scale = 0.5;
    if (self.iterations > 0 && ([UIScreen mainScreen].scale > 1 || self.contentMode == UIViewContentModeScaleAspectFill))
    {
        CGFloat blockSize = 12.0f/self.iterations;
        scale = blockSize/MAX(blockSize * 2, floor(self.blurRadius));
    }
    CGSize size = self.bounds.size;
    size.width = ceilf(size.width * scale) / scale;
    size.height = ceilf(size.height * scale) / scale;
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, [[UIScreen mainScreen]scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, -self.frame.origin.x, -self.frame.origin.y);
    CGContextScaleCTM(context, size.width / self.bounds.size.width, size.height / self.bounds.size.height);
    NSArray *hiddenViews = [self prepareSuperviewForSnapshot:superview];
    [superview.layer renderInContext:context];
    [self restoreSuperviewAfterSnapshot:hiddenViews];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshot;
}

- (NSArray *)prepareSuperviewForSnapshot:(UIView *)superview
{
    NSMutableArray *views = [NSMutableArray array];
    NSInteger index = [superview.subviews indexOfObject:self];
    if (index != NSNotFound)
    {
        for (NSUInteger i = index; i < [superview.subviews count]; i++)
        {
            UIView *view = superview.subviews[i];
            if (!view.hidden)
            {
                view.hidden = YES;
                [views addObject:view];
            }
        }
    }
    return views;
}

- (void)restoreSuperviewAfterSnapshot:(NSArray *)hiddenViews
{
    for (UIView *view in hiddenViews)
    {
        view.hidden = NO;
    }
}

@end

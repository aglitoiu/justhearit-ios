#import "Sequence.h"
#import "Spinner.h"

@implementation Spinner

@synthesize activityImageView, statusImage, parentView, offsetX;

+ (Spinner *)initWithParentView:(UIView*)view {
    return [Spinner initWithParentView:view withOffsetX:0];
}

+ (Spinner *)initWithParentView:(UIView *)view withOffsetX:(NSInteger) offsetX {
    Spinner *spinner = [[Spinner alloc] initWithParentView:view withOffsetX:offsetX];
    [view addSubview:spinner.activityImageView];
    [spinner show:NO];
    return spinner;
}

+ (Spinner *)initWithParentView:(UIView *)view withFixedWidth:(CGFloat)width withHeight:(CGFloat)height {
    Spinner *spinner = [[Spinner alloc] initWithParentView:view withFixedWidth:width withHeight:height];
    [view addSubview:spinner.activityImageView];
    [spinner show:NO];
    return spinner;
}

- (id) initWithParentView:(UIView *)view withFixedWidth:(CGFloat) width withHeight:(CGFloat) height {
    
    if (self=[super init]) {
        
        //self.offsetX = offsetX;
        
        parentView = view;
        
        Sequence *ary = sequence(@"01", @"02", @"03", @"04", @"05", @"06", @"07", @"08", @"09", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30", @"31", @"32", @"33", @"34", @"35", @"36", @"37",nil);
        
        NSArray *pngSequence = [[ary map:^(NSString *item){
            return [UIImage imageNamed:[NSString stringWithFormat:@"loader00%@.png", item]];
        }] asArray];
        
        
        statusImage = [UIImage imageNamed:@"loader0001.png"];
        activityImageView = [[UIImageView alloc] initWithImage:statusImage];
        
        
        activityImageView.animationImages = pngSequence;
        
        activityImageView.animationDuration = 0.8;
        
        activityImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        CGRect frame = parentView.frame;
        
        //make sure the frame isn't larger than our spinner, and also if the parent is smaller on a side, resize the images so they fit inside
        CGFloat min = MIN(parentView.frame.size.width, parentView.frame.size.height);
        min = MIN(min, statusImage.size.width);
        frame.size.width = min;
        frame.size.height = min;
        
        activityImageView.frame = CGRectMake(
                                             width/2 -frame.size.width/2,
                                             height/2-frame.size.height/2,
                                             frame.size.width,
                                             frame.size.height);
        
        //activityImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    }
    
    
    return self;
}
- (id) initWithParentView:(UIView *)view withOffsetX:(NSInteger) offsetX {
    if (self=[super init]) {

        self.offsetX = offsetX;

        parentView = view;

        Sequence *ary = sequence(@"01", @"02", @"03", @"04", @"05", @"06", @"07", @"08", @"09", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30", @"31", @"32", @"33", @"34", @"35", @"36", @"37",nil);

        NSArray *pngSequence = [[ary map:^(NSString *item){
            return [UIImage imageNamed:[NSString stringWithFormat:@"loader00%@.png", item]];
        }] asArray];


        statusImage = [UIImage imageNamed:@"loader0001.png"];
        activityImageView = [[UIImageView alloc] initWithImage:statusImage];


        activityImageView.animationImages = pngSequence;

        activityImageView.animationDuration = 0.8;

        activityImageView.contentMode = UIViewContentModeScaleAspectFit;

        CGRect frame = parentView.frame;

        //make sure the frame isn't larger than our spinner, and also if the parent is smaller on a side, resize the images so they fit inside
        CGFloat min = MIN(parentView.frame.size.width, parentView.frame.size.height);
        min = MIN(min, statusImage.size.width);
        frame.size.width = min;
        frame.size.height = min;

        activityImageView.frame = CGRectMake(
                parentView.frame.size.width/2 -frame.size.width/2 + offsetX,
                parentView.frame.size.height/2-frame.size.height/2,
                frame.size.width,
                frame.size.height);

        activityImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    }
    return self;
}

- (void) start {
    [activityImageView startAnimating];
    activityImageView.hidden = NO;
}

- (void) stop {
    [activityImageView stopAnimating];
    activityImageView.hidden = YES;
}

- (void) show:(BOOL)state {
    Log(@"spinner show: %d", state);
    if (parentView) {
        [parentView bringSubviewToFront:activityImageView];

        if (state) {
            [self start];
        } else {
            [self stop];
        }
    }
    
}

- (void) hide {
    //Log(@"spinner hide");
    if (parentView) {
        [parentView sendSubviewToBack:activityImageView];
        [self stop];
    }
}

@end

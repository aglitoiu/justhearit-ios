#import "NewPlaylistViewController.h"
#import "JusthearitClient.h"

@implementation NewPlaylistViewController

@synthesize delegate, playlistName, songs, renamePlaylist;

- (void)viewDidLoad {
    [super viewDidLoad];

    playlistName.delegate = self;
    playlistName.returnKeyType = UIReturnKeyDone;
    [playlistName becomeFirstResponder];

    self.background.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.background addGestureRecognizer:tap];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    if (renamePlaylist) playlistName.text = renamePlaylist.name;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(IS_DEVICE == IS_IPAD) {
        if([Utils isLandscape]) {
            
            CGFloat offsetX;
            
            offsetX = 50.0f;
            
           // playlistName.frame = CGRectMake( ( [Utils currentWidth] - playlistName.frame.origin.x ) / 2, playlistName.frame.origin.y - offsetX, playlistName.frame.size.width, playlistName.frame.size.height);
        }
    }
}

- (void)dismissViewController:(id)sender {
   [self dismissViewControllerAnimated:YES completion:nil];
    //[delegate newPlaylistViewControllerDidFinish:self];
}
- (void)orientationChanged:(NSNotification *)notification{
    if(IS_DEVICE==IS_IPAD){
    //[delegate newPlaylistViewControllerDidFinish:self];
    }
}

- (void) dismissKeyboard:(UITapGestureRecognizer *)gesture {
    [playlistName resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    if (playlistName.text.length > 0) {

        if (renamePlaylist) {
            renamePlaylist.name = playlistName.text;
            [delegate newPlaylistViewControllerDidFinish:self withRenamePlaylist:renamePlaylist];
        } else {
            Playlist *playlist;
            playlist = [[Playlist alloc] init];
            playlist.name = playlistName.text;
            playlist.songs = [songs mutableCopy];
            [delegate newPlaylistViewControllerDidFinish:self withCreatePlaylist:playlist];
        }

    }
    return YES;
}

@end

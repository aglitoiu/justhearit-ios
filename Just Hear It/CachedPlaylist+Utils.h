//
// Created by andrei st on 1/16/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "CachedPlaylist.h"

@class CachedPlaylistSong;

#define OFFLINE_RECENTLY_PLAYED_ID @"offline_recently_played"

@interface CachedPlaylist (Utils)






+ (CachedPlaylist *)findByPlaylistId:(NSString *)playlistId;

+ (void)findOrCreateByPlaylist:(Playlist *)playlist withBlock:(void (^)(CachedPlaylist *))block;

+ (NSArray *)allAsPlaylists;

+ (void)removeFromCache:(CachedPlaylist *)playlist;




- (BOOL)songAlreadyInPlaylist:(CachedPlaylistSong *)cachedPlaylistSong;
@end

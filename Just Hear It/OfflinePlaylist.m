#import "OfflinePlaylist.h"
#import "Playlists.h"
#import "SongCache.h"


@implementation OfflinePlaylist

- (id) init {
    if(self=[super init]) {
        self.offlinePlaylist = YES;
    }
    return self;
}

+ (OfflinePlaylist *) offlineRecentlyPlayed {
    OfflinePlaylist *playlist = [[OfflinePlaylist alloc] init];
    playlist.playlistId = [NSString stringWithFormat:@"offline_recently_played"];
    playlist.name = @"Cached History";
    playlist.playlistType = @(playlistTypeRecentlyPlayed);
    playlist.offlinePlaylistType = offlinePlaylistRecentlyPlayed;
    playlist.playlistGroupObject = [[Playlists sharedInstance] defaultPlaylistsGroup];
    return playlist;
}

@end
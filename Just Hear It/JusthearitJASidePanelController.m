//
//  JusthearitJASidePanelController.m
//  Just Hear It
//
//  Created by andrei st on 10/11/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import "JusthearitJASidePanelController.h"

@interface JusthearitJASidePanelController ()

@end

@implementation JusthearitJASidePanelController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

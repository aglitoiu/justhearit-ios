#import "Playlist.h"
#import "PlaylistGroup.h"
#import "Pagination.h"
#import "PlaylistSong.h"
#import "SongCache.h"
#import "JusthearitClient.h"
#import "CachedPlaylist.h"
#import "CachedPlaylist+Utils.h"
#import "CachedLocalPlaylist.h"
#import "CachedLocalPlaylist+Utils.h"
#import "CachedPlaylistSong.h"
#import "CachedPlaylistSong+Utils.h"
#import "CachedLocalPlaylistSong.h"
#import "CachedLocalPlaylistSong+Utils.h"
#import "SongCacheDownloadManager.h"

@implementation Playlist

- (id) init {
    if(self=[super init]) {
        _pagination = [[Pagination alloc] init];
        _songs = [[NSMutableArray alloc] init];
        _localPlaylist = NO;
        _offlinePlaylist = NO;
        _playState = playStateStopped;
        _playlists = [[NSMutableArray alloc] init];
        _loaded = NO;
    }
    return self;
}

- (BOOL) isPlayingOrPaused {
    return _playState == playStatePlaying || _playState == playStatePaused;
}

- (BOOL) equalsToPlaylist:(Playlist *)playlist {
    return playlist && [playlist.playlistId isEqualToString:self.playlistId];
}

- (void) mergeWithPlaylist:(Playlist *)playlist {
    if (_pagination && playlist.pagination) {
        _pagination.totalPages = playlist.pagination.totalPages;
    }

    if (self.isLocalSearchPlaylistsWith) {
        [_playlists addObjectsFromArray:playlist.playlists];
    } else {
        
        if (_songs == nil) _songs = [[NSMutableArray alloc] init];
        
        [_songs addObjectsFromArray:playlist.songs];
        _.arrayEach(playlist.songs, ^(PlaylistSong *playlistSong){
            playlistSong.playlist = self;
        });
        
        _suggestions = playlist.suggestions;
    }
}

- (void)reset {
    _loaded = NO;
    [_pagination reset];
    [_songs removeAllObjects];
    [_playlists removeAllObjects];
}

- (void)setPlaylistGroupObject:(PlaylistGroup *)playlistGroup {
    _playlistGroupObject = playlistGroup;
    if (playlistGroup && (self.isLocalPlaylist || self.isOfflinePlaylist)) {
        _playlistGroup = playlistGroup.groupId;
    }
}

- (NSArray *)songIds {
    _songIds = _.arrayMap(_songs, ^(PlaylistSong *song) {
        return song.songId;
    });
    return _songIds;
}

- (BOOL) isMyUploads            { return _playlistType.intValue == playlistTypeMyUploads;}
- (BOOL) isNormalPlaylist       { return _playlistType.intValue == playlistTypeNormal; }
- (BOOL) isRecentlyPlayed       { return _playlistType.intValue == playlistTypeRecentlyPlayed; }
- (BOOL) isMusicFeed            { return _playlistType.intValue == playlistTypeMusicFeed; }

- (BOOL) isSharedPlaylist       { return _originalSharedPlaylistId != nil; }
- (BOOL) isSharedSearchPlaylist { return _playlistType.intValue == playlistTypeSearch; }

- (BOOL) isLocalPlaylist        { return _localPlaylist; }
- (BOOL) isLocalSearchPlaylist  { return self.isLocalPlaylist && _localPlaylistType == localPlaylistSearch; }
- (BOOL) isLocalSearchPlaylistsWith  { return self.isLocalPlaylist && _localPlaylistType == localPlaylistSearchPlaylists; }
- (BOOL) isLocalSharedPlaylist  { return self.isLocalPlaylist && _localPlaylistType == localPlaylistShared; }
- (BOOL) isSongTokenPlaylist    { return self.isLocalPlaylist && _localPlaylistType == localPlaylistSongToken; }

- (BOOL) isOfflinePlaylist { return _offlinePlaylist; }
- (BOOL) isOfflineRecentlyPlayedPlaylist { return self.isOfflinePlaylist && _offlinePlaylistType == offlinePlaylistRecentlyPlayed; }

- (BOOL) isSearchPlaylist { return self.isSharedSearchPlaylist || self.isLocalSearchPlaylist; }

- (BOOL) shouldShowEmptyResults { return self.showEmptyResults || self.showSuggestionResults; }
- (BOOL) showEmptyResults { return _loaded && self.isSearchPlaylist && !self.hasSongs && !self.hasSuggestions; }
- (BOOL) showSuggestionResults { return _loaded && self.isSearchPlaylist && !self.hasSongs && self.hasSuggestions; }

- (BOOL) hasSongs { return _songs.count > 0; }
- (BOOL) hasSuggestions { return _suggestions.count > 0; }

- (BOOL) isErasable { return _erasableContent.boolValue; }
- (BOOL) isSystem { return _system.boolValue; }

- (BOOL) isInCache {
    
    CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist findByPlaylistId:self.playlistId];
    
    if (existingPlaylist) {
        return YES;
    }else {
        return NO;
    }

}

- (int)isMyPlaylist { return _playlistType.intValue; }

- (BOOL) hasSongsInCache {
    
    CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist findByPlaylistId:self.playlistId];
    
    //Log(@"count songs in cache: %lu", (unsigned long)[existingPlaylist.cachedLocalPlaylistSongs count]);
    if([existingPlaylist.cachedLocalPlaylistSongs count] > 0) {
        return YES;
    }else {
        return NO;
    }
}

- (void)cache {

    if (_songs.count > 0) [self cacheSongs];
    else {
        Playlist *p = [[Playlist alloc] init];
        p.playlistId = self.playlistId;

        [[JusthearitClient sharedInstance] loadNextPageFromPlaylist:p withSuccessBlock:^(NSArray *array) {
            Playlist *loadedPlaylistWithSongs = _.first(array);
            self.songs = loadedPlaylistWithSongs.songs;
            [self cacheSongs];
        } withErrorBlock:^(NSError *error) {}];
    }
}

- (void)cacheNoDownload {
    
    if (_songs.count > 0) [self cacheSongsNoDownload];
    else {
        Playlist *p = [[Playlist alloc] init];
        p.playlistId = self.playlistId;
        
        
        
        [[JusthearitClient sharedInstance] loadNextPageFromPlaylist:p withSuccessBlock:^(NSArray *array) {
            Playlist *loadedPlaylistWithSongs = _.first(array);
            self.songs = loadedPlaylistWithSongs.songs;
            [self cacheSongsNoDownload];
        } withErrorBlock:^(NSError *error) {}];
    }
}

- (void)cacheSongs {
    [[SongCacheDownloadManager sharedInstance] clearQueue];

    [CachedPlaylist findOrCreateByPlaylist:self withBlock:^(CachedPlaylist *playlist) {
        _.arrayEach(_songs, ^(PlaylistSong*song){
            SongCache *cache = [SongCache initWithSong:song];
            [cache saveToCachedPlaylist:playlist];
        });
    }];

}

- (void) cacheSongsNoDownload {
    [CachedLocalPlaylist findOrCreateByPlaylist:self withBlock:^(CachedLocalPlaylist *playlist) {
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            //make your changes in the localContext
            CachedLocalPlaylist *localPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:playlist.playlistId inContext:localContext];
            
            //Log(@"self.pagination.totalPages: %@", self.pagination.totalPages);
            //Log(@"localPlaylist.totalPages: %@",localPlaylist.totalPages);
            
            if(localPlaylist) {
                
                localPlaylist.currentPage = self.pagination.currentPage;
                localPlaylist.totalPages = self.pagination.totalPages;
                
                Log(@"currentPage: %@", self.pagination.currentPage);
                Log(@"totalPages: %@", self.pagination.totalPages);
            }

        }];
        
           
        
        //Log(@"localPlaylist.totalPages: %@",localPlaylist.totalPages);
        
        __block NSNumber *order = [NSNumber numberWithInt:0];
        _.arrayEach(_songs, ^(PlaylistSong*song){
            
            SongCache *cache = [SongCache initWithSong:song];
            //Log(@"song position: %@", song.position);
           
            [cache saveToCachedPlaylistNoDownload:playlist andOrder:order];
            order = [NSNumber numberWithInt:(int)[order integerValue] + 1];
        });
        
    }];
}

- (void) cacheSongsNoDownloadMusicFeed {
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
   
    CachedLocalPlaylist *musicFeedPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistType" withValue:@"5" inContext:localContext];
    
    Log(@"music feed playlist id: %@", musicFeedPlaylist.playlistId);
    
    if([musicFeedPlaylist.cachedLocalPlaylistSongs count] > 0) {
    
        __block NSNumber *order = [NSNumber numberWithInt:0];
        
        _.arrayEach(_songs, ^(PlaylistSong*song){
            
            Log(@"song cache playlist id: %@", song.playlistId);
            
            song.playlistId = musicFeedPlaylist.playlistId;
            
            Log(@"song cache playlist id: %@", song.playlistId);
            
            SongCache *cache = [SongCache initWithSong:song];
            
            //Log(@"song cache playlist id: %@", cache.playlistId);
            
            [cache saveToCachedPlaylistNoDownload:musicFeedPlaylist andOrder:order];
            //order = [NSNumber numberWithInt:[order integerValue] + 1];
        });
    }
}

- (void) cacheSongsNoDownloadRecentlyPlayed:(PlaylistSong *)song {
    
    //Log(@"song to recent: %@", song);
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    CachedLocalPlaylist *recentlyPlayedPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistType" withValue:@"3" inContext:localContext];
    
    if(recentlyPlayedPlaylist && [recentlyPlayedPlaylist.cachedLocalPlaylistSongs count] > 0) {
        
        song.position = [NSNumber numberWithInt:0];
        
        SongCache *cache = [SongCache initWithSong:song];
        NSNumber *order = [NSNumber numberWithInt:0];
        
        NSArray *songs =  [recentlyPlayedPlaylist.cachedLocalPlaylistSongs allObjects];
        
        if([songs count]>0) {
            
            for (CachedLocalPlaylistSong *ssong in songs) {
                if([ssong.position intValue] == 69) {
                    [SongCache deleteSongFromCoreDataNoDownload:ssong];
                }
            }
            
        }
        
        for (CachedLocalPlaylistSong *song in [recentlyPlayedPlaylist.cachedLocalPlaylistSongs allObjects]) {
            
            song.position = [NSNumber numberWithInt:[song.position intValue]+1];
            [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
            
        }
        
        [cache saveToCachedPlaylistNoDownload:recentlyPlayedPlaylist andOrder:order];
        
    }
    
}

- (void) cacheSongsNoDownloadNormalPlaylist:(PlaylistSong *)song {
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    CachedLocalPlaylist *existingsPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:song.playlistId inContext:localContext];
    //Log(@"existing playlist name: %@", existingsPlaylist.name);
    if (existingsPlaylist) {
        NSNumber *order = [NSNumber numberWithInt:0];
        
        NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        NSArray *songsCached = [[existingsPlaylist.cachedLocalPlaylistSongs allObjects] sortedArrayUsingDescriptors:@[orderDescriptor]];
        
        PlaylistSong *lastSong = [songsCached lastObject];
        
        Log(@"last song in playlist: %@ %@", lastSong.name, lastSong.position);
        
        
        if(song.position == nil) {
            song.position = [NSNumber numberWithInt:[lastSong.position intValue] + 1];
        }
        
        SongCache *cache = [SongCache initWithSong:song];
        [cache saveToCachedPlaylistNoDownload:existingsPlaylist andOrder:order];
    }
    
}

- (void) cacheSongsNoDownloadSearchPlaylist:(PlaylistSong *)song {
    
    Log(@"song to recent: %@", song);
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    CachedLocalPlaylist *searchPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:song.playlistId inContext:localContext];
    
    if(searchPlaylist) {
        
        song.position = [NSNumber numberWithInt:0];
        
        SongCache *cache = [SongCache initWithSong:song];
        NSNumber *order = [NSNumber numberWithInt:0];
        
        NSArray *songs =  [searchPlaylist.cachedLocalPlaylistSongs allObjects];
        
        if([songs count]>0) {
            
            for (CachedLocalPlaylistSong *ssong in songs) {
                if([ssong.position intValue] == 69) {
                    [SongCache deleteSongFromCoreDataNoDownload:ssong];
                }
            }
            
        }
        
        for (CachedLocalPlaylistSong *song in [searchPlaylist.cachedLocalPlaylistSongs allObjects]) {
            
            song.order = [NSNumber numberWithInt:[song.order intValue]+1];
            [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
            
        }
        
        [cache saveToCachedPlaylistNoDownload:searchPlaylist andOrder:order];
        
    }
    
}

- (void) cacheSongsNoDownloadMyUploadsPlaylist:(PlaylistSong *)song {
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    CachedLocalPlaylist *existingsPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:song.playlistId inContext:localContext];
    //Log(@"existing playlist name: %@", existingsPlaylist.name);
    if (existingsPlaylist) {
        NSNumber *order = [NSNumber numberWithInt:0];
        
        NSSortDescriptor *orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        NSArray *songsCached = [[existingsPlaylist.cachedLocalPlaylistSongs allObjects] sortedArrayUsingDescriptors:@[orderDescriptor]];
        
        PlaylistSong *lastSong = [songsCached lastObject];
        
        if(song.position == nil) {
            song.position = [NSNumber numberWithInt:[lastSong.position intValue] + 1];
        }
        
        SongCache *cache = [SongCache initWithSong:song];
        [cache saveToCachedPlaylistNoDownload:existingsPlaylist andOrder:order];
        
    }
    
}

@end

//
//  BackgroundViewController.m
//  Just Hear It
//
//  Created by andrei st on 10/17/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import "BackgroundViewController.h"

@implementation BackgroundViewController

@synthesize background, JAHorizontalSlide;

- (void)viewDidLoad
{
    [super viewDidLoad];

    JAHorizontalSlide = NO;
    
	if(IS_DEVICE == IS_IPAD) {
        
        //background = [UIImageView imageNamed:@"background_landscape_ipad.png"];
        background = [UIImageView imageNamed:@"background.png"];
        background.frame = CGRectMake(0, 0, [Utils currentWidth], [Utils currentHeight]);
        
        if(![Utils isLandscape]) {
            background.autoresizingMask = UIViewAutoresizingAll;
        }
        
        
    }else {
        background = [UIImageView imageNamed:@"background.png"];
        background.frame = CGRectMake(0, 0, [Utils currentWidth], [Utils currentHeight]);
       background.autoresizingMask = UIViewAutoresizingAll;
    }
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];

    
    //background.autoresizingMask = UIViewAutoresizingAll;
    /*self.view.backgroundColor = [UIColor clearColor];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = self.view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.view addSubview:blurEffectView];*/
    
}


- (void)centerViewControllerMovedXOffset:(CGFloat)offset {
    if (JAHorizontalSlide) {
        CGRect frame = self.background.frame;
        frame.origin.x = -offset;
        self.background.frame = frame;
    }
}

@end

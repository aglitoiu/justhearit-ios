#import <UIKit/UIKit.h>

@interface PlaylistGroupEmptyCell : UITableViewCell

@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UIView *borderedView;
@property (nonatomic, strong) CAShapeLayer *border;

@end

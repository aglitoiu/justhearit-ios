#import <QuartzCore/QuartzCore.h>
#import "TransparentTextField.h"


@implementation TransparentTextField

@synthesize clearButton, leftSideIcon, paddingView, initialized, transparentTextFieldDelegate;

- (void)awakeFromNib {
    if (initialized != YES) [self initialize];
    [super awakeFromNib];
}

- (void)setLeftSideIconImage:(UIImage *)leftSideIconImage {
    if (initialized != YES) [self initialize];
    paddingView.frame = CGRectMake(0, 0, 40, 20);
    leftSideIcon.image = leftSideIconImage;
}

- (void)initialize {
    initialized = YES;
    self.keyboardAppearance = UIKeyboardAppearanceAlert;
    self.layer.borderColor = RGBA(255,255,255,0.2).CGColor;
    self.layer.cornerRadius = 4;
    self.layer.borderWidth = 1.0f;
    self.backgroundColor = RGBA(255,255,255,0.05);
    self.textColor = RGBA(255,255,255,0.7);
    self.font = [UIFont boldSystemFontOfSize:16];
    self.leftViewMode = UITextFieldViewModeAlways;
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: RGBA(255,255,255,0.7)}];

    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];

    leftSideIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 20, 20)];

    [paddingView addSubview:leftSideIcon];

    self.leftView = paddingView;

    UIImage *closeIcon = [UIImage imageNamed:@"close.png"];

    CGFloat closeIconWidth = closeIcon.size.width * [[UIScreen mainScreen] scale];
    CGFloat closeIconHeight = closeIcon.size.height * [[UIScreen mainScreen] scale];

    clearButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, closeIconWidth, closeIconHeight)];

    [clearButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [clearButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateHighlighted];
    [clearButton addTarget:self action:@selector(doClear:) forControlEvents:UIControlEventTouchUpInside];

    self.rightView = clearButton;
    self.rightViewMode = UITextFieldViewModeAlways;

    if (self.text.length == 0) {
        clearButton.hidden = YES;
    }

    [self addTarget:self action:@selector(handleClearButton:) forControlEvents:UIControlEventEditingChanged];
}

- (void) changeText:(NSString *)text {
    self.text = text;
    [self handleClearButton:nil];
}

- (void)doClear:(id)sender {
    clearButton.hidden = YES;
    [self becomeFirstResponder];
    self.text = @"";
    if ([self.transparentTextFieldDelegate respondsToSelector:@selector(transparentTextFieldTextCleared:)])
        [self.transparentTextFieldDelegate transparentTextFieldTextCleared:self];
}

- (void) handleClearButton:(id) sender {
    if (self.text.length > 0) {
        clearButton.hidden = NO;
    } else {
        clearButton.hidden = YES;
    }
}

@end

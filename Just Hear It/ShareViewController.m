#import "ShareViewController.h"

@implementation ShareViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)dismissViewController:(id)sender {
    [delegate shareViewControllerDidFinish:self];
}

@end

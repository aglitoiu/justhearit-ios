#import "CacheSliderView.h"
#import "FileManager.h"
@implementation CacheSliderView

@synthesize sizeLabel, freeSpace,usedSpace;

- (void) setup {

    freeSpace = (int)[Utils getFreeDiskspace];
    usedSpace =  [FileManager cacheFolderSize];
   

    [super setup];
    [self.slider addTarget:self action:@selector(sliderPositionChanged:) forControlEvents:UIControlEventValueChanged];
    [self.slider addTarget:self action:@selector(sliderEndDrag:) forControlEvents:UIControlEventTouchUpOutside];
    [self.slider addTarget:self action:@selector(sliderEndDrag:) forControlEvents:UIControlEventTouchUpInside];
    [self.slider addTarget:self action:@selector(sliderStartDrag:) forControlEvents:UIControlEventTouchDown];
    

    sizeLabel = [[UILabel alloc] init];
    sizeLabel.frame = CGRectMake(20, 0, 280, 20);
    sizeLabel.font = [UIFont boldSystemFontOfSize:14];
    sizeLabel.textColor = RGBA(255,255,255,.6);
    sizeLabel.textAlignment = NSTextAlignmentCenter;

    [self addSubview:sizeLabel];

    self.slider.frame = CGRectMake(10, 35, 300, 15);
    self.sliderProgress.frame = CGRectMake(10, 35, 300, 15);
    UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:nil action:nil];
    [self.slider addGestureRecognizer:panGesture];
    panGesture.cancelsTouchesInView = NO;

    
    CGRect LoadedFrame = self.loaded.frame;
    LoadedFrame.size.width -= 24;
    self.loaded.frame = LoadedFrame;
    self.loaded.center = self.slider.center;
    
  
    [self refreshSpaceUsed];
    [NSTimer scheduledTimerWithTimeInterval:6.0 target:self selector:@selector(refreshSpaceUsed) userInfo:nil repeats:YES];
    
    self.slider.value = [SETTINGS.maxCacheGb floatValue]/freeSpace;

    [self updateCacheLimitTextWithSliderValue:self.slider.value];
}
- (void) sliderStartDrag:(id)sender{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"sliderStartdrag"
     object:self];
}
-(void) setFramesWidth:(CGFloat)width
{
    CGRect frame=sizeLabel.frame;
    frame.size.width=width-frame.origin.x*2;
    sizeLabel.frame=frame;
    frame=self.slider.frame;
    frame.size.width=width-frame.origin.x*2;
    self.slider.frame=frame;
    frame=self.sliderProgress.frame;
    frame.size.width=width-frame.origin.x*2;
    self.sliderProgress.frame=frame;
    frame=self.loaded.frame;
    frame.size.width=width-frame.origin.x*2;
    self.loaded.frame=frame;
}

- (void) updateCacheLimitTextWithSliderValue:(float)sliderValue {
    float maxGb = sliderValue * freeSpace;


    sizeLabel.text = [NSString stringWithFormat:@"Cache Limit: %.02f GB (Used: %.02f GB)", maxGb, usedSpace/1024];
}

- (void)sliderPositionChanged:(id) sender {
    [self updateCacheLimitTextWithSliderValue:self.slider.value];

}

- (void)sliderEndDrag:(id) sender {
    SETTINGS.maxCacheGb = [NSNumber numberWithFloat:self.slider.value * freeSpace];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"sliderEnddrag"
     object:self];
    
}
- (void) refreshSpaceUsed{
    freeSpace = (int)[Utils getFreeDiskspace];
    usedSpace =  [FileManager cacheFolderSize];
    
    self.sliderProgress.value=usedSpace/1024/freeSpace;
    float maxGb = self.slider.value*freeSpace;


    
    sizeLabel.text = [NSString stringWithFormat:@"Cache Limit: %.02f GB (Used: %.02f GB)", maxGb, usedSpace/1024];
    
}

@end

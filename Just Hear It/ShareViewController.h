#import "ModalBackgroundViewController.h"

@class ShareViewController;

@protocol ShareViewControllerDelegate <NSObject>

- (void) shareViewControllerDidFinish:(ShareViewController*) controller;

@end

@interface ShareViewController : ModalBackgroundViewController

@property (weak) id <ShareViewControllerDelegate> delegate;

- (void)dismissViewController:(id)sender;

@end
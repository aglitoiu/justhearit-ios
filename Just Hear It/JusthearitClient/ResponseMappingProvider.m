#import "ResponseMappingProvider.h"
#import "Session.h"
#import "Playlist.h"
#import "PlaylistSong.h"
#import "Autocomplete.h"
#import "Pagination.h"
#import "PlaylistsPreviewList.h"
#import "User.h"
#import "PlaylistPreview.h"
#import "FayeConfig.h"
#import "Settings.h"


@implementation ResponseMappingProvider

+ (RKMapping*) sessionMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Session class]];
    [mapping addAttributeMappingsFromDictionary:
            @{
                    @"token": @"token",
                    @"status": @"status",
                    @"code": @"code",
                    @"message": @"message",
                    @"user_id": @"userId",
                    @"login": @"login",
                    @"password": @"password",
                    @"fb_auth_token": @"fbAuthToken",
                    @"valid": @"validToken",
                    @"full_name": @"fullName"
            }];
    return mapping;
}
+ (RKMapping*) registrationMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:nil];
    [mapping addAttributeMappingsFromDictionary:
     @{
       @"token": @"token",
       @"status": @"status",
       @"message": @"message",
       @"user_id": @"userId",
       @"login": @"login",
       @"password": @"password",
       @"fb_auth_token": @"fbAuthToken",
       @"valid": @"validToken",
       @"full_name": @"fullName"
       }];
    return mapping;
}
+ (RKMapping *) playlistMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Playlist class]];
    [mapping addAttributeMappingsFromDictionary:[ResponseMappingProvider playlistAttributes]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"playlist" toKeyPath:@"playlistData" withMapping:[ResponseMappingProvider simplePlaylistMapping]]];

    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"playlists" toKeyPath:@"playlists" withMapping:[ResponseMappingProvider playlistPreviewMapping]]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"songs" toKeyPath:@"songs" withMapping:[ResponseMappingProvider playlistSongMapping]]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pagination" toKeyPath:@"pagination" withMapping:[ResponseMappingProvider paginationMapping]]];
    return mapping;
}

+ (RKMapping *) playlistUpdatesMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Playlist class]];
    [mapping addAttributeMappingsFromDictionary:[ResponseMappingProvider playlistAttributes]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"playlist" toKeyPath:@"playlistData" withMapping:[ResponseMappingProvider simplePlaylistMapping]]];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"playlists" toKeyPath:@"playlists" withMapping:[ResponseMappingProvider playlistPreviewMapping]]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"songs" toKeyPath:@"songs" withMapping:[ResponseMappingProvider playlistSongUpdateMapping]]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pagination" toKeyPath:@"pagination" withMapping:[ResponseMappingProvider paginationMapping]]];
    return mapping;
}

+ (RKMapping *) simplePlaylistMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Playlist class]];
    [mapping addAttributeMappingsFromDictionary:[ResponseMappingProvider playlistAttributes]];
    return mapping;
}

+ (NSDictionary *) playlistAttributes {
    return @{
            @"id": @"playlistId",
            @"name": @"name",
            @"position": @"position",
            @"system": @"system",
            @"erasable_content": @"erasableContent",
            @"playlist_type": @"playlistType",
            @"playlist_group": @"playlistGroup",
            @"badge_count": @"badgeCount",
            @"original_shared_playlist_id": @"originalSharedPlaylistId",
            @"read_permission": @"readPermission",
            @"write_permission": @"writePermission",
            @"playlist_songs_count": @"playlistSongsCount",
            @"play_count": @"playCount",
            //@"default": @"isDefault",
            @"is_default": @"isDefault",
            @"suggestions": @"suggestions",
            @"share_url": @"shareUrl",
            @"user_id": @"userId",
            @"private": @"private",
            @"updated_at": @"updatedAt"
    };
}

+ (RKMapping *) playlistPreviewMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[PlaylistPreview class]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"playlist" toKeyPath:@"playlist" withMapping:[ResponseMappingProvider simplePlaylistMapping]]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"owner" toKeyPath:@"owner" withMapping:[ResponseMappingProvider userMapping]]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"songs" toKeyPath:@"songs" withMapping:[ResponseMappingProvider playlistSongMapping]]];
    return mapping;
}

+ (RKMapping *) userMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[User class]];
    [mapping addAttributeMappingsFromDictionary:
            @{
                    @"id": @"userId",
                    @"name": @"name",
                    @"first_name": @"firstName",
                    @"last_name": @"lastName",
                    @"to_label": @"toLabel"
            }];
    return mapping;
}

+ (RKMapping *) paginationMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Pagination class]];
    [mapping addAttributeMappingsFromDictionary:
            @{
                    @"per_page": @"perPage",
                    @"total_pages": @"totalPages",
                    @"current_page": @"currentPage"
            }];
    return mapping;
}

+ (RKMapping *) playlistSongMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[PlaylistSong class]];
    [mapping addAttributeMappingsFromDictionary:
            @{
                    @"id": @"songId",
                    @"name": @"name",
                    @"playlist_id": @"playlistId",
                    @"artist": @"artist",
                    @"album": @"album",
                    @"read_url": @"readUrl",
                    @"share_url": @"shareUrl",
                    @"encrypted_url": @"encryptedUrl",
                    @"url": @"url",
                    @"encrypted": @"encrypted",
                    @"token": @"token",
                    @"file_size": @"fileSize",
                    @"cover": @"cover",
                    @"thumb": @"thumb",
                    @"year": @"year",
                    @"genre": @"genre",
                    @"playlist_song_id": @"playlistSongId",
                    @"duration": @"duration",
                    @"created_at": @"createdAt",
                    @"playlist_song_update_id": @"playlistSongUpdateId",
                    @"new_playlist_song": @"isNewPlaylistSong",
                    @"position": @"position"
            }];

    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"playlist_origin" toKeyPath:@"playlistOrigin" withMapping:[ResponseMappingProvider simplePlaylistMapping]]];

    return mapping;
}
+ (RKMapping *) playlistSongUpdateMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[PlaylistSong class]];
    [mapping addAttributeMappingsFromDictionary:
     @{
       @"id": @"songId",
       @"name": @"name",
       @"playlist_id": @"playlistId",
       @"artist": @"artist",
       @"album": @"album",
       @"read_url": @"readUrl",
       @"share_url": @"shareUrl",
       @"encrypted_url": @"encryptedUrl",
       @"url": @"url",
       @"encrypted": @"encrypted",
       @"token": @"token",
       @"file_size": @"fileSize",
       @"cover": @"cover",
       @"thumb": @"thumb",
       @"year": @"year",
       @"genre": @"genre",
       @"playlist_song_id": @"playlistSongId",
       @"duration": @"duration",
       @"playlist_song_update_created_at": @"createdAt",
       @"playlist_song_update_id": @"playlistSongUpdateId",
       @"new_playlist_song": @"isNewPlaylistSong",
       @"position": @"position"
       }];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"playlist_origin" toKeyPath:@"playlistOrigin" withMapping:[ResponseMappingProvider simplePlaylistMapping]]];
    
    return mapping;
}
+ (RKMapping *) autocompleteMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Autocomplete class]];
    [mapping addAttributeMappingsFromDictionary:
            @{
                    @"query": @"query",
                    @"suggestions": @"suggestions"
            }];
    return mapping;
}
+ (RKMapping *) echoprintMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Autocomplete class]];
    [mapping addAttributeMappingsFromDictionary:
     @{
       @"name": @"name"
       }];
    return mapping;
}

+ (RKMapping *)fayeConfigMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[FayeConfig class]];
    [mapping addAttributeMappingsFromDictionary:
            @{
                    @"signature": @"signature",
                    @"timestamp": @"timestamp"
            }];
    return mapping;
}

+ (RKMapping *)settingsMapping {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Settings class]];
    [mapping addAttributeMappingsFromDictionary:
            @{
                    @"disable_ads": @"disableAds",
                    @"first_ad_after_minutes": @"firstAdAfterMinutes",
                    @"first_ad_length": @"firstAdLength",
                    @"normal_ads_length": @"normalAdsLength",
                    @"normal_ads_after_minutes": @"normalAdsAfterMinutes"
            }];
    return mapping;
}


@end

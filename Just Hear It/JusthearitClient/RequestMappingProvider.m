#import "RKRelationshipMapping.h"
#import "RequestMappingProvider.h"


@implementation RequestMappingProvider

+ (RKObjectMapping *)sessionsMapping {
    RKObjectMapping *classicLoginMapping = [RKObjectMapping requestMapping];
    [classicLoginMapping addAttributeMappingsFromDictionary:@{
            @"login": @"login",
            @"password": @"password",
            @"fbAuthToken": @"fb_auth_token"
    }];
    return classicLoginMapping;
}

+ (RKObjectMapping *)registrationMapping {
    RKObjectMapping *registrationMapping = [RKObjectMapping requestMapping];
    [registrationMapping addAttributeMappingsFromDictionary: @{
                                                               @"email": @"email",
                                                               @"password": @"password",
                                                               @"password_confirmation": @"password_confirmation",
                                                               @"full_name": @"full_name"
                                                               }];
    return registrationMapping;
}

+ (RKObjectMapping *)confirmationMapping {
    RKObjectMapping *confirmationMapping = [RKObjectMapping requestMapping];
    [confirmationMapping addAttributeMappingsFromDictionary: @{
                                                               @"email": @"email"
                                                               }];
    return confirmationMapping;
}

+ (RKObjectMapping *)searchMapping {
    RKObjectMapping *searchMapping = [RKObjectMapping requestMapping];
    [searchMapping addAttributeMappingsFromDictionary:@{
            @"query" : @"query",
            @"pagination.currentPage" : @"page",
    }];
    return searchMapping;
}

+ (RKObjectMapping *) searchPlaylistsMapping {
    return [RequestMappingProvider searchMapping];
}

+ (RKObjectMapping *) createPlaylistWithSongsMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"name" : @"name",
    }];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"songs" toKeyPath:@"playlist_songs_attributes" withMapping:[RequestMappingProvider playlistSongsAttributesMapping]]];
    return mapping;
}

+ (RKObjectMapping *)updatePlaylistMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"name" : @"name",
            @"private" : @"private"
    }];
    return mapping;
}

+ (RKObjectMapping *)updatePlaylistIsDefaultMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
          @"isDefault":@"is_default"
          }];
    return mapping;
}

/*+ (RKObjectMapping *)updatePlaylistIsDefaultMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
          @"name" : @"name",
          @"isDefault" : @"is_default"
          }];
    return mapping;
}*/

+ (RKObjectMapping *)addSongsToPlaylistMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"songIds": @"song_ids"
    }];
    return mapping;
}

+ (RKObjectMapping *)movePlaylistMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"position": @"position"
    }];
    return mapping;
}

+ (RKObjectMapping *)movePlaylistSongMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"position": @"position"
    }];
    return mapping;
}


+ (RKObjectMapping *) playlistSongsAttributesMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"songId": @"song_id"
    }];
    return mapping;
}

+ (RKObjectMapping *) songLogAttributesMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"playlistId": @"playlist_id",
            @"playlistSongId": @"playlist_song_id",
            @"playlistSongUpdateId": @"playlist_song_update_id"
    }];
    return mapping;
}

+ (RKObjectMapping *) userAttributesMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"fullName": @"full_name"
    }];
    return mapping;
}

+ (RKObjectMapping *) songTokenPlaylistAttributesMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
            @"token": @"token"
    }];
    return mapping;
}
+ (RKObjectMapping *) echoprintMapping{
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"code": @"code"
                                                  }];
    return mapping;
}

/*+ (RKObjectMapping *) playlistAttributesMapping {
    RKObjectMapping *mapping = [RKObjectMapping requestMapping];
    [mapping addAttributeMappingsFromDictionary:@{
          @"playlistId": @"playlist_id"
          }];
    return mapping;
}*/

@end

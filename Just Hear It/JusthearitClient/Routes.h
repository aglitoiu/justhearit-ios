#import <Foundation/Foundation.h>
#import "RestKit.h"

#define SESSIONS_PATH @"sessions.json"
#define SESSIONS_PATH_NAME @"sessions"

#define REGISTRATIONS_PATH @"registrations.json"
#define REGISTRATIONS_PATH_NAME @"registrations"

#define CONFIRMATIONS_PATH @"confirmations.json"
#define CONFIRMATIONS_PATH_NAME @"confirmations"
//aici echoprintu
#define ECHOPRINT_PATH @"echoprint.json"
#define ECHOPRINT_PATH_NAME @"echoprint"

#define VALID_TOKEN_PATH @"sessions/valid_token.json"
#define VALID_TOKEN_PATH_NAME @"validToken"

#define FAYE_CONFIG_PATH @"sessions/faye_config.json"
#define FAYE_CONFIG_PATH_NAME @"fayeConfig"

#define PLAYLISTS_PATH @"playlists.json"
#define PLAYLISTS_PATH_NAME @"playlists"

#define PASSWORDS_PATH @"passwords.json"
#define PASSWORDS_PATH_NAME @"passwords"

#define USER_EDIT_PATH @"users/edit.json"
#define USER_EDIT_PATH_NAME @"userEditPath"

#define PLAYLIST_PATH @"playlists/:playlistId\\.json"
#define PLAYLIST_PATH_NAME @"playlistPath"

#define SONG_LOG_PATH @"songs/:token/log.json"
#define SONG_LOG_PATH_NAME @"songLogPath"

#define SHARED_PLAYLISTS_PATH @"shared_playlists.json"
#define SHARED_PLAYLISTS_PATH_NAME @"sharedPlaylists"

#define PLAYLISTS_SONGS_PATH @"playlists/:playlistId/songs.json"
#define PLAYLISTS_SONGS_PATH_NAME @"playlistSongs"

#define PLAYLISTS_SONGS_UPDATES_PATH @"playlists/:playlistId/songs/index_for_updates.json"
#define PLAYLISTS_SONGS_UPDATES_PATH_NAME @"playlistSongsUpdates"

#define PLAYLISTS_ADD_SONGS_PATH @"playlists/:playlistId/playlist_songs/add.json"
#define PLAYLISTS_ADD_SONGS_PATH_NAME @"playlistsAddSongs"

#define PLAYLIST_MOVE_PATH @"playlists/:playlistId/move.json"
#define PLAYLIST_MOVE_PATH_NAME @"playlistsMove"

#define PLAYLIST_SONG_PATH @"playlists/:playlistId/playlist_songs/:playlistSongId\\.json"
#define PLAYLIST_SONG_PATH_NAME @"playlistSongPath"

#define PLAYLIST_SONG_MOVE_PATH @"playlists/:playlistId/playlist_songs/:playlistSongId/move.json"
#define PLAYLIST_SONG_MOVE_PATH_NAME @"playlistSongsMove"

#define SEARCH_PATH @"search.json"
#define SEARCH_PATH_NAME @"search"

#define AUTOCOMPLETE_PATH @"search/autocomplete.json"
#define AUTOCOMPLETE_PATH_NAME @"autocomplete"

#define SEARCH_PLAYLISTS_PATH @"search/playlists.json"
#define SEARCH_PLAYLISTS_PATH_NAME @"searchPlaylists"

#define SONG_TOKEN_PATH @"songs/:token"
#define SONG_TOKEN_PATH_NAME @"songToken"

#define SETTINGS_PATH @"settings.json"
#define SETTINGS_PATH_NAME @"settings"

@interface Routes : NSObject

+ (RKRoute *) sessionsPath;
+ (RKRoute *) registrationsPath;
+ (RKRoute *) playlistsPath;
+ (RKRoute *) updatePlaylistPath;
+ (RKRoute *) loadPlaylistPath;
+ (RKRoute *) playlistSongsPath;
+ (RKRoute *) playlistSongsUpdatesPath;
+ (RKRoute *) validTokenPath;
+ (RKRoute *) fayeConfigPath;
+ (RKRoute *) autocompletePath;
+ (RKRoute *) searchPath;
+ (RKRoute *) searchPlaylistsPath;
+ (RKRoute *) addSongsToPlaylistPath;
+ (RKRoute *) movePlaylistPath;
+ (RKRoute *) movePlaylistSongPath;
+ (RKRoute *) deletePlaylistPath;
+ (RKRoute *) deletePlaylistSongPath;
+ (RKRoute *) songLogPath;
+ (RKRoute *) passwordsPath;
+ (RKRoute *) updateUserPath;
+ (RKRoute *) getSongPath;
+ (RKRoute *) settingsPath;
+ (RKRoute *) loadPlaylistSongPath;

+ (RKRoute *) echoprintPath;

@end

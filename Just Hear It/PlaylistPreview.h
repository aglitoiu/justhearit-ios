#import <Foundation/Foundation.h>

@class Playlist;
@class User;

@interface PlaylistPreview : NSObject

@property(nonatomic, strong) Playlist *playlist;
@property(nonatomic, strong) User *owner;
@property(nonatomic, strong) NSMutableArray *songs;

@property(nonatomic, strong) NSArray *genreList;

- (NSArray *) genres;
- (NSString *) genresAsString;

@end
#import <Foundation/Foundation.h>

@interface Pagination : NSObject

@property(nonatomic, strong) NSNumber *currentPage;
@property(nonatomic, strong) NSNumber *totalPages;
@property(nonatomic, strong) NSNumber *perPage;

- (NSNumber *) nextPage;

- (BOOL) hasNextPage;

- (void) incrementCurrentPage;

- (void)reset;

- (void)restoreCurrentPage;

@end
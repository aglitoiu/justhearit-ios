#import <Foundation/Foundation.h>
#import "RestKit.h"

@class Pagination;
@class PlaylistGroup;
@class CachedPlaylist;
@class CachedLocalPlaylist;

@interface Playlist : NSObject

enum PlaylistTypes {
    playlistTypeNormal = 1,
    playlistTypeMyUploads = 2,
    playlistTypeRecentlyPlayed = 3,
    playlistTypeSearch = 4,
    playlistTypeMusicFeed = 5,
    playlistTypeShared = 6,

};

enum LocalPlaylistTypes {
    localPlaylistSearch = 1,
    localPlaylistSearchPlaylists = 2,
    localPlaylistShared = 3,
    localPlaylistSongToken = 4
};

enum OfflinePlaylistTypes {
    offlinePlaylistRecentlyPlayed = 1,
    offlinePlaylistTypeNormal = 2
};

enum PlaylistPlayStates {
    playStatePlaying = 1,
    playStatePaused = 2,
    playStateStopped = 3
};

enum PlaylistGroups {
    playlistGroupMyPlaylists = 1
};

@property(nonatomic, strong) NSString *playlistId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSDate *updatedAt;
@property(nonatomic, strong) NSNumber *position;
@property(nonatomic, strong) NSNumber *system;
@property(nonatomic, strong) NSNumber *erasableContent;
@property(nonatomic, strong) NSNumber *playlistType;
@property(nonatomic, strong) NSNumber *playlistGroup;
@property(nonatomic, strong) NSNumber *badgeCount;
@property(nonatomic, strong) NSString *originalSharedPlaylistId;
@property(nonatomic, strong) NSNumber *readPermission;
@property(nonatomic, strong) NSNumber *writePermission;
@property(nonatomic, strong) NSNumber *playlistSongsCount;
@property(nonatomic, strong) NSNumber *playCount;
@property(nonatomic, strong) NSNumber *isDefault;
@property(nonatomic, strong) NSMutableArray *songs;
@property(nonatomic, strong) Pagination *pagination;
@property(nonatomic, strong) PlaylistGroup *playlistGroupObject;
@property(nonatomic, strong) NSArray *suggestions;
@property(nonatomic, strong) NSMutableArray *playlists;
@property(nonatomic, strong) NSString *shareUrl;
@property(nonatomic, strong) NSNumber *userId;
@property(nonatomic, strong) NSNumber *private;
@property(nonatomic, strong) Playlist *playlistData;

@property(nonatomic, assign) BOOL localPlaylist;
@property(nonatomic, assign) NSInteger localPlaylistType;

@property(nonatomic, assign) BOOL offlinePlaylist;
@property(nonatomic, assign) NSInteger offlinePlaylistType;

@property(nonatomic, assign) NSInteger playState;
@property(nonatomic, assign) BOOL loaded;

@property(nonatomic, strong) NSArray *songIds;
@property(nonatomic, strong) CachedPlaylist *cachedPlaylist;

@property(nonatomic, strong) CachedLocalPlaylist *cachedLocalPlaylist;

@property(nonatomic, assign) BOOL hasNotification;
@property(nonatomic, assign) BOOL isDroppedInFollowing;
//@property(nonatomic, assign) BOOL blank;

//+ (Playlist *)blankPlaylist;

- (BOOL) isPlayingOrPaused;

- (BOOL) equalsToPlaylist:(Playlist *) playlist;

- (void) mergeWithPlaylist:(Playlist *) playlist;

- (void) reset;

- (BOOL) isMyUploads;

- (BOOL)isNormalPlaylist;

- (BOOL) isRecentlyPlayed;

- (BOOL) isMusicFeed;

- (BOOL) isSharedPlaylist;

- (BOOL) isSharedSearchPlaylist;

- (BOOL) isLocalPlaylist;

- (BOOL) isLocalSearchPlaylist;

- (BOOL)isLocalSearchPlaylistsWith;

- (BOOL)isLocalSharedPlaylist;

- (int)isMyPlaylist;

- (BOOL)isOfflinePlaylist;

- (BOOL)isOfflineRecentlyPlayedPlaylist;

//if you need to show the no results screen
- (BOOL)shouldShowEmptyResults;

//if you need to show no results screen without did you mean
- (BOOL)showEmptyResults;

//if you need to show no results screen with suggestions
- (BOOL)showSuggestionResults;

- (BOOL)hasSongs;

- (BOOL)hasSuggestions;

- (BOOL)isErasable;
- (BOOL)isSystem;

- (void)cache;

- (BOOL)isSongTokenPlaylist;

- (BOOL) isInCache;

- (void) cacheNoDownload;

- (void) cacheSongsNoDownloadMusicFeed;
- (void) cacheSongsNoDownloadRecentlyPlayed:(PlaylistSong *)song;
- (void) cacheSongsNoDownloadNormalPlaylist:(PlaylistSong *)song;
- (void) cacheSongsNoDownloadSearchPlaylist:(PlaylistSong *)song;
- (void) cacheSongsNoDownloadMyUploadsPlaylist:(PlaylistSong *)song;

- (BOOL) hasSongsInCache;
@end

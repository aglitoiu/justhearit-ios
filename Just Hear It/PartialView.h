//
//  PartialView.h
//  Just Hear It
//
//  Created by Aglitoiu Marius on 2/8/18.
//  Copyright © 2018 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartialTransparentView : UIVisualEffectView {
    NSArray *rectsArray;
    UIColor *backgroundColor;
}

- (id)initWithFrame:(CGRect)frame effect:(UIVisualEffect*)effect backgroundColor:(UIColor*)color andTransparentRects:(NSArray*)rects;

@end

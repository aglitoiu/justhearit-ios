//
// Created by andrei st on 1/9/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import "FileManager.h"
#import "SongCache.h"
#import "CachedPlaylistSong.h"


@implementation FileManager

+ (void) createCacheFolder {
    NSString *dataPath = [FileManager cacheFolderPath];

    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil]; //Create folder
}

+ (NSString *) cacheFolderPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    return [documentsDirectory stringByAppendingPathComponent:CACHE_FOLDER];
}

+(NSArray *) filesInCacheFolder {
    NSString *folderPath = [FileManager cacheFolderPath];
    return [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
}

+(unsigned long long int) cacheFolderSize {
    NSString *folderPath = [FileManager cacheFolderPath];

    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;

    while (fileName = [filesEnumerator nextObject])
        fileSize += [FileManager fileSizeForPath:[folderPath stringByAppendingPathComponent:fileName]];

    return fileSize/1048576; //file size in MB
}

+ (unsigned long long int) fileSizeForPath:(NSString *)path {
    return [[[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil] fileSize];
}

+ (NSString *) filePathForSongId:(NSString *)songId {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.mp3", CACHE_FOLDER, songId]];
}

+ (NSString *) filePathForInstaPlaySongId:(NSString *)songId {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.mp3", INSTAPLAY_FOLDER, songId]];
}

+ (void) deleteFileAtPath:(NSString *)path {
    Log(@"deleting cached file at %@", path);

    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:path error:nil];
}

+ (void) createInstaPlayFolder {
    NSString *dataPath = [FileManager instaPlayFolderPath];

    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil]; //Create folder
}

+ (NSString *) instaPlayFolderPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    return [documentsDirectory stringByAppendingPathComponent:INSTAPLAY_FOLDER];
}



@end
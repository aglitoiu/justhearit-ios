#import "Pagination.h"

@implementation Pagination

- (id) init {
    if(self=[super init]) {
        [self reset];
    }
    return self;
}

- (NSNumber *) nextPage {
    return [NSNumber numberWithInt:_currentPage.intValue+1];
}

- (BOOL) hasNextPage {
    return _totalPages.intValue > _currentPage.intValue;
}

- (void) incrementCurrentPage {
    _currentPage = [NSNumber numberWithInt:_currentPage.intValue+1];
}


- (void)reset {
    _totalPages = @1;
    _currentPage = @0;
    _perPage = nil;
}

- (void)restoreCurrentPage {
    
    _currentPage = @1;
    
}
@end
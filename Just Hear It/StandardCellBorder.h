//
// Created by andreis on 10/26/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface StandardCellBorder : NSObject

@property (nonatomic, strong) UIImageView *borderBottom;
@property (nonatomic, strong) UIImageView *borderLeft;
@property (nonatomic, strong) UIImageView *selectedView;

- (void)initWithParent:(UIView *)parent;

- (void)markSelected:(BOOL)state;

- (void)markPlaying:(BOOL)state;


@end

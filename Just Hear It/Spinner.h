#import <Foundation/Foundation.h>

@interface Spinner : NSObject

@property (nonatomic, strong) UIImage *statusImage;
@property (nonatomic, strong) UIImageView *activityImageView;
@property (nonatomic, strong) UIView *parentView;
@property (nonatomic, assign) NSInteger offsetX;

+ (Spinner *)initWithParentView:(UIView *)view ;

+ (Spinner *)initWithParentView:(UIView *)view withOffsetX:(NSInteger)offsetX;

+ (Spinner *)initWithParentView:(UIView *)view withFixedWidth:(CGFloat)width withHeight:(CGFloat)height;

- (void)start;

- (void)stop;

- (void)show:(BOOL)state;
- (void)hide;
@end
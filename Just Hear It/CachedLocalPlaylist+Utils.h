//
// Created by andrei st on 1/16/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "CachedLocalPlaylist.h"

@class CachedPlaylistSong;

@interface CachedLocalPlaylist (Utils)

+ (CachedLocalPlaylist *)findByPlaylistId:(NSString *)playlistId;

+ (void)findOrCreateByPlaylist:(Playlist *)playlist withBlock:(void (^)(CachedLocalPlaylist *))block;

+ (NSArray *)allAsPlaylists;

+ (void)removeFromCache:(CachedLocalPlaylist *)playlist;

+ (void)removePL:(CachedLocalPlaylist *)playlist;

- (BOOL)songAlreadyInPlaylist:(CachedPlaylistSong *)cachedPlaylistSong;

- (void) refreshSongsInRecentlyPlayed:(CachedLocalPlaylist *)playlist;

- (void) markHasNew;

@end

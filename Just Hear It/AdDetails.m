#import "AdDetails.h"

@implementation AdDetails

+ (AdDetails *)prerollAdWithSeconds:(NSInteger)seconds {
    return [AdDetails adWithType:JusthearitAdTypePreroll andSeconds:seconds];
}

+ (AdDetails *)normalAdWithSeconds:(NSInteger)seconds {
    return [AdDetails adWithType:JusthearitAdTypeNormal andSeconds:seconds];
}

+ (AdDetails *)adWithType:(NSInteger)adType andSeconds:(NSInteger)seconds {
    AdDetails *ad = [[AdDetails alloc] init];
    ad.adType = adType;
    ad.seconds = seconds;
    return ad;
}

@end
#import <UIKit/UIKit.h>
#import "PullableView.h"
#import "PlayerView.h"
#import "JASidePanelController.h"
#import "FMMoveTableView.h"
#import "NavigationBarView.h"
#import "BackgroundViewController.h"
#import "Playlist.h"
#import "FMMoveTableViewCell.h"
#import "SongCell.h"
#import "StandardCellBorder.h"
#import "UIViewController+JASidePanel.h"
#import "JusthearitClient.h"
#import "PlaylistSong.h"
#import "Spinner.h"
#import "Playlists.h"
#import "Player.h"
#import "NewPlaylistViewController.h"
#import "ShareViewController.h"
#import "DiscoverViewController.h"
#import <AVFoundation/AVFoundation.h>

@class MainViewController;
@class MasterViewController;

@protocol MainViewControllerDelegate <NSObject>

- (void) testOpenPlayer:(int)row;

@end

@interface MainViewController : BackgroundViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate, NSURLSessionTaskDelegate, SongCellSlideDelegate, ShareViewControllerDelegate, DiscoverViewControllerDelegate, PullableViewDelegate, JASidePanelControllerDelegate, FMMoveTableViewDataSource, FMMoveTableViewDelegate>
    {
        AVAudioRecorder *audioRecorder;
        AVAudioPlayer *audioPlayer;
    }
@property (nonatomic, retain) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) NSUserDefaults *defaults;
@property (strong, retain) NSString *audioFilePath;

@property (nonatomic, strong) IBOutlet NavigationBarView *navigationBar;
@property (nonatomic, strong) IBOutlet UIButton *EchoprintBtn;
@property (nonatomic, strong)  UIFeedbackGenerator *fdbG;
@property (nonatomic, strong) IBOutlet PlayerView* playerView;
@property (nonatomic, strong) IBOutlet FMMoveTableView *songsTable;
@property (nonatomic, strong) IBOutlet UIButton *menuBtn;
@property (nonatomic, strong) IBOutlet UIView *welcomeView;
@property (nonatomic, strong) IBOutlet UILabel *welcomeTitle;
@property (nonatomic, strong) IBOutlet UILabel *welcomeMessage;
@property (nonatomic, strong) NSMutableArray* songList;
@property (nonatomic, strong) NSMutableArray* songListTmp;
@property (nonatomic, strong) NSMutableArray* playlistsList;
@property (nonatomic, strong) Spinner *spinner;
@property (nonatomic, strong) Spinner *spinnerFooter;
@property (nonatomic, strong) Playlists *playlists;
@property (nonatomic, assign) CGFloat navigationBarHeight;
@property (nonatomic, weak) MasterViewController *masterVC;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *OfflineBtn;
@property (nonatomic, assign) BOOL playerStarted;
@property (nonatomic, assign) BOOL isTableResized;
@property (nonatomic, assign) BOOL hasNewSongs;
@property (nonatomic, assign) BOOL hasUpdates;
@property (nonatomic, assign) CGFloat scrollOffsetY;
@property (nonatomic, strong) IBOutlet UIButton *nSongsButton;
@property (nonatomic, assign) CGRect topFrame;
@property (nonatomic, assign) NSTimer *timer;
@property (nonatomic, assign) BOOL isUpdating;
@property (nonatomic, assign) NSInteger countNewSongs;
@property (nonatomic, assign) int isOffline;

@property (weak) id <MainViewControllerDelegate> delegate;

@property(nonatomic, assign) BOOL playlistsDrawerIsOpenFromDragging;

@property (nonatomic, assign) BOOL isLoadingSongs;

+ (id)sharedInstance;

- (void) hidePlayer;

- (IBAction) openSidebar:(id) sender;
- (IBAction)openEchoprint:(id) sender;
- (IBAction) scrollToTop:(id) sender;

- (void) scrollToBottom;
- (BOOL) alreadyOffline;
- (void) loadPlaylistSongs:(Playlist *)playlist fromScratch:(BOOL)fromScratch;

- (void) loadPlaylistSongsFromUrlPath:(NSString *)path;

- (void) addToWithPlaylistSongs:(NSArray *)songs;

- (void) stopMusic;
-(void) openSide;
- (IBAction) closeWelcomeMsg:(id) sender;

- (void) setSongsTableFullScreen;

- (void) setSongsTablePlayerScreen;

- (void) spinnerHide:(NSNotification*)notification;

- (void) repositionOnRotation:(float)width :(float)height;

- (void) repositionBackground;

- (void) mhandlePlaylistNotification:(NSNotification *)notification;

- (void) showNewSongsButton;

- (void) hideNewSongsButton;

- (void) removeRowsWhenPlaylistDelete:(NSArray *)songs andPlaylist:(Playlist *)playlist;

@end

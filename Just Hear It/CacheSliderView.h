#import "JusthearitSlider.h"
#import "AKProgressView.h"
#define MAX_GB 25

@interface CacheSliderView : JusthearitSliderView

@property(nonatomic, strong) UILabel *sizeLabel;
@property(nonatomic, assign) int freeSpace;
@property(nonatomic, assign) float usedSpace;
-(void) setFramesWidth:(CGFloat)width;

@end

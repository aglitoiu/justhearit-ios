#import <Foundation/Foundation.h>
#import "Playlist.h"
@class Playlist;
@class PlaylistGroup;
@class LocalPlaylist;
@class PlaylistBadgeChangeNotification;

@interface Playlists : NSObject

+ (id)sharedInstance;

//the actual playlist collection
@property (nonatomic, strong) NSMutableArray *collection;

//this is where the temporary search playlists will go
@property (nonatomic, strong) NSMutableArray *temporaryCollection;

//an array of PlaylistGroup objects, made up from the collection
@property (nonatomic, strong) NSMutableArray *playlistGroups;

@property (nonatomic, strong) Playlist *openedPlaylist;

@property(nonatomic, strong) PlaylistGroup *myPlaylists;
@property(nonatomic, strong) PlaylistGroup *followingPlaylists;
@property(nonatomic, strong) PlaylistGroup *defaultPlaylists;

@property(nonatomic, assign) BOOL offlineMode;

- (void)loadPlaylistsWithBlock:(void (^)(void))block;

- (void) loadLocalCachedPlaylists;

- (void) loadCachedPlaylists;

- (void) refreshPlaylistGroups;

- (void) refreshWithArray:(NSArray *)array;

- (void) openPlaylist:(Playlist *)playlist;

- (void) updatePosition:(NSString *)playlistId andOrder:(NSNumber *)position;

- (void) updateHasNotification:(Playlist *)playlistWithNotification;

- (Playlist *) playlistAtIndexPath:(NSIndexPath *)indexPath;

- (BOOL)playlistIsLastInSection:(NSIndexPath *)indexPath;

- (NSInteger) numberOfPlaylistsInGroupIndex:(NSInteger) index;

- (Playlist *) findByPlaylist:(Playlist*) playlist;

- (void)movePlaylistFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath;

- (Playlist *)addLocalPlaylist:(LocalPlaylist *)playlist;

- (PlaylistGroup *)defaultPlaylistsGroup;

- (PlaylistGroup *)myPlaylistsGroup;

- (PlaylistGroup *)followingPlaylistsGroup;

- (Playlist *)musicFeed;

- (Playlist *)defaultPlaylist;

- (NSMutableArray *)allPlaylists;

- (void)removePlaylist:(Playlist *)playlist;

- (void)localMode;

- (void)onlineMode;

- (void)handlePlaylistNotification:(PlaylistBadgeChangeNotification *)notification;

- (BOOL)handleOpenUrl:(NSString *)path;

- (Playlist *) findByPlaylistId: (NSString*) playlistId;

- (NSMutableArray *) playlistsForGroupKey:(NSInteger)playlistGroupIdValue;

@end

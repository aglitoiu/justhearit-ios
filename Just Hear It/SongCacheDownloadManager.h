#import <Foundation/Foundation.h>

@class PlaylistSong;
@class SongCache;
@class CachedPlaylistSong;

@interface SongCacheDownloadManager : NSObject

@property(nonatomic, strong) NSMutableArray *queue;
@property(nonatomic, assign) BOOL isDownloading;
@property(nonatomic, strong) AFRKHTTPRequestOperation *operation;
@property(nonatomic, assign) NSTimer *timer;

+ (id) sharedInstance;

- (void)downloadNextSongIfPossible;

- (void) pushSong:(CachedPlaylistSong *)song;

- (void)downloadSong:(CachedPlaylistSong *)song;

- (void)downloadSong:(CachedPlaylistSong *)song toFilePath:(NSString *)path;

- (void)downloadStarted;

- (void)cleanupSongs;
- (void)clearQueue;
- (void) clearDownload;
- (void)cleanupIncompleteSongs;
- (void) StartDownloadingAfterOffline;
- (void)resumeDownloads;

- (void)checkWifi:(id)sender;

@end

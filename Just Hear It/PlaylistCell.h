//
//  PlaylistCell.h
//  Just Hear It
//
//  Created by andrei st on 10/15/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMMoveTableViewCell.h"

@class PlaylistCell;
@class Playlist;

@protocol PlaylistCellDelegate <NSObject>

- (void) playlistCellDidSwipeLeft:(PlaylistCell *)cell;
- (void) playlistCellDidHitOptions:(PlaylistCell *)cell;

@end

extern NSString *const PlaylistCellEnclosingTableViewDidBeginScrollingNotification;

@interface PlaylistCell : UITableViewCell <FMMoveTableViewCell>

@property (nonatomic, strong) UIButton *optionsButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *badgeLabel;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UIView *selection;
@property (nonatomic, weak) id<PlaylistCellDelegate> playlistCellDelegate;
@property (nonatomic, weak) Playlist *playlist;

+ (UIView *)makeSelectionView;

- (CGRect) selectionFrame;

- (void) showDeleteButton;

- (void)hideOptionsButton;

- (void)repositionBadgeWithText:(NSString *)text;

- (void)markSelected:(BOOL)selected;

- (void)toggleOptionsButton;
@end

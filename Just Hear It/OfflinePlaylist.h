#import <Foundation/Foundation.h>
#import "Playlist.h"


@interface OfflinePlaylist : Playlist

+ (OfflinePlaylist *)offlineRecentlyPlayed;

@end
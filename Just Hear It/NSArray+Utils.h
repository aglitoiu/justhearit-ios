//
// Created by andrei st on 1/7/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSArray (Utils)

- (id) next:(id)el;
- (id) prev:(id)el;

@end
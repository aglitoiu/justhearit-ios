//
// Created by andrei st on 1/14/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface LatestSearch : NSObject

+ (instancetype) sharedInstance;

@property(nonatomic, strong) NSString *query;


@end
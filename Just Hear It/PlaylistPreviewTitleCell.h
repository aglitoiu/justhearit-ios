//
//  PlaylistPreviewTitleCell.h
//  Just Hear It
//
//  Created by andrei st on 12/20/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaylistPreviewTitleCell : UITableViewCell

@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UIImageView *icon;

@end

//
// Created by andrei st on 12/12/13.
// Copyright (c) 2013 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface PlaylistsPreviewList : NSObject

@property(nonatomic, strong) NSMutableArray *playlists;

@end
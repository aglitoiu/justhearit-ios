//
// Created by andreis on 4/26/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "Analytics.h"
#import "GAIDictionaryBuilder.h"


@implementation Analytics

+ (void)event:(NSString *)action {
    [Analytics event:action:action];
}

+ (void)event:(NSString *)action :(NSString *)label {

#ifndef DEBUG
    #ifndef TEST_MODE

    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:@"justhearit"
                                                                                        action:action
                                                                                         label:label
                                                                                         value:nil] build]];
    #endif
#endif
}

@end
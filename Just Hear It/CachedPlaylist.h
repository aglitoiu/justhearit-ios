//
//  CachedPlaylist.h
//  Just Hear It
//
//  Created by andrei st on 1/16/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CachedPlaylistSong;

@interface CachedPlaylist : NSManagedObject

@property (nonatomic, retain) NSString * playlistId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * playlistType;
@property (nonatomic, retain) NSSet *cachedPlaylistSongs;
@property (nonatomic, retain) NSString * position;
@end

@interface CachedPlaylist (CoreDataGeneratedAccessors)

- (void)addCachedPlaylistSongsObject:(CachedPlaylistSong *)value;
- (void)removeCachedPlaylistSongsObject:(CachedPlaylistSong *)value;
- (void)addCachedPlaylistSongs:(NSSet *)values;
- (void)removeCachedPlaylistSongs:(NSSet *)values;

@end

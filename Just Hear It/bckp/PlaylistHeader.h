//
// Created by andrei st on 10/15/13.
// Copyright (c) 2013 HearIt Inc. All rights reserved.
//


#import <UIKit/UIKit.h>

@class PlaylistGroup;
@class PlaylistHeader;

@protocol PlaylistHeaderDelegate <NSObject>

- (void) playlistHeaderHit:(PlaylistHeader *)playlistHeader;

@end

@interface PlaylistHeader : UIView

@property (nonatomic, strong) PlaylistGroup *playlistGroup;
@property (nonatomic, strong) UIImageView *icon;
@property (weak) id <PlaylistHeaderDelegate> delegate;

+ (PlaylistHeader *)initWithPlaylistGroup:(PlaylistGroup *)playlistGroup andWidth:(CGFloat)width;

@end
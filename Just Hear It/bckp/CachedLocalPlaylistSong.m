//
//  CachedPlaylistSong.m
//  Just Hear It
//
//  Created by andrei st on 1/16/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "CachedLocalPlaylistSong.h"
#import "CachedLocalPlaylist.h"


@implementation CachedLocalPlaylistSong
@synthesize token;

@dynamic album;
@dynamic artist;
@dynamic fileSize;
@dynamic lastListenedAt;
@dynamic name;
@dynamic songId;
@dynamic url;
@dynamic downloaded;
@dynamic cachedPlaylists;
@dynamic cachedLocalPlaylists;
@dynamic nodownload;
@dynamic isNewPlaylistSong;
@dynamic playlistId;
@dynamic createdAt;
@dynamic order;
@dynamic position;
@dynamic playlistSongId;
@dynamic shareUrl;

@end

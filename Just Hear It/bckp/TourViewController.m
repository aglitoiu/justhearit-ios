#import "TourViewController.h"
#import "TourPageViewController.h"

@implementation TourViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Create the data model
    _pageDescriptions = @[
            @"Tap and hold a song to move it",
            @"Move it to the left and drop it on a playlist",
            @"You can also move playlists by tapping and holding",
            @"Drag any search results to your 'Following' group to receive notifications",
            @"Swipe to the left on any song or playlist for options",
            @"Slide the player up or down"
    ];
    _pageImages = @[@"tour_1.png", @"tour_2.png", @"tour_3.png", @"tour_4.png", @"tour_5.png", @"tour_6.png"];

    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;

    TourPageViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    _pageViewController.view.opaque = NO;
    _pageViewController.view.backgroundColor = [UIColor clearColor];
    [self.pageViewController didMoveToParentViewController:self];

    [self.view bringSubviewToFront:self.closeButton];
}

- (TourPageViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if ((self.pageDescriptions.count == 0) || (index >= [self.pageDescriptions count])) {
        return nil;
    }

    // Create a new view controller and pass suitable data.
    TourPageViewController *tourPageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TourPageViewController"];
    tourPageViewController.imageFile = self.pageImages[index];
    tourPageViewController.titleText = self.pageDescriptions[index];
    tourPageViewController.pageIndex = index;
    tourPageViewController.view.opaque = NO;
    tourPageViewController.view.backgroundColor = [UIColor clearColor];


    return tourPageViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((TourPageViewController*) viewController).pageIndex;

    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }

    index--;
    return [self viewControllerAtIndex:index];
}
- (void)orientationChanged:(NSNotification *)notification{
    if(IS_DEVICE==IS_IPAD)
    {
    [delegate tourViewControllerDidFinish:self];
    }
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((TourPageViewController*) viewController).pageIndex;

    if (index == NSNotFound) {
        return nil;
    }

    index++;
    if (index == [self.pageDescriptions count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageDescriptions count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (void)dismissViewController:(id)sender {
       [delegate tourViewControllerDidFinish:self];}

@end

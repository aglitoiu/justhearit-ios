#import "DidYouMeanCell.h"

@implementation DidYouMeanCell

@synthesize titleLabel, icon;

- (void) awakeFromNib {
    //setup the label
    titleLabel = [[UILabel alloc] init];
    [self addSubview:titleLabel];
    titleLabel.frame = CGRectMake(PLAYLISTS_PADDING_LEFT, 0, PLAYLISTS_CELL_MAXIMUM_SELECTION_WIDTH - 45, PLAYLISTS_CELL_HEIGHT);
    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    titleLabel.textColor = RGB(255,255,255);

    //setup the icon
    icon = [[UIImageView alloc] init];
    icon.frame = CGRectMake(15, 0, 12, PLAYLISTS_CELL_HEIGHT);
    icon.contentMode = UIViewContentModeCenter;
    [self addSubview:icon];
}

@end

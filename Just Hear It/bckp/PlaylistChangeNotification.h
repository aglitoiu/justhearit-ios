//
//  PlaylistChangeNotification.h
//  Just Hear It
//
//  Created by Goiceanu Ciprian on 19/12/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlaylistChangeNotification : NSObject

@property(nonatomic, strong) NSString *playlistId;
@property(nonatomic, strong) NSString *event;
@property(nonatomic, strong) NSString *position;

@end

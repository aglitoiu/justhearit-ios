//
//  LoginViewController.h
//  Just Hear It
//
//  Created by andrei st on 10/16/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import "TransparentTextField.h"
#import "AppDelegate.h"
#import "ModalBackgroundViewController.h"
#import "Spinner.h"
#import "PlayerView.h"
#import "NavigationBarView.h"
#import "ZFModalTransitionAnimator.h"

@class LoginViewController;
@class JusthearitSliderView;
@class CacheSliderView;
@class MainViewController;

@protocol LoginViewControllerDelegate <NSObject>

- (void) loginViewControllerDidFinish:(LoginViewController*) controller;
- (void) sessionChanged:(LoginViewController*) controller;

@end


@interface LoginViewController: ModalBackgroundViewController <UITextFieldDelegate, UIAlertViewDelegate>
@property (nonatomic) NSInteger myInteger;
@property (nonatomic, strong) IBOutlet TransparentTextField *email;
@property (nonatomic, strong) IBOutlet TransparentTextField *password;
@property (nonatomic, strong) Spinner *animatedSpinner;
@property (nonatomic, strong) IBOutlet UIButton *facebookButton;
@property (nonatomic, strong) IBOutlet UIView *orLine;
@property (nonatomic, strong) IBOutlet UIButton *signOutButton;
@property (nonatomic, strong) CacheSliderView *sliderView;
@property (nonatomic, strong) MainViewController *mainViewController;
@property (nonatomic, strong) ModalBackgroundViewController *mdbkg;
@property (nonatomic, strong) IBOutlet NavigationBarView *navigationBar;
@property (nonatomic, strong) IBOutlet PlayerView* playerView;
@property (nonatomic, strong) IBOutlet UIProgressView* progrs;
@property (nonatomic, strong) UIView *OfflineView;
@property (nonatomic, strong) UIView *lteView;
@property(nonatomic, strong) UISwitch *lteSwitch;
@property(nonatomic, strong) UISwitch *OfflineSwitch;
@property(nonatomic, strong) UILabel *lteSwitchLabel;
@property(nonatomic, strong) UILabel *OfflineSwitchLabel;
@property(nonatomic, strong) UIButton *forgotPasswordButton;
@property (nonatomic, strong) ZFModalTransitionAnimator *animator;

@property (nonatomic, strong) IBOutlet TransparentTextField *email_register;
@property (nonatomic, strong) IBOutlet TransparentTextField *password_register;
@property (nonatomic, strong) IBOutlet TransparentTextField *password_confirm;
@property (nonatomic, strong) IBOutlet TransparentTextField *full_name;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIButton *registerButton;
@property (nonatomic, strong) IBOutlet UIButton *createAccountButton;

@property (nonatomic, strong) FBSDKLoginManager *login;

@property (weak) id <LoginViewControllerDelegate> delegate;
- (void) orientationChanged;
- (void)dismissViewController:(id)sender;

- (IBAction)facebookLogin:(id)sender;

- (IBAction)logout;

- (void)loginWithEmail:(NSString *)email1 andPassword:(NSString *)password;

- (void) registerWithData:(NSString *)email1 andPassword:(NSString *)password andPasswordConfirmation:(NSString *)password_confirmation andFullName:(NSString *)fullname withCompleteBlock:(void (^)(void))completeBlock;

- (void) resendConfirmation:(NSString *)email withCompleteBlock:(void (^)(void))completeBlock;

@end

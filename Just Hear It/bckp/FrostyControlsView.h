#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"
#import "ScrubBarView.h"


@class ScrubBarView;
@class MainViewController;

@interface FrostyControlsView : UIView <ScrubBarViewDelegate, UIPopoverControllerDelegate>

@property(nonatomic, strong) MainViewController *mainViewController;
@property(nonatomic, strong) MainViewController *masterViewController;
@property(nonatomic, strong) UIButton *playButton;
@property(nonatomic, strong) UIButton *prevButton;
@property(nonatomic, strong) UIButton *nextButton;
@property(nonatomic, strong) UIButton *repeatButton;
@property(nonatomic, strong) UIButton *shuffleButton;
@property(nonatomic, strong) UIPopoverController *popover;
@property(nonatomic, strong) UIView *buttonsContainer;
@property(nonatomic, strong) UIView *dropToPlayNext;
@property(nonatomic, strong) UILabel *dropToPlayNextLabel;
@property(nonatomic, strong) ScrubBarView *scrubBarView;
@property(nonatomic, strong) UIPanGestureRecognizer* panGesture;

@property (nonatomic, strong) MarqueeLabel *songNameMarquee;
@property (nonatomic, strong) MarqueeLabel *artistNameMarquee;

@property(nonatomic, strong) UIButton *shareButton;
@property(nonatomic, strong) UIButton *addToButton;
@property(nonatomic, strong) UIButton *discoverButton;

@property(nonatomic, strong) UIView *songActionsView;
@property(nonatomic, strong) UIView *songMetadataView;

@property(nonatomic, assign) BOOL scrubbing;

- (void) setupButtons;

- (void)showAllButtonsAnimated:(BOOL)animated;

- (void)showThreeButtonsAnimated:(BOOL)animated;

- (void) setup;

- (void)showDropHereToPlayNext;

- (void)highlightDropHereToPlayNext:(BOOL)highlight;

- (void)showControls;

- (void)reposition;

@end

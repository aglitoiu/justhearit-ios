#import <UIKit/UIKit.h>
#import "JEProgressView.h"
#import "UIView+Geometry.h"

@interface JusthearitSliderView : UIView

@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UISlider *sliderProgress;
@property (nonatomic, strong) UISlider *sliderProgress2;

@property (nonatomic, strong) UIProgressView *loaded;
@property (nonatomic, strong) UIProgressView *audioloaded2;
@property (nonatomic, strong) UILabel *label;

@property (nonatomic, assign) BOOL isDragging;

- (void)setup;

@end

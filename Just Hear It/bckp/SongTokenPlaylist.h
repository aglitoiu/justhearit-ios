#import "Playlist.h"

@interface SongTokenPlaylist : Playlist

@property(nonatomic, strong) NSString *token;

@end
#import <Foundation/Foundation.h>
#import "RKObjectMapping.h"

@class RKObjectMapping;

@interface RequestMappingProvider : NSObject

+ (RKObjectMapping *)sessionsMapping;

+ (RKObjectMapping *)searchMapping;

+ (RKObjectMapping *)searchPlaylistsMapping;

+ (RKObjectMapping *)createPlaylistWithSongsMapping;

+ (RKObjectMapping *)updatePlaylistMapping;

+ (RKObjectMapping *)updatePlaylistIsDefaultMapping;

+ (RKObjectMapping *)addSongsToPlaylistMapping;

+ (RKObjectMapping *)movePlaylistMapping;

+ (RKObjectMapping *)movePlaylistSongMapping;

+ (RKObjectMapping *)songLogAttributesMapping;

+ (RKObjectMapping *)userAttributesMapping;

+ (RKObjectMapping *)songTokenPlaylistAttributesMapping;

+ (RKObjectMapping *)registrationMapping;

+ (RKObjectMapping *) playlistAttributesMapping;
@end
#import <Foundation/Foundation.h>
#import "RestKit.h"

@interface ResponseMappingProvider : NSObject

+(RKMapping*) sessionMapping;
+(RKMapping*) registrationMapping;
+(RKMapping*) playlistMapping;
+(RKMapping*) playlistPreviewMapping;
+(RKMapping*) paginationMapping;
+(RKMapping*) playlistSongMapping;
+(RKMapping*) playlistSongUpdateMapping;
+(RKMapping*) autocompleteMapping;
+(RKMapping*) fayeConfigMapping;
+(RKMapping*) settingsMapping;
+(RKMapping*) playlistUpdatesMapping;
+(RKMapping*) simplePlaylistMapping;

@end

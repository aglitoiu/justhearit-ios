#import "Routes.h"
#import "PlaylistAddSongs.h"
#import "PlaylistMove.h"
#import "PlaylistSongMove.h"
#import "User.h"
#import "SongTokenPlaylist.h"

@implementation Routes

+ (RKRoute*) sessionsPath {
    return [RKRoute routeWithName:SESSIONS_PATH_NAME pathPattern:SESSIONS_PATH method:RKRequestMethodPOST];
}

+ (RKRoute*) registrationsPath {
    return [RKRoute routeWithName:REGISTRATIONS_PATH_NAME pathPattern:REGISTRATIONS_PATH method:RKRequestMethodPOST];
}

+ (RKRoute*) confirmationsPath {
    return [RKRoute routeWithName:CONFIRMATIONS_PATH_NAME pathPattern:CONFIRMATIONS_PATH method:RKRequestMethodPOST];
}

+ (RKRoute*) validTokenPath{
    return [RKRoute routeWithName:VALID_TOKEN_PATH_NAME pathPattern:VALID_TOKEN_PATH method:RKRequestMethodGET];
}

+ (RKRoute*) fayeConfigPath{
    return [RKRoute routeWithName:FAYE_CONFIG_PATH_NAME pathPattern:FAYE_CONFIG_PATH method:RKRequestMethodGET];
}

+ (RKRoute *) autocompletePath {
    return [RKRoute routeWithName:AUTOCOMPLETE_PATH_NAME pathPattern:AUTOCOMPLETE_PATH method:RKRequestMethodGET];
}

+ (RKRoute *) playlistsPath {
    return [RKRoute routeWithName:PLAYLISTS_PATH_NAME pathPattern:PLAYLISTS_PATH method:RKRequestMethodGET];
}

+ (RKRoute *) updatePlaylistPath {
    return [RKRoute routeWithClass:[Playlist class] pathPattern:PLAYLIST_PATH method:RKRequestMethodPUT];
}

+ (RKRoute *) updatePlaylistDataPath {
    return [RKRoute routeWithClass:[Playlist class] pathPattern:PLAYLIST_PATH method:RKRequestMethodGET];
}

+ (RKRoute *) loadPlaylistPath {
    return [RKRoute routeWithName:PLAYLIST_PATH_NAME pathPattern:PLAYLIST_PATH method:RKRequestMethodGET];
}

+ (RKRoute *) playlistSongsPath {
    return [RKRoute routeWithName:PLAYLISTS_SONGS_PATH_NAME pathPattern:PLAYLISTS_SONGS_PATH method:RKRequestMethodGET];
}

+ (RKRoute *) playlistSongsUpdatesPath {
    return [RKRoute routeWithName:PLAYLISTS_SONGS_UPDATES_PATH_NAME pathPattern:PLAYLISTS_SONGS_UPDATES_PATH method:RKRequestMethodGET];
}

+ (RKRoute *) searchPath {
    return [RKRoute routeWithName:SEARCH_PATH_NAME pathPattern:SEARCH_PATH method:RKRequestMethodPOST];
}

+ (RKRoute *)searchPlaylistsPath {
    return [RKRoute routeWithName:SEARCH_PLAYLISTS_PATH_NAME pathPattern:SEARCH_PLAYLISTS_PATH method:RKRequestMethodPOST];
}

+ (RKRoute *) addSongsToPlaylistPath {
    return [RKRoute routeWithClass:[PlaylistAddSongs class] pathPattern:PLAYLISTS_ADD_SONGS_PATH method:RKRequestMethodPOST];
}

+ (RKRoute *)movePlaylistPath {
    return [RKRoute routeWithClass:[PlaylistMove class] pathPattern:PLAYLIST_MOVE_PATH method:RKRequestMethodPOST];
}

+ (RKRoute *)movePlaylistSongPath {
    return [RKRoute routeWithClass:[PlaylistSongMove class] pathPattern:PLAYLIST_SONG_MOVE_PATH method:RKRequestMethodPOST];
}

+ (RKRoute *)deletePlaylistPath {
    return [RKRoute routeWithClass:[Playlist class] pathPattern:PLAYLIST_PATH method:RKRequestMethodDELETE];
}

+ (RKRoute *)deletePlaylistSongPath {
    return [RKRoute routeWithClass:[PlaylistSong class] pathPattern:PLAYLIST_SONG_PATH method:RKRequestMethodDELETE];
}

+ (RKRoute *)loadPlaylistSongPath {
    return [RKRoute routeWithName:PLAYLIST_SONG_PATH_NAME pathPattern:PLAYLIST_SONG_PATH method:RKRequestMethodGET];
}

+ (RKRoute *)songLogPath {
    return [RKRoute routeWithClass:[PlaylistSong class] pathPattern:SONG_LOG_PATH method:RKRequestMethodPOST];
}

+ (RKRoute *)passwordsPath {
    return [RKRoute routeWithName:PASSWORDS_PATH_NAME pathPattern:PASSWORDS_PATH method:RKRequestMethodPOST];
}

+ (RKRoute *)updateUserPath {
    return [RKRoute routeWithClass:[User class] pathPattern:USER_EDIT_PATH method:RKRequestMethodPUT];
}

+ (RKRoute *)getSongPath {
    return [RKRoute routeWithClass:[SongTokenPlaylist class] pathPattern:SONG_TOKEN_PATH method:RKRequestMethodGET];
}

+ (RKRoute *)settingsPath {
    return [RKRoute routeWithName:SETTINGS_PATH_NAME pathPattern:SETTINGS_PATH method:RKRequestMethodGET];
}


@end
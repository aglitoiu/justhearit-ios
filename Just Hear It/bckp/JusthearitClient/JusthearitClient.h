#import <Foundation/Foundation.h>
#import "RKObjectManager.h"
#import "Playlist.h"
#import "Session.h"

@class PlaylistMove;
@class PlaylistAddSongs;
@class PlaylistSongMove;
@class PlaylistSongLog;
@class PlaylistIsDefault;
@class LocalPlaylist;
@class User;

@interface JusthearitClient : RKObjectManager

+ (id)sharedInstance;

- (void)login:(Session *) session withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)registration:(NSString *)email andPassword:(NSString*)password andPasswordConfirmation:(NSString *)password_confirmation andFullName:(NSString*)fullname withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

-(void)resendConfirmation:(NSString*)email withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)updateUser:(User *)user withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)logoutWithSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)deletePlaylistSong:(PlaylistSong *)song withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)checkLoginTokenWithSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

//- (void)getFayeSubscriptionParametersForChannel:(NSString *)channel withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)getSettingsWithSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)loadPlaylists:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void) loadPlaylist:(Playlist *)playlist  withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)loadNextPageFromPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)loadNextPageFromSongTokenPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))success withErrorBlock:(void (^)(NSError *))error;

- (void)autocompleteWithQuery:(NSString *)query withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)createPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void) loadNextPageFromNormalPlaylistUpdates:(Playlist *)playlist andIds:(NSDictionary *)ids withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)sendPasswordResetToEmail:(NSString *)email withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)addSongsToPlaylist:(PlaylistAddSongs *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)movePlaylist:(PlaylistMove *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)movePlaylistSong:(PlaylistSongMove *)playlistSong withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)createSharedSearchFromPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)deletePlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)logSong:(PlaylistSong *)song withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)createSharedPlaylistFromPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)updatePlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void)updatePlaylistIsDefault:(PlaylistIsDefault *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void) loadSongsFromPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

- (void) loadSong:(PlaylistSong *)playlistSong  withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;
@end

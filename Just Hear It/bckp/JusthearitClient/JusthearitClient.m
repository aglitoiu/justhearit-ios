#import "JusthearitClient.h"
#import "Routes.h"
#import "ResponseMappingProvider.h"
#import "RequestMappingProvider.h"
#import "Playlist.h"
#import "Pagination.h"
#import "LocalPlaylist.h"
#import "SearchParams.h"
#import "SearchPlaylistsParams.h"
#import "PlaylistAddSongs.h"
#import "PlaylistMove.h"
#import "PlaylistSongMove.h"
#import "PlaylistSongLog.h"
#import "PlaylistIsDefault.h"
#import "AFRKHTTPClient.h"
#import "User.h"
#import "SongTokenPlaylist.h"


@implementation JusthearitClient

+ (id)sharedInstance {
    static JusthearitClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseURL = [NSURL URLWithString:JhiApiUrl];
        __sharedInstance = [JusthearitClient managerWithBaseURL:baseURL];
        __sharedInstance.requestSerializationMIMEType = RKMIMETypeJSON;
        [__sharedInstance.router.routeSet addRoute:[Routes sessionsPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes registrationsPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes playlistsPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes playlistSongsPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes playlistSongsUpdatesPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes validTokenPath]];

        [__sharedInstance.router.routeSet addRoute:[Routes autocompletePath]];
        [__sharedInstance.router.routeSet addRoute:[Routes addSongsToPlaylistPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes movePlaylistPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes movePlaylistSongPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes deletePlaylistPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes songLogPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes deletePlaylistSongPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes updatePlaylistPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes loadPlaylistPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes loadPlaylistSongPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes passwordsPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes updateUserPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes getSongPath]];
        [__sharedInstance.router.routeSet addRoute:[Routes settingsPath]];
        [__sharedInstance addRequestDescriptors];
        [__sharedInstance addResponseDescriptors];

       

        [__sharedInstance.HTTPClient setReachabilityStatusChangeBlock:^(AFRKNetworkReachabilityStatus status) {
//            if (firstTime == 1) {
//                firstTime = 0;
//                return;
//            }
            [[NSNotificationCenter defaultCenter] postNotificationName:Ev.reachabilityChanged object:nil userInfo:nil];
        }];

    });

    return __sharedInstance;
}

- (void) addResponseDescriptors {
    NSIndexSet *statusCodeSet = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);

    RKResponseDescriptor *sessionsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider sessionMapping]
                                                                                                    method: RKRequestMethodPOST
                                                                                               pathPattern:SESSIONS_PATH
                                                                                                   keyPath:nil
                                                                                               statusCodes:statusCodeSet];
    [self addResponseDescriptor:sessionsResponseDescriptor];

    RKResponseDescriptor *searchResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistMapping]
                                                                                                  method: RKRequestMethodPOST
                                                                                             pathPattern:SEARCH_PATH
                                                                                                 keyPath:nil
                                                                                             statusCodes:statusCodeSet];
    [self addResponseDescriptor:searchResponseDescriptor];
    
    
    RKResponseDescriptor *registrationResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider registrationMapping]
                                                                                                  method: RKRequestMethodPOST
                                                                                             pathPattern:REGISTRATIONS_PATH
                                                                                                 keyPath:nil
                                                                                             statusCodes:statusCodeSet];
    [self addResponseDescriptor:registrationResponseDescriptor];

    RKResponseDescriptor *searchPlaylistsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistMapping]
                                                                                                           method: RKRequestMethodPOST
                                                                                                      pathPattern:SEARCH_PLAYLISTS_PATH
                                                                                                          keyPath:nil
                                                                                                      statusCodes:statusCodeSet];
    [self addResponseDescriptor:searchPlaylistsResponseDescriptor];

    RKResponseDescriptor *validTokenResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider sessionMapping]
                                                                                                      method: RKRequestMethodGET
                                                                                                 pathPattern:VALID_TOKEN_PATH
                                                                                                     keyPath:nil
                                                                                                 statusCodes:statusCodeSet];
    [self addResponseDescriptor:validTokenResponseDescriptor];



    RKResponseDescriptor *playlistsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistMapping]
                                                                                                     method: RKRequestMethodGET
                                                                                                pathPattern:PLAYLISTS_PATH
                                                                                                    keyPath:nil
                                                                                                statusCodes:statusCodeSet];
    [self addResponseDescriptor:playlistsResponseDescriptor];

    RKResponseDescriptor *playlistSongResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistMapping]
                                                                                                        method: RKRequestMethodGET
                                                                                                   pathPattern:PLAYLISTS_SONGS_PATH
                                                                                                       keyPath:nil
                                                                                                   statusCodes:statusCodeSet];

    [self addResponseDescriptor:playlistSongResponseDescriptor];

    RKResponseDescriptor *playlistUpdatesSongResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistUpdatesMapping]
                                                                                                        method: RKRequestMethodGET
                                                                                                   pathPattern:PLAYLISTS_SONGS_UPDATES_PATH
                                                                                                       keyPath:nil
                                                                                                   statusCodes:statusCodeSet];
    
    [self addResponseDescriptor:playlistUpdatesSongResponseDescriptor];
    
    RKResponseDescriptor *playlistUpdateResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider simplePlaylistMapping]
                                                                                                               method: RKRequestMethodPUT
                                                                                                          pathPattern:PLAYLIST_PATH
                                                                                                              keyPath:nil
                                                                                                          statusCodes:statusCodeSet];
    
    
    [self addResponseDescriptor:playlistUpdateResponseDescriptor];
    
    RKResponseDescriptor *movePlaylistResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider simplePlaylistMapping]
                                                                                                          method: RKRequestMethodPOST
                                                                                                     pathPattern:PLAYLIST_MOVE_PATH
                                                                                                         keyPath:nil
                                                                                                     statusCodes:statusCodeSet];
    
    
    [self addResponseDescriptor:movePlaylistResponseDescriptor];
    
    RKResponseDescriptor *songTokenResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistMapping]
                                                                                                        method: RKRequestMethodGET
                                                                                                   pathPattern:SONG_TOKEN_PATH
                                                                                                       keyPath:nil
                                                                                                   statusCodes:statusCodeSet];

    [self addResponseDescriptor:songTokenResponseDescriptor];

    RKResponseDescriptor *autocompleteResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider autocompleteMapping]
                                                                                                        method: RKRequestMethodGET
                                                                                                   pathPattern:AUTOCOMPLETE_PATH
                                                                                                       keyPath:nil
                                                                                                   statusCodes:statusCodeSet];

    [self addResponseDescriptor:autocompleteResponseDescriptor];

    RKResponseDescriptor *playlistsCreateResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistMapping]
                                                                                                           method: RKRequestMethodPOST
                                                                                                      pathPattern:PLAYLISTS_PATH
                                                                                                          keyPath:nil
                                                                                                      statusCodes:statusCodeSet];

    [self addResponseDescriptor:playlistsCreateResponseDescriptor];

    RKResponseDescriptor *createSharedSearchPlaylistResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistMapping]
                                                                                                                      method: RKRequestMethodPOST
                                                                                                                 pathPattern:SHARED_PLAYLISTS_PATH
                                                                                                                     keyPath:nil
                                                                                                                 statusCodes:statusCodeSet];

    [self addResponseDescriptor:createSharedSearchPlaylistResponseDescriptor];

    RKResponseDescriptor *settingsResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider settingsMapping]
                                                                                                                      method: RKRequestMethodGET
                                                                                                                 pathPattern:SETTINGS_PATH
                                                                                                                     keyPath:nil
                                                                                                                 statusCodes:statusCodeSet];

    [self addResponseDescriptor:settingsResponseDescriptor];
    
    RKResponseDescriptor *loadPlaylistResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider simplePlaylistMapping]
                                                                                                       method: RKRequestMethodGET
                                                                                                  pathPattern:PLAYLIST_PATH
                                                                                                      keyPath:nil
                                                                                                  statusCodes:statusCodeSet];
    [self addResponseDescriptor:loadPlaylistResponseDescriptor];
    
    RKResponseDescriptor *loadSongResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[ResponseMappingProvider playlistSongMapping]
                                                                                                        method: RKRequestMethodGET
                                                                                                   pathPattern:PLAYLIST_SONG_PATH
                                                                                                       keyPath:nil
                                                                                                   statusCodes:statusCodeSet];
    [self addResponseDescriptor:loadSongResponseDescriptor];
    
    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
    // The entire value at the source key path containing the errors maps to the message
    [errorMapping addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"errorMessage"]];

    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    // Any response in the 4xx status code range with an "errors" key path uses this mapping
    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping method:RKRequestMethodAny pathPattern:REGISTRATIONS_PATH keyPath:@"message" statusCodes:statusCodes];
    

    [self addResponseDescriptor:errorDescriptor];
}

- (void) addRequestDescriptors {
    RKRequestDescriptor *sessionsDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider sessionsMapping]
                                                                                    objectClass:[Session class]
                                                                                    rootKeyPath:nil
                                                                                         method:RKRequestMethodPOST];
    
    RKRequestDescriptor *searchDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider searchMapping]
                                                                                  objectClass:[SearchParams class]
                                                                                  rootKeyPath:nil
                                                                                       method:RKRequestMethodPOST];
   
   
    
    RKRequestDescriptor *searchPlaylistsDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider searchPlaylistsMapping]
                                                                                           objectClass:[SearchPlaylistsParams class]
                                                                                           rootKeyPath:nil
                                                                                                method:RKRequestMethodPOST];

    RKRequestDescriptor *createPlaylistWithSongsDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider createPlaylistWithSongsMapping]
                                                                                                   objectClass:[Playlist class]
                                                                                                   rootKeyPath:nil
                                                                                                        method:RKRequestMethodPOST];

    RKRequestDescriptor *addSongsToPlaylistDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider addSongsToPlaylistMapping]
                                                                                              objectClass:[PlaylistAddSongs class]
                                                                                              rootKeyPath:nil
                                                                                                   method:RKRequestMethodPOST];

    RKRequestDescriptor *movePlaylistDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider movePlaylistMapping]
                                                                                        objectClass:[PlaylistMove class]
                                                                                        rootKeyPath:nil
                                                                                             method:RKRequestMethodPOST];

    RKRequestDescriptor *movePlaylistSongDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider movePlaylistSongMapping]
                                                                                            objectClass:[PlaylistSongMove class]
                                                                                            rootKeyPath:nil
                                                                                                 method:RKRequestMethodPOST];

    RKRequestDescriptor *songLogDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider songLogAttributesMapping]
                                                                                   objectClass:[PlaylistSongLog class]
                                                                                   rootKeyPath:nil
                                                                                        method:RKRequestMethodPOST];

    RKRequestDescriptor *updatePlaylistDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider updatePlaylistMapping]
                                                                                                   objectClass:[Playlist class]
                                                                                                   rootKeyPath:nil
                                                                                                        method:RKRequestMethodPUT];
    
    RKRequestDescriptor *updatePlaylistIsDefaultDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider updatePlaylistIsDefaultMapping]
                                                                                          objectClass:[PlaylistIsDefault class]
                                                                                          rootKeyPath:nil
                                                                                               method:RKRequestMethodPUT];

    RKRequestDescriptor *editUserDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider userAttributesMapping]
                                                                                          objectClass:[User class]
                                                                                          rootKeyPath:nil
                                                                                               method:RKRequestMethodPUT];

    RKRequestDescriptor *getSongDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider songTokenPlaylistAttributesMapping]
                                                                                    objectClass:[SongTokenPlaylist class]
                                                                                    rootKeyPath:nil
                                                                                         method:RKRequestMethodGET];

    /*RKRequestDescriptor *getPlaylistDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[RequestMappingProvider playlistAttributesMapping]
                                                                                   objectClass:[Playlist class]
                                                                                   rootKeyPath:nil
                                                                                        method:RKRequestMethodGET];*/
    
    [self addRequestDescriptor: songLogDescriptor];
    [self addRequestDescriptor: sessionsDescriptor];
    [self addRequestDescriptor: searchDescriptor];
    [self addRequestDescriptor: searchPlaylistsDescriptor];
    [self addRequestDescriptor: createPlaylistWithSongsDescriptor];
    [self addRequestDescriptor: addSongsToPlaylistDescriptor];
    [self addRequestDescriptor: movePlaylistDescriptor];
    [self addRequestDescriptor: movePlaylistSongDescriptor];
    [self addRequestDescriptor: updatePlaylistDescriptor];
    [self addRequestDescriptor: updatePlaylistIsDefaultDescriptor];
    [self addRequestDescriptor: editUserDescriptor];
    [self addRequestDescriptor: getSongDescriptor];
    //[self addRequestDescriptor: getPlaylistDescriptor];
    
}

-(void) login:(Session *) session withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self postObject:session path:SESSIONS_PATH parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

-(void) registration:(NSString *)email andPassword:(NSString *)password andPasswordConfirmation:(NSString *)password_confirmation andFullName:(NSString *)fullname withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self postObject:nil path:REGISTRATIONS_PATH parameters:@{@"user": @{@"email" : email, @"password": password, @"password_confirmation": password_confirmation, @"full_name": fullname }} success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}
-(void)resendConfirmation:(NSString*)email withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self postObject:nil path:CONFIRMATIONS_PATH parameters:@{@"user": @{@"email" : email}} success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}
-(void) updateUser:(User *) user withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self putObject:user path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) logoutWithSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self deleteObject:nil path:SESSIONS_PATH parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

-(void)deletePlaylistSong:(PlaylistSong *)song withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self deleteObject:song path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) checkLoginTokenWithSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self getObjectsAtPath:VALID_TOKEN_PATH parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.reachabilityChanged object:nil userInfo:nil];
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.reachabilityChanged object:nil userInfo:nil];
        errorBlock(error);
    }];
}



- (void) getSettingsWithSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self getObjectsAtPath:SETTINGS_PATH parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) loadPlaylists:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];

    
    [self getObjectsAtPath:PLAYLISTS_PATH parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        successBlock(
                     mappingResult.array);

    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) loadPlaylist:(Playlist *)playlist  withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self getObjectsAtPathForRouteNamed:PLAYLIST_PATH_NAME object:playlist parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) loadSong:(PlaylistSong *)playlistSong  withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self getObjectsAtPathForRouteNamed:PLAYLIST_SONG_PATH_NAME object:playlistSong parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) loadNextPageFromPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    
    [[RKObjectManager sharedManager] cancelAllObjectRequestOperationsWithMethod:RKRequestMethodGET matchingPathPattern:PLAYLISTS_SONGS_PATH];
    //[[RKObjectManager sharedManager].operationQueue cancelAllOperations];
    
    [self addHeaders];

    if (playlist.isLocalSearchPlaylist) {
        [self loadNextPageFromSearchPlaylist:playlist withSuccessBlock:successBlock withErrorBlock:errorBlock];
    } else if (playlist.isLocalSearchPlaylistsWith) {
        [self loadNextPageFromSearchPlaylistsWith:playlist withSuccessBlock:successBlock withErrorBlock:errorBlock];
    } else if (playlist.isSongTokenPlaylist) {
        [self loadNextPageFromSongTokenPlaylist:playlist withSuccessBlock:successBlock withErrorBlock:errorBlock];
    } else {
        //we open normal playlists and shared playlists here
        [self loadNextPageFromNormalPlaylist:playlist withSuccessBlock:successBlock withErrorBlock:errorBlock];
    }
}

- (void) loadNextPageFromSongTokenPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {

    SongTokenPlaylist *songTokenPlaylist = [[SongTokenPlaylist alloc] init];
    songTokenPlaylist.token = playlist.name;

    [self getObject:songTokenPlaylist path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [playlist.pagination incrementCurrentPage];
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) loadNextPageFromNormalPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self getObjectsAtPathForRouteNamed:PLAYLISTS_SONGS_PATH_NAME object:playlist parameters:@{@"page": playlist.pagination.nextPage} success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [playlist.pagination incrementCurrentPage];
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) loadNextPageFromNormalPlaylistUpdates:(Playlist *)playlist andIds:(NSDictionary *)ids withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self getObjectsAtPathForRouteNamed:PLAYLISTS_SONGS_UPDATES_PATH_NAME object:playlist parameters:@{@"ids":ids} success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [playlist.pagination incrementCurrentPage];
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

#pragma mark LOAD SONGS WHEN NEW ADDED NOTIF
- (void) loadSongsFromPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self getObjectsAtPathForRouteNamed:PLAYLISTS_SONGS_PATH_NAME object:playlist parameters:@{@"page": [NSNumber numberWithInt:1]} success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [playlist.pagination incrementCurrentPage];
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) loadNextPageFromSearchPlaylist:(LocalPlaylist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    if (playlist.searchParams.pagination.currentPage.intValue == 0) {
        [playlist.searchParams.pagination incrementCurrentPage];
    }

    [self postObject:playlist.searchParams path:SEARCH_PATH parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [playlist.pagination incrementCurrentPage];
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) loadNextPageFromSearchPlaylistsWith:(LocalPlaylist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    if (playlist.searchParams.pagination.currentPage.intValue == 0) {
        [playlist.searchParams.pagination incrementCurrentPage];
    }

    [self postObject:playlist.searchParams path:SEARCH_PLAYLISTS_PATH parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [playlist.pagination incrementCurrentPage];
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

-(void) autocompleteWithQuery:(NSString *) query withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self getObjectsAtPathForRouteNamed:AUTOCOMPLETE_PATH_NAME object:nil parameters:@{@"query": query} success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) createPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self postObject:playlist path:PLAYLISTS_PATH parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) sendPasswordResetToEmail:(NSString *)email withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self postObject:nil path:PASSWORDS_PATH parameters:@{@"user": @{@"email" : email}} success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void)updatePlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self putObject:playlist path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void)updatePlaylistIsDefault:(PlaylistIsDefault *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self putObject:playlist path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) addSongsToPlaylist:(PlaylistAddSongs *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self postObject:playlist path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) movePlaylist:(PlaylistMove *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self postObject:playlist path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) movePlaylistSong:(PlaylistSongMove *)playlistSong withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];
    [self postObject:playlistSong path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) createSharedSearchFromPlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];

    NSDictionary *params = @{@"type": @"search", @"name": playlist.name, @"position": playlist.position };

    [self postObject:nil path:SHARED_PLAYLISTS_PATH parameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) deletePlaylist:(Playlist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];

    [self deleteObject:playlist path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) logSong:(PlaylistSong *)song withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];

    PlaylistSongLog *playlistSongLog = [[PlaylistSongLog alloc] init];
    playlistSongLog.token = song.token;

    if (song.playlistSongUpdateId) {
        playlistSongLog.playlistSongUpdateId = song.playlistSongUpdateId;
    } else {
        playlistSongLog.playlistSongId = song.playlistSongId;
        playlistSongLog.playlistId = song.playlistId;
    }
    NSString *string1 = [NSString stringWithFormat:@"1: %@, 2: %@, 3: %@",
                         playlistSongLog.token, playlistSongLog.playlistSongUpdateId, playlistSongLog.playlistId];
    Log(@"playlist song Log: %@", string1);
    [self postObject:playlistSongLog path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) addHeaders {
    [self.HTTPClient setDefaultHeader:@"x-device-uuid" value:SETTINGS.udid];
    [self.HTTPClient setDefaultHeader:@"x-api-token" value:SETTINGS.token];
}

- (void)createSharedPlaylistFromPlaylist:(LocalPlaylist *)playlist withSuccessBlock:(void (^)(NSArray *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    [self addHeaders];

    NSDictionary *params = @{@"type": @"shared", @"playlist_id": playlist.playlistId ,@"position": playlist.position };

    [self postObject:nil path:SHARED_PLAYLISTS_PATH parameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        successBlock(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}


@end

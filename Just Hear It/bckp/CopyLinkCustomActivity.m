//
// Created by andrei st on 1/28/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import "CopyLinkCustomActivity.h"


@implementation CopyLinkCustomActivity

@synthesize link;

- (NSString *)activityType {
    return @"com.justhearit.copylink";
}

- (NSString *)activityTitle
{
    return @"Copy Link";
}

- (UIImage *)activityImage {
    // Note: These images need to have a transparent background and I recommend these sizes:
    // iPadShare@2x should be 126 px, iPadShare should be 53 px, iPhoneShare@2x should be 100
    // px, and iPhoneShare should be 50 px. I found these sizes to work for what I was making.
    return [UIImage imageNamed:@"copyLink.png"];

}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    return YES;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
    NSLog(@"%s",__FUNCTION__);
}

- (UIViewController *)activityViewController {
    NSLog(@"%s",__FUNCTION__);
    return nil;
}

- (void)performActivity {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = link;
    [self activityDidFinish:YES];
}

@end
#import "Session.h"

@implementation Session

+ (Session *)initWithLogin:(NSString *)login andPassword:(NSString *)password {
    Session *session = [[Session alloc] init];
    session.login = login;
    session.password = password;
    return session;
}

+ (Session *)initWithRegisters:(NSString *)login andPassword:(NSString *)password {
    Session *session = [[Session alloc] init];
    session.login = login;
    session.password = password;
    return session;
}

+ (Session *)initWithfbAuthToken:(NSString *)fbAuthToken {
    Session *session = [[Session alloc] init];
    session.fbAuthToken = fbAuthToken;
    return session;
}


@end
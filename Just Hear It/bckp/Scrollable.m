
#import "Scrollable.h"


@implementation Scrollable

@synthesize autoscrollDistance, autoscrollTimer, autoscrollThreshold, scroll, scrollingPaused;

- (void)getAutoscrollDistance
{
    float minimumLegalDistance = [scroll contentOffset].y * -1;
    float maximumLegalDistance = [scroll contentSize].height - ([scroll frame].size.height + [scroll contentOffset].y);
    autoscrollDistance = MAX(autoscrollDistance, minimumLegalDistance);
    autoscrollDistance = MIN(autoscrollDistance, maximumLegalDistance);
}

- (void)scrollTimerFired:(NSTimer *)timer {
    if (!scrollingPaused) {
        [self getAutoscrollDistance];

        CGPoint contentOffset = [scroll contentOffset];
        contentOffset.y += autoscrollDistance;
        [scroll setContentOffset:contentOffset];
    }
}

- (void)stopAutoscroll {
    autoscrollDistance = 0;
    [autoscrollTimer invalidate];
    autoscrollTimer = nil;
}

- (void)scrollBeganWithView:(UIView *)view {
    scrollingPaused = NO;
    autoscrollThreshold = view.frame.size.height * 0.6;
    autoscrollDistance = 0;
}

- (void) scrollingWithView:(UIView *)view {
    CGPoint location = view.center;

    //set the y location accordingly to the content offset
    location.y += scroll.contentOffset.y;

    //minus the table's current origin (if the table is lower than top of the screen we do a minus for that offset
    location.y -= scroll.frame.origin.y;
    [self autoscrollForImageView:view atLocation:location];
}

- (void) scrollingEnded {
    [self stopAutoscroll];
}

- (void)autoscrollForImageView:(UIView *)view atLocation:(CGPoint) location
{
    autoscrollDistance = 0;

    if (scroll.frame.size.height < scroll.contentSize.height)
    {
        CGPoint touchLocation = location;

        float distanceToTopEdge  = touchLocation.y - CGRectGetMinY(scroll.bounds);
        float distanceToBottomEdge = CGRectGetMaxY(scroll.bounds) - touchLocation.y;

        if (distanceToTopEdge < autoscrollThreshold) {
            autoscrollDistance = -[self autoscrollDistanceWithProximity:distanceToTopEdge];
        } else if (distanceToBottomEdge < autoscrollThreshold) {
            autoscrollDistance = [self autoscrollDistanceWithProximity:distanceToBottomEdge];
        }
    }

    if (autoscrollDistance == 0) {
        [autoscrollTimer invalidate];
        autoscrollTimer = nil;
    } else if (!autoscrollTimer) {
        autoscrollTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0 / 60.0) target:self selector:@selector(scrollTimerFired:) userInfo:view repeats:YES];
    }
}


- (float)autoscrollDistanceWithProximity:(float)proximity {
    return ceilf((autoscrollThreshold - proximity) / 5.0);
}

@end
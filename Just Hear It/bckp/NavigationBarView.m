#import "NavigationBarView.h"

@implementation NavigationBarView

@synthesize logoView, handle, handleShadow, titleLabel,subTitleLabel,btnMNU;

- (void) setup {
    [self createLogo];
    [self addHandle];
    [self createTitle];

    [self addHandleShadow];
    [self bringSubviewToFront:logoView];
    [self bringSubviewToFront:titleLabel];
  
}

- (void) showLogo {
    [UIView animateWithDuration:1 animations:^{
        subTitleLabel.alpha=0;
        titleLabel.alpha = 0;
        logoView.alpha = 1;
        
    }];
}

- (void) showTitle:(NSString *)title :(NSString *)subtitle {
    titleLabel.text = title;
    subTitleLabel.text=subtitle;
    [UIView animateWithDuration:1 animations:^{
        titleLabel.alpha = 1;
        subTitleLabel.alpha=0.6;
        logoView.alpha = 0;
    }];
}

- (void) showRedHandle {
    handle.image = [Utils imageFromColor:RGBA(255,0,0,.42) withWidth:self.frame.size.width andHeight:self.frame.size.height];

  
}
- (void) showHalfRedHandle {
    handle.image = [Utils imageFromColor:RGBA(255,0,0,.20) withWidth:self.frame.size.width andHeight:self.frame.size.height];
    
    
}

- (void) showNormalHandle {
    handle.image = [Utils imageFromColor:RGBA(255,255,255,.15) withWidth:self.frame.size.width andHeight:self.frame.size.height];
    /*UIViewController * contributeViewController = [[UIViewController alloc] init];
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
    UIVisualEffectView *beView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    beView.frame=self.frame;
    beView.alpha=.85;
    
    
    [self insertSubview:beView atIndex:0];

    [self bringSubviewToFront:logoView];
    [self bringSubviewToFront:titleLabel];*/
}

- (void)createLogo {
    NSLog(@"device is: %d",(IS_DEVICE == IS_IPAD));
    if(IS_DEVICE==IS_IPAD)
    {
    logoView = [UIImageView imageNamed:@"logo_title.png"];
    logoView.contentMode = UIViewContentModeCenter;
    [self addSubview:logoView];
    
    CGPoint center;
    if([Utils isLandscape]) {
        center = CGPointMake([Utils currentWidth]/2, self.frame.size.height/2);
    }else {
        center = self.center;
    }
    
    logoView.center = center;
    CGRect frame = logoView.frame;
    frame.size.height = 44;
    frame.origin.y = self.frame.size.height - frame.size.height - 3;
    
    }
    else if (IS_DEVICE==IS_IPHONE) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
            CGPoint center;
            center = CGPointMake([Utils currentWidth]/2, self.frame.size.height);
            CGRect frmm=self.frame;
            frmm.size.height=frmm.size.height+20;
            self.frame=frmm;
            logoView.center=center;
            CGRect btnfrmm=btnMNU.frame;
            btnfrmm.origin.y=btnfrmm.origin.y+5;
            btnMNU.frame=btnfrmm;
            
        }
        
    }
    

    logoView.alpha = 0;
}

- (void) createTitle {
    titleLabel = [[UILabel alloc] init];
    subTitleLabel = [[UILabel alloc] init];
    CGRect frame = self.frame;
    CGRect frame2 = self.frame;

   
    frame2.size.width -= 88;
    frame2.origin.x += 44;

 
    frame.size.width -= 88;
    
    frame.origin.x += 44;

    titleLabel.frame = frame;

    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    subTitleLabel.frame = frame2;
    subTitleLabel.font = [UIFont boldSystemFontOfSize:14];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.alpha=0.6;
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLabel];
    [self addSubview:subTitleLabel];
}

- (void) awakeFromNib {
    
    [self setup];
    [super awakeFromNib];
}

- (void) addHandle {
    handle = [[UIImageView alloc] initWithImage:[Utils imageFromColor:RGBA(255,255,255,.12) withWidth:self.frame.size.width andHeight:self.frame.size.height]];
   
    [self addSubview:handle];
    handle.frame = self.frame;
    handle.autoresizingMask = UIViewAutoresizingAll;
}

- (void) addHandleShadow {
    handleShadow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu_bottom_shadow.png"]];
    [self addSubview:handleShadow];
    CGRect frame = self.frame;
    frame.size.width = [Utils currentWidth];
    frame.size.height = 5;
    frame.origin.y = self.frame.size.height;
    handleShadow.frame = frame;
}

-(void) layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame;
    CGRect frame2;
    if (IS_DEVICE==IS_IPHONE) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
    frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y+3, [Utils currentWidth] - 88, titleLabel.frame.size.height);
    titleLabel.frame = frame;
    frame2 = CGRectMake(subTitleLabel.frame.origin.x, titleLabel.frame.origin.y+23 , [Utils currentWidth] - 88, subTitleLabel.frame.size.height);
    subTitleLabel.frame = frame2;
        }
        else
        {
    frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y-6, [Utils currentWidth] - 88, titleLabel.frame.size.height);
    titleLabel.frame = frame;
    frame2 = CGRectMake(subTitleLabel.frame.origin.x, titleLabel.frame.origin.y+17 , [Utils currentWidth] - 88, subTitleLabel.frame.size.height);
    subTitleLabel.frame = frame2;
        }
    }
    
    frame = CGRectMake(handleShadow.frame.origin.x, handleShadow.frame.origin.y,[Utils currentWidth],handleShadow.frame.size.height);
    handleShadow.frame = frame;
    frame2 = CGRectMake(subTitleLabel.frame.origin.x, titleLabel.frame.origin.y+17 , [Utils currentWidth] - 88, subTitleLabel.frame.size.height);
    subTitleLabel.frame = frame2;
    
    frame2 = CGRectMake(handleShadow.frame.origin.x, handleShadow.frame.origin.y,[Utils currentWidth],handleShadow.frame.size.height);
    handleShadow.frame = frame2;
    
}
@end


#import <Foundation/Foundation.h>

#define CACHE_FOLDER @"/cache"

@class PlaylistSong;
@class CachedPlaylistSong;
@class CachedLocalPlaylistSong;
@class CachedPlaylist;
@class CachedLocalPlaylist;

@interface SongCache : NSObject

@property(nonatomic, strong) PlaylistSong *song;

+ (SongCache *)initWithSong:(PlaylistSong *)song;



+ (NSArray *)songsOnlyInCachedPlaylist:(CachedPlaylist *)cachedPlaylist;

+ (NSArray *)songsNotCompleted;

+ (void)deleteSongFromCache:(CachedPlaylistSong *)song1;

- (void) saveToCachedPlaylist:(CachedPlaylist *)playlist;

- (void) saveToCachedPlaylistNoDownload:(CachedLocalPlaylist *)playlist andOrder:(NSNumber *)order;



+ (NSMutableArray *)mostPlayedSongsAsPlaylistSongs;

+ (NSMutableArray *)cachedPlaylistSongsAsPlaylistSongs:(NSArray *)cachedPlaylistSongs;

+ (NSMutableArray *)cachedPlaylistSongsAsPlaylistSongsNoDownload:(NSArray *)cachedPlaylistSongs;

- (BOOL)isCached;

+ (NSArray *) songsOnlyInCachedLocalPlaylist:(CachedLocalPlaylist *)cachedLocalPlaylist;

+ (void) deleteSongFromCoreDataNoDownload:(CachedLocalPlaylistSong *)song;

- (void) updateNewPlaylistSong;

+ (NSArray *) songsOnlyInCachedMusicFeed:(CachedLocalPlaylist *)cachedLocalPlaylist;

@end

#import "SuggestionCell.h"

@implementation SuggestionCell

@synthesize label, icon;

- (void) awakeFromNib {
    self.backgroundColor = [UIColor clearColor];

    //add the cell bottom border
    UIImageView* borderBottom = [[UIImageView alloc] initWithImage:[Utils imageFromColor:RGBA(255,255,255,.1)]];
    borderBottom.frame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1);
    borderBottom.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    [self addSubview:borderBottom];

    //setup the label position to the right
    label = [[UILabel alloc] init];
    CGRect labelFrame = self.frame;
    labelFrame.origin.x = 44;
    labelFrame.size.width -= 44;
    label.frame = labelFrame;
    [self addSubview:label];

    label.font = [UIFont boldSystemFontOfSize:14];
    label.textColor = [UIColor whiteColor];

    //setup the icon
    icon = [[UIImageView alloc] init];
    icon.frame = CGRectMake(15, 0, 12, PLAYLISTS_CELL_HEIGHT);
    icon.contentMode = UIViewContentModeCenter;
    [self addSubview:icon];
}

@end
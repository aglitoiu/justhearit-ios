#import "Settings.h"

@implementation Settings

+ (Settings *) defaultSettings {
    Settings *settings = [[Settings alloc] init];

    settings.disableAds = @YES;
    settings.firstAdAfterMinutes = @15;
    settings.firstAdLength = @10;
    settings.normalAdsLength = @60;
    settings.normalAdsAfterMinutes = @25;

    return settings;
}

- (BOOL)adsDisabled {
    return _disableAds.boolValue;
}


@end
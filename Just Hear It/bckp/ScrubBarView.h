#import <UIKit/UIKit.h>

#import "OBSlider.h"
#import "UIView+Geometry.h"
#import "SongTime.h"
#import "Player.h"
#import "JEProgressView.h"
#import "AKProgressView.h"

@class ScrubBarView;

@protocol ScrubBarViewDelegate <NSObject>

- (void) scrubBarSpeedChanged:(CGFloat)speed;
- (void) scrubBarDragStarted;
- (void) scrubBarDragEnded;

@end

@interface ScrubBarView : UIView

@property (nonatomic, strong) OBSlider *slider;
@property (nonatomic, strong) AKProgressView *audioLoaded;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) SongTime* songTime;
@property (nonatomic) CGRect lastKnownThumbRect;
@property (nonatomic, assign) BOOL isDragging;
@property (nonatomic, assign) BOOL isCache;

@property (weak) id <ScrubBarViewDelegate> delegate;

- (void)setup;

@end

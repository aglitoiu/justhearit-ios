//
//  NWReachabilityManger.m
//  Just Hear It
//
//  Created by Goiceanu Ciprian on 17/11/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "NWReachabilityManger.h"
#import "Reachability.h"

@implementation NWReachabilityManger

#pragma mark -
#pragma mark Default Manager
+ (NWReachabilityManger *)sharedManager {
    static NWReachabilityManger *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

#pragma mark -
#pragma mark Memory Management
- (void)dealloc {
    // Stop Notifier
    if (_reachability) {
        [_reachability stopNotifier];
    }
}

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable {
    return [[[NWReachabilityManger sharedManager] reachability] isReachable];
}

+ (BOOL)isUnreachable {
    return ![[[NWReachabilityManger sharedManager] reachability] isReachable];
}

+ (BOOL)isReachableViaWWAN {
    return [[[NWReachabilityManger sharedManager] reachability] isReachable];
}

+ (BOOL)isReachableViaWiFi {
    return [[[NWReachabilityManger sharedManager] reachability] isReachable];
}

#pragma mark -
#pragma mark Private Initialization
- (id)init {
    self = [super init];
    
    if (self) {
        // Initialize Reachability
        self.reachability = [Reachability reachabilityForInternetConnection];
        
        // Start Monitoring
        [self.reachability startNotifier];
    }
    
    return self;
}

@end

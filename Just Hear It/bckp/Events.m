#import "Events.h"

@implementation Events {
@private
    NSString *_headsetInsert;
    NSString *_killQueue;
    NSString *_songStartedPlay;
}


@synthesize songStartedBuffering, playPauseButtonHit, songPaused, playlistModified, playlistSongDeleted, songProgressChanged, nowPlayingWasModified,
songResumed, prevButtonHit, nextButtonHit, playlistSelected, playlistRefresh, openInputTextViewController, playlistsInitialized, songProgressManuallyChanged,
enableQueuePlaylist, disableQueuePlaylist, realtimeSearch, submitSearch, forcePause, forcePlay, messageCreateFromSearch, playlistSelectedWithoutClosePlaylistsDrawer;

@synthesize findPlaylists, findPlaylistSongs, findPlaylistSongsInPage, findTimeout, requestEnd, requestStart, fbAuthenticationDone, fbAuthenticationFail, logoutDone, classicLoginDone, invalidToken,
restricted, classicLoginFail, loadLyrics, audioPercentageAvailable, findPlaylistsFromSearch, songMovedInPlaylist;

@synthesize searchClicked, getAutocompleteResults, loadSongsFromUrl;

@synthesize adTick, adSongStopped;

@synthesize searchButtonTriggered, loginButtonTriggered;
@synthesize genreButtonHit;
@synthesize albumButtonHit;
@synthesize artistButtonHit;
@synthesize findPlaylistSongsForceUpdate = _findPlaylistSongsForceUpdate;
@synthesize headsetUnplugged = _headsetUnplugged;
@synthesize headsetInsert = _headsetInsert;
@synthesize killQueue = _killQueue;
@synthesize songStartedPlay = _songStartedPlay;

@synthesize reachabilityChanged, playlistsShouldReload,playlistsShouldReloadOffline, navigationBarShouldReload, playlistsNotification;

@synthesize shouldHideSpinner;
@synthesize shouldUpdateNewSongs;
@synthesize isScrubbing;

+ (Events *)sharedInstance {
    static Events *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Events alloc] init];
        
        sharedInstance.realtimeSearch = @"search:realtime";
        sharedInstance.submitSearch = @"search:submit";
        sharedInstance.songStartedBuffering = @"song:buffering";
        sharedInstance.songStartedPlay = @"song:play";
        sharedInstance.songPaused = @"song:pause";
        sharedInstance.songResumed = @"song:resume";
        sharedInstance.playPauseButtonHit = @"buttons:playpause:hit";
        sharedInstance.prevButtonHit = @"buttons:prev:hit";
        sharedInstance.nextButtonHit = @"buttons:next:hit";
        sharedInstance.playlistSelected = @"playlist:selected";
        sharedInstance.playlistSelectedWithoutClosePlaylistsDrawer = @"playlist:selected:withoutcloseplaylistsdrawer";
        sharedInstance.playlistRefresh = @"playlist:refresh";
        sharedInstance.openInputTextViewController = @"inputTextVC:open";
        sharedInstance.playlistModified = @"playlist:modified";
        sharedInstance.playlistSongDeleted = @"playlistsong:deleted";
        sharedInstance.playlistsInitialized = @"playlists:initialized";
        sharedInstance.songProgressChanged = @"song:progress:changed";
        sharedInstance.songProgressManuallyChanged = @"song:progress:manuallychanged";
        sharedInstance.nowPlayingWasModified = @"nowplaying:modified";
        sharedInstance.enableQueuePlaylist = @"nowplaying:queue:enableplaylist";
        sharedInstance.disableQueuePlaylist = @"nowplaying:queue:disableplaylist";
        sharedInstance.forcePause = @"player:forcepause";
        sharedInstance.forcePlay = @"player:forceplay";
        sharedInstance.messageCreateFromSearch = @"create_from_search";

        sharedInstance.loginButtonTriggered = @"mobile:loginbutton:click";
        sharedInstance.searchButtonTriggered = @"mobile:searchbutton:click";

        sharedInstance.findPlaylists = @"playlist:findAll";
        sharedInstance.findPlaylistsFromSearch = @"playlist:findAll:fromSearch";
        sharedInstance.findPlaylistSongs = @"playlist_songs:find";
        sharedInstance.findPlaylistSongsInPage = @"playlist_songs:find:in_page";
        sharedInstance.findTimeout = @"find:timeout";
        sharedInstance.requestEnd = @"request:end";
        sharedInstance.requestStart = @"request:start";
        sharedInstance.fbAuthenticationDone = @"fbauth:done";
        sharedInstance.fbAuthenticationFail = @"fbauth:fail";
        sharedInstance.logoutDone = @"logout:done";
        sharedInstance.classicLoginDone = @"login:done";
        sharedInstance.classicLoginFail = @"login:fail";
        sharedInstance.invalidToken = @"login:token:invalid";
        sharedInstance.restricted = @"restricted";
        sharedInstance.loadLyrics = @"lyrics:load";
        sharedInstance.audioPercentageAvailable = @"audio:percentage:update";

        sharedInstance.searchClicked = @"search:clicked";
        sharedInstance.getAutocompleteResults = @"search:autocomplete_results";
        sharedInstance.songMovedInPlaylist = @"song:movedInPlaylist";

        sharedInstance.genreButtonHit = @"playerView:genrebutton:hit";
        sharedInstance.albumButtonHit = @"playerView:albumbutton:hit";
        sharedInstance.artistButtonHit = @"playerView:artistbutton:hit";

        sharedInstance.reachabilityChanged = @"reachability:changed";
        sharedInstance.playlistsShouldReload = @"playlists:should_reload";
        sharedInstance.playlistsShouldReloadOffline = @"playlists:should_reloadoffline";
        sharedInstance.navigationBarShouldReload = @"navigation_bar:should_reload";
        sharedInstance.gotOffline = @"gotOffline:should_reload";
        sharedInstance.gotOnline = @"gotOnline:should_reload";

        sharedInstance.findPlaylistSongsForceUpdate = @"playlistSongs:forceUpdate";
        sharedInstance.playlistsNotification = @"playlist:notification";
        sharedInstance.playlistsChangeNotification = @"playlist:changeNotification";
        
        sharedInstance.headsetUnplugged = @"headset:unplugged";
        sharedInstance.headsetInsert = @"headset:insert";

        sharedInstance.killQueue = @"queue:kill";

        sharedInstance.loadSongsFromUrl = @"loadsongs:url";
        sharedInstance.adTick = @"ad:tick";
        sharedInstance.adSongStopped = @"ad:song:stop";
        
        sharedInstance.shouldHideSpinner = @"spinner:hide";
        sharedInstance.shouldUpdateNewSongs = @"songs:updatenew";
        sharedInstance.isScrubbing = @"playerView:scrubbing";
        
    });
    return sharedInstance;
}

@end

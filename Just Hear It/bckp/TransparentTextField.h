#import <Foundation/Foundation.h>

@class TransparentTextField;

@protocol TransparentTextFieldDelegate <NSObject>

- (void) transparentTextFieldTextCleared:(TransparentTextField*) transparentTextField;

@end

@interface TransparentTextField : UITextField

@property (nonatomic, strong) UIButton *clearButton;
@property (nonatomic, strong) UIImageView *leftSideIcon;
@property (nonatomic, strong) UIView *paddingView;
@property (nonatomic, assign) BOOL initialized;
@property (weak) id <TransparentTextFieldDelegate> transparentTextFieldDelegate;

- (void)setLeftSideIconImage:(UIImage *)leftSideIconImage;

- (void)initialize;

- (void)changeText:(NSString *)text;

- (void) doClear:(id) sender;

- (void) handleClearButton:(id) sender;


@end


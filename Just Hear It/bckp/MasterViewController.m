#import "MasterViewController.h"
#import "Playlists.h"
#import "PlaylistsViewController.h"
#import "AudioPlayer.h"
#import "TargetSpotDelegate.h"
#import "TargetSpot.h"
#import "AdManager.h"
#import "TourViewController.h"
#import "FrostyControlsView.h"
#import "SongCache.h"
#import "CachedPlaylistSong.h"
#import "CachedPlaylistSong+Utils.h"
#import "NWReachabilityManger.h"
#import "Bluuur.h"

@implementation MasterViewController
static int isOffline=0;
@synthesize playlists, playlistsViewController, mainViewController, loginViewController, adView, navigationBar, navigationBackground, searchButton, playerView, playerStarted;

- (void)viewDidLoad {
    [super viewDidLoad];
    //SETTINGS.welcomeMessage = YES;
    
    //FAKFontAwesome *searchIcon = [FAKFontAwesome searchIconWithSize:48];
    //[searchIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    //searchIcon.iconFontSize = 19;
    
    //UIImage *searchIconImage = [searchIcon imageWithSize:CGSizeMake(20, 20)];
    
    //[searchButton setImage:searchIconImage forState:UIControlStateNormal];
    
    //_adManager = [AdManager sharedInstance];
    //_adManager.delegate = self;
    self.allowRightSwipe = YES;
    self.allowLeftSwipe = YES;
    self.recognizesPanGesture = YES;
    self.delegate=self.centerPanel;
    
    if(IS_DEVICE == IS_IPAD) { /*self.leftGapPercentage = 0.4f; self.shouldResizeLeftPanel = YES;*/ self.leftFixedWidth = LEFTPANEL_WIDTH_IPAD;}

    /*@try {
        adView = [TargetSpotAdView adView];
        adView.delegate = self;
        [adView setDesiredPlaybackLengths:[NSArray arrayWithObject:[NSNumber numberWithInt:90]]];
        [self.view addSubview:adView];
        [adView startAdSession];
    }
    @catch (...) { }*/
    
    if(IS_DEVICE == IS_IPAD) {
        
        [self showLeftPanelAnimated:NO];
        
        self.allowRightSwipe = NO;
        self.allowLeftSwipe = NO;
        self.recognizesPanGesture = NO;
        
        navigationBar.frame = CGRectMake(navigationBar.frame.origin.x, navigationBar.frame.origin.y, [Utils currentWidth], navigationBar.frame.size.height);
        
        [self.view bringSubviewToFront:navigationBackground];
        [self.view bringSubviewToFront:navigationBar];
        
        [self refreshNavigationBar];
        
        int width = [Utils currentWidth];
        int height = [Utils currentHeight];
        
        [playerView initialize];
        playerView.delegate = self;
        playerView.frostyControlsView.mainViewController = mainViewController;
        [playerView initPositions:width :height];
        
        [self hidePlayer];
        
        [self.view bringSubviewToFront:playerView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshNavigationBar) name:Ev.navigationBarShouldReload object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(askForOffline) name:Ev.gotOffline object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setOnline) name:Ev.gotOnline object:nil];
        navigationBar.logoView.center = CGPointMake([Utils currentWidth]/2, navigationBar.logoView.center.y);
        
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadPlaylistSongsFromUrlPath:) name:Ev.loadSongsFromUrl object:nil];
        
    }else {
        
        [navigationBar setHidden:YES];
        [navigationBackground setHidden:YES];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshNavigationBar) name:Ev.navigationBarShouldReload object:nil];
        
    }
    /* from awake from nib */
    if(IS_DEVICE == IS_IPHONE) {
        
        ///[self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"playlistsViewController"]];
        ///[self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"]];
        
        playlists = [Playlists sharedInstance];
        playlistsViewController = (PlaylistsViewController *)self.leftPanel;
        mainViewController = (MainViewController*)self.centerPanel;
        
        [playlistsViewController loadPlaylists];
    }
    else{
        playlists = [Playlists sharedInstance];
        playlistsViewController = (PlaylistsViewController *)self.leftPanel;
        if(IS_DEVICE == IS_IPAD)
            mainViewController = (MainViewController*)self.centerPanel;
        
        
        [playlistsViewController loadPlaylists];

    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playlistsN:) name:Ev.playlistsNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mhandlePN:) name:Ev.shouldUpdateNewSongs object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playlistsCN:) name:Ev.playlistsChangeNotification object:nil];
    /* from awake from nib */
    
    //[self performSelector:@selector(adButtonPressed:) withObject:self afterDelay:0.1];
}


-(void) disableLeftRight{
    self.allowRightSwipe = NO;
    self.allowLeftSwipe = NO;
    self.recognizesPanGesture = NO;
}
-(void) enableLeftRight{
    self.allowRightSwipe = YES;
    self.allowLeftSwipe = YES;
    self.recognizesPanGesture = YES;
}
- (IBAction)StartOffline:(id)sender {
    if(IS_DEVICE==IS_IPAD){
       

    if(isOffline==1)
    {
        [navigationBar showRedHandle];
        [navigationBar showTitle:@"Off the grid" :@"Tap to go online"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
        
        
        isOffline=2;
    }
    else if(isOffline==2)
    {
        
        [self checkForOnline];
    }
    else if(isOffline==3)
    {
        
        if([NWReachabilityManger isReachable])
        {
            [SETTINGS setOfflineOnly:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
            
            [navigationBar showNormalHandle];
            [navigationBar showLogo];
            isOffline=0;
        }
    }
    }
    
    
}
- (void) refreshNavigationBar {
    
    // Log(@"isReachable: %d", [NWReachabilityManger isReachable] && !playlists.offlineMode);
    if(SETTINGS.OfflineOnly==YES){
        
        [navigationBar showRedHandle];
        [navigationBar showTitle:@"Off the grid" :@"Tap to go online"];
        isOffline=3;
        
        
        
        
        
    }
    else if(isOffline==0)
    {
        [navigationBar showNormalHandle];
        [navigationBar showLogo];
    }
    else if(isOffline==1&&SETTINGS.OfflineOnly==NO)
    {
        [navigationBar showHalfRedHandle];
        [navigationBar showTitle:@"Connection lost" :@"Tap to go off the grid"];
    }
    else if(isOffline==2)
    {
        [navigationBar showRedHandle];
        [navigationBar showTitle:@"Off the grid" :@"Tap to go online"];
    }
    
    
}




-(void) askForOffline{
    if(IS_DEVICE==IS_IPAD)
    {
    if(isOffline==0)
    {
        [navigationBar showHalfRedHandle];
        [navigationBar showTitle:@"Connection lost" :@"Tap to go off the grid"];
        isOffline=1;
    }
    else if(isOffline==4)
    {
        [navigationBar showHalfRedHandle];
        [navigationBar showTitle:@"Connection lost" :@"Tap to go off the grid"];
        
    }
    }
    
}
-(void) setOnline
{
    if(IS_DEVICE==IS_IPAD)
    {
    if(isOffline==1)
    {
        
        [navigationBar showNormalHandle];
        [navigationBar showLogo];
        isOffline=0;
        
    }
    else if(isOffline==4)
    {
        [navigationBar showNormalHandle];
        [navigationBar showLogo];
        isOffline=0;
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
    }
    }
}
-(void) checkForOnline{
    if(IS_DEVICE==IS_IPAD)
    {
    if(isOffline==2)
    {
        
        [navigationBar showTitle:@"Refreshing" :@"Please wait"];
        if([NWReachabilityManger isReachable])
        {
            isOffline=0;
            [navigationBar showNormalHandle];
            
            //
            [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
            [navigationBar showLogo];
            
        }
        else
        {
            isOffline=4;
            [self askForOffline];
        }
    }
    }
    
    
}

-(BOOL) alreadyOffline{
    if (isOffline!=0) {
        return YES;
        
    }
    else
    {
        return NO;
    }
    
}

- (void) mhandlePN:(NSNotification *)notification {
    Log(@"handle");
    [mainViewController mhandlePlaylistNotification:notification];
}
- (void) playlistsN:(NSNotification *)notification {
    Log(@"handle p");
    [playlistsViewController playlistsNotification:notification];
}
- (void) playlistsCN:(NSNotification *)notification {
    Log(@"playlists have changed");
    [playlistsViewController playlistsChangeNotification:notification];
}
- (void) showPlayer {

    [playerView closeView];
    [mainViewController setSongsTablePlayerScreen];
    
}

- (void) hidePlayer {
    playerStarted=NO;
    [playerView hideView];
    [mainViewController setSongsTableFullScreen];
    
}

- (void)pullableView:(PullableView *)pView movingToYOffset:(CGFloat)offset {
    [playerView offsetBackground:offset];
}

- (void)pullableView:(PullableView *)pView didChangeState:(int)state {
    if (state == STATE_OPEN && playlistsViewController.playlistsMode == playlistsViewControllerAddTo) {
        [playlistsViewController setNormalMode];
    }
}

- (void)pullableView:(PullableView *)pView willChangeStateFromState:(int)fromState toState:(int)toState {
    if (toState == STATE_HIDDEN) {
        [UIView animateWithDuration:.2 animations:^{
            [mainViewController setSongsTableFullScreen];
        }];
    } else if (toState == STATE_CLOSED && fromState == STATE_HIDDEN) {
        [UIView animateWithDuration:.2 animations:^{
            [mainViewController setSongsTablePlayerScreen];
        }];
    }
    
    if (toState == STATE_OPEN) {
        [playerView.frostyControlsView showAllButtonsAnimated:YES];
    } else {
        [playerView.frostyControlsView showThreeButtonsAnimated:YES];
    }
}

- (void)pullableViewStartedDrag:(PullableView *)pView fromState:(int)state {
    [Analytics event:@"player_view":@"drag"];
}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(IS_DEVICE==IS_IPAD)
    {
    navigationBar.logoView.center = CGPointMake([Utils currentWidth]/2, navigationBar.logoView.center.y);
    [playerView repositionOnRotation:[Utils currentWidth] :[Utils currentHeight]];
    
    [mainViewController repositionOnRotation:[Utils currentWidth] :[Utils currentHeight]];
    
    
    //[self.sidePanelController styleContainer:self.sidePanelController.centerPanel.view animate:NO duration:0.0f];
    [playerView repositionOnRotation:[Utils currentWidth] :[Utils currentHeight]];
    
    if([[Player sharedInstance] currentPlayingSong]) {
        //[playerView closeView];
        [playerView setState:STATE_CLOSED animated:YES];
    }else {
        //[playerView hideView];
        [playerView setState:STATE_HIDDEN animated:YES];
    }
    }

    [self becomeFirstResponder];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (IBAction)openSearchViewController {
    
    [self performSegueWithIdentifier:@"searchSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"discoverSegue"]) {
        
        
        DiscoverViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DiscoverViewC"];
        
        
        
        UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
        MLWBluuurView *beView = [[MLWBluuurView alloc] initWithEffect:blurEffect];
        beView.blurRadius=10;
        beView.frame = self.view.bounds;
        
        vc.view.frame = self.view.bounds;
        vc.view.backgroundColor = [UIColor clearColor];
        [vc.view insertSubview:beView atIndex:0];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.delegate = self.mainViewController;
        vc.song = sender;
        alert(vc.song.name);
        [self presentViewController:vc animated:YES completion:nil];
        
        
        
        
    }
    else if ([segue.identifier isEqualToString:@"tourSegue"]) {
        TourViewController *lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"TourViewC"];
        
        
        
        UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
        MLWBluuurView *beView = [[MLWBluuurView alloc] initWithEffect:blurEffect];
        beView.blurRadius=10;
        beView.frame = self.view.bounds;
        
        lvc.view.frame = self.view.bounds;
        lvc.view.backgroundColor = [UIColor clearColor];
        [lvc.view insertSubview:beView atIndex:0];
        lvc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        lvc.delegate = self;
        [self presentViewController:lvc animated:YES completion:nil];
        
        
        
        /* vc.view.frame = self.view.bounds;
         vc.view.backgroundColor = [UIColor clearColor];
         
         
         
         //lvc.preferredContentSize=CGSizeMake([Utils currentWidth],[Utils currentHeight]);
         
         UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
         
         MLWBluuurView *beView = [[MLWBluuurView alloc] initWithEffect:blurEffect];
         beView.frame = self.view.bounds;
         beView.blurRadius=10;
         [vc.view insertSubview:beView atIndex:0];
         UIVibrancyEffect *vib=[UIVibrancyEffect effectForBlurEffect:blurEffect];
         UIVisualEffectView *vibView=[[UIVisualEffectView alloc  ] initWithEffect:vib];
         vibView.frame = self.view.bounds;
         
         
         
         
         
         [beView.contentView addSubview:vibView];
         //[beView addSubview:vibView];
         
         
         
         vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;*/
        
        
        // [self presentViewController:vc animated:YES completion:nil];
        
        
    }
    else if([segue.identifier isEqualToString:@"searchSegue"]) {
        SearchViewController * vc = (SearchViewController*) segue.destinationViewController;
        vc.delegate = self;
    }
}

- (IBAction)adButtonPressed:(id)sender {
    //see if any breaks are available
    int anyBreakAvail = [adView breakAvail];
    Log(@"--- GET AD --- anyBreakAvail = %i",anyBreakAvail);
    /*if(anyBreakAvail == 0) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"justhearit"
                              message: [NSString stringWithFormat:@"Breaks available: %d",anyBreakAvail]
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];
        [alert show];
    }*/
   
    //set the new desired break lengths
    Log(@"set desired playback break lengths");
    [adView setDesiredPlaybackLengths:[NSArray arrayWithObject:[NSNumber numberWithInt:90]]];
    //refresh
    Log(@"---END GET AD --- refresh new ads");
    [adView refreshAds];
}

#pragma mark -
#pragma tour view controller delegate

- (void)tourViewControllerDidFinish:(TourViewController *)controller {
    SETTINGS.tutorialDisabled = [NSNumber numberWithInt:1];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark -
#pragma ad manager delegate

- (void)adManager:(AdManager *)manager fireAds:(AdDetails *)ads {
    //_adManager.firstAdShown = YES;
    //[self presentAdDetails:ads];
}

- (void) presentAdDetails:(AdDetails *)details {
    @try {
        if (details.adType == JusthearitAdTypePreroll) {
//            [adView showPrerollAd];
            [adView showAnimated:YES];
        } else {
            [adView showAnimated:YES];
        }
    }
    @catch (...) { }
}

#pragma mark -
#pragma target spot delegate

- (NSString *)targetSpotStation {
    return @"JUSTHEARIT";
}

- (BOOL)targetSpotTestMode {
    return YES;
}

- (NSInteger)targetSpotAdPlaybackLength {
    return _adDetailsToShow.seconds;
}

- (void)targetSpotAdViewDidFinishAction:(TargetSpotAdView *)adView {
    _adDetailsToShow = nil;
    [adView hideAnimated:YES];
    [[AudioPlayer sharedInstance] resumeHaltedSong];
}

- (void)targetSpotAdViewWillDisplayAd:(TargetSpotAdView *)adView {

}

- (void)targetSpotAdViewDidHideAd:(TargetSpotAdView *)adView {

}
- (BOOL)targetSpotManagedMode
{
    return NO;
}
#pragma mark - remote control events
- (void) remoteControlReceivedWithEvent: (UIEvent *) receivedEvent {
    [[AudioPlayer sharedInstance] remoteControlReceivedWithEvent:receivedEvent];
}

-(void) awakeFromNib {
    
    if(IS_DEVICE == IS_IPAD) {
        
       [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"playlistsViewController"]];
        [self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"]];
        
          }else {
        
        [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"playlistsViewController"]];
        [self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"]];
        
    }
}

- (void) playlistsChanged {
    [playlistsViewController.playlistsTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)stylePanel:(UIView *)panel {
    panel.clipsToBounds = YES;
}

#pragma mark -
#pragma mark Search View controller delegate

- (void)searchViewControllerDidFinish:(SearchViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)openAfterSearch:(MainViewController *)mainVC andPlaylist:(id)playlist{
    
    [mainViewController loadPlaylistSongs:playlist fromScratch:YES];
    
}

- (void) testOpenPlayer:(int)row {
    
    NSMutableArray* songList;
    
    playlists = [Playlists sharedInstance];
    songList = playlists.openedPlaylist.songs;
    
    playerStarted = YES;
    [Analytics event:@"main_view_controller":@"hit_play_song"];
    [playerView setStateClosedAnimated];
    
    [[Player sharedInstance] playSong:songList[row] fromPlaylist:playlists.openedPlaylist];
    
    PlaylistsViewController *playlistsViewController = (PlaylistsViewController *)self.sidePanelController.leftPanel;
    [playlistsViewController.playlistsTable reloadData];
    
    [mainViewController.songsTable reloadData];
    
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
   if(IS_DEVICE==IS_IPAD)
   {
    
    navigationBar.logoView.center = CGPointMake([Utils currentWidth]/2, navigationBar.logoView.center.y);
   // [navigationBar refreshTitle];
   
    [mainViewController repositionOnRotation:[Utils currentWidth] :[Utils currentHeight]];


    //[self.sidePanelController styleContainer:self.sidePanelController.centerPanel.view animate:NO duration:0.0f];
    [playerView repositionOnRotation:[Utils currentWidth] :[Utils currentHeight]];
    
    if([[Player sharedInstance] currentPlayingSong]) {
        //[playerView closeView];
        [playerView setState:STATE_CLOSED animated:YES];
    }else {
        //[playerView hideView];
        [playerView setState:STATE_HIDDEN animated:YES];
    }
}
}
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [mainViewController repositionBackground];
}
/*-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    NSLog(@"current width: %d", [Utils currentWidth]);
    NSLog(@"current heigth: %d", [Utils currentHeight]);
    
    navigationBar.logoView.center = CGPointMake([Utils currentWidth]/2, navigationBar.logoView.center.y);
    
    [playerView repositionOnRotation:[Utils currentWidth] :[Utils currentHeight]];
    
    //[self hidePlayer];
}*/
#pragma mark -
#pragma mark SongCell Delegate

- (void)cellDidSelectAddTo:(SongCell *)cell {
    [mainViewController addToWithPlaylistSongs:@[cell.song]];
}

- (void)cellDidSelectDiscover:(SongCell *)cell {
    [mainViewController performSegueWithIdentifier:@"discoverSegue" sender:cell.song];
}

- (void)cellDidSelectShare:(SongCell *)cell {
    [mainViewController presentViewController:[Utils sharingViewControllerForSong:cell.song] animated:YES completion:nil];
}

- (void)cellDidSelectRemove:(SongCell *)cell {
    
    PlaylistSong *song = cell.song;
    
    [song.playlist.songs removeObject:song];
    
    if (song.isPlayingOrPaused) [mainViewController stopMusic];
    
    if (song.playlist.isPlayingOrPaused) {
        [[Player sharedInstance] refreshNextSongsInPlaylist];
    }
    
    NSIndexPath *indexPath = [mainViewController.songsTable indexPathForCell:cell];
    
    [mainViewController.songsTable beginUpdates];
    [mainViewController.songsTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [mainViewController.songsTable endUpdates];
    
    [[JusthearitClient sharedInstance] deletePlaylistSong:song withSuccessBlock:^(NSArray *array) {
    }                                      withErrorBlock:^(NSError *error) {
    }];
    
    //NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"(playlistSongId = %d) AND (playlistId = %d)", song.playlistSongId, song.playlistId];
    
    //CachedPlaylistSong *existingSong = [CachedPlaylistSong MR_findFirstWithPredicate:predicate1];
    
    CachedPlaylistSong *existingSong = [CachedPlaylistSong MR_findFirstByAttribute:@"playlistSongId" withValue:song.playlistSongId];
    
    if(existingSong) {
        [SongCache deleteSongFromCache:existingSong];
    }
}
@end

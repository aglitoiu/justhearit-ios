#import "JusthearitSlider.h"

@implementation JusthearitSliderView

@synthesize label, loaded, isDragging, slider,sliderProgress,sliderProgress2;

- (void) setup {
    sliderProgress = [[UISlider alloc] init];
    slider = [[UISlider alloc] init];
    
    //    [slider addTarget:self action:@selector(sliderPositionChanged:) forControlEvents:UIControlEventValueChanged];
    //    [slider addTarget:self action:@selector(sliderEndDrag:) forControlEvents:UIControlEventTouchUpInside];
    //    [slider addTarget:self action:@selector(sliderEndDrag:) forControlEvents:UIControlEventTouchUpOutside];
    //    [slider addTarget:self action:@selector(sliderStartDrag:) forControlEvents:UIControlEventTouchDown];
    
    CGRect sliderFrame = CGRectMake(0, 0, 320, 15);
    sliderProgress.frame=sliderFrame;
    sliderProgress.maximumValue=1;
    sliderProgress2.frame=sliderFrame;
    sliderProgress2.maximumValue=1;
    slider.frame = sliderFrame;
    slider.maximumValue = 1;
    
    CGFloat inset = 7+17.5f;
    
    UIImage *trackSlider = [Utils image:[[UIImage imageNamed:@"scrub_bar_background.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, inset, 0, inset)] byApplyingAlpha:0.4f];
    UIImage *backgroundSlider = [Utils image:[[UIImage imageNamed:@"scrub_bar_background.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, inset, 0, inset)] byApplyingAlpha:0.2f];
    
    NSLog(@"<<<<<<<<< PROGRESS VIEW 1 >>>>>>>>>");
    
    loaded = [[JEProgressView alloc] initWithFrame:sliderFrame];
    
    [loaded setProgressImage:backgroundSlider];
    [loaded setTrackImage:trackSlider];
    loaded.frame = sliderFrame;
    CGRect audioLoadedFrame = loaded.frame;
    audioLoadedFrame.size.width -= (35.5f);
    loaded.frame = audioLoadedFrame;
    loaded.center = slider.center;
    
    audioLoadedFrame = loaded.frame;
    audioLoadedFrame.origin.x += 2;
    loaded.frame = audioLoadedFrame;
    
    loaded.autoresizingMask = slider.autoresizingMask;
    
    
    [sliderProgress setMinimumTrackImage:[[UIImage imageNamed:@"scrub_progress_gradient.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, inset, 0, inset)] forState:UIControlStateNormal];
    [sliderProgress setMaximumTrackImage:[Utils imageFromColor:[UIColor clearColor] withWidth:1 andHeight:15] forState:UIControlStateNormal];
    [sliderProgress setThumbImage:[Utils imageFromColor:[UIColor clearColor] ]forState:UIControlStateNormal];
    
    
    
    [slider setMinimumTrackImage:[Utils imageFromColor:[UIColor clearColor] withWidth:1 andHeight:15] forState:UIControlStateNormal];
    [slider setMaximumTrackImage:[Utils imageFromColor:[UIColor clearColor] withWidth:1 andHeight:15] forState:UIControlStateNormal];
    [slider setThumbImage:[UIImage imageNamed:@"scrub_handle.png"] forState:UIControlStateNormal];
    
    label = [[UILabel alloc] init];
    CGRect timeFrame = sliderFrame;
    timeFrame.size.width -= 22;
    label.frame = timeFrame;
    label.textColor = RGBA(255,255,255,.6);
    label.font = [UIFont boldSystemFontOfSize:14];
    
    label.textAlignment = NSTextAlignmentRight;
    label.text = @"";
    sliderProgress2.alpha=0.6;
    [self addSubview:loaded];
    [self addSubview:sliderProgress];

    [self addSubview:slider];
    [self addSubview:label];
    
}

@end

//
//  Utils.m
//  Just Hear It
//
//  Created by Andrei S on 9/7/12.
//  Copyright (c) 2012 Hearit Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "Utils.h"
#import "PlaylistSong.h"
#import "JusthearitClient.h"
#import "Playlist.h"
#import "CopyLinkCustomActivity.h"
#import "Playlists.h"
#import "CJAMacros.h"

@implementation Utils

+(BOOL) isPortrait {
    UIInterfaceOrientation o = [UIApplication sharedApplication].statusBarOrientation;
    return (o == UIInterfaceOrientationPortrait || o == UIInterfaceOrientationPortraitUpsideDown);
}
+(BOOL) IS_IPHONE_X {
    if (IS_DEVICE==IS_IPHONE) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
            return YES;
        }
        return NO;
       
    }
    return NO;
}



+(BOOL) isPortrait:(UIDeviceOrientation)orientation {
    return orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown;
}
+(BOOL) isLandscape {
    
    if(DEVICE_ORIENTATION == UIInterfaceOrientationLandscapeLeft || DEVICE_ORIENTATION == UIInterfaceOrientationLandscapeRight) {
        return YES;
    }else {
        return NO;
    }

}
+ (void) alert:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"justhearit" message:message delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alertView show];
}

+ (int) screenWidth {
    
    return [UIScreen mainScreen].bounds.size.width;
    
}

+ (int) screenHeight {
    return [UIScreen mainScreen].bounds.size.height;
}

+ (int) currentHeight {
    int height = [self screenHeight];
    
    if(![self isPortrait]) {
        if(SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            height = [self screenWidth];
        }else {
            height = [self screenHeight];
        }
    }
    
    if ([self statusBar]) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
            height -= STATUS_BAR_HEIGHT;
        }
    }
    
    return height;
}

+(int) currentWidth {
    int width = [self screenWidth];
    
    if(![self isPortrait]) {
        if(SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            width = [self screenHeight];
        }else {
            width = [self screenWidth];
        }
    }
    
    return width;
}

+(int) currentHeight:(UIDeviceOrientation)orientation {
    int height = [self screenHeight];
    
    if(![self isPortrait:orientation]) {
        
        if(SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            height = [self screenWidth];
        }else {
            height = [self screenHeight];
        }
        
    }
    
    if ([self statusBar]) {
        height -= STATUS_BAR_HEIGHT;
    }
    
    return height;
}

+(int) currentWidth:(UIDeviceOrientation)orientation {
    int width = [self screenWidth];
    
    if(![self isPortrait:orientation]) {
        
        if(SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            width = [self screenHeight];
        }else {
            width = [self screenWidth];
        }
    }
    
    return width;
}

+(BOOL) statusBar {
    return [UIApplication sharedApplication].statusBarHidden == false;
}

+ (UIImage *) imageFromColor:(UIColor *)color withWidth:(int)width andHeight:(int)height {
    CGRect rect = CGRectMake(0, 0, width, height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, ([color CGColor]));
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *) imageFromColor:(UIColor *)color {
    return [Utils imageFromColor:color withWidth:1 andHeight:1];
}

+ (void)makeSearchStyleTextField:(UITextField *)textField {
    textField.layer.borderColor = RGBA(255,255,255,0.2).CGColor;
    textField.layer.cornerRadius = 16;
    textField.layer.borderWidth = 1.0f;
    [textField setBackground:[UIImage imageNamed:@"small_background.png"]];
    [textField setTextColor:RGBA(255,255,255,0.7)];
    textField.leftViewMode = UITextFieldViewModeAlways;

    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];

    UIImageView *mag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mag.png"]];
    mag.frame = CGRectMake(10, 0, 20, 20);

    [paddingView addSubview:mag];

    textField.leftView = paddingView;
}

+ (void) queryBlock:(id (^)(void))block inBackgroundQueue:(NSString *) backgroundQueue completion:(void (^)(id))completion {
    dispatch_queue_t callerQueue = dispatch_get_current_queue();
    dispatch_queue_t queue = dispatch_queue_create([backgroundQueue UTF8String], NULL);
    dispatch_async(queue, ^{
        id results = block();
        dispatch_async(callerQueue, ^{
            completion(results);
        });
    });
//    dispatch_release(queue);
}

+ (NSString *) uuid {
    return [[NSProcessInfo processInfo] globallyUniqueString];
}

+ (UIImage *) UIImageFromView:(UIView *)view {

    UIGraphicsBeginImageContext(view.frame.size);
    UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImageView *) UIImageViewFromView:(UIView *)view {
    return [[UIImageView alloc] initWithImage:[Utils UIImageFromView:view]];
}

+ (UIImage *) image:(UIImage*) image byApplyingAlpha:(CGFloat) alpha {
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);

    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);

    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);

    CGContextSetAlpha(ctx, alpha);

    CGContextDrawImage(ctx, area, image.CGImage);

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return newImage;
}


+ (UIViewController *)sharingViewControllerForSong:(PlaylistSong *)song {
    NSString *shareString = [NSString stringWithFormat:@"%@ - %@", song.artist, song.name];
    //SCHIMBA AICI CAND PUI PE MASTER!!!!!!!
    NSString *stringdeurl = [NSString stringWithFormat:@"https://justhearit.com/hear/%@", song.token];
    NSURL *shareUrl = [NSURL URLWithString:stringdeurl];
    NSArray *activityItems = @[shareString, shareUrl];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;

    activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypePostToTencentWeibo, UIActivityTypePostToVimeo, UIActivityTypePostToFlickr, UIActivityTypeSaveToCameraRoll, UIActivityTypeAssignToContact, UIActivityTypeAddToReadingList, UIActivityTypePrint];
    return activityViewController;
}

+ (UIViewController *)sharingViewControllerForPlaylist:(Playlist *)playlist {
    NSString *shareString = playlist.name;
    NSURL *shareUrl = [NSURL URLWithString:playlist.shareUrl];
    NSString *shr=[NSString stringWithFormat:@"%@", shareUrl];
    
    NSString *newStr = [shr substringFromIndex:11];
    NSString *finalStr=@"https://";
    
    finalStr=[finalStr stringByAppendingString:newStr];
    
    shareUrl=[NSURL URLWithString:finalStr];


    CopyLinkCustomActivity *ca = [[CopyLinkCustomActivity alloc]init];
    ca.link = finalStr;
    
    //Log(@"Copylink : %@", ca.link);
    
    NSArray *activityItems = @[shareString, shareUrl];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:@[ca]];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;

    activityViewController.excludedActivityTypes = @[UIActivityTypeCopyToPasteboard, UIActivityTypePostToWeibo, UIActivityTypePostToTencentWeibo, UIActivityTypePostToVimeo, UIActivityTypePostToFlickr, UIActivityTypeSaveToCameraRoll, UIActivityTypeAssignToContact, UIActivityTypeAddToReadingList, UIActivityTypePrint];
    
    return activityViewController;
}

+ (uint64_t) getFreeDiskspace {
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];

    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
       // Log(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    } else {
        Log(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }

    return totalFreeSpace/1024ll/1024ll/1024ll;
}

+ (BOOL) isReachable {
    int status = [[[JusthearitClient sharedInstance] HTTPClient] networkReachabilityStatus];
    return status != AFRKNetworkReachabilityStatusNotReachable;
}

+ (BOOL) isReachableOverWifi {
    return [[[JusthearitClient sharedInstance] HTTPClient] networkReachabilityStatus] == AFRKNetworkReachabilityStatusReachableViaWiFi;
}

+ (BOOL) isReachableOverCellular {
    return [[[JusthearitClient sharedInstance] HTTPClient] networkReachabilityStatus] == AFRKNetworkReachabilityStatusReachableViaWWAN;
}

+ (BOOL) applicationInBackground {
    return [[UIApplication sharedApplication] applicationState] != UIApplicationStateActive;
}

+ (BOOL) isVersionLessThan_8_0 {
    return SYSTEM_VERSION_LESS_THAN(@"8.0");
}

+ (BOOL) isVersionMoreThan_8_0 {
    return SYSTEM_VERSION_GREATER_THAN(@"8.0");
}
@end

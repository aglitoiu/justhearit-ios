#import <UIKit/UIKit.h>

@interface DidYouMeanCell : UITableViewCell

@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) UIImageView *icon;

@end

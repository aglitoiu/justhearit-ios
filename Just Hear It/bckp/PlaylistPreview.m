#import "PlaylistPreview.h"
#import "Playlist.h"
#import "User.h"

@implementation PlaylistPreview

- (NSArray *) genres {
    if(_genreList) return _genreList;

    NSArray *genres = _.array(_songs).pluck(@"genre").filter(^BOOL (NSString *genre){
        return genre.length > 0 && ![genre.lowercaseString isEqualToString:@"other"];
    }).unwrap;

    if (genres.count == 0) genres = @[@"Unknown"];

    _genreList = _.head([[genres valueForKeyPath:@"@distinctUnionOfObjects.self"] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)], 5);
    return _genreList;
}

- (NSString *)genresAsString {
    return [self.genres componentsJoinedByString:@", "];
}


@end
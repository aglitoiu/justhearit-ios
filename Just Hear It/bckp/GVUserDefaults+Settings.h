#import "GVUserDefaults.h"

@class Session;

#define SETTINGS [GVUserDefaults standardUserDefaults]

@interface GVUserDefaults (Settings)

@property (nonatomic, weak) NSString *token;
@property (nonatomic, weak) NSString *udid;
@property (nonatomic, weak) NSNumber *maxCacheGb;
@property (nonatomic, weak) NSNumber *wifiOnlySync;
@property (nonatomic, weak) NSNumber *OfflineOnlySync;
@property (nonatomic, weak) NSNumber *userId;
@property (nonatomic, weak) NSString *fullName;
@property (nonatomic, weak) NSNumber *tutorialDisabled;
@property (nonatomic, weak) NSNumber *welcomeMessageDisabled;

- (void)setDefaultMaxCacheGb;

- (float) defaultMaxCacheGb;

- (BOOL) wifiOnly;
- (BOOL) OfflineOnly;
- (void) setWifiOnly:(BOOL)value;
- (void) setOfflineOnly:(BOOL)value;

- (BOOL) isLoggedIn;
- (void) logout;
- (void) login:(Session *)session;
@end

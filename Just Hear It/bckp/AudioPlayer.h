#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Playlists.h"
#import "SongCache.h"

@class PlaylistSong;
@interface AudioPlayer : NSObject <AVAudioPlayerDelegate, AVAudioSessionDelegate>
@property (nonatomic, strong) AVQueuePlayer *currentPlayer;
@property (nonatomic, strong) AVQueuePlayer *queuePlayer;

@property (nonatomic, strong) AVPlayerItem *item;

@property (nonatomic, assign) float audioAvailable;

@property (nonatomic, assign) BOOL playerWasInterrupted;

@property (strong) id playerObserver;
@property (strong) id tempPlayerObserver;

@property (nonatomic) NSInteger playerStatus;

@property (nonatomic) BOOL isLoaded;
@property (nonatomic) BOOL AlreadyReady;

@property(nonatomic, assign) NSInteger playSource;

@property(nonatomic, assign) UIBackgroundTaskIdentifier bgTaskId;
@property(nonatomic, assign) UIBackgroundTaskIdentifier removedId;

@property(nonatomic, strong) PlaylistSong *haltedSong;

+ (id) sharedInstance;



- (void) playSong:(PlaylistSong *)song withUserAction:(BOOL)userAction;


- (void) handlePlayButtonHit:(NSNotification *)notification;

- (void) remoteControlReceivedWithEvent:(UIEvent *)receivedEvent;





- (void) playerItemDidReachEnd:(NSNotification *)notification;

- (void) playNextSongWithUserAction;
- (void) playNextSongWithoutUserAction;

- (void) playPrevSongWithUserAction;
- (void) playPrevSongWithoutUserAction;

- (void)resumeHaltedSong;


- (void)stopMusic;
@end


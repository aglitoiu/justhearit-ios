//
//  ControlsView.h
//  Just Hear It
//
//  Created by andrei st on 10/10/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXBlurView.h"

@interface ControlsView : FXBlurView

@end

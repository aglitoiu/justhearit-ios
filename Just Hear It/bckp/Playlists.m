#import "Playlists.h"
#import "Playlist.h"
#import "PlaylistGroup.h"
#import "NSDictionary+OCTotallyLazy.h"
#import "Pagination.h"
#import "LocalPlaylist.h"
#import "JusthearitClient.h"
#import "SongCache.h"
#import "OfflinePlaylist.h"
#import "PlaylistBadgeChangeNotification.h"
#import "CachedPlaylist.h"
#import "MagicalRecord+ShorthandMethods.h"
#import "CachedPlaylist+Utils.h"
#import "CachedLocalPlaylist+Utils.h"
#import "MasterViewController.h"

@implementation Playlists

@synthesize playlistGroups, collection, openedPlaylist, temporaryCollection;
@synthesize myPlaylists, followingPlaylists, defaultPlaylists, offlineMode;

+ (id)sharedInstance {
    static Playlists *__sharedInstance;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        __sharedInstance = [[Playlists alloc] init];
        __sharedInstance.offlineMode = NO;
    });

    return __sharedInstance;
}


- (id) init {
    if (self=[super init]) {
        collection     = [[NSMutableArray alloc] init];
        playlistGroups = [[NSMutableArray alloc] init];
        temporaryCollection = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) loadPlaylistsWithBlock:(void (^)(void))block{
if(SETTINGS.OfflineOnly)
{
    [self loadCachedPlaylists];
    offlineMode = YES;
  
}
    else
    {
        
    [[JusthearitClient sharedInstance] loadPlaylists:^(NSArray *response) {
        
        [self refreshWithArray:response];
        [self createLocalCacheWithArray:response];
        
        
        offlineMode = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.navigationBarShouldReload object:nil userInfo:nil];
        block();
    } withErrorBlock:^(NSError *error) {
        if (!offlineMode) {
            [self loadCachedPlaylists];
        }

        offlineMode = YES;

        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.navigationBarShouldReload object:nil userInfo:nil];

        if (error.code == NSURLErrorTimedOut) {
            [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
        }

        block();
    }];
    }
}


- (void) loadCachedPlaylists {
    [collection removeAllObjects];
    [temporaryCollection removeAllObjects];
    [self refreshPlaylistGroups];
    [self refreshWithArray:[CachedPlaylist allAsPlaylists]];
}

- (void) loadLocalCachedPlaylists {
    [collection removeAllObjects];
    
    [collection addObjectsFromArray:[CachedLocalPlaylist allAsPlaylists]];
    [self refreshPlaylistGroups];
}

- (void) refreshPlaylistGroups {
    if (playlistGroups.count == [PlaylistGroup PlaylistGroups].count) {
        [playlistGroups foreach:^(PlaylistGroup *group) {
            [group.collection removeAllObjects];
            [group.collection addObjectsFromArray:[self playlistsForGroupKey:group.groupId.intValue]];
            _.arrayEach(group.collection, ^(Playlist *playlist){
                playlist.playlistGroupObject = group;
            });
        }];
    } else {
        [playlistGroups removeAllObjects];
        myPlaylists = nil;
        defaultPlaylists = nil;
        followingPlaylists = nil;

        [[PlaylistGroup PlaylistGroups] foreach:^(NSDictionary* key) {
            NSInteger playlistGroupKeyValue = [[key objectForKey:@"id"] intValue];
            [playlistGroups addObject:[PlaylistGroup initWithKey:key andCollection:[self playlistsForGroupKey:playlistGroupKeyValue]]];
        }];
    }
}

- (NSMutableArray *) playlistsForGroupKey:(NSInteger)playlistGroupIdValue {
    NSMutableArray *playlists = _.array(collection).filter(^BOOL(Playlist* playlist) {
        return playlist.playlistGroup.intValue == playlistGroupIdValue;
    }).sort(^(Playlist* p1, Playlist *p2){
        return [p1.position compare:p2.position];
    }).unwrap.mutableCopy;

    NSArray *temporaryPlaylists = _.array(temporaryCollection).filter(^BOOL(Playlist* playlist) {
        return playlist.playlistGroup.intValue == playlistGroupIdValue;
    }).sort(^(Playlist* p1, Playlist *p2){
        return [p1.position compare:p2.position];
    }).unwrap;

    [playlists addObjectsFromArray:temporaryPlaylists];
    return playlists;
}

- (void) refreshWithArray:(NSArray *)array {


    [collection removeAllObjects];
    [collection addObjectsFromArray:array];
    [self refreshPlaylistGroups];
}

- (void) createLocalCacheWithArray: (NSArray *)array {
    
      
  
    
        for (Playlist *cachedLocalPlaylist in array) {
        
        //Log(@"local playlist to be cached: %@", cachedLocalPlaylist.name);
        [CachedLocalPlaylist findOrCreateByPlaylist:cachedLocalPlaylist withBlock:^(CachedLocalPlaylist *cachedLocalPlaylist) {
        }];
        
    }
    
}

- (void) openPlaylist:(Playlist *)playlist {
    
    if (openedPlaylist && openedPlaylist != playlist && !openedPlaylist.isPlayingOrPaused) {
        [openedPlaylist.songs removeAllObjects];
        [openedPlaylist.pagination reset];
    }
    
    if(playlist.isInCache && playlist.hasSongsInCache) {
        
        CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist findByPlaylistId:playlist.playlistId];

        openedPlaylist.pagination.totalPages = existingPlaylist.totalPages;
        openedPlaylist.pagination.currentPage = existingPlaylist.currentPage;
        
    }

    openedPlaylist = playlist;
    
    //Log(@"opened playlist pagination: %@", playlist.pagination.totalPages);
    openedPlaylist.badgeCount = @0;
    
    
}

- (void) updateHasNotification:(Playlist *)playlistWithNotification {
    
    [collection foreach:^(Playlist *playlist) {
        if([playlist.playlistId isEqual:playlistWithNotification.playlistId]) {
            playlist.hasNotification = YES;
        }
    }];
    
}

- (void) updatePosition:(NSString *)playlistId andOrder:(NSNumber *)position {
    
    ///[collection]
    
    [collection foreach:^(Playlist *playlist) {
        
        //Log(@"playlist id : %@, %@, %hhd", playlist.playlistId, playlistId, [playlist.playlistId isEqual:playlistId]);
        if([playlist.playlistId isEqual:playlistId]) {
            Log(@"playlist position before : %@", playlist.position);
            playlist.position = position;
            Log(@"playlist position after : %@", playlist.position);
        }
       
    }];
    
}

- (Playlist *) playlistAtIndexPath:(NSIndexPath *)indexPath {
    return [[self.playlistGroups[indexPath.section] collection] objectAtIndex:indexPath.row];
}

- (BOOL) playlistIsLastInSection:(NSIndexPath *)indexPath {
    NSArray *sectionList = [self.playlistGroups[indexPath.section] collection];
    return sectionList.count - 1 == indexPath.row;
}

- (NSInteger) numberOfPlaylistsInGroupIndex:(NSInteger)index {
    if (playlistGroups.count == 0) return 0;

    PlaylistGroup *group = playlistGroups[index];
    if (group.collapsed)
        return 0;
    else
        return group.collection.count;
}

- (Playlist *) findByPlaylist: (Playlist*) playlist {
    return _.find(collection, ^BOOL (Playlist *p) {
        return [p equalsToPlaylist:playlist];
    });
}

- (Playlist *) findByPlaylistId: (NSString*) playlistId {
    
    return _.find(collection, ^BOOL (Playlist *p) {
        return [p.playlistId isEqualToString:playlistId];
    });
    
    
}



- (void)movePlaylistFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    Playlist *playlist = [self playlistAtIndexPath:fromIndexPath];
    [[self.playlistGroups[fromIndexPath.section] collection] removeObjectAtIndex:fromIndexPath.row];
    [[self.playlistGroups[toIndexPath.section] collection] insertObject:playlist atIndex:toIndexPath.row];
}

- (Playlist *) playlistAlreadyInLocalPlaylists:(LocalPlaylist *)localPlaylist {
    return _.filter(temporaryCollection, ^BOOL(Playlist *playlist){
        return [playlist equalsToPlaylist:localPlaylist];
    }).firstObject;
}

- (Playlist *) playlistAlreadyInCollection:(LocalPlaylist *)localPlaylist {
    return _.filter(collection, ^BOOL(Playlist *playlist){
        return [playlist equalsToPlaylist:localPlaylist];
    }).firstObject;
}

- (Playlist *) playlistExistsAsShared:(LocalPlaylist *)localPlaylist {
    return _.filter(collection, ^BOOL(Playlist *playlist){
        return [playlist.originalSharedPlaylistId isEqualToString:localPlaylist.playlistId];
    }).firstObject;
}

- (Playlist *) playlistQueryFoundInFollowing:(LocalPlaylist *) localPlaylist {
    if (localPlaylist.isLocalSearchPlaylist) {
        if (self.followingPlaylistsGroup) {
            return _.filter(self.followingPlaylistsGroup.collection, ^BOOL(Playlist *playlist){
                return [playlist.name.lowercaseString isEqualToString:localPlaylist.name.lowercaseString] && playlist.isSharedSearchPlaylist;
            }).firstObject;
        }
    }
    return nil;
}

- (Playlist *)nonTemporaryPlayingPlaylist:(LocalPlaylist *)localPlaylist {
    return _.filter(temporaryCollection, ^BOOL(Playlist* playlist){
        return playlist.localPlaylistType == localPlaylist.localPlaylistType && playlist.playState == playStateStopped;
    }).firstObject;
}

- (Playlist *) addLocalPlaylist:(LocalPlaylist *) localPlaylist {

    Playlist* playlistExists = [self playlistAlreadyInLocalPlaylists:localPlaylist];
    if (!playlistExists) playlistExists = [self playlistAlreadyInCollection:localPlaylist];
    if (!playlistExists) playlistExists = [self playlistQueryFoundInFollowing:localPlaylist];
    if (!playlistExists) playlistExists = [self playlistExistsAsShared:localPlaylist];
    
    Log(@"playlist exists: %@", playlistExists);
    
    if (!playlistExists) {
        //check if there's a non-playing playlist with the same localPlaylistType. if it is, replace it with this one

        Playlist* nonPlayingPlaylist = [self nonTemporaryPlayingPlaylist:localPlaylist];
        if (nonPlayingPlaylist) {
            [temporaryCollection replaceObjectAtIndex:[temporaryCollection indexOfObject:nonPlayingPlaylist] withObject:localPlaylist];
        } else {
            [temporaryCollection addObject:localPlaylist];
        }

        [self refreshPlaylistGroups];
        return localPlaylist;
    }

    return playlistExists;
}

- (PlaylistGroup *)defaultPlaylistsGroup {
    if (defaultPlaylists) return defaultPlaylists;

    defaultPlaylists = _.filter(playlistGroups, ^BOOL (PlaylistGroup *playlistGroup) {
        return playlistGroup.isDefaultPlaylistsGroup;
    }).firstObject;

    return defaultPlaylists;
}

- (PlaylistGroup *) myPlaylistsGroup {
    if (myPlaylists) return myPlaylists;

    myPlaylists = _.filter(playlistGroups, ^BOOL (PlaylistGroup *playlistGroup) {
        return playlistGroup.isMyPlaylistsGroup;
    }).firstObject;

    return myPlaylists;
}

- (PlaylistGroup *) followingPlaylistsGroup {
    if (followingPlaylists) return followingPlaylists;

    followingPlaylists = _.filter(playlistGroups, ^BOOL (PlaylistGroup *playlistGroup) {
        return playlistGroup.isFollowingPlaylistsGroup;
    }).firstObject;

    return followingPlaylists;
}

- (Playlist *) musicFeed {
    return _.filter(collection, ^BOOL(Playlist* playlist){
        return playlist.isMusicFeed;
    }).firstObject;
}

- (Playlist *) defaultPlaylist {
    return _.filter(collection, ^BOOL(Playlist* playlist){
        return [playlist.isDefault boolValue];
    }).firstObject;
}

- (NSMutableArray *)allPlaylists {
    NSMutableArray *playlists = [[NSMutableArray alloc] init];
    _.arrayEach(playlistGroups, ^(PlaylistGroup *playlistGroup){
        [playlists addObjectsFromArray:playlistGroup.collection];
    });
    return playlists;
}

- (void)removePlaylist:(Playlist *)playlist {
    [collection removeObject:playlist];
    [temporaryCollection removeObject:playlist];
    _.arrayEach(playlistGroups, ^(PlaylistGroup* group) {
        [group.collection removeObject:playlist];
    });
}

- (void)localMode {

}

- (void)onlineMode {

}

- (void)handlePlaylistNotification:(PlaylistBadgeChangeNotification *)notification {
    _.arrayEach(collection, ^(Playlist *p){
        if ([p.playlistId isEqualToString:notification.playlistId]) {
            if(!p.isMyUploads && !p.isRecentlyPlayed) {
                p.badgeCount = notification.badgeCount;
            }
            p.hasNotification = YES;
            return;
        }
    });
}

- (BOOL)handleOpenUrl:(NSString *)path{

    //we have 4 possible routes
    //    /playlists/:id
    //    /search/:query
    //    /search/playlists/:query
    //    /hear/:token

//    alert(@"handle open url called");

    NSString *identifier = [path split:@"/"].lastObject;

    //NU STIU SIGUR
    LocalPlaylist *playlist;

    BOOL canOpenUrl = YES;

    if ([path hasPrefix:@"/playlists"]) {
        Playlist *temp = [[Playlist alloc] init];
        temp.playlistId = identifier;
        temp.name = @"Shared";
        playlist = [LocalPlaylist localSharedPlaylistWithPlaylist:temp];
    } else if ([path hasPrefix:@"/search/playlists"]) {
        playlist = [LocalPlaylist localSearchPlaylistsWithQuery:identifier];
    } else if ([path hasPrefix:@"/search"]) {
        playlist = [LocalPlaylist localSearchPlaylistWithQuery:identifier];
    } else if ([path hasPrefix:@"/hear"]) {
        playlist = [LocalPlaylist localPlaylistWithSongToken:identifier];
    } else {
        canOpenUrl = NO;
    }
    
    Log(@"can open url");
    
    if (canOpenUrl) {
        Playlist *added;

        if (playlist.isSongTokenPlaylist) {
            added = playlist;
        } else {
            added = [[Playlists sharedInstance] addLocalPlaylist:playlist];
        }
        
        Log(@"added: %@", added);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.loadSongsFromUrl object:nil userInfo:added];
        return YES;
    }

    return NO;
}

@end

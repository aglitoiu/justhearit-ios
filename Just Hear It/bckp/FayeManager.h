//
// Created by andrei st on 1/14/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@class MZFayeClient;

enum FayeManagerStates {
    FayeManagerConnected = 1,
    FayeManagerDisconnected = 2
};

@interface FayeManager : NSObject

+ (FayeManager *) sharedInstance;

- (void)disconnect;

- (void)verifySessionAndConnect;

- (void)connect;

@property(nonatomic, strong) MZFayeClient *client;

@property(nonatomic, assign) NSInteger state ;


@end
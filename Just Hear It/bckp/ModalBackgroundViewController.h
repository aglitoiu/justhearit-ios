//
//  ModalBackgroundViewController.h
//  Just Hear It
//
//  Created by andrei st on 10/17/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXBlurViewScaled.h"
#import "BackgroundViewController.h"

@interface ModalBackgroundViewController: BackgroundViewController

@property (nonatomic, strong) UIImageView *parentImageView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *closeButtonw;

@end

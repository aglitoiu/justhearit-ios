#import <Foundation/Foundation.h>

@interface PlaylistBadgeChangeNotification : NSObject

@property(nonatomic, strong) NSString *playlistId;
@property(nonatomic, strong) NSNumber *badgeCount;
@property(nonatomic, strong) NSMutableArray *song_ids;

@end
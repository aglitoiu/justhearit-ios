
#import "SongTime.h"

@implementation SongTime

@synthesize elapsed,remaining,percentageDone, currentSeconds, totalSeconds, remainingSeconds;

+ (SongTime *) initWithCurrentTime:(CMTime) currentTime andTotalTime:(CMTime) totalTime {
    SongTime *songTime = [[SongTime alloc] init];
    songTime.percentageDone = 0;

    if (CMTimeCompare(totalTime, kCMTimeZero) != 0) {
        songTime.percentageDone = (double) currentTime.value / (double) totalTime.value;
    }

    songTime.currentSeconds = CMTimeGetSeconds(currentTime);
    songTime.totalSeconds = CMTimeGetSeconds(totalTime);
    songTime.remainingSeconds = songTime.totalSeconds - songTime.currentSeconds;

    songTime.elapsed = [songTime humanTimeElapsedWithSeconds:songTime.currentSeconds];
    songTime.remaining = [songTime humanTimeRemainingWithSeconds:songTime.remainingSeconds];

    return songTime;
}

- (Float64) secondsFromPercentageDone:(double) percentage {
    return percentage * totalSeconds;
}

- (NSString *)humanTimeRemainingWithSeconds:(Float64) remainingSeconds {
    int mins = remainingSeconds/60.0;
    int secs = fmodf(remainingSeconds, 60.0);
    return [self humanTimeFromMins:mins andSeconds:secs];
}

- (NSString *)humanTimeElapsedWithSeconds:(Float64) currentSeconds {
    int mins = currentSeconds/60.0;
    int secs = fmodf(currentSeconds, 60.0);
    return [self humanTimeFromMins:mins andSeconds:secs];
}

- (NSString *) humanTimeFromMins:(int)mins andSeconds:(int) seconds {
    NSString *minsString = mins < 10 ? [NSString stringWithFormat:@"0%d", mins] : [NSString stringWithFormat:@"%d", mins];
    NSString *secsString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
    return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
}



@end
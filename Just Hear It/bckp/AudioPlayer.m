#import "AudioPlayer.h"
#import "PlaylistSong.h"
#import "Player.h"
#import "SongTime.h"
#import "SongCache.h"
#import "JusthearitClient.h"
#import "FileManager.h"
#import "AdManager.h"
int *AlreadyReady=0;
NSString * const kLoadedTimeRangesKey   = @"loadedTimeRanges";
NSString * const kRateKey   = @"rate";
NSString * const kStatus    = @"status";
static void *AudioControllerBufferingObservationContext = &AudioControllerBufferingObservationContext;
static void *StreamingRateObservationContext = &StreamingRateObservationContext;

@implementation AudioPlayer

enum AudioRoutes {
    Headphones = 1,
    Speaker = 2
};


enum PlayerStatus {
    Playing = 1,
    Paused = 2
};

enum PlaySource {
    PlaySourceCache = 1,
    PlaySourceNormal = 2
};

@synthesize bgTaskId, removedId, currentPlayer, queuePlayer, tempPlayerObserver, playerObserver, item, audioAvailable, playerStatus, isLoaded,AlreadyReady, playSource;

@synthesize haltedSong;

+ (id)sharedInstance {
    static AudioPlayer *__sharedInstance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[AudioPlayer alloc] init];
    });
    
    return __sharedInstance;
}

- (void) setAudioSession {
    NSError *setCategoryError = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &setCategoryError];
    if (setCategoryError) {
        Log(@"Error setting category! %@", [setCategoryError localizedDescription]);
    }
    
    [[AVAudioSession sharedInstance] setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
}

- (id) init {
    self = [super init];
    if (self) {
        
        playerStatus = Paused;
        isLoaded = NO;
      
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(interruption:) name:AVAudioSessionInterruptionNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(routeChange:) name:AVAudioSessionRouteChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mediaReset:) name:AVAudioSessionMediaServicesWereResetNotification object:nil];
        
        [self setAudioSession];
        
        queuePlayer = [[AVQueuePlayer alloc] init];
        queuePlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        currentPlayer = queuePlayer;
        
        [self longTimeBufferBackground];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePlayButtonHit:) name:Ev.playPauseButtonHit object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleForcePause:) name:Ev.forcePause object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playPrevSongWithUserAction) name:Ev.prevButtonHit object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playNextSongWithUserAction) name:Ev.nextButtonHit object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSongProgressManuallyChanged:) name:Ev.songProgressManuallyChanged object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleHeadsetInsert:) name:Ev.headsetInsert object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleHeadsetUnplug:) name:Ev.headsetUnplugged object:nil];
        
        //        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKillQueue:) name:Ev.killQueue object:nil];
        
        CMTime interval = CMTimeMake(1, 1);
        //CRED---
        [queuePlayer removeTimeObserver:self.playerObserver];
        self.playerObserver = [queuePlayer addPeriodicTimeObserverForInterval:interval queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
            if (isLoaded == YES && currentPlayer == queuePlayer) {
                CMTime endTime = CMTimeConvertScale (item.asset.duration, queuePlayer.currentTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
                [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songProgressChanged
                                                                    object:nil
                                                                  userInfo:[SongTime initWithCurrentTime:queuePlayer.currentTime andTotalTime:endTime]];
                //[[NSNotificationCenter defaultCenter] postNotificationName:Ev.adTick object:nil userInfo:nil];
            }
        }];
        
    }
    
    return self;
}

- (void)mediaReset:(id)mediaReset {
    if (playerStatus == Playing) {
        Log(@"MEDIARESET");
        
        [self resumeSong];
         }
    
        
      
   
}

- (void)routeChange:(NSNotification *)notification {
    Log(@"route change notification");
    int reason = [[notification.userInfo objectForKey:@"AVAudioSessionRouteChangeReasonKey"] intValue];
    if (reason == AVAudioSessionRouteChangeReasonOldDeviceUnavailable) {
        [self handleHeadsetUnplug:nil];
    } else if (reason == AVAudioSessionRouteChangeReasonNewDeviceAvailable) {
        [self handleHeadsetInsert:nil];
    }
}

- (void)interruption:(NSNotification*)notification {
   
    if ([[notification.userInfo objectForKey:@"AVAudioSessionInterruptionTypeKey"] intValue] == AVAudioSessionInterruptionTypeEnded) {
        
        if ([[notification.userInfo objectForKey:@"AVAudioSessionInterruptionOptionKey"] intValue] == AVAudioSessionInterruptionOptionShouldResume) {
            Log(@"AVAudioSessionInterruptionTypeEnded && AVAudioSessionInterruptionOptionShouldResume");
            if (playerStatus == Playing)
            {
                           [self resumeSong];
            }
        } else {
            Log(@"AVAudioSessionInterruptionTypeEnded. no resume");
            [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songPaused object:nil userInfo:nil];
        }
        
    } else if ([[notification.userInfo objectForKey:@"AVAudioSessionInterruptionTypeKey"] intValue] == AVAudioSessionInterruptionTypeBegan) {
        if ([[notification.userInfo objectForKey:@"AVAudioSessionInterruptionOptionKey"] intValue] == AVAudioSessionInterruptionOptionShouldResume) {
            Log(@"AVAudioSessionInterruptionTypeBegan && AVAudioSessionInterruptionOptionShouldResume");
            if (playerStatus == Playing) {
                         [self resumeSong];
            }
        } else {
            Log(@"AVAudioSessionInterruptionTypeBegan && no resume");
            [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songPaused object:nil userInfo:nil];
        }
    }
}

- (NSInteger) playerRoute {
    AVAudioSessionPortDescription* desc = [[[AVAudioSession sharedInstance] currentRoute].outputs objectAtIndex:0];
    
    if ([desc.portType isEqualToString:AVAudioSessionPortHeadsetMic] || [desc.portType isEqualToString:AVAudioSessionPortHeadphones]) {
        return Headphones;
    } else {
        return Speaker;
    }
}

- (void)handleHeadsetUnplug:(NSNotification *)notification {
    playerStatus = Paused;
    [self pauseSong];
}

- (void)handleHeadsetInsert:(NSNotification *)notification {
    if (playerStatus == Playing) {
      
        [self resumeSong];
    }
}

- (void) handleForcePause:(NSNotification *)notification {
    //    if (queuePlayer.rate > 0.0) playerWasInterrupted = YES;
    Log(@"force pause");
}

- (void) handleSongProgressManuallyChanged:(NSNotification *)notification {
    SongTime *time = (SongTime *)notification.userInfo;
    CMTime interval = CMTimeMake(time.currentSeconds, 1);
    [currentPlayer seekToTime:interval];
    //song progress manually changed is now triggered when the scrub bar is released. if the player was playing, force play (it sometimes stops because of buffering)
    if ([currentPlayer rate] > 0.0 && playerStatus == Playing) {
          [self play];
    }
    [self updateInfoCenterWithSong:nil];

}

- (void) playerItemDidReachEnd:(NSNotification *) notification {
    Log(@"player item did reach end");
    if (![[Player sharedInstance] hasNextSong]) {
        [self stopPlayer];
    } else {
        [self playNextSongWithoutUserAction];
    }
}

- (void)stopPlayer {
    [self pauseSong];
    playerStatus = Paused;
    [currentPlayer seekToTime:CMTimeMake(0, 1)];
}

- (void) playNextSongWithUserAction {
    [self playNextSongWithUserAction:YES];
}

- (void) playNextSongWithoutUserAction {
    [self playNextSongWithUserAction:NO];
}

- (void) playNextSongWithUserAction:(BOOL)userAction {
  
    if ([[Player sharedInstance] hasNextSong]) {
        
        
        [self playSong:[[Player sharedInstance] nextSong] withUserAction:userAction];
    }
}

- (void) playPrevSongWithUserAction {
    [self playPrevSongWithUserAction:YES];
}

- (void) playPrevSongWithoutUserAction {
    [self playPrevSongWithUserAction:NO];
}

- (void) playPrevSongWithUserAction:(BOOL)userAction {
    CMTime endTime = CMTimeConvertScale (currentPlayer.currentItem.asset.duration, currentPlayer.currentTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
    SongTime *songTime = [SongTime initWithCurrentTime:currentPlayer.currentTime andTotalTime:endTime];
    
    if (songTime.currentSeconds > 5) {
        [currentPlayer seekToTime:CMTimeMake(0, 1)];
    } else {
        if ([[Player sharedInstance] hasPrevSong]) [self playSong:[[Player sharedInstance] prevSong] withUserAction:userAction];
    }
}

- (void) beginInterruption {
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.forcePause object:nil userInfo:nil];
    
}

- (void) audioPlayerBeginInterruption:(AVAudioPlayer *)player {
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.forcePause object:nil userInfo:nil];
    
}

- (void) endInterruptionWithFlags:(NSUInteger)flags {
    
    // Validate if there are flags available.
    if (flags) {
        // Validate if the audio session is active and immediately ready to be used.
        if (flags == AVAudioSessionInterruptionOptionShouldResume) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                if (playerStatus == Playing&&[currentPlayer rate] > 0.0) {
                    
                [self resumeSong];
                
                }
            });
        }
    }
}

#pragma mark - remote control events

- (void) resumeHaltedSong {
    if (haltedSong) {
        [self setAudioSession];
        [self playSong:haltedSong];
              haltedSong = nil;
    }
}

- (void) playSong:(PlaylistSong *)song {
   
    isLoaded = NO;
    AlreadyReady=NO;
    if (song) {
        
        if (item) {
            @try {
                [item removeObserver:self forKeyPath:kLoadedTimeRangesKey];
                [item removeObserver:self forKeyPath:kStatus];
            }
            @catch (...) { }
        }
        
        [queuePlayer removeAllItems];
        
        SongCache *cache = [SongCache initWithSong:song];
        
        NSURL *url = nil;
        
        if (cache.isCached) {
            playSource = PlaySourceCache;
            url = [NSURL fileURLWithPath:[FileManager filePathForSongId:song.songId]];
        } else {
            playSource = PlaySourceNormal;
            url = [NSURL URLWithString:song.url];
        }
        
        item = [AVPlayerItem playerItemWithURL:url];

        
        
        
        
        [item addObserver:self
               forKeyPath:kLoadedTimeRangesKey
                  options:NSKeyValueObservingOptionNew
                  context:AudioControllerBufferingObservationContext];
        
        [item addObserver:self forKeyPath:kStatus options:NSKeyValueObservingOptionNew context:AudioControllerBufferingObservationContext];
        
        [currentPlayer insertItem:item afterItem:nil];
        
        [self updateInfoCenterWithSongMetadata:song];
        [self updateInfoCenterWithSong:song];
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songStartedBuffering object:nil userInfo:nil];
        [[JusthearitClient sharedInstance] logSong:song withSuccessBlock:^(NSArray *array) {} withErrorBlock:^(NSError *error) {}];
    } else {
        [self stopPlayer];
    }
}

- (void) playSong:(PlaylistSong *) song withUserAction:(BOOL)userAction {
    /*if ([[AdManager sharedInstance] playerShouldHalt]) {
     [self pauseSong];
     haltedSong = song;
     [[AdManager sharedInstance] songWillPlayWithUserAction:userAction];
     } else { */
    [self playSong:song];
    /* } */
}

- (void) updateInfoCenterWithSongMetadata:(PlaylistSong *)song {
    NSDictionary *currentlyPlayingTrackInfo = @{
                                                MPMediaItemPropertyTitle: song.name,
                                                MPMediaItemPropertyArtist: song.artist
                                                };
    
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = currentlyPlayingTrackInfo;
}

- (void) updateInfoCenterWithSong:(PlaylistSong *)song {
    
    
    if (song == nil) song = [[Player sharedInstance] currentPlayingSong];
    
    NSNumber *playbackRate = [NSNumber numberWithFloat:queuePlayer.rate];


    NSNumber *elapsedPlaybackTime = [NSNumber numberWithDouble:CMTimeGetSeconds(queuePlayer.currentItem.currentTime)];
    
    Log(@"%@", elapsedPlaybackTime);
    NSNumber *duration = [NSNumber numberWithDouble:CMTimeGetSeconds(queuePlayer.currentItem.duration)];
    if([song.cover isEqualToString:@"http://dev.justhearit.com/now-playing.png"]){
        song.cover=@"http://dev.justhearit.com/now-playing-dark.png";
    }

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        
        
        UIImage *artworkImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString: song.cover]]];
        
        MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage: artworkImage];
    NSDictionary *currentlyPlayingTrackInfo = @{
                                                MPMediaItemPropertyTitle: song.name,
                                                MPMediaItemPropertyArtist: song.artist,
                                                MPMediaItemPropertyPlaybackDuration: song.duration,
                                           
                                                MPNowPlayingInfoPropertyElapsedPlaybackTime: elapsedPlaybackTime,
                                                MPMediaItemPropertyArtwork: albumArt,
                                                MPNowPlayingInfoPropertyPlaybackRate: playbackRate

                                                };
  
    
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = currentlyPlayingTrackInfo;
    
        
        
        

    });
}

/*
 * Tells OS this application starts one or more long-running tasks, should end background task when completed.
 */
-(void)longTimeBufferBackground
{
    bgTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:removedId];
        bgTaskId = UIBackgroundTaskInvalid;
    }];
    
    if (bgTaskId != UIBackgroundTaskInvalid && removedId == 0 ? YES : (removedId != UIBackgroundTaskInvalid)) {
        [[UIApplication sharedApplication] endBackgroundTask: removedId];
    }
    removedId = bgTaskId;
}

-(void)longTimeBufferBackgroundCompleted
{
    if (bgTaskId != UIBackgroundTaskInvalid && removedId != bgTaskId) {
        [[UIApplication sharedApplication] endBackgroundTask: bgTaskId];
        removedId = bgTaskId;
        
    }
    
}

-(void)observeValueForKeyPath:(NSString*)aPath ofObject:(id)anObject change:(NSDictionary*)aChange context:(void*)aContext {
    
    if (aContext == AudioControllerBufferingObservationContext) {
        
        AVPlayerItem* playerItem = (AVPlayerItem*)anObject;
        
        if ([aPath isEqualToString:kStatus]) {
            switch(playerItem.status) {
                case AVPlayerItemStatusFailed:
                    Log(@"player item status failed, %@", playerItem.error);
                    [self pauseSong];
                    //                    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.audioPercentageAvailable object:nil userInfo:[NSNumber numberWithFloat:0]];
                    //                    playerStatus = Paused;
                    break;
                case AVPlayerItemStatusReadyToPlay: {
                    if(AlreadyReady==NO)
                    {
                    AlreadyReady=YES;
                    isLoaded = YES;
                        [self play];
                    
                    playerStatus = Playing;
                    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songStartedPlay object:nil userInfo:nil];
                    //[self updateInfoCenterWithSong:nil];
                    Log(@"player item status is ready to play");
                    break;
                    }
                }
                    
                case AVPlayerItemStatusUnknown:
                    Log(@"player item status is unknown");
                    [self pauseSong];
                    playerStatus = Paused;
                    break;
                    
            }
        } else if ([aPath isEqualToString:@"playbackBufferEmpty"]) {
            if (playerItem.playbackBufferEmpty) {
                Log(@"player item playback buffer is empty");
            }
        } else if ([aPath isEqualToString:kLoadedTimeRangesKey]) {
            
            NSArray* times = playerItem.loadedTimeRanges;
            
            // there is only ever one NSValue in the array
            NSValue* value = [times objectAtIndex:0];
            
            CMTimeRange range;
            [value getValue:&range];
            float start = CMTimeGetSeconds(range.start);
            float duration = CMTimeGetSeconds(range.duration);
            
            audioAvailable = start + duration;
            Log(@"loaded time ranges called with audioAvailable = %f", audioAvailable);
            
            CMTime playerDuration = [currentPlayer.currentItem duration];
            
            if (currentPlayer.rate == 0 && playerStatus == Playing) {
                [self longTimeBufferBackground];
                
                CMTime bufferdTime = CMTimeAdd(range.start, range.duration);
                CMTime milestone = CMTimeAdd(currentPlayer.currentTime, CMTimeMakeWithSeconds(5.0f, range.duration.timescale));
                
                if (CMTIME_COMPARE_INLINE(bufferdTime , >, milestone) && currentPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
                   [self play];
                    [self longTimeBufferBackgroundCompleted];
                }
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:Ev.audioPercentageAvailable object:nil userInfo:[NSNumber numberWithFloat:audioAvailable/CMTimeGetSeconds(playerDuration)]];
        }
        
    }
}

- (void) handlePlayButtonHit:(NSNotification *)notification {
    if (currentPlayer.rate > 0.0) {
        [self pauseSong];
        playerStatus = Paused;
    } else {
        Log(@"PLAYHIT");
       
        [self resumeSong];
        playerStatus = Playing;
    }
}

- (void) pauseSong {
    [currentPlayer pause];
    //[self updateInfoCenterWithSong:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songPaused object:nil userInfo:nil];
}

- (void) resumeSong {
 
    [self play];
    //[self updateInfoCenterWithSong:nil];
    if (currentPlayer.currentItem.status != AVPlayerItemStatusReadyToPlay) {
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songStartedBuffering object:nil userInfo:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.songResumed object:nil userInfo:nil];
    }
}
- (void) play{
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(sysVer>9.9)
    {
        [currentPlayer playImmediatelyAtRate:1.0];
    }
    else{
        [currentPlayer play];
    }
}

- (void) remoteControlReceivedWithEvent: (UIEvent *) receivedEvent {
   if (receivedEvent.type == UIEventTypeRemoteControl) {
        switch (receivedEvent.subtype) {
            case UIEventSubtypeRemoteControlPlay: {
                [self handlePlayButtonHit:nil];
                break;
            }
            case UIEventSubtypeRemoteControlPause: {
                [self handlePlayButtonHit:nil];
                break;
            }
            case UIEventSubtypeRemoteControlTogglePlayPause: {
                [self handlePlayButtonHit:nil];
                break;
            }
            case UIEventSubtypeRemoteControlNextTrack: {
                [self playNextSongWithoutUserAction];
                break;
            }
            case UIEventSubtypeRemoteControlPreviousTrack: {
                [self playPrevSongWithoutUserAction];
                break;
            }
            case UIEventSubtypeRemoteControlBeginSeekingBackward: {
                break;
            }
            case UIEventSubtypeRemoteControlEndSeekingBackward: {
                break;
            }
            case UIEventSubtypeRemoteControlBeginSeekingForward: {
                break;
            }
            case UIEventSubtypeRemoteControlEndSeekingForward: {
                break;
            }
            default:
                break;
        }
    }
}

- (void)stopMusic {
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nil;
    [self stopPlayer];
}

@end

//
// Created by andrei st on 1/15/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import "LatestSearch.h"


@implementation LatestSearch

@synthesize query;

+ (instancetype)sharedInstance {
    static LatestSearch *__sharedInstance;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        __sharedInstance = [[LatestSearch alloc] init];
        __sharedInstance.query = @"";
    });

    return __sharedInstance;
}

@end
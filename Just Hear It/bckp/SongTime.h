#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@interface SongTime : NSObject

@property (nonatomic, strong) NSString *remaining;
@property (nonatomic, strong) NSString *elapsed;
@property (nonatomic, assign) double percentageDone;
@property (nonatomic, assign) Float64 currentSeconds;
@property (nonatomic, assign) Float64 totalSeconds;
@property (nonatomic, assign) Float64 remainingSeconds;

+ (SongTime *)initWithCurrentTime:(CMTime)currentTime andTotalTime:(CMTime)totalTime;

- (Float64)secondsFromPercentageDone:(double)percentage;

- (NSString *)humanTimeElapsedWithSeconds:(Float64)currentSeconds;

- (NSString *)humanTimeRemainingWithSeconds:(Float64)remainingSeconds;

- (NSString *)humanTimeFromMins:(int)mins andSeconds:(int)seconds;


@end
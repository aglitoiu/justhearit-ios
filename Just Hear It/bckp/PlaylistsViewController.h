#import <UIKit/UIKit.h>
#import "FMMoveTableView.h"
#import "PlaylistCell.h"
#import "PlaylistHeader.h"
#import "PlaylistGroup.h"
#import "LoginViewController.h"
#import "SearchViewController.h"
#import "JASidePanelController.h"
#import "UIViewController+JASidePanel.h"
#import "FontAwesomeKit.h"
#import "Playlists.h"
#import "Playlist.h"
#import "Spinner.h"
#import "NewPlaylistViewController.h"
#import "ZFModalTransitionAnimator.h"
@class Scrollable;
@class MainViewController;


enum PlaylistsViewControllerModes {
    playlistsViewControllerNormal = 1,
    playlistsViewControllerAddTo = 2
};

@interface PlaylistsViewController : UIViewController <NewPlaylistViewControllerDelegate, UIActionSheetDelegate, PlaylistCellDelegate, FMMoveTableViewDataSource, FMMoveTableViewDelegate, LoginViewControllerDelegate, PlaylistHeaderDelegate, SearchViewControllerDelegate>

@property (nonatomic, strong) IBOutlet FMMoveTableView *playlistsTable;
@property (nonatomic, strong) IBOutlet UIButton *loginButton;
@property (nonatomic, strong) IBOutlet UIButton *searchButton;
@property (nonatomic, strong) IBOutlet UIButton *addPlaylistButton;
@property (nonatomic, strong) IBOutlet UIImageView *background;
@property (nonatomic, strong) Playlists *playlists;
@property (nonatomic, strong) Spinner *animatedSpinner;
@property (nonatomic, assign) NSInteger playlistsMode;
@property (nonatomic, strong) NSArray *addToSongs;
@property (nonatomic, strong) Playlist *movingPlaylist;
@property (nonatomic, strong) Scrollable *tableScroll;
@property(nonatomic, weak) PlaylistCell *playlistCellForActionSheet;
@property (nonatomic, assign) BOOL playlistNotification;
@property (nonatomic, assign) CGFloat lastScrollPosition;
@property (nonatomic, strong) NSMutableArray *cachedLocalPlaylists;
@property (nonatomic, strong) NSMutableArray *cachedPlaylists;
@property (nonatomic, strong) ZFModalTransitionAnimator *animator;
@property (nonatomic, strong) UIView *dashed;

- (void)loadPlaylists;

- (IBAction) openLoginViewController;

- (IBAction) openSearchViewController;

- (IBAction) openSongsViewController;
- (void) changetest;
- (void) setAddToModeWithSongs:(NSArray *)songs;

- (void) setNormalMode;

- (void) createPlaylist:(Playlist*)playlist;

- (void)draggingSongOverStartedWithGesture:(UILongPressGestureRecognizer *)gesture andContainer:(UIView *)container;

- (void)draggingSongOverStoppedWithGesture:(UILongPressGestureRecognizer *)gesture andContainer:(UIView *)container;

- (void)draggingSongOverWithGesture:(UILongPressGestureRecognizer *)recognizer andContainer:(UIView *)container;

- (void)droppedSongOverWithGesture:(UILongPressGestureRecognizer *)recognizer andContainer:(UIView *)container;

- (void) openPlaylist:(Playlist *)playlist andShowMain:(BOOL)showMain;

- (void) repositionBottomButtons;
- (void) repositionNegativeBottomButtons;
- (void) playlistsNotification:(NSNotification *)notification;

- (void)searchViewControllerDidFinish:(SearchViewController *)controller;

- (void)reachabilityDidChange:(NSNotification *)notification;

- (void) playlistsChangeNotification:(NSNotification *)notification;

@end

#import "SearchParams.h"
#import "Pagination.h"

@implementation SearchParams

- (id) init {
    if (self=[super init]) {
        _pagination = [[Pagination alloc] init];
    }
    return self;
}

@end
//
// Created by andrei st on 1/16/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "NSArray+OCTotallyLazy.h"
#import "CachedLocalPlaylist+Utils.h"
#import "Playlist.h"
#import "CachedLocalPlaylistSong.h"
#import "NSSet+OCTotallyLazy.h"
#import "OfflinePlaylist.h"
#import "Playlists.h"
#import "SongCache.h"
#import "FileManager.h"
#import "Pagination.h"

@implementation CachedLocalPlaylist (Utils)

+ (CachedLocalPlaylist *) findByPlaylistId:(NSString *)playlistId {
    return [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:playlistId];
}

+ (void) findOrCreateByPlaylist:(Playlist *)playlist withBlock:(void (^)(CachedLocalPlaylist *))block {
    CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist findByPlaylistId:playlist.playlistId];

    if (existingPlaylist) {
        block(existingPlaylist);
    } else {
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        CachedLocalPlaylist *cachedPlaylist = [CachedLocalPlaylist MR_createEntityInContext:localContext];
        
        cachedPlaylist.name = playlist.name;
        cachedPlaylist.playlistId = playlist.playlistId;
        cachedPlaylist.updatedAt = playlist.updatedAt;
       
   
        

        if(playlist.isSharedPlaylist)
        {
            cachedPlaylist.playlistType=[NSString stringWithFormat:@"%d", playlistTypeShared];
        }
        else{
        
        cachedPlaylist.playlistType = [playlist.playlistType stringValue];
        }
    
        cachedPlaylist.playlistGroup = [playlist.playlistGroup stringValue];
        cachedPlaylist.position = [playlist.position stringValue];
        
        if( [playlist.isDefault boolValue] ) {
            cachedPlaylist.isDefault = @(YES);
        }else {
            cachedPlaylist.isDefault = @(NO);
        }
        
        if( [playlist.erasableContent boolValue] ) {
            cachedPlaylist.erasableContent = @(YES);
        }else {
            cachedPlaylist.erasableContent = @(NO);
        }
        
        cachedPlaylist.shareUrl = playlist.shareUrl;
        
        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {
            block(cachedPlaylist);
        }];
    }
}

- (BOOL) songAlreadyInPlaylist:(CachedLocalPlaylistSong *)cachedPlaylistSong {
    __block BOOL songAlreadyInPlaylist = NO;

    [self.cachedLocalPlaylistSongs.asArray foreach:^(CachedLocalPlaylistSong *song) {
        if ([song.songId isEqualToString:cachedPlaylistSong.songId]) {
            songAlreadyInPlaylist = YES;
        }
    }];

    return songAlreadyInPlaylist;
}

+ (NSArray *) allAsPlaylists {
    NSArray *cachedPlaylists = [CachedLocalPlaylist MR_findAll];
    return _.arrayMap(cachedPlaylists, ^(CachedLocalPlaylist *cachedPlaylist){
        Playlist *playlist = [[Playlist alloc] init];
        playlist.playlistId = cachedPlaylist.playlistId;
        playlist.name = cachedPlaylist.name;
        playlist.cachedLocalPlaylist = cachedPlaylist;
        playlist.pagination.currentPage = cachedPlaylist.currentPage;
        playlist.pagination.totalPages = cachedPlaylist.totalPages;
        playlist.playlistType = [NSNumber numberWithInteger: [cachedPlaylist.playlistType integerValue]];
        //playlist.offlinePlaylistType = offlinePlaylistTypeNormal;
        playlist.playlistGroup = [NSNumber numberWithInteger: [cachedPlaylist.playlistGroup integerValue]];
        playlist.position = [NSNumber numberWithInteger: [cachedPlaylist.position integerValue]];
        playlist.playlistGroupObject = [[Playlists sharedInstance] defaultPlaylistsGroup];
        playlist.updatedAt = cachedPlaylist.updatedAt;
        //Log(@"cached playlist from db updated at: %@", cachedPlaylist.updatedAt);
        
        playlist.isDefault = [NSNumber numberWithInteger: [cachedPlaylist.isDefault integerValue]];
        playlist.erasableContent = [NSNumber numberWithInteger:[cachedPlaylist.erasableContent integerValue]];
        
        playlist.shareUrl = cachedPlaylist.shareUrl;
        
        if([cachedPlaylist.playlistGroup  isEqual: @"2"]) {
            playlist.playlistGroupObject = [[Playlists sharedInstance] defaultPlaylistsGroup];
        }else if([cachedPlaylist.playlistGroup  isEqual: @"1"]) {
            playlist.playlistGroupObject = [[Playlists sharedInstance] myPlaylistsGroup];
        }else if([cachedPlaylist.playlistGroup  isEqual: @"3"]) {
            playlist.playlistGroupObject = [[Playlists sharedInstance] followingPlaylistsGroup];
        }
        return playlist;
    });
}

+ (void)removeFromCache:(CachedLocalPlaylist *)playlist {
    if (!playlist) return;

    NSArray *deletableSongsLC = [SongCache songsOnlyInCachedMusicFeed:playlist];
    Log(@"number of deletable songs: %lu", (unsigned long)[deletableSongsLC.asArray count]);
    
    //Log(@"deletable", [deletableSongsLC.asArray count]);
    
    [deletableSongsLC foreach:^(CachedLocalPlaylistSong *song) {
        [SongCache deleteSongFromCoreDataNoDownload:song];
    }];
    
    NSArray *deletableSongs = [SongCache songsOnlyInCachedLocalPlaylist:playlist];
    ///Log(@"deletable songs: %@", deletableSongs);
    [deletableSongs foreach:^(CachedLocalPlaylistSong *song) {
        [SongCache deleteSongFromCoreDataNoDownload:song];
    }];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        //make your changes in the localContext
    
    [playlist MR_deleteEntityInContext:localContext];
   }];
}


- (void) refreshSongsInRecentlyPlayed:(CachedLocalPlaylist *)playlist {
    
    
    
    NSArray *songs =  [playlist.cachedLocalPlaylistSongs allObjects];
    
    if([songs count]>0) {
        
        for (CachedLocalPlaylistSong *ssong in songs) {
            if([ssong.position intValue] == 69) {
                [SongCache deleteSongFromCoreDataNoDownload:ssong];
            }
        }
        
    }
    
    for (CachedLocalPlaylistSong *song in [playlist.cachedLocalPlaylistSongs allObjects]) {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            //make your changes in the localContext
        
        song.position = [NSNumber numberWithInt:[song.position intValue]+1];
        }];
        
    }
    
    
}

@end

//
// Created by andrei st on 10/22/13.
// Copyright (c) 2013 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@class Autocomplete;


@interface AutocompleteCache : NSObject

@property(nonatomic, strong) NSMutableDictionary *autocompletes;
@property(nonatomic, strong) NSTimer *typingTimer;
@property(nonatomic, assign) CGFloat interval;

-(void) autocomplete:(NSString *) query withSuccessBlock:(void (^)(Autocomplete *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;
@end
#import <UIKit/UIKit.h>
#import "ModalBackgroundViewController.h"
#import "TransparentTextField.h"
#import "Playlist.h"

@class NewPlaylistViewController;

@protocol NewPlaylistViewControllerDelegate <NSObject>

- (void) newPlaylistViewControllerDidFinish:(NewPlaylistViewController*) controller;
- (void)newPlaylistViewControllerDidFinish:(NewPlaylistViewController *)controller withCreatePlaylist:(Playlist*)playlist;
- (void)newPlaylistViewControllerDidFinish:(NewPlaylistViewController *)controller withRenamePlaylist:(Playlist*)playlist;

@end

@interface NewPlaylistViewController : ModalBackgroundViewController <UITextFieldDelegate>

@property(nonatomic, strong) IBOutlet TransparentTextField *playlistName;
@property(nonatomic, strong) NSArray *songs;
@property(nonatomic, strong) Playlist *renamePlaylist;

@property (weak) id <NewPlaylistViewControllerDelegate> delegate;

- (void)dismissViewController:(id)sender;

@end

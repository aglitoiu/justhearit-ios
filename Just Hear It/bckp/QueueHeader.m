#import "QueueHeader.h"

@implementation QueueHeader

+ (UIView *)initWithTitle:(NSString *)title {
    //setup the view
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [Utils screenWidth], QUEUE_HEADER_HEIGHT)];
    view.backgroundColor = RGBA(0,0,0,0.3);
    
    //setup the label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(SONG_CELL_PADDING_LEFT, 0, [Utils screenWidth], QUEUE_HEADER_HEIGHT)];
    label.font = [UIFont boldSystemFontOfSize:14];
    label.textColor = RGBA(255,255,255,.8);
    label.text = title;
    [view addSubview:label];
    
    //border bottom
    UIImageView *borderBottom = [[UIImageView alloc] initWithImage:[Utils imageFromColor:RGBA(255,255,255,.1)]];
    borderBottom.frame = CGRectMake(0, view.frame.size.height, view.frame.size.width, 1);
    borderBottom.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    [view addSubview:borderBottom];
    
    return view;
}

@end

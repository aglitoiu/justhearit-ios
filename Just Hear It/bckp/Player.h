#import <Foundation/Foundation.h>

enum ShuffleModes {
    ShuffleModeOff = 0,
    ShuffleModeOn = 1
};

enum RepeatModes {
    RepeatModeOff = 0,
    RepeatModeOne = 1,
    RepeatModeAll = 2
};

@class PlaylistSong;
@class Playlist;

@class Player;

@protocol PlayerQueueDelegate <NSObject>

- (void) nextPlaylistSongsChanged:(NSArray *)songs;

@end

@interface Player : NSObject

@property(nonatomic, strong) NSMutableArray *queue;
@property(nonatomic, strong) NSMutableArray *shuffledQueue;
@property(nonatomic, strong) NSMutableArray *playingQueue;

@property(nonatomic, strong) NSMutableArray *upNextQueue;

@property(nonatomic, strong) NSMutableArray *nextSongsInPlaylist;

@property(nonatomic, strong) PlaylistSong *currentPlayingSong;
@property(nonatomic, strong) PlaylistSong *playingSongBeforeQueue;

@property(nonatomic, strong) Playlist *currentPlayingPlaylist;

@property(nonatomic, assign) NSInteger shuffleMode;
@property(nonatomic, assign) NSInteger repeatMode;
@property (nonatomic) BOOL isinUpNext;

@property (weak) id <PlayerQueueDelegate> queueDelegate;

+ (id)sharedInstance;

- (void) playSong:(PlaylistSong *)song fromPlaylist:(Playlist *) playlist;

- (PlaylistSong *)nextSong;

- (PlaylistSong *)prevSong;


- (BOOL)hasNextSong;

- (BOOL)hasPrevSong;

- (NSInteger)toggleShuffle;

- (NSInteger)toggleRepeat;

- (void)refreshNextSongsInPlaylist;

- (NSArray *)getNextSongsInPlaylist;

- (void)stopMusic;

- (BOOL)hasUpNextSongs;

- (void)addUpNextSong:(PlaylistSong *)playlistSong;

- (void)addQueueSong:(PlaylistSong *)playlistSong;
@end

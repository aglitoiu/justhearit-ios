#import "FontAwesomeKit/FAKFontAwesome.h"
#import "PlaylistCell.h"

#define OPTIONS_BUTTON_WIDTH 40

@implementation PlaylistCell

NSString *const PlaylistCellEnclosingTableViewDidBeginScrollingNotification = @"PlaylistCellEnclosingTableViewDidScrollNotification";

@synthesize titleLabel, icon, selection, playlistCellDelegate;

@synthesize optionsButton, badgeLabel;

+ (UIView *)makeSelectionView {
    UIView* selection = [[UIView alloc] init];
    selection.frame = CGRectMake(-5, 5, 320, PLAYLISTS_CELL_HEIGHT - 10);
    selection.layer.cornerRadius = 5;
    selection.backgroundColor = RGBA(255,255,255,.2);
    return selection;
}

- (CGRect) selectionFrame {
    
    NSDictionary *attributes = @{NSFontAttributeName: titleLabel.font};
    
    //CGSize textSize = [titleLabel.text sizeWithFont:titleLabel.font];
    CGSize textSize = [titleLabel.text sizeWithAttributes:attributes];
    
    CGFloat strikeWidth = textSize.width;

    if (badgeLabel.text.length > 0) {
        strikeWidth += badgeLabel.frame.size.width + 100;
    }
    
    CGFloat selectionFrameWidth;
    
    if (IS_DEVICE == IS_IPAD) {
        selectionFrameWidth = LEFTPANEL_WIDTH_IPAD - 45 + 15;
    }else {
        selectionFrameWidth = 320 - 90;
    }
    
    CGRect frame = CGRectMake(-5, 5, selectionFrameWidth, PLAYLISTS_CELL_HEIGHT - 10);
    CGFloat cellContentWidth = strikeWidth + titleLabel.frame.origin.x + 15;
    
    if (cellContentWidth > selectionFrameWidth)
        frame.size.width = selectionFrameWidth;
    else
        frame.size.width = cellContentWidth;

    return frame;
}

-(void)prepareForReuse {
    [super prepareForReuse];
    [self hideOptionsButton];
}

- (void) showDeleteButton {
    if (optionsButton.alpha == 1) return;

    CGRect frame = [self selectionFrame];
    optionsButton.frame = CGRectMake(frame.size.width + 10, 0, OPTIONS_BUTTON_WIDTH, PLAYLISTS_CELL_HEIGHT);
    [UIView animateWithDuration:.3 animations:^{
        optionsButton.alpha = 1;
        optionsButton.frame = CGRectMake(frame.size.width, 0, OPTIONS_BUTTON_WIDTH, PLAYLISTS_CELL_HEIGHT);
    }];
    CABasicAnimation *halfTurn;
    halfTurn = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    halfTurn.fromValue = [NSNumber numberWithFloat:0];
    halfTurn.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    halfTurn.duration = 0.5;
    halfTurn.repeatCount = 1;
    [optionsButton.imageView.layer addAnimation:halfTurn forKey:@"180"];

}

- (void)hideOptionsButton {
    if (optionsButton.alpha == 0) return;

    CGRect frame = [self selectionFrame];
    [UIView animateWithDuration:.3 animations:^{
        optionsButton.alpha = 0;
        optionsButton.frame = CGRectMake(frame.size.width + 10, 0, OPTIONS_BUTTON_WIDTH, PLAYLISTS_CELL_HEIGHT);
    }];
}

- (void) repositionBadgeWithText:(NSString *)text {
    badgeLabel.text = text;
    CGRect frame = titleLabel.frame;
   
    frame.origin.x = frame.origin.x + [titleLabel.text sizeWithAttributes:@{NSFontAttributeName: titleLabel.font}].width + 10;
    frame.size.width = [badgeLabel.text sizeWithAttributes:@{NSFontAttributeName: badgeLabel.font}].width;
    badgeLabel.frame = frame;

//    CGRect selectionFrame = selection.frame;
//    selectionFrame.origin.x += frame.size.width + 10;
//    selection.frame = selectionFrame;
}

- (void) awakeFromNib {
    //setup the selection
    selection = [PlaylistCell makeSelectionView];
    [self addSubview:selection];

    //setup the label
    titleLabel = [[UILabel alloc] init];
    [self addSubview:titleLabel];
    
    CGFloat titleLabelWidth;
    
    if (IS_DEVICE == IS_IPAD) {
        titleLabelWidth = LEFTPANEL_WIDTH_IPAD - PLAYLISTS_PADDING_LEFT - 45;
    }else {
        titleLabelWidth = PLAYLISTS_CELL_MAXIMUM_SELECTION_WIDTH - 55;
    }
    
    titleLabel.frame = CGRectMake(PLAYLISTS_PADDING_LEFT, 0, titleLabelWidth, PLAYLISTS_CELL_HEIGHT);
    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    titleLabel.textColor = RGB(255,255,255);

    [self markSelected:NO];

    //setup the badge
    badgeLabel = [[UILabel alloc] init];
    [self addSubview:badgeLabel];
    badgeLabel.font = [UIFont systemFontOfSize:12];
    badgeLabel.textColor = RGBA(255,255,255,.6);
    [self repositionBadgeWithText:@""];

    //setup the icon
    icon = [[UIImageView alloc] init];
    icon.frame = CGRectMake(15, 0, 12, PLAYLISTS_CELL_HEIGHT);
    icon.contentMode = UIViewContentModeCenter;
    [self addSubview:icon];

    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swiped:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:swipeGesture];

    optionsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    optionsButton.contentEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0);

    FAKFontAwesome *cogIcon = [FAKFontAwesome cogIconWithSize:12];
    [cogIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    cogIcon.iconFontSize = 12;

    UIImage *cogIconImage = [cogIcon imageWithSize:CGSizeMake(12, 12)];

    [optionsButton setImage:cogIconImage forState:UIControlStateNormal];

    optionsButton.alpha = 0;
    [optionsButton addTarget:self action:@selector(optionsButtonHit) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:optionsButton];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enclosingTableViewDidScroll:) name:PlaylistCellEnclosingTableViewDidBeginScrollingNotification object:nil];
}

- (void)optionsButtonHit {
    if ([playlistCellDelegate respondsToSelector:@selector(playlistCellDidHitOptions:)]) {
        [playlistCellDelegate playlistCellDidHitOptions:self];
    }
}

- (void) swiped:(UISwipeGestureRecognizer *)gestureRecognizer {
    if ([playlistCellDelegate respondsToSelector:@selector(playlistCellDidSwipeLeft:)]) {
        [playlistCellDelegate playlistCellDidSwipeLeft:self];
    }
}

- (void) enclosingTableViewDidScroll:(NSNotification *)notification {
    PlaylistCell *cell = (PlaylistCell *)notification.userInfo;
    if (self != cell) [self hideOptionsButton];
}

- (void) markSelected:(BOOL)val {
    if (val) selection.frame = [self selectionFrame];
    selection.alpha = val;
}

- (void)takeShotIn:(UIView *)snapshotImageView{

    CGRect cellFrame = [self bounds];

//    [self markSelected:selected];
    CGFloat width;
    if(IS_DEVICE == IS_IPAD) {
        width = LEFTPANEL_WIDTH_IPAD;
        cellFrame.size.width = width;
    }else {
        width = [Utils screenWidth];
    }
    
    UIGraphicsBeginImageContextWithOptions(cellFrame.size, NO, [[UIScreen mainScreen] scale]);
    [[self layer] renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    UIImageView *imageView = [[UIImageView alloc] initWithImage:snapshotImage];
    snapshotImageView.backgroundColor = [UIColor clearColor];
    [snapshotImageView addSubview:imageView];
    imageView.frame = CGRectMake(0, 0, width, snapshotImageView.frame.size.height);
}

- (void) prepareForMove {
    self.backgroundColor      = [UIColor clearColor];
    self.textLabel.text       = @"";
    self.detailTextLabel.text = @"";
    self.imageView.image      = nil;

    titleLabel.text = @"";
    icon.image = nil;
    selection.alpha = 0;
    optionsButton.alpha = 0;
    badgeLabel.text = @"";
}

- (void)toggleOptionsButton {
    if (optionsButton.alpha == 0) {
        [self showDeleteButton];
    } else {
        [self hideOptionsButton];
    }
}
@end

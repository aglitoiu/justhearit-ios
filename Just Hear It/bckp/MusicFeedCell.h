#import <Foundation/Foundation.h>
#import "SongCell2.h"

@interface MusicFeedCell : SongCell2

@property (nonatomic, strong) UILabel *artistAndSongNameLabel;
@property (nonatomic, strong) UILabel *timestamp;
@property (nonatomic, strong) UIImageView *playlistIcon;
@property (nonatomic, strong) UILabel *playlistName;

@end

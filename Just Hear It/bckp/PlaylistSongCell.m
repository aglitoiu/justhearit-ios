//
// Created by andrei st on 1/15/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import "PlaylistSongCell.h"


@implementation PlaylistSongCell

- (void) awakeFromNib {
    [super awakeFromNib];
    [self setupPlaylistIcon];
    [self setupPlaylistName];
    [self setupTimestamp];
    [self setupSongName];
    
    [self.artistLabel removeFromSuperview];
    [self.songNameLabel removeFromSuperview];
    
    [self.frontView addSubview:_playlistIcon];
    [self.frontView addSubview:_playlistName];
    [self.frontView addSubview:_timestamp];
    [self.frontView addSubview:_artistAndSongNameLabel];
}

- (void) setupPlaylistIcon {
    _playlistIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 5, 8, 16)];
    _playlistIcon.contentMode = UIViewContentModeCenter;
    _playlistIcon.alpha = 0.6;
    [self.contentView addSubview:_playlistIcon];
}

- (void)prepareForMove {
    [super prepareForMove];
    _playlistName.text = @"";
    _timestamp.text = @"";
    _artistAndSongNameLabel.text = @"";
    _playlistIcon.alpha = 0;
}

- (void) setupPlaylistName {
    

}

- (void) setupTimestamp {
    CGFloat x = _playlistName.frame.origin.x + _playlistName.frame.size.width + 5;
    
    CGFloat mainWidth;
    
    if(IS_DEVICE == IS_IPAD) {
        mainWidth = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD;
    }else {
        mainWidth = [Utils screenWidth];
    }
    
    _timestamp = [[UILabel alloc] initWithFrame:CGRectMake(x, 5, mainWidth - x - 10, 16)];
    _timestamp.font = [UIFont boldSystemFontOfSize:12];
    _timestamp.alpha = 0.6;
    _timestamp.textAlignment = NSTextAlignmentRight;
    _timestamp.textColor = [UIColor whiteColor];
    _timestamp.adjustsFontSizeToFitWidth = NO;
    [self.contentView addSubview:_timestamp];
}

- (void) setupSongName {
    
    CGFloat asdWidth;
    
    if(IS_DEVICE == IS_IPAD) {
        asdWidth = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - 30;
    }else {
        asdWidth = 290;
    }
    
    _artistAndSongNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(_playlistIcon.frame.origin.x + _playlistIcon.frame.size.width + 5, 20, asdWidth, 30)];
    _artistAndSongNameLabel.font = [UIFont boldSystemFontOfSize:16];
    _artistAndSongNameLabel.alpha = 1;
    _artistAndSongNameLabel.textAlignment = NSTextAlignmentLeft;
    _artistAndSongNameLabel.textColor = [UIColor whiteColor];
    _artistAndSongNameLabel.adjustsFontSizeToFitWidth = NO;
    [self.contentView addSubview:_artistAndSongNameLabel];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat asdWidth;
    
    if(IS_DEVICE == IS_IPAD) {
        asdWidth = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - 30;
    }else {
        asdWidth = 290;
    }
    
    _artistAndSongNameLabel.frame = CGRectMake(_playlistIcon.frame.origin.x + _playlistIcon.frame.size.width + 5, 20, asdWidth, 30);
    
    CGFloat x = _playlistName.frame.origin.x + _playlistName.frame.size.width + 5;
    CGFloat mainWidth;
    
    if(IS_DEVICE == IS_IPAD) {
        mainWidth = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD;
    }else {
        mainWidth = [Utils screenWidth];
    }
    _timestamp.frame = CGRectMake(x, 5, mainWidth - x - 10, 16);
}

@end

#import "SongCell.h"
#import "StandardCellBorder.h"
#import "PlaylistSong.h"
#import "SongCellScrollView.h"
#import "Player.h"
#import "AudioPlayer.h"
#import "MainViewController.h"
#import "JASidePanelController.h"
NSString *const SongCellEnclosingTableViewDidBeginScrollingNotification = @"SongCellEnclosingTableViewDidScrollNotification";
UISwipeGestureRecognizer * swipeleft;
#define kCatchWidth 320

@interface SongCell () <UIScrollViewDelegate>



@end


@implementation SongCell

@synthesize artistLabel;
@synthesize songNameLabel;
@synthesize cellBorder;
@synthesize frontView, slideButtonsView, shareButton, addToButton, discoverButton, removeButton;
@synthesize scrollViewSelf;
@synthesize canShare = _canShare;
@synthesize canAddTo = _canAddTo;
@synthesize canRemove = _canRemove;
@synthesize canDiscover = _canDiscover;

- (void) awakeFromNib {
    [super awakeFromNib];
    [self setupFrontView];
    [self setupButtons];
    [self setupSlideCell];
}

- (void) setOnlineSongInteractions {
    self.canShare = YES;
    self.canAddTo = YES;
    self.canRemove = YES;
    self.canDiscover = YES;
}

- (void) setOfflineSongInteractions {
    self.canShare = NO;
    self.canAddTo = NO;
    self.canRemove = YES;
    self.canDiscover = NO;
}

- (void) setCanShare:(BOOL)val {
    [self setButton:shareButton active:val];
}

- (void) setCanAddTo:(BOOL)val {
    [self setButton:addToButton active:val];
}

- (void)setCanRemove:(BOOL)val{
    //Log(@"set can remove: %hhd", val);
    [self setButton:removeButton active:val];
}

- (void)setCanDiscover:(BOOL)val{
    [self setButton:discoverButton active:val];
}

- (void) setButton:(UIButton *)button active:(BOOL)active {
    if (active) {
        button.alpha = 1;
        button.userInteractionEnabled = YES;
    } else {
        button.alpha = 0.2;
        button.userInteractionEnabled = NO;
    }
}

- (void) setupFrontView {
    
    if(IS_DEVICE == IS_IPAD) {
        frontView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD, self.frame.size.height)];
    }else {
        frontView = [[UIView alloc] initWithFrame:self.frame];
    }
    
    //Log(@"cell frame: %f",frontView.frame.size.width);
    
    frontView.backgroundColor = [UIColor clearColor];
    
    cellBorder = [[StandardCellBorder alloc] init];
    
    self.icon = [[UIImageView alloc] init];
    
    [frontView addSubview:self.icon];
    
    [frontView addSubview:artistLabel];
    [frontView addSubview:songNameLabel];
    
    
    [cellBorder initWithParent:self.contentView];
   
    

}
-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    alert(@"ok");
}

- (void) setupButton:(UIButton *)button withTitle:(NSString *)title andIconName:(NSString *)iconName {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, button.frame.size.width, 18)];
    label.text = title;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:13];
    label.textAlignment = NSTextAlignmentCenter;
    [button addSubview:label];
    
    UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
    icon.center = label.center;
    CGRect frame = icon.frame;
    frame.origin.y = 10;
    icon.frame = frame;
    [button addSubview:icon];
    
    button.backgroundColor = [UIColor clearColor];
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    
    [slideButtonsView addSubview:button];
}

- (CGRect) buttonFrameForIndex:(NSInteger)index forTotalButtons:(NSInteger)totalButtons {
    
    CGFloat w;
    CGFloat initX = 24;
    
    if(IS_DEVICE == IS_IPAD) {
        w = (slideButtonsView.frame.size.width/2)/totalButtons - 18.0f;
        initX = (slideButtonsView.frame.size.width/2) + 65.0f;
    }else {
        w = slideButtonsView.frame.size.width/totalButtons-6;
    }
    
    return CGRectMake(initX + w*index, 0, w, slideButtonsView.frame.size.height);
}

- (void) setupButtons {
    
    slideButtonsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-1)];
    slideButtonsView.clipsToBounds = YES;
    UIImageView *background = [[UIImageView alloc] initWithImage:[Utils imageFromColor:RGBA(0,0,0,0.5) withWidth:self.bounds.size.width andHeight:self.bounds.size.height]];
    [slideButtonsView addSubview:background];
    background.frame = slideButtonsView.frame;
    background.contentMode = UIViewContentModeScaleToFill;
    background.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    int totalButtons;
    int index;
    
    totalButtons = 5;
    index = 1;
    
    shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = [self buttonFrameForIndex:index++ forTotalButtons:totalButtons];
    [self setupButton:shareButton withTitle:@"Share" andIconName:@"icon_share.png"];
    [shareButton addTarget:self action:@selector(userPressedShareButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    addToButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addToButton.frame = [self buttonFrameForIndex:index++ forTotalButtons:totalButtons];
    [self setupButton:addToButton withTitle:@"Add To" andIconName:@"icon_normal_playlist.png"];
    [addToButton addTarget:self action:@selector(userPressedAddToButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    discoverButton = [UIButton buttonWithType:UIButtonTypeCustom];
    discoverButton.frame = [self buttonFrameForIndex:index++ forTotalButtons:totalButtons];
    [self setupButton:discoverButton withTitle:@"Discover" andIconName:@"icon_playlists_with.png"];
    [discoverButton addTarget:self action:@selector(userPressedDiscoverButton:) forControlEvents:UIControlEventTouchUpInside];
    
    removeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    removeButton.frame = [self buttonFrameForIndex:index++ forTotalButtons:totalButtons];
    [self setupButton:removeButton withTitle:@"Remove" andIconName:@"icon_close_red.png"];
    [removeButton addTarget:self action:@selector(userPressedRemoveButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self closeSlideButtonsAnimated:NO];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    self.scrollViewSelf.contentSize = CGSizeMake(self.bounds.size.width * 2, self.bounds.size.height);
    self.scrollViewSelf.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    self.slideButtonsView.frame = CGRectMake(self.bounds.size.width, 0, 0, self.slideButtonsView.frame.size.height);


    
}

- (void) closeSlideButtonsAnimated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:.2 animations:^{
            slideButtonsView.frame = CGRectMake([Utils screenWidth], 0, 0, self.bounds.size.height);
        }];
    } else {
        slideButtonsView.frame = CGRectMake([Utils screenWidth], 0, 0, self.bounds.size.height);
    }
}

- (void) openSlideButtonsAnimated:(BOOL)animated {
    
    
    if (animated) {
        [UIView animateWithDuration:1 animations:^{
            slideButtonsView.frame = CGRectMake(0, 0, [Utils screenWidth], self.bounds.size.height);
        }];
    } else {
        slideButtonsView.frame = CGRectMake(0, 0, [Utils screenWidth], self.bounds.size.height);
    }
    
}

- (void) setupSlideCell {
    // Set up our contentView hierarchy
    //Log(@"scroll cell width %f",self.bounds.size.width);
    SongCellScrollView *scrollView = [[SongCellScrollView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    scrollView.contentSize = CGSizeMake(self.bounds.size.width*2, self.bounds.size.height);
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.backgroundColor = [UIColor clearColor];
   
    self.scrollViewSelf = scrollView;
    swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionRight;
    
   

    
    

    
    //[self.scrollView addSubview:frontView];
    [self.scrollViewSelf addSubview: songNameLabel];
    [self.scrollViewSelf addSubview: artistLabel];
    [self.scrollViewSelf addSubview:slideButtonsView];
    

    
    
    [self.contentView addSubview:self.scrollViewSelf];
    
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enclosingTableViewDidScroll) name:SongCellEnclosingTableViewDidBeginScrollingNotification object:nil];
}
- (BOOL)gestureRecognizer:(UIPanGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UISwipeGestureRecognizer *)otherGestureRecognizer
    {

        return YES;
    }
-(void)userPressedAddToButton:(id)sender {
    [Analytics event:@"song_cell":@"press_add_to"];
    if ([self.slideDelegate respondsToSelector:@selector(cellDidSelectAddTo:)]) [self.slideDelegate cellDidSelectAddTo:self];
}
-(void)launchalert:(UIGestureRecognizer *) gestt{
    
    
}

-(void)userPressedShareButton:(id)sender {
    [Analytics event:@"song_cell":@"press_share"];
    if ([self.slideDelegate respondsToSelector:@selector(cellDidSelectShare:)]) [self.slideDelegate cellDidSelectShare:self];
}

-(void)userPressedDiscoverButton:(id)sender {
    [Analytics event:@"song_cell":@"press_discover"];
    if ([self.slideDelegate respondsToSelector:@selector(cellDidSelectDiscover:)]) [self.slideDelegate cellDidSelectDiscover:self];
}

-(void)userPressedRemoveButton:(id)sender {
    [Analytics event:@"song_cell":@"press_remove"];
    
    //Log(@"slide delegate: %@",self.slideDelegate);
      //[[Player sharedInstance] stop];
  //AICIII
   // [[Player sharedInstance] nextSong];
    //[[AudioPlayer sharedInstance] nextSong];
    

    
    if ([self.slideDelegate respondsToSelector:@selector(cellDidSelectRemove:)]) [self.slideDelegate cellDidSelectRemove:self];
}

-(void)prepareForReuse {
    [super prepareForReuse];
    [self.scrollViewSelf setContentOffset:CGPointZero animated:YES];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([self.slideDelegate respondsToSelector:@selector(cellWasTapped:)]) [self.slideDelegate cellWasTapped:self];
}

#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.slideDelegate respondsToSelector:@selector(cellStartedRevealing:)]) [self.slideDelegate cellStartedRevealing:self];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(CGPoint *)targetContentOffset {
    if(IS_DEVICE==IS_IPAD) {
        if (scrollView.contentOffset.x > kCatchWidth/2+15) {
            targetContentOffset->x = kCatchWidth+30;
        } else {
            *targetContentOffset = CGPointZero;
            [UIView animateWithDuration:.25 animations:^{
                [scrollView setContentOffset:CGPointZero];
            }];
        }
    }
    else
    {
        if (scrollView.contentOffset.x > kCatchWidth/2) {
            targetContentOffset->x = kCatchWidth;
        } else {
            *targetContentOffset = CGPointZero;
            [UIView animateWithDuration:.25 animations:^{
                [scrollView setContentOffset:CGPointZero];
            }];
            
            
        }
        
    }}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat maxWidth;
    
    if(IS_DEVICE == IS_IPAD) {
        maxWidth = self.frame.size.width;
    }else {
        maxWidth = [Utils currentWidth];
    }
    
    
    if (scrollView.contentOffset.x < 0/*&&scrollView.contentOffset.x > -18*/) {
        
        scrollView.contentOffset = CGPointZero;
    }
    /*else if(scrollView.contentOffset.x<-18){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"openmenunot"
         object:self];
        NSLog(@"da");
        
        
    }*/else if (scrollView.contentOffset.x > maxWidth) {
        scrollView.contentOffset = CGPointMake(maxWidth, 0);
    }
    
    self.slideButtonsView.frame = CGRectMake(maxWidth, 0, scrollView.contentOffset.x, slideButtonsView.frame.size.height);
}

-(void)enclosingTableViewDidScroll {
    [self.scrollViewSelf setContentOffset:CGPointZero animated:YES];

}


#undef kCatchWidth

- (void)takeShotIn:(UIView *)snapshotImageView{
    
    CGRect cellFrame = [self bounds];
    
    //Log(@"CELL FRAME BOUNDS: %@", NSStringFromCGRect(snapshotImageView.frame));
    cellBorder.borderBottom.alpha = 0;
    cellBorder.selectedView.alpha=0;
    
    UIGraphicsBeginImageContextWithOptions(cellFrame.size, NO, [[UIScreen mainScreen] scale]);
    [[self layer] renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    cellBorder.borderBottom.alpha = 1;
    cellBorder.selectedView.alpha=1;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:snapshotImage];
    
    snapshotImageView.backgroundColor = [UIColor clearColor];
    
    UIImageView *topShadowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shadow_bottom.png"]];
    UIImageView *bottomShadowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shadow_bottom.png"]];
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"drawer_handle.png"]];
    
    [snapshotImageView addSubview:background];
    [snapshotImageView addSubview:topShadowView];
    [snapshotImageView addSubview:bottomShadowView];
    [snapshotImageView addSubview:imageView];
    
    CGFloat width;
    CGFloat x;
    
    /*if(IS_DEVICE == IS_IPAD) {
     width = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD;
     //x = - LEFTPANEL_WIDTH_IPAD / 2;
     x = - ( ( [Utils currentWidth] - cellFrame.size.width ) / 2 );
     
     if([Utils isLandscape]) {
     x += 125.0f;
     }
     }else {
     width = [Utils currentWidth];
     x = 0;
     }*/
    if(IS_DEVICE == IS_IPAD) {
        x = self.frame.origin.x;
        width = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD;
    } else {
        x = 0;
        width = [Utils currentWidth];
    }
    imageView.frame = CGRectMake(x, 0, width, snapshotImageView.frame.size.height);
    background.frame = imageView.frame;
    topShadowView.transform = CGAffineTransformMakeRotation(M_PI);
    topShadowView.frame = CGRectMake(x, -10, width, 10);
    bottomShadowView.frame = CGRectMake(x, snapshotImageView.frame.size.height, width, 10);
    
    //Log(@"CELL FRAME BOUNDS: %@", NSStringFromCGRect(snapshotImageView.frame));
    
    
}

- (void) prepareForMove {
    self.backgroundColor      = [UIColor clearColor];
    self.textLabel.text       = @"";
    self.detailTextLabel.text = @"";
    self.imageView.image      = nil;
    
    [self removeIcon];
    
    cellBorder.borderBottom.alpha = 0;
    cellBorder.borderLeft.alpha   = 0;
    cellBorder.selectedView.alpha = 0;
    
    artistLabel.text   = @"";
    songNameLabel.text = @"";
}

- (void) markSelected:(BOOL)state {
    //Log(@"state: %hhd", state);
    [cellBorder markSelected:state];
}

- (void) markPlaying:(BOOL)state {
    [cellBorder markPlaying:state];
}

- (void) addIcon:(UIImage *)iconImage {
    [self.icon initWithImage: iconImage];
    [self addSubview:self.icon];
    self.icon.frame = CGRectMake(10, (52 - iconImage.size.height)/2, iconImage.size.width, iconImage.size.height);
}

- (void) removeIcon {
    if (self.icon) [self.icon removeFromSuperview];
}

- (void) setUnseen:(BOOL)unseen {
    [self markSelected:!unseen];
}



@end


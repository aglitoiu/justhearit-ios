//
// Created by andrei st on 10/15/13.
// Copyright (c) 2013 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface UIImageView (Utils)

+ (UIImageView*) imageNamed:(NSString *)name;

@end
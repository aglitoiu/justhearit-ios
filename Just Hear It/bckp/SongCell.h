#import "FMMoveTableViewCell.h"

@class StandardCellBorder;
@class SongCell;
@class PlaylistSong;

@protocol SongCellSlideDelegate <UIGestureRecognizerDelegate>

- (void) cellDidSelectShare:(SongCell *)cell;
- (void) cellDidSelectAddTo:(SongCell *)cell;
- (void) cellDidSelectDiscover:(SongCell *)cell;
- (void) cellDidSelectRemove:(SongCell *)cell;
- (void) cellStartedRevealing:(SongCell *)cell;
- (void) cellWasTapped:(SongCell *)cell;

@end

extern NSString *const SongCellEnclosingTableViewDidBeginScrollingNotification;

@interface SongCell : UITableViewCell <FMMoveTableViewCell>

@property (nonatomic, weak) UIScrollView *scrollViewSelf;

@property (nonatomic, strong) IBOutlet UILabel* artistLabel;
@property (nonatomic, strong) IBOutlet UILabel* songNameLabel;

@property (nonatomic, strong) PlaylistSong *song;

@property (nonatomic, strong) UIView *frontView;

@property (nonatomic, strong) StandardCellBorder *cellBorder;
@property (nonatomic, strong) UIImageView *icon;

@property (nonatomic, strong) UIView *slideButtonsView;

@property(nonatomic, strong) UIButton *shareButton;
@property(nonatomic, strong) UIButton *addToButton;
@property(nonatomic, strong) UIButton *discoverButton;
@property(nonatomic, strong) UIButton *removeButton;

@property(nonatomic, assign) BOOL canShare;
@property(nonatomic, assign) BOOL canAddTo;
@property(nonatomic, assign) BOOL canDiscover;
@property(nonatomic, assign) BOOL canRemove;


@property (nonatomic, weak) id<SongCellSlideDelegate> slideDelegate;

- (void)setOnlineSongInteractions;

- (void)setOfflineSongInteractions;

- (void)enclosingTableViewDidScroll;

- (void) prepareForMove;

- (void) markSelected:(BOOL)state;
- (void) markPlaying:(BOOL)state;
- (void) addIcon:(UIImage *) iconImage;
-(void) launchalert:(UIGestureRecognizer *) gestt;
- (void)removeIcon;

- (void)setUnseen:(BOOL)unseen;
@end

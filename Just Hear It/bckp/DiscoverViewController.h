#import "ModalBackgroundViewController.h"
#import "PlaylistSong.h"

@class DiscoverViewController;

@protocol DiscoverViewControllerDelegate <NSObject>

- (void) discoverViewController:(DiscoverViewController*)controller didFinishSearchWithQuery:(NSString*)query forPlaylists:(BOOL)forPlaylists;

@end

@interface DiscoverViewController : ModalBackgroundViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak) id <DiscoverViewControllerDelegate> delegate;
@property (nonatomic, strong) PlaylistSong *song;
@property (nonatomic, strong) NSMutableArray *rows;
@property (nonatomic, strong) IBOutlet UITableView *discoverTable;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

- (void)dismissViewController:(id)sender;

@end
//
// Created by andrei st on 1/14/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import "MZFayeClient/MZFayeClient.h"
#import "FayeManager.h"
#import "JusthearitClient.h"
#import "FayeConfig.h"
#import "PlaylistBadgeChangeNotification.h"
#import "PlaylistChangeNotification.h"
#import "Playlists.h"


@implementation FayeManager

@synthesize state;

+ (FayeManager *)sharedInstance {
    static FayeManager *__sharedInstance;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        __sharedInstance = [[FayeManager alloc] init];
    });

    return __sharedInstance;
}

- (void) disconnect {
    if (self.client.isConnected) {
        [self.client disconnect:^{
            
        } failure:^(NSError *error) {
            
        }];
    }
}

- (void) verifySessionAndConnect {
    if (SETTINGS.isLoggedIn) {
        [[JusthearitClient sharedInstance] checkLoginTokenWithSuccessBlock:^(NSArray *array) {
            if ([[(Session *) _.first(array) validToken] isEqualToString:@"valid"]) {
                [self connect];
            } else {
                SETTINGS.logout;
            }
        } withErrorBlock:^(NSError *error) {
            Log(@"error %@", error);
            
        }];
    }
}

- (NSString*) channel {
    return [NSString stringWithFormat:@"/notifications/%d", SETTINGS.userId.intValue];
}

- (void) connect {
    
    [self disconnect];
    
 
    
//    [[JusthearitClient sharedInstance] getFayeSubscriptionParametersForChannel:self.channel withSuccessBlock:^(NSArray *array)
//    {
//        FayeConfig *config = array.firstObject;
//        self.client = [[MZFayeClient alloc] initWithURL:[NSURL URLWithString:JhiFayeUrl]];
//        [self.client setExtension:@{ @"private_pub_timestamp": config.timestamp, @"private_pub_signature": config.signature } forChannel:self.channel];
//
//        [self.client subscribeToChannel:self.channel usingBlock:^(NSDictionary *message)
//        {
//            Log(@"Received FAYE Notification %@",message);
//            NSDictionary *data = message[@"data"];
//
//            if (data && data[@"playlist_badge_change"])
//            {
//                PlaylistBadgeChangeNotification *notification = [[PlaylistBadgeChangeNotification alloc] init];
//                notification.badgeCount = data[@"playlist_badge_change"][@"badge_count"];
//                notification.playlistId = [NSString stringWithFormat:@"%d", [data[@"playlist_badge_change"][@"id"] intValue]];
//                notification.song_ids = [NSMutableArray arrayWithArray:data[@"playlist_badge_change"][@"song_ids"]];
//                
//                Log(@"song_ids %@",data[@"playlist_badge_change"][@"song_ids"]);
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsNotification object:nil userInfo:notification];
//                [[NSNotificationCenter defaultCenter] postNotificationName:Ev.shouldUpdateNewSongs object:nil userInfo:notification];
//            }
//            if (data && data[@"mobile"][@"playlist_badge_change"]) {
//                PlaylistBadgeChangeNotification *notification = [[PlaylistBadgeChangeNotification alloc] init];
//                notification.badgeCount = data[@"mobile"][@"playlist_badge_change"][@"badge_count"];
//                notification.playlistId = [NSString stringWithFormat:@"%d", [data[@"mobile"][@"playlist_badge_change"][@"id"] intValue]];
//                notification.song_ids = [NSMutableArray arrayWithArray:data[@"mobile"][@"playlist_badge_change"][@"song_ids"]];
//                
//                Log(@"song_ids %@",data[@"mobile"][@"playlist_badge_change"][@"song_ids"]);
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsNotification object:nil userInfo:notification];
//                [[NSNotificationCenter defaultCenter] postNotificationName:Ev.shouldUpdateNewSongs object:nil userInfo:notification];
//            }
//            if(data && [data[@"mobile"][@"event"] isEqualToString:@"delete"]) {
//                PlaylistChangeNotification *notification = [[PlaylistChangeNotification alloc] init];
//                notification.playlistId = [NSString stringWithFormat:@"%d", [data[@"mobile"][@"playlist_id"] intValue]];
//                notification.event = data[@"mobile"][@"event"];
//                
//                Log(@"playlist id: %@", notification.playlistId);
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsChangeNotification object:nil userInfo:notification];
//            }
//            if(data && [data[@"mobile"][@"event"] isEqualToString:@"new_playlist"]) {
//                PlaylistChangeNotification *notification = [[PlaylistChangeNotification alloc] init];
//                notification.playlistId = [NSString stringWithFormat:@"%d", [data[@"mobile"][@"playlist_id"] intValue]];
//                notification.event = data[@"mobile"][@"event"];
//                
//                Log(@"playlist id: %@", notification.playlistId);
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsChangeNotification object:nil userInfo:notification];
//            }
//            if(data && [data[@"mobile"][@"event"] isEqualToString:@"position"]) {
//                PlaylistChangeNotification *notification = [[PlaylistChangeNotification alloc] init];
//                notification.playlistId = [NSString stringWithFormat:@"%d", [data[@"mobile"][@"playlist_id"] intValue]];
//                notification.event = data[@"mobile"][@"event"];
//                notification.position = [NSString stringWithFormat:@"%d", [data[@"mobile"][@"position"] intValue]];
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsChangeNotification object:nil userInfo:notification];
//            }
//        }];
//
//        self.client.connect;
//    } withErrorBlock:^(NSError *error) {
//        Log(@"error %@", error);
//    }];

}

@end

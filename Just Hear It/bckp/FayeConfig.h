#import <Foundation/Foundation.h>

@interface FayeConfig : NSObject

@property(nonatomic, strong) NSString *timestamp;
@property(nonatomic, strong) NSString *signature;

@end
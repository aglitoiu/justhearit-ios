
#import "UIView+Geometry.h"


@implementation UIView (Geometry)

- (void) paddingLeft:(int) left {
    CGRect newFrame = self.frame;
    newFrame.origin.x += left;
    newFrame.size.width -= left;
    self.frame = newFrame;
}

- (void) paddingRight:(int) right {
    CGRect newFrame = self.frame;
    newFrame.size.width -= right;
    self.frame = newFrame;
}

- (void) width:(int)w {
    CGRect newFrame = self.frame;
    newFrame.size.width = w;
    self.frame = newFrame;
}

- (void) marginLeft:(int) left {
    CGRect newFrame = self.frame;
    newFrame.origin.x += left;
    self.frame = newFrame;
}

- (void) top:(int) top {
    CGPoint center = self.center;
    center.y = top;
    self.center = center;
}

@end
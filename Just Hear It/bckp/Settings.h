//
// Created by andrei st on 1/30/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Settings : NSObject

@property(nonatomic, strong) NSNumber *disableAds;
@property(nonatomic, strong) NSNumber *firstAdAfterMinutes;
@property(nonatomic, strong) NSNumber *firstAdLength;
@property(nonatomic, strong) NSNumber *normalAdsLength;
@property(nonatomic, strong) NSNumber *normalAdsAfterMinutes;

+ (Settings *) defaultSettings;

- (BOOL) adsDisabled;

@end
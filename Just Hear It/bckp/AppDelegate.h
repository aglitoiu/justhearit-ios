//
//  AppDelegate.h
//  Just Hear It
//
//  Created by andrei st on 10/9/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

extern NSString *const FBSessionStateChangedNotification;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSTimer *reachabilityTimer;
@property (nonatomic, assign) BOOL online;
@property (nonatomic, strong) NSString *applicationShouldLoadResourceAfterLoadingPlaylists;




- (void)closeSession;

@end

#import <Foundation/Foundation.h>
#import "AdDetails.h"

#define MINUTE_SECONDS 60

@class AdManager;
@class AudioPlayer;
@class Settings;

@protocol AdManagerDelegate <NSObject>

- (void) adManager:(AdManager*)manager fireAds:(AdDetails *)ads;

@end

@interface AdManager : NSObject

+ (instancetype) sharedInstance;
- (void)songWillPlayWithUserAction:(BOOL)userAction;

@property(nonatomic, strong) Settings *settings;
@property (weak) id <AdManagerDelegate> delegate;
@property (nonatomic, assign) NSInteger currentSeconds;
@property (nonatomic, assign) BOOL firstAdShown ;
@property (nonatomic, assign) BOOL playerShouldHalt;

@end
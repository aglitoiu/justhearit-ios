//
//  CachedLocalPlaylist.m
//  Just Hear It
//
//  Created by ciprian st on 11/05/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "CachedLocalPlaylist.h"
#import "CachedPlaylistSong.h"

@implementation CachedLocalPlaylist

@dynamic playlistId;
@dynamic playlistType;
@dynamic playlistGroup;
@dynamic isDefault;
@dynamic name;
@dynamic updatedAt;
@dynamic cachedLocalPlaylistSongs;
@dynamic totalPages;
@dynamic currentPage;
@dynamic position;
@dynamic erasableContent;
@dynamic shareUrl;



@end

#import "AdManager.h"
#import "AudioPlayer.h"
#import "JusthearitClient.h"
#import "Settings.h"

@implementation AdManager

+ (instancetype)sharedInstance {
    static AdManager *__sharedInstance;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        __sharedInstance = [[AdManager alloc] init];
        __sharedInstance.settings = [Settings defaultSettings];

        [[JusthearitClient sharedInstance] getSettingsWithSuccessBlock:^(NSArray *array) {
            if ([array.firstObject isKindOfClass:[Settings class]]) {
                __sharedInstance.settings = array.firstObject;
            }
        } withErrorBlock:^(NSError *error) {}];
    });

    return __sharedInstance;
}

- (id) init {
    self = [super init];

    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tick) name:Ev.adTick object:nil];

        _firstAdShown = NO;
        _currentSeconds = 0;
        _playerShouldHalt = NO;
    }

    return self;
}

- (void) tick {
    
    NSLog(@"<<<<<<<<<< tick >>>>>>>>>>");
    if (_settings.adsDisabled) {
        _currentSeconds = 0;
        _playerShouldHalt = NO;
    } else {
        _currentSeconds++;
        if (_firstAdShown) {
            if (_currentSeconds > _settings.normalAdsAfterMinutes.intValue*MINUTE_SECONDS) _playerShouldHalt = YES;
        } else {
            if (_currentSeconds > _settings.firstAdAfterMinutes.intValue*MINUTE_SECONDS) _playerShouldHalt = YES;
        }
    }
}

- (void)songWillPlayWithUserAction:(BOOL)userAction {
    //this is where we decide the number of ads to show, and if preroll or not

    if (_playerShouldHalt) {
        id ads;

        _currentSeconds = 0;
        _playerShouldHalt = NO;

        if (_firstAdShown) {
            ads = [AdDetails adWithType:userAction andSeconds:_settings.normalAdsLength.intValue];
        } else {
            ads = [AdDetails adWithType:userAction andSeconds:_settings.firstAdLength.intValue];
        }

        [self.delegate adManager:self fireAds:ads];
    }

}
@end
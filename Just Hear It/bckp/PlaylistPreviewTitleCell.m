#import "PlaylistPreviewTitleCell.h"

@implementation PlaylistPreviewTitleCell

@synthesize titleLabel, icon;

- (void) awakeFromNib {
    //setup the icon
    icon = [[UIImageView alloc] init];
    icon.frame = CGRectMake(PLAYLISTS_PREVIEW_ICON_PADDING_LEFT, 0, PLAYLISTS_PREVIEW_ICON_WIDTH, PLAYLISTS_CELL_HEIGHT);
    icon.contentMode = UIViewContentModeCenter;
    [self addSubview:icon];

    //setup the label
    titleLabel = [[UILabel alloc] init];
    [self addSubview:titleLabel];
    titleLabel.frame = CGRectMake(icon.frame.origin.x + icon.frame.size.width + PLAYLISTS_PREVIEW_ICON_PADDING_LEFT, 0, PLAYLISTS_CELL_MAXIMUM_SELECTION_WIDTH - 45, PLAYLISTS_CELL_HEIGHT);
    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    titleLabel.textColor = RGB(255,255,255);

}

- (void)takeShotIn:(UIView *)snapshotImageView { }
@end

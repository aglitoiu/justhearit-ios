//
//  TargetSpot.h
//  libTargetSpot
//
//  Created by Kirby Turner on 5/14/10.
//
//  Copyright 2010 TargetSpot, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TargetSpotDelegate;

@interface TargetSpotAdView : UIView {
  @package
  id<TargetSpotDelegate> delegate_;
  NSArray *desiredPlaybackLengths_;
  NSString *currentTargetSpotStation_;
  NSInteger currentTargetSpotAdPlackLength_;
}

@property (nonatomic, assign) id<TargetSpotDelegate> delegate;

@property (nonatomic, retain) NSArray *desiredPlaybackLengths;

@property (nonatomic, copy, setter=updateTargetSpotStation:) NSString *currentTargetSpotStation;

@property (nonatomic, setter=updateTargetSpotAdPlaybackLength:) NSInteger currentTargetSpotAdPlackLength;

/**
 * Creates and returns an auto-released reference to an instance of
 * the TargetSpotAdView class.
 */
+ (TargetSpotAdView *)adView;

/**
 * Shows the TargetSpot ad and starts playback.
 */
- (void)showAnimated:(BOOL)animated;

/**
 * Hides the TargetSpot ad.
 */
- (void)hideAnimated:(BOOL)animated;

/** 
 * Starts an ad session. 
 * This will automatically download new ads and store them in a queue
 * awaiting display.
 */
- (void)startAdSession;

/** 
 * Stops an ad session.
 * This will halt an ad session and release any associated memory and
 * system resources.
 */
- (void)stopAdSession;

/**
 * Read-only flag indicating whether the ad session has started or not.
 */
- (BOOL)isAdSessionStarted;

/**
 * Retrieves and displays a new preroll ad.
 */
- (void)showPrerollAd;

/**
 * Convenience method for inline ad break.
 */
- (BOOL)beginAdBreak;

/**
 * Convenience method for preroll ad break.
 */
- (BOOL)beginPreroll;

/**
 * Begin an ad break.
 * This will begin an ad break in unmanaged mode, will return TRUE if adBreak is available, FALSE if not.
 */
- (BOOL)beginAdBreak:(BOOL)preroll;

/**
 * Begin an ad break.
 * This will begin an ad break in back-to-back (unmanaged) mode, will return return TRUE if adBreak is available, FALSE if not.
 */
- (BOOL)beginB2BAdBreak:(BOOL)preroll adplb:(NSInteger)adPlaybackLength;

/**
 * End an ad break.
 * This will end an ad break in unmanaged mode.
 */
- (void)endAdBreak;

/**
 * Helper Method for Back-to-Back (Unmanaged) Mode
 * This returns a list of available ad break lengths; the list is a dictionary where the key (of type NSString) is the ad playback length and the value is the number of ad breaks.
 */
- (NSMutableDictionary *)getAvailBreaks;

/**
 * Helper Method for Back-to-Back (Unmanaged) Mode
 * This will return TRUE if any adBreak is available, FALSE if not.
 */
- (BOOL)breakAvail;

/**
 * Helper Method for Back-to-Back (Unmanaged) Mode
 * This will return TRUE if an adBreak of the specified playback length is available , FALSE if not.
 */
- (BOOL)breakAvail:(NSInteger)adPlaybackLength;

/**
 * This will tell the SDK that current ad has played and log the impression.
 */
- (void)adPlayed;

/**
 * This will tell the SDK that the current ad visual has been clicked and log the click.
 */
- (void)adClicked;

/**
 * After beginAdBreak has been called, this function will allow you to loop through each ad in the break.
 */
- (BOOL)hasAd;

/**
 * When in Back-to-Back mode, the SDK has to be explicitly notified to refresh ads.
 */
- (void)refreshAds;

/**
 * Get audioFile for current ad in unmanaged mode.
 */
- (NSURL *)getAdAudioFileURL;

/**
 * Get visualFile for current ad in unmanaged mode.
 */
- (NSURL *)getAdVisualFileURL;

/**
 * Get clickThrough URL for current ad in unmanaged mode. 
 */
- (NSURL *)getAdClickThroughURL;

/** 
 * Get company name for current ad in unmanaged mode.
 */
- (NSString *)getAdCompanyName;

/**
 * Get additional info URL for current ad in unmanaged mode. 
 */
- (NSString *)getAdAdditionalInfo;

@end

@interface TargetSpotAdHistoryView : UIView
/**
 * Creates and returns an auto-released reference to an instance of
 * the TargetSpotAdView class.
 */
+ (TargetSpotAdHistoryView *)adHistoryViewWithOrigin:(CGPoint)origin;

@end

/**
 *  TSGender
 *  
 *  Discussion:
 *    Type used to represent the gender of the listener.
 *
 */
typedef enum
{
  TSGenderUnknown = 0,
  TSGenderMale,
  TSGenderFemale,
} TSGender;

/**
 *  TSContentSize
 *  
 *  Discussion:
 *    Type used to represent the content size.
 *
 */
typedef enum
{
  TSContentSizeSmall = 0,
  TSContentSizeLarge,
  TSContentSizeUnknown
} TSContentSize;


/**
 *  TSAdType
 *  
 *  Discussion:
 *    Type used to represent the type of ad.
 *
 */
typedef enum
{
  TSAdTypeAudio = 0,   // Default
  TSAdTypeVideo,
  TSAdTypeText,
  TSAdTypeGraphic,
} TSAdType;

/**
 *  TSAudioFormat
 *  
 *  Discussion:
 *    Type used to represent the format of the audio.
 *
 */
typedef enum
{
  TSAudioMP3 = 0,   // Default
  TSAudioAAC,
  TSAudioOGG
} TSAudioFormat;

/**
 *  TSAudioBitrate
 *  
 *  Discussion:
 *    Type used to represent the bitrate of the audio.
 *
 */
typedef enum
{
  TSAudioBitrate64kbps = 0,   // Default
  TSAudioBitrate32kbps,
  TSAudioBitrate16kbps
} TSAudioBitrate;

/**
 *  TSLocationDegrees
 *  
 *  Discussion:
 *    Type used to represent a latitude or longitude coordinate in degrees under the WGS 84 reference
 *    frame. The degree can be positive (North and East) or negative (South and West).  
 */
typedef double TSLocationDegrees;

/**
 *  TSLocationCoordinate
 *  
 *  Discussion:
 *    A structure that contains a geographical coordinate.
 *
 *  Fields:
 *    latitude:
 *      The latitude in degrees.
 *    longitude:
 *      The longitude in degrees.
 */
typedef struct {
	TSLocationDegrees latitude;
	TSLocationDegrees longitude;
} TSLocationCoordinate;

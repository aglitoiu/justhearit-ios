#import <UIKit/UIKit.h>
#import "BackgroundViewController.h"
#import "UIViewController+JASidePanel.h"
#import "MainViewController.h"
#import "PlayerView.h"
#import "SuggestionCell.h"
#import "Autocomplete.h"
#import "AutocompleteCache.h"

@class SearchViewController;

@protocol SearchViewControllerDelegate <NSObject>

- (void) searchViewControllerDidFinish:(SearchViewController*) controller;
- (void) playlistsChanged;
- (void) openAfterSearch: (MainViewController*)mainVC andPlaylist:(id)playlist;

@end

@interface SearchViewController : BackgroundViewController <JASidePanelControllerDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) IBOutlet UITextField *textInput;
@property(nonatomic, strong) IBOutlet UIButton *closeButton;
@property(nonatomic, strong) IBOutlet UITableView *suggestionsTable;
@property(nonatomic, strong) IBOutlet NavigationBarView *navigationBar;
@property(nonatomic, strong) NSMutableArray *results;
@property(nonatomic, strong) AutocompleteCache *autocompleteCache;
@property(nonatomic, strong) MainViewController *mainV;
@property(nonatomic, strong) PlayerView *playerV;
@property (weak) id <SearchViewControllerDelegate> delegate;

- (void)dismissViewController:(id)sender;

- (IBAction) closeAction;

@end

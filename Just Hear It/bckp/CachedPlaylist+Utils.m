//
// Created by andrei st on 1/16/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "NSArray+OCTotallyLazy.h"
#import "CachedPlaylist+Utils.h"
#import "Playlist.h"
#import "CachedPlaylistSong.h"
#import "NSSet+OCTotallyLazy.h"
#import "OfflinePlaylist.h"
#import "Playlists.h"
#import "SongCache.h"
#import "FileManager.h"


@implementation CachedPlaylist (Utils)





+ (CachedPlaylist *) findByPlaylistId:(NSString *)playlistId {
    return [CachedPlaylist MR_findFirstByAttribute:@"playlistId" withValue:playlistId];
}

+ (void) findOrCreateByPlaylist:(Playlist *)playlist withBlock:(void (^)(CachedPlaylist *))block {
    CachedPlaylist *existingPlaylist = [CachedPlaylist findByPlaylistId:playlist.playlistId];
    
    if (existingPlaylist) {
        block(existingPlaylist);
    } else {
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];

        CachedPlaylist *cachedPlaylist = [CachedPlaylist MR_createEntityInContext:localContext];

        cachedPlaylist.name = playlist.name;
        cachedPlaylist.playlistId = playlist.playlistId;
                cachedPlaylist.playlistType = [playlist.playlistType stringValue];
        
 
        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {
            block(cachedPlaylist);
        }];
    }
}



- (BOOL) songAlreadyInPlaylist:(CachedPlaylistSong *)cachedPlaylistSong {
    __block BOOL songAlreadyInPlaylist = NO;

    [self.cachedPlaylistSongs.asArray foreach:^(CachedPlaylistSong *song) {
        if ([song.songId isEqualToString:cachedPlaylistSong.songId]) {
            songAlreadyInPlaylist = YES;
        }
    }];

    return songAlreadyInPlaylist;
}

+ (NSArray *) allAsPlaylists {
    NSArray *cachedPlaylists = [CachedPlaylist MR_findAll];
    return _.arrayMap(cachedPlaylists, ^(CachedPlaylist *cachedPlaylist){
        OfflinePlaylist *playlist = [[OfflinePlaylist alloc] init];
        playlist.playlistId = cachedPlaylist.playlistId;
        playlist.name = cachedPlaylist.name;
        playlist.cachedPlaylist = cachedPlaylist;
      
        
            playlist.playlistType = cachedPlaylist.playlistType;
            playlist.offlinePlaylistType = offlinePlaylistTypeNormal;
        

        playlist.playlistGroupObject = [[Playlists sharedInstance] defaultPlaylistsGroup];
        return playlist;
    });
}

+ (void)removeFromCache:(CachedPlaylist *)playlist {
    if (!playlist) return;

    NSArray *deletableSongs = [SongCache songsOnlyInCachedPlaylist:playlist];
    [deletableSongs foreach:^(CachedPlaylistSong *song) {
              [SongCache deleteSongFromCache:song];
        
    }];
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    [playlist MR_deleteEntityInContext:localContext];
    [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
}

@end

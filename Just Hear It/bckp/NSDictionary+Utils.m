#import "NSDictionary+Utils.h"

@implementation NSDictionary (Utils)

- (NSInteger)intValueForKey:(NSString *)key {
    if (key) {
        id obj = [self objectForKey:key];
        obj = [self nullToNil:obj];
        if ([obj isKindOfClass:[NSString class]]) return [obj intValue];
        if ([obj isKindOfClass:[NSNumber class]]) return [obj intValue];
    }
    return 0;
}

- (BOOL) boolValueForKey:(NSString *)key {
    if (key) {
        id obj = [self objectForKey:key];
        obj = [self nullToNil:obj];
        if ([obj isKindOfClass:[NSString class]]) {
            if ([obj intValue] > 0) return YES;
        }

        if ([obj isKindOfClass:[NSNumber class]]) {
            if ([obj intValue] > 0) return YES;
        }
    }
    return NO;
}

- (NSString *) stringValueForKey:(NSString *)key {
    if (key) return [self nullToNil:[self objectForKey:key]];
    return nil;
}

- (id)nullToNil:(id)object {
    if ([object isKindOfClass:[NSNull class]]) return nil;
    return object;
}


@end
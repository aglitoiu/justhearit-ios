//
// Created by andrei st on 12/12/13.
// Copyright (c) 2013 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@class Pagination;


@interface SearchParams : NSObject

@property(nonatomic, strong) NSString *query;
@property(nonatomic, strong) Pagination *pagination;

@end
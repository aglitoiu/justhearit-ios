#import "AFNetworking/AFRKHTTPRequestOperation.h"
#import "NSArray+OCTotallyLazy.h"
#import "InstaPlayDownloadManager.h"
#import "FileManager.h"
#import "PlaylistSong.h"
#import "PlaylistSong.h"

@implementation InstaPlayDownloadManager

+ (id)sharedInstance {
    static InstaPlayDownloadManager *__sharedInstance;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        __sharedInstance = [[InstaPlayDownloadManager alloc] init];
        __sharedInstance.isDownloading = NO;
        [FileManager createInstaPlayFolder];
    });

    return __sharedInstance;
}

- (void) downloadSong:(PlaylistSong *)song {
    [self downloadSong:song toFilePath:[FileManager filePathForInstaPlaySongId:song.songId]];
}

- (void) downloadSong:(PlaylistSong *)song toFilePath:(NSString *)path {
    [self cleanupSongs];

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:song.url]];
    AFRKHTTPRequestOperation *operation = [[AFRKHTTPRequestOperation alloc] initWithRequest:request];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];

    [self downloadStarted];

    __weak AFRKHTTPRequestOperation *weakOperation = operation;
    __weak InstaPlayDownloadManager *weakSelf = self;
    __weak NSMutableArray *weakQueue = self.queue;

    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        if (totalBytesRead > 150000) {
            Log(@"exceeded more than 150kb, stop download");
            [weakOperation cancel];
            [weakQueue removeObject:song];
            [weakSelf downloadNextSongIfPossible];
        }
    }];

    [operation start];
}

- (void)downloadNextSongIfPossible {
    [super downloadNextSongIfPossible];
}

- (void)pushSong:(PlaylistSong *)song {
    __block BOOL exists = NO;

    [self.queue foreach:^(PlaylistSong *s) {
        if ([s.songId isEqualToString:song.songId]) exists = YES;
    }];

    if(!exists) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:[FileManager filePathForInstaPlaySongId:song.songId]]) {
            [self.queue addObject:song];
            Log(@"adding instaplay song");
        }

        if (!self.isDownloading && self.queue.firstObject) {
            [self downloadSong:self.queue.firstObject];
        }
    }
}

- (void) cleanupSongs {

}

- (void) cleanupIncompleteSongs {

}

@end

#import "PlaylistGroupEmptyCell.h"


@implementation PlaylistGroupEmptyCell

@synthesize borderedView, titleLabel;

- (void)awakeFromNib {
    _border = [CAShapeLayer layer];
    _border.strokeColor = [UIColor colorWithRed:67/255.0f green:37/255.0f blue:83/255.0f alpha:1].CGColor;
    _border.fillColor = nil;
    _border.lineDashPattern = @[@4, @2];
    _border.cornerRadius = 5;

    borderedView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 100, 100)];
    [borderedView.layer addSublayer:_border];

    [self addSubview:borderedView];

    _border.path = [UIBezierPath bezierPathWithRect:borderedView.bounds].CGPath;
    _border.frame = borderedView.bounds;
}

@end
#import "PlaylistSong.h"
#import "Playlist.h"

@implementation PlaylistSong

- (BOOL) isPlayingOrPaused {
    return self.isPlaying || self.isPaused;
}

- (BOOL) isPlaying {
    return _playState == playStatePlaying;
}

- (BOOL) isPaused {
    return _playState == playStatePaused;
}

- (BOOL) hasProperty:(NSString *)prop {
    return prop && prop.length > 0 && ![[prop lowercaseString] isEqualToString:@"unknown"] && ![[prop lowercaseString] isEqualToString:@"untitled"];;
}

- (BOOL) isReorderable {
    return _playlist && _playlist.isErasable && !_playlist.isSystem;
}

- (BOOL)isEncrypted {
    return _encrypted.boolValue;
}

- (BOOL)isNew {
    //Log(@"is new playlist song is New: %hhd, %@", _isNewPlaylistSong.boolValue, _isNewPlaylistSong);
    return _isNewPlaylistSong.boolValue;
}

@end
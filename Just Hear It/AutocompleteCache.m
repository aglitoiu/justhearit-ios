#import "AutocompleteCache.h"
#import "JusthearitClient.h"
#import "Autocomplete.h"
#import "NSTimer+Blocks.h"

@implementation AutocompleteCache

@synthesize autocompletes, typingTimer, interval;

- (id) init {
    if (self=[super init]) {
        autocompletes = [[NSMutableDictionary alloc] init];
        interval = .3;
    }
    return self;
}

-(void) autocomplete:(NSString *) query withSuccessBlock:(void (^)(Autocomplete *))successBlock withErrorBlock:(void (^)(NSError *))errorBlock {
    if (autocompletes[query]) {
        successBlock(autocompletes[query]);
    } else {
        if (typingTimer && typingTimer.isValid) [typingTimer invalidate];

        typingTimer = [NSTimer scheduledTimerWithTimeInterval:interval block:^ {
            [[JusthearitClient sharedInstance] autocompleteWithQuery:query withSuccessBlock:^(NSArray *array) {
                Autocomplete *autocompleteResults = _.first(array);
                autocompletes[query] = autocompleteResults;
                successBlock(autocompleteResults);
            } withErrorBlock:^(NSError *error) {
                errorBlock(error);
            }];
        } repeats:NO];
    }
}

@end
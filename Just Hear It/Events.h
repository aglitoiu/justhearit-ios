
#import <Foundation/Foundation.h>

#define Ev ((Events *)[Events sharedInstance])

@interface Events : NSObject

+ (Events*) sharedInstance;

@property (nonatomic, strong) NSString* realtimeSearch;
@property (nonatomic, strong) NSString* submitSearch;
@property (nonatomic, strong) NSString* songStartedBuffering;
@property (nonatomic, strong) NSString* songPaused;
@property (nonatomic, strong) NSString* songResumed;
@property (nonatomic, strong) NSString* forcePause;
@property (nonatomic, strong) NSString* forcePlay;
@property (nonatomic, strong) NSString* playPauseButtonHit;
@property (nonatomic, strong) NSString* prevButtonHit;
@property (nonatomic, strong) NSString* nextButtonHit;
@property (nonatomic, strong) NSString* playlistSelected;
@property (nonatomic, strong) NSString* playlistSelectedWithoutClosePlaylistsDrawer;
@property (nonatomic, strong) NSString* playlistRefresh;
@property (nonatomic, strong) NSString* playlistsInitialized;
@property (nonatomic, strong) NSString* openInputTextViewController;
@property (nonatomic, strong) NSString* playlistModified;
@property (nonatomic, strong) NSString* playlistSongDeleted;
@property (nonatomic, strong) NSString* songProgressChanged;
@property (nonatomic, strong) NSString* songProgressManuallyChanged;
@property (nonatomic, strong) NSString* nowPlayingWasModified;
@property (nonatomic, strong) NSString* enableQueuePlaylist;
@property (nonatomic, strong) NSString* disableQueuePlaylist;
@property (nonatomic, strong) NSString* loginButtonTriggered;
@property (nonatomic, strong) NSString* searchButtonTriggered;

@property (nonatomic, strong) NSString* findPlaylists;
@property (nonatomic, strong) NSString* findPlaylistsFromSearch;
@property (nonatomic, strong) NSString* findPlaylistSongs;
@property (nonatomic, strong) NSString* findPlaylistSongsInPage;
@property (nonatomic, strong) NSString* requestStart;
@property (nonatomic, strong) NSString* requestEnd;
@property (nonatomic, strong) NSString* findTimeout;
@property (nonatomic, strong) NSString* fbAuthenticationDone;
@property (nonatomic, strong) NSString* classicLoginDone;
@property (nonatomic, strong) NSString* fbAuthenticationFail;
@property (nonatomic, strong) NSString* classicLoginFail;
@property (nonatomic, strong) NSString* logoutDone;
@property (nonatomic, strong) NSString* invalidToken;
@property (nonatomic, strong) NSString* restricted;
@property (nonatomic, strong) NSString* loadLyrics;
@property (nonatomic, strong) NSString* audioPercentageAvailable;
@property(nonatomic, strong) NSString *loadSongsFromUrl;
@property(nonatomic, strong) NSString *adTick;
@property(nonatomic, strong) NSString *adSongStopped;

@property (nonatomic, strong) NSString *songMovedInPlaylist;

@property (nonatomic, strong) NSString* searchClicked;
@property (nonatomic, strong) NSString* getAutocompleteResults;


@property (nonatomic, strong) NSString* messageCreateFromSearch;

@property(nonatomic, strong) NSString *genreButtonHit;
@property(nonatomic, strong) NSString *albumButtonHit;
@property(nonatomic, strong) NSString *artistButtonHit;
@property(nonatomic, strong) NSString *reachabilityChanged;
@property(nonatomic, strong) NSString *playlistsShouldReload;
@property(nonatomic, strong) NSString *playlistsShouldReloadOffline;
@property(nonatomic, strong) NSString *gotOffline;
@property(nonatomic, strong) NSString *gotOnline;
@property(nonatomic, strong) NSString *playlistsNotification;
@property(nonatomic, strong) NSString *playlistsChangeNotification;
@property(nonatomic, strong) NSString *navigationBarShouldReload;
@property(nonatomic, strong) NSString *findPlaylistSongsForceUpdate;
@property(nonatomic, strong) NSString *headsetUnplugged;
@property(nonatomic, strong) NSString *headsetInsert;
@property(nonatomic, strong) NSString *killQueue;
@property(nonatomic, strong) NSString *songStartedPlay;

@property(nonatomic, strong) NSString *shouldHideSpinner;
@property(nonatomic, strong) NSString *shouldUpdateNewSongs;

@property(nonatomic, strong) NSString *isScrubbing;

@end

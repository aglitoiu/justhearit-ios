#import "PlaylistHeader.h"
#import "PlaylistGroup.h"

@implementation PlaylistHeader

@synthesize playlistGroup, delegate, icon;

+ (PlaylistHeader *)initWithPlaylistGroup:(PlaylistGroup *)playlistGroup andWidth:(CGFloat)width {

    NSInteger offsetTop = 0;
    NSLog(@"");
    //setup the view
    PlaylistHeader *view = [[PlaylistHeader alloc] initWithFrame:CGRectMake(0, offsetTop, width, PLAYLISTS_HEADER_LABEL_HEIGHT)];
    view.backgroundColor = [UIColor clearColor];

    view.playlistGroup = playlistGroup;

    //setup the label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(PLAYLISTS_PADDING_LEFT, offsetTop, width, PLAYLISTS_HEADER_LABEL_HEIGHT)];
    label.font = [UIFont boldSystemFontOfSize:14];
    label.textColor = RGBA(255,255,255,.8);
    label.text = playlistGroup.name;
    [view addSubview:label];
    label.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:view action:@selector(headerHit)];
    [view addGestureRecognizer:tap];

    //setup the icon
    UIImageView *icon = [UIImageView imageNamed:@"dropdown.png"];
    icon.frame = CGRectMake(15, offsetTop, 12, PLAYLISTS_HEADER_LABEL_HEIGHT);
    icon.contentMode = UIViewContentModeCenter;
    [view addSubview:icon];
    view.icon = icon;

    NSString *zRotationKeyPath = @"transform.rotation.z";
    if (playlistGroup.collapsed) {
        [icon.layer setValue:@(-M_PI_2) forKeyPath:zRotationKeyPath];
    } else {
        [icon.layer setValue:@(0.0) forKeyPath:zRotationKeyPath];
    }

    return view;
}

- (void) headerHit {
    playlistGroup.collapsed = !playlistGroup.collapsed;
    if ([delegate respondsToSelector:@selector(playlistHeaderHit:)]) {
        [delegate playlistHeaderHit:self];
    }

    NSString *zRotationKeyPath = @"transform.rotation.z"; // The killer of typos

    // Change the model to the new "end value" (key path can work like this but properties don't)
    CGFloat currentAngle = [[icon.layer valueForKeyPath:zRotationKeyPath] floatValue];

    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:zRotationKeyPath];
    animation.duration = 0.2;

    animation.fromValue = @(currentAngle);
    if (currentAngle < 0)
        animation.toValue = @(0.0);
    else
        animation.toValue = @(-M_PI_2);

    [icon.layer setValue:animation.toValue forKeyPath:zRotationKeyPath];
    [icon.layer addAnimation:animation forKey:@"90rotation"];
}

@end
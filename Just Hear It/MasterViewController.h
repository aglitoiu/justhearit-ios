#import "JASidePanelController.h"
#import "NavigationBarView.h"
#import "TargetSpotDelegate.h"
#import "AdManager.h"
#import "TourViewController.h"
#import "SearchViewController.h"
#import "FontAwesomeKit.h"
#import "PullableView.h"
#import "PlayerView.h"


@class Playlists;
@class PlaylistsViewController;
@class MainViewController;
@class LoginViewController;
@interface MasterViewController : JASidePanelController <TargetSpotDelegate, AdManagerDelegate, TourViewControllerDelegate, SearchViewControllerDelegate, PullableViewDelegate, MainViewControllerDelegate>

@property(nonatomic, strong) Playlists * playlists;
@property(nonatomic, strong) PlaylistsViewController *playlistsViewController;
@property(nonatomic, strong) MainViewController *mainViewController;
@property(nonatomic, strong) LoginViewController *loginViewController;

@property(nonatomic, strong) TargetSpotAdView *adView;
@property(nonatomic, strong) AdManager *adManager;
@property(nonatomic, strong) AdDetails *adDetailsToShow;

@property (nonatomic, strong) IBOutlet NavigationBarView *navigationBar;
@property (nonatomic, strong) IBOutlet UIImageView * navigationBackground;
@property (nonatomic, strong) IBOutlet UIButton * searchButton;
@property (nonatomic, strong) IBOutlet PlayerView* playerView;
@property (nonatomic, assign) BOOL playerStarted;
-(void) hidePlayer;
- (void)playlistsChanged;
-(void) disableLeftRight;
-(void) enableLeftRight;
- (IBAction)adButtonPressed:(id)sender;
- (void) showPlayer;
-(BOOL) alreadyOffline;
- (void) mhandlePN:(NSNotification *)notification;
- (void) playlistsN:(NSNotification *)notification;
@end

#import "LoginViewController.h"
#import "JusthearitClient.h"
#import "FontAwesomeKit.h"
#import "JusthearitSlider.h"
#import "CacheSliderView.h"
#import "FayeManager.h"
#import "User.h"
#import "SongCacheDownloadManager.h"
#import "MPCoachMarks.h"

int iLog=0;

@implementation LoginViewController

- (IBAction)closeview:(id)sender {
   
    
}
@synthesize delegate, email, OfflineSwitch, OfflineSwitchLabel, password, mainViewController,facebookButton, mdbkg, navigationBar,playerView, orLine, signOutButton, sliderView, lteSwitchLabel, lteSwitch, forgotPasswordButton, email_register, password_register, password_confirm, full_name, createAccountButton, cancelButton, registerButton, animatedSpinner,progrs,OfflineView,lteView,animator,myInteger;

- (void)viewDidLoad {
    
    if(IS_DEVICE == IS_IPAD) {
        animatedSpinner = [Spinner initWithParentView:self.view withFixedWidth:LEFTPANEL_WIDTH_IPAD withHeight:[Utils currentHeight]];
    }else {
        animatedSpinner = [Spinner initWithParentView:self.view];
    }
    _login = [[FBSDKLoginManager alloc] init];
    
    email.delegate = self;
    password.delegate = self;
    email_register.delegate = self;
    password_register.delegate = self;
    password_confirm.delegate = self;
    full_name.delegate = self;
    
    [self setupFacebookButton];
    [self setupCacheSlider];
    [self setupLteSwitch];
    [self setupOfflineSwitch];
    self.animator = [[ZFModalTransitionAnimator alloc] initWithModalViewController:self];
    self.animator.dragable = YES;
    
    self.animator.bounces = YES;
    self.animator.behindViewAlpha = 1;
    self.animator.behindViewScale = 1;
    self.animator.transitionDuration = 0.7f;
    
    
    
    
    self.transitioningDelegate = self.animator;
    
    if (SETTINGS.isLoggedIn) [self showLogoutElements];
    else [self showLoginElements];
    
    [super viewDidLoad];
    
    
    //[self registers:@"mdacesazic@yahoo.com"];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(startedSliding:)    name:@"sliderStartdrag"  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(endedSliding:)    name:@"sliderEnddrag"  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(showLoginEl:)    name:@"showLoginEl"  object:nil];
  
    
    __weak typeof(self) weakSelf = self;
    
    //check if we're logged in, make sure we have a valid token. logout if we don't
    if (SETTINGS.isLoggedIn) {
        [[JusthearitClient sharedInstance] checkLoginTokenWithSuccessBlock:^(NSArray *array) {
            if ([[(Session *) _.first(array) validToken] isEqualToString:@"invalid"]) {
                               [weakSelf logout];
            }
        } withErrorBlock:^(NSError *error) {
           
            [weakSelf logout];
            
        }];
    }
    
    forgotPasswordButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgotPasswordButton setTitle:@"Forgot Password?" forState:UIControlStateNormal];
    forgotPasswordButton.frame = orLine.frame;
    forgotPasswordButton.alpha = 0;
    forgotPasswordButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.view addSubview:forgotPasswordButton];
    [forgotPasswordButton addTarget:self action:@selector(forgotPasswordHit) forControlEvents:UIControlEventTouchUpInside];
    forgotPasswordButton.titleLabel.textColor = RGBA(255,255,255,.6);
    
    createAccountButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [createAccountButton setTitle:@"Create Account" forState:UIControlStateNormal];
    createAccountButton.frame = CGRectMake(facebookButton.frame.origin.x, facebookButton.frame.origin.y + 60.0f, facebookButton.frame.size.width, facebookButton.frame.size.height);
    createAccountButton.alpha = 0;
    createAccountButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.view addSubview:createAccountButton];
    [createAccountButton addTarget:self action:@selector(createAccountHit) forControlEvents:UIControlEventTouchUpInside];
    [createAccountButton setTitleColor:RGBA(255,255,255,.6) forState:UIControlStateNormal];
    //createAccountButton.titleLabel.textColor = RGBA(255,255,255,.6);
    if (!SETTINGS.isLoggedIn) {
        [UIView animateWithDuration:0.9 animations:^{
            createAccountButton.alpha = 1;
        }];
    }
    
    [self setupCancelButton];
    [self setupRegisterButton];
    
    //AICI TUTORIALUU
    if(myInteger==1)
    {
       // [self StartTutL];
    }
   
}
- (void) StartTutL
{
    
    [self showLogoutElements];
    CGRect coachmark1 = sliderView.frame;
    CGRect coachmark2 = lteSwitch.frame;
    coachmark2.origin.x=lteSwitchLabel.frame.origin.x-5;
    
    coachmark2.origin.y-=5;
    coachmark2.size.height+=10;
    CGRect coachmark3 = OfflineSwitch.frame;
    coachmark3.origin.x=OfflineSwitchLabel.frame.origin.x-5;
    
    coachmark3.origin.y-=5;
    coachmark3.size.height+=10;
    
    if(IS_DEVICE==IS_IPAD)
    {
        coachmark1=sliderView.frame;
        coachmark1.origin.x=coachmark2.origin.x;
        
        coachmark2.size.width+=220;
        coachmark1.size.width=coachmark2.size.width;
        coachmark3.size.width+=220;
    }
    else
    {
        coachmark2.size.width+=210;
        coachmark3.size.width+=210;
    }
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"drag slider to specify cache storage limit",
                                @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"turn this switch ON to only download songs over WiFi",
                                @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark3],
                                @"caption": @"turn this switch ON to go off the grid",
                                @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            
                            ];

    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
   // [self.view addSubview:coachMarksView];
    //[coachMarksView start];

    
    
    
    
    
    
    
    
    
    
    
}
-(void) showLoginEl:(NSNotification *)notification{
    [self showLoginElements];
}

- (void) startedSliding:(NSNotification *) notification{
    self.animator.gesture.enabled=NO;
}
- (void) endedSliding:(NSNotification *) notification{
    self.animator.gesture.enabled=YES;
}

- (void)orientationChanged:(NSNotification *)notification{
    
    if(IS_DEVICE==IS_IPAD)
    {
        CGFloat curw=self.view.bounds.size.width;
        cancelButton.frame = CGRectMake(full_name.frame.origin.x, full_name.frame.origin.y + 60.0f, (full_name.frame.size.width/2.0f) - 10.0f, full_name.frame.size.height);
        
        registerButton.frame = CGRectMake(full_name.frame.origin.x + cancelButton.frame.size.width + 20.0f, full_name.frame.origin.y + 60.0f, (full_name.frame.size.width/2.0f) - 10.0f, full_name.frame.size.height);
        
        
        
        createAccountButton.frame = CGRectMake(facebookButton.frame.origin.x, facebookButton.frame.origin.y + 60.0f, facebookButton.frame.size.width, facebookButton.frame.size.height);
        sliderView.frame = CGRectMake((curw - sliderView.slider.frame.size.width)/2, signOutButton.frame.origin.y + 100.0f, sliderView.slider.frame.size.width, 70);
        
        
        
        
        
        OfflineSwitchLabel.frame = CGRectMake(sliderView.frame.origin.x+30, sliderView.frame.origin.y+sliderView.frame.size.height+lteSwitch.frame.size.height+5, 200, 31);
        OfflineSwitch.frame = CGRectMake(full_name.frame.origin.x+full_name.frame.size.width-51, sliderView.frame.origin.y+sliderView.frame.size.height+lteSwitch.frame.size.height+5, 51, 31);
        lteSwitchLabel.frame = CGRectMake(sliderView.frame.origin.x+30, sliderView.frame.origin.y+sliderView.frame.size.height, 200, 31);
        lteSwitch.frame = CGRectMake(full_name.frame.origin.x+full_name.frame.size.width-51, sliderView.frame.origin.y+sliderView.frame.size.height, 51, 31);
        sliderView.frame = CGRectMake((curw - sliderView.slider.frame.size.width)/2-15, signOutButton.frame.origin.y + 100.0f, sliderView.slider.frame.size.width, 70);
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    /* if(IS_DEVICE == IS_IPAD) {
     if([Utils isLandscape]) {
     
     CGFloat offsetY = 200.0f;
     
     
     email_register.frame = CGRectMake( ( [Utils currentWidth] - email_register.frame.size.width ) / 2, email_register.frame.origin.y - offsetY, email_register.frame.size.width, email_register.frame.size.height);
     password_register.frame = CGRectMake( ( [Utils currentWidth] - password_register.frame.size.width ) / 2, password_register.frame.origin.y - offsetY, password_register.frame.size.width, password_register.frame.size.height);
     password_confirm.frame = CGRectMake( ( [Utils currentWidth] - password_confirm.frame.size.width ) / 2, password_confirm.frame.origin.y - offsetY, password_confirm.frame.size.width, password_confirm.frame.size.height);
     full_name.frame = CGRectMake( ( [Utils currentWidth] - full_name.frame.size.width ) / 2, full_name.frame.origin.y - offsetY, full_name.frame.size.width, full_name.frame.size.height);
     cancelButton.frame = CGRectMake(full_name.frame.origin.x, full_name.frame.origin.y + 60.0f, (full_name.frame.size.width/2.0f) - 10.0f, full_name.frame.size.height);
     registerButton.frame = CGRectMake(full_name.frame.origin.x + cancelButton.frame.size.width + 20.0f, full_name.frame.origin.y + 60.0f, (full_name.frame.size.width/2.0f) - 10.0f, full_name.frame.size.height);
     
     offsetY = 125.0f;
     email.frame = CGRectMake( ( [Utils currentWidth] - email.frame.size.width ) / 2 , email.frame.origin.y - offsetY, email.frame.size.width, email.frame.size.height);
     password.frame = CGRectMake( ( [Utils currentWidth] - password.frame.size.width ) / 2 , password.frame.origin.y - offsetY, password.frame.size.width, password.frame.size.height);
     signOutButton.frame = CGRectMake( ( [Utils currentWidth] - signOutButton.frame.size.width ) / 2 , signOutButton.frame.origin.y - offsetY, signOutButton.frame.size.width, signOutButton.frame.size.height);
     sliderView.frame = CGRectMake( sliderView.frame.origin.x, sliderView.frame.origin.y - offsetY, sliderView.frame.size.width, sliderView.frame.size.height);
     lteSwitch.frame = CGRectMake( lteSwitch.frame.origin.x, lteSwitch.frame.origin.y - offsetY, lteSwitch.frame.size.width, lteSwitch.frame.size.height);
     lteSwitchLabel.frame = CGRectMake( lteSwitchLabel.frame.origin.x, lteSwitchLabel.frame.origin.y - offsetY, lteSwitchLabel.frame.size.width, lteSwitchLabel.frame.size.height);
     OfflineSwitch.frame = CGRectMake(OfflineSwitch.frame.origin.x, OfflineSwitch.frame.origin.y - offsetY, OfflineSwitch.frame.size.width, OfflineSwitch.frame.size.height);
     OfflineSwitchLabel.frame = CGRectMake( OfflineSwitchLabel.frame.origin.x, OfflineSwitchLabel.frame.origin.y - offsetY, OfflineSwitchLabel.frame.size.width, OfflineSwitchLabel.frame.size.height);
     
     forgotPasswordButton.frame = CGRectMake( ( [Utils currentWidth] - forgotPasswordButton.frame.size.width ) / 2, forgotPasswordButton.frame.origin.y - offsetY, forgotPasswordButton.frame.size.width, forgotPasswordButton.frame.size.height);
     createAccountButton.frame = CGRectMake( ( [Utils currentWidth] - createAccountButton.frame.size.width ) / 2, createAccountButton.frame.origin.y - offsetY, createAccountButton.frame.size.width, createAccountButton.frame.size.height);
     facebookButton.frame = CGRectMake( ( [Utils currentWidth] - facebookButton.frame.size.width ) / 2, facebookButton.frame.origin.y - offsetY, facebookButton.frame.size.width, facebookButton.frame.size.height);
     orLine.frame = CGRectMake( ( [Utils currentWidth] - orLine.frame.size.width ) / 2, orLine.frame.origin.y - offsetY, orLine.frame.size.width, orLine.frame.size.height);
     
     
     }
     
     }*/
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    /*if(IS_DEVICE == IS_IPAD) {
     if([Utils isLandscape]) {
     
     CGFloat offsetY = -200.0f;
     
     email_register.frame = CGRectMake( ( [Utils currentWidth] - email_register.frame.size.width ) / 2, email_register.frame.origin.y - offsetY, email_register.frame.size.width, email_register.frame.size.height);
     password_register.frame = CGRectMake( ( [Utils currentWidth] - password_register.frame.size.width ) / 2, password_register.frame.origin.y - offsetY, password_register.frame.size.width, password_register.frame.size.height);
     password_confirm.frame = CGRectMake( ( [Utils currentWidth] - password_confirm.frame.size.width ) / 2, password_confirm.frame.origin.y - offsetY, password_confirm.frame.size.width, password_confirm.frame.size.height);
     full_name.frame = CGRectMake( ( [Utils currentWidth] - full_name.frame.size.width ) / 2, full_name.frame.origin.y - offsetY, full_name.frame.size.width, full_name.frame.size.height);
     cancelButton.frame = CGRectMake(full_name.frame.origin.x, full_name.frame.origin.y + 60.0f, (full_name.frame.size.width/2.0f) - 10.0f, full_name.frame.size.height);
     registerButton.frame = CGRectMake(full_name.frame.origin.x + cancelButton.frame.size.width + 20.0f, full_name.frame.origin.y + 60.0f, (full_name.frame.size.width/2.0f) - 10.0f, full_name.frame.size.height);
     
     offsetY = -125.0f;
     email.frame = CGRectMake( ( [Utils currentWidth] - email.frame.size.width ) / 2 , email.frame.origin.y - offsetY, email.frame.size.width, email.frame.size.height);
     password.frame = CGRectMake( ( [Utils currentWidth] - password.frame.size.width ) / 2 , password.frame.origin.y - offsetY, password.frame.size.width, password.frame.size.height);
     signOutButton.frame = CGRectMake( ( [Utils currentWidth] - signOutButton.frame.size.width ) / 2 , signOutButton.frame.origin.y - offsetY, signOutButton.frame.size.width, signOutButton.frame.size.height);
     sliderView.frame = CGRectMake( sliderView.frame.origin.x, sliderView.frame.origin.y - offsetY, sliderView.frame.size.width, sliderView.frame.size.height);
     lteSwitch.frame = CGRectMake( lteSwitch.frame.origin.x, lteSwitch.frame.origin.y - offsetY, lteSwitch.frame.size.width, lteSwitch.frame.size.height);
     lteSwitchLabel.frame = CGRectMake( lteSwitchLabel.frame.origin.x, lteSwitchLabel.frame.origin.y - offsetY, lteSwitchLabel.frame.size.width, lteSwitchLabel.frame.size.height);
     OfflineSwitch.frame = CGRectMake(OfflineSwitch.frame.origin.x, OfflineSwitch.frame.origin.y - offsetY, OfflineSwitch.frame.size.width, OfflineSwitch.frame.size.height);
     OfflineSwitchLabel.frame = CGRectMake( OfflineSwitchLabel.frame.origin.x, OfflineSwitchLabel.frame.origin.y - offsetY, OfflineSwitchLabel.frame.size.width, OfflineSwitchLabel.frame.size.height);
     
     forgotPasswordButton.frame = CGRectMake( ( [Utils currentWidth] - forgotPasswordButton.frame.size.width ) / 2, forgotPasswordButton.frame.origin.y - offsetY, forgotPasswordButton.frame.size.width, forgotPasswordButton.frame.size.height);
     createAccountButton.frame = CGRectMake( ( [Utils currentWidth] - createAccountButton.frame.size.width ) / 2, createAccountButton.frame.origin.y - offsetY, createAccountButton.frame.size.width, createAccountButton.frame.size.height);
     facebookButton.frame = CGRectMake( ( [Utils currentWidth] - facebookButton.frame.size.width ) / 2, facebookButton.frame.origin.y - offsetY, facebookButton.frame.size.width, facebookButton.frame.size.height);
     orLine.frame = CGRectMake( ( [Utils currentWidth] - orLine.frame.size.width ) / 2, orLine.frame.origin.y - offsetY, orLine.frame.size.width, orLine.frame.size.height);
     
     }
     
     }
     /* resign first responder, hide keyboard, move views */
}

- (void) createAccountHit {
    [self showRegisterElements];
}

- (void) cancelHit {
    [self hideRegisterElements];
}

- (void) lteSwitchHit:(id)sender {
    [SETTINGS setWifiOnly:lteSwitch.on];
}
- (void) OfflineSwitchHit:(id)sender {
    [SETTINGS setOfflineOnly:OfflineSwitch.on];
     [[SongCacheDownloadManager sharedInstance]StartDownloadingAfterOffline ];
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.navigationBarShouldReload object:nil userInfo:nil];
    
    
}

- (void) forgotPasswordHit {
    if ([self validateEmail:email.text]) {
        [self showForgotPassword:NO];
        
        [[JusthearitClient sharedInstance] sendPasswordResetToEmail:email.text withSuccessBlock:^(NSArray *array) {} withErrorBlock:^(NSError *error) {}];
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"justhearit"
                              message: [NSString stringWithFormat:@"We sent you an email at %@ with instructions on how to reset your password", email.text]
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"justhearit"
                              message: @"Please enter a correct email address"
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];
        [alert show];
    }
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
- (void) setupOfflineSwitch
{
    OfflineView=[[UIView alloc] init];
    
    
    OfflineSwitch = [[UISwitch alloc] init];
    CGRect frame;
    
    if(IS_DEVICE == IS_IPAD) {
        frame = CGRectMake(sliderView.frame.origin.x+ 240, sliderView.frame.origin.y + sliderView.frame.size.height+lteSwitch.frame.size.height, 51, 31);
    }else {
        frame = CGRectMake(email.frame.origin.x+240-51, sliderView.frame.origin.y + sliderView.frame.size.height+lteSwitch.frame.size.height+5, 51, 31);
    }
    
    
    OfflineSwitch.frame = frame;
    [OfflineSwitch addTarget: self action: @selector(OfflineSwitchHit:) forControlEvents: UIControlEventValueChanged];
    
    
    OfflineSwitchLabel = [[UILabel alloc] init];
    
    if(IS_DEVICE == IS_IPAD) {
        OfflineSwitchLabel.frame = CGRectMake(sliderView.frame.origin.x+30, sliderView.frame.origin.y+sliderView.frame.size.height+lteSwitch.frame.size.height+5, 200, 31);
    }else {
        OfflineSwitchLabel.frame = CGRectMake(email.frame.origin.x, sliderView.frame.origin.y+sliderView.frame.size.height+lteSwitch.frame.size.height+5, 200, 31);
    }
    
    OfflineSwitchLabel.text = @"Off the grid";
    OfflineSwitchLabel.font = [UIFont boldSystemFontOfSize:14];
    OfflineSwitchLabel.textColor = RGBA(255,255,255,.6);
    [OfflineSwitch setOnTintColor:[UIColor redColor]];
    [self.view addSubview:OfflineSwitchLabel];
    [self.view addSubview:OfflineSwitch];
    
    OfflineSwitch.on = [SETTINGS OfflineOnly];
}
- (void) setupLteSwitch {
    lteSwitch = [[UISwitch alloc] init];
    CGRect frame;
    
    if(IS_DEVICE == IS_IPAD) {
        frame = CGRectMake(sliderView.frame.origin.x + 240, sliderView.frame.origin.y + sliderView.frame.size.height, 51, 31);
    }else {
        frame = CGRectMake(email.frame.origin.x+240-51, sliderView.frame.origin.y + sliderView.frame.size.height, 51, 31);
    }
    
    lteSwitch.frame = frame;
    [lteSwitch addTarget: self action: @selector(lteSwitchHit:) forControlEvents: UIControlEventValueChanged];
    [self.view addSubview:lteSwitch];
    
    lteSwitchLabel = [[UILabel alloc] init];
    
    if(IS_DEVICE == IS_IPAD) {
        lteSwitchLabel.frame = CGRectMake(sliderView.frame.origin.x + 30, sliderView.frame.origin.y+sliderView.frame.size.height, 200, 31);
    }else {
        lteSwitchLabel.frame = CGRectMake(email.frame.origin.x, sliderView.frame.origin.y+sliderView.frame.size.height, 200, 31);
    }
    
    lteSwitchLabel.text = @"WiFi sync only";
    lteSwitchLabel.font = [UIFont boldSystemFontOfSize:14];
    lteSwitchLabel.textColor = RGBA(255,255,255,.6);
    
    [self.view addSubview:lteSwitchLabel];
    
    lteSwitch.on = [SETTINGS wifiOnly];
}

- (void) setupCacheSlider {
    sliderView = [[CacheSliderView alloc] init];
    
    [sliderView setup];
    
    if(IS_DEVICE == IS_IPAD) {
        sliderView.frame = CGRectMake(([Utils currentWidth] - sliderView.slider.frame.size.width)/2, signOutButton.frame.origin.y + 100.0f, [Utils currentWidth], 70);
    }else {
        sliderView.frame = CGRectMake(0, [Utils screenHeight] - 160, [Utils currentWidth], 70);
        [sliderView setFramesWidth:[Utils screenWidth]];
    }
    
    
    
    [self.view addSubview:sliderView];
    UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:nil action:nil];
    [sliderView.slider addGestureRecognizer:panGesture];
    panGesture.cancelsTouchesInView = NO;
    UIPanGestureRecognizer* panGesture2 = [[UIPanGestureRecognizer alloc] initWithTarget:nil action:nil];
    [sliderView.sliderProgress addGestureRecognizer:panGesture];
    panGesture.cancelsTouchesInView = NO;
    UIPanGestureRecognizer* panGesture3 = [[UIPanGestureRecognizer alloc] initWithTarget:nil action:nil];
    [sliderView.sliderProgress2 addGestureRecognizer:panGesture];
    panGesture.cancelsTouchesInView = NO;
    

    
    
}

- (void)setupFacebookButton
{
    facebookButton.backgroundColor = HexColor(0x3b559f);
    facebookButton.layer.cornerRadius = 5;
    facebookButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -250);
    facebookButton.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    
    FAKFontAwesome *facebookIcon = [FAKFontAwesome facebookIconWithSize:20];
    [facebookIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    facebookIcon.iconFontSize = 19;
    
    UIImage *facebookIconImage = [facebookIcon imageWithSize:CGSizeMake(20, 20)];
    
    [facebookButton setImage:facebookIconImage forState:UIControlStateNormal];
}

- (void)setupCancelButton {
    cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.backgroundColor = HexColor(0xcb324d);
    cancelButton.layer.cornerRadius = 5;
    cancelButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -250);
    cancelButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.userInteractionEnabled = YES;
    
    cancelButton.frame = CGRectMake(full_name.frame.origin.x, full_name.frame.origin.y + 60.0f, (full_name.frame.size.width/2.0f) - 10.0f, full_name.frame.size.height);
    
    [self.view addSubview:cancelButton];
    
    cancelButton.hidden = YES;
    
    [cancelButton addTarget:self action:@selector(cancelHit) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupRegisterButton {
    registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    registerButton.backgroundColor = HexColor(0x3b559f);
    registerButton.layer.cornerRadius = 5;
    registerButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -250);
    registerButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [registerButton setTitle:@"Register" forState:UIControlStateNormal];
    registerButton.userInteractionEnabled = YES;
    
    registerButton.frame = CGRectMake(full_name.frame.origin.x + cancelButton.frame.size.width + 20.0f, full_name.frame.origin.y + 60.0f, (full_name.frame.size.width/2.0f) - 10.0f, full_name.frame.size.height);
    
    [self.view addSubview:registerButton];
    
    registerButton.hidden = YES;
    
    [registerButton addTarget:self action:@selector(registration) forControlEvents:UIControlEventTouchUpInside];
}

- (void) dismissKeyboard:(UITapGestureRecognizer *)gesture {
    [email resignFirstResponder];
    [password resignFirstResponder];
    [email_register resignFirstResponder];
    [password_register resignFirstResponder];
    [password_confirm resignFirstResponder];
    [full_name resignFirstResponder];
    
    if (SETTINGS.isLoggedIn) {
        if (SETTINGS.fullName) {
            if (email.text.length == 0) {
                [email changeText:SETTINGS.fullName];
            }
        }
    }
}

//- (void)sessionStateChanged:(NSNotification*) notification {
//    if (FBSession.activeSession.isOpen && !SETTINGS.isLoggedIn)
//        [self loginWithFacebook];
//    else {
//        [facebookButton setTitle:@"Sign In" forState:UIControlStateNormal];
//        facebookButton.userInteractionEnabled = YES;
//    }
//
//}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    
    if ((long)[theTextField tag] == 1) {
        
        [self registration];
        
    }else {
        
        if (SETTINGS.isLoggedIn) {
            if (email.text.length > 0) {
                User *user = [[User alloc] init];
                user.fullName = email.text;
                [[JusthearitClient sharedInstance] updateUser:user withSuccessBlock:^(NSArray *array) {} withErrorBlock:^(NSError *error) {}];
                SETTINGS.fullName = email.text;
            } else {
                [email changeText:SETTINGS.fullName];
            }
        } else {
            if (email.text.length > 0 && password.text.length > 0) [self loginWithEmail:email.text andPassword:password.text];
        }
        
    }
    
    [theTextField resignFirstResponder];
    return YES;
}

#pragma mark Interface actions

- (void)dismissViewController:(id)sender {
    
    if(SETTINGS.isLoggedIn&&!SETTINGS.notfirstLogin){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"CoachPH4"
         object:self];
        [SETTINGS setnotfirstLogin];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction) facebookLogin:(id)sender {
    [facebookButton setTitle:@"Connecting..." forState:UIControlStateNormal];
    facebookButton.userInteractionEnabled = NO;
    //    Log( @"### running FB sdk version: %@", [FBSettings sdkVersion] );
    //    if (FBSession.activeSession.isOpen){
    //        [self logout];
    //    } else {
    //        [ApplicationDelegate openSessionWithAllowLoginUI:YES];
    //    }
    
    
    [self.login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            NSLog(@"Process error");
        } else if (result.isCancelled)
        {
            NSLog(@"Cancelled");
        }
        else {
            NSLog(@"Logged in");
            [facebookButton setTitle:@"Connecting..." forState:UIControlStateNormal];
            facebookButton.userInteractionEnabled = NO;
            
            FBSDKAccessToken *token = (FBSDKAccessToken *) result.token;
            NSLog(@"Token: %@",token.tokenString);
            [self loginWithSession:[Session initWithfbAuthToken:token.tokenString] withCompleteBlock:^
             {
                 [facebookButton setTitle:@"Sign In" forState:UIControlStateNormal];
                 facebookButton.userInteractionEnabled = YES;
             }];
        }
    }];
    
}

- (IBAction) logout
{
    [self.login logOut];
    //    [ApplicationDelegate closeSession];
    SETTINGS.logout;
    FayeManager.sharedInstance.disconnect;
    [self showLoginElements];
    
    [[JusthearitClient sharedInstance] logoutWithSuccessBlock:^(NSArray *array) {
        [self.delegate sessionChanged:self];
        
    } withErrorBlock:^(NSError *error) {}];
}



#pragma mark register functions

- (IBAction) registration {
    
    [email_register resignFirstResponder];
    [password_register resignFirstResponder];
    [password_confirm resignFirstResponder];
    [full_name resignFirstResponder];
    
    [registerButton setTitle:@"Sending..." forState:UIControlStateNormal];
    [registerButton setUserInteractionEnabled:NO];
    
    BOOL hasError = NO;
    NSString *errMessage;
    
    if (![self validateEmail:email_register.text]) {
        hasError = YES;
        errMessage = @"Please enter a correct email address!";
    }else if(![password_register.text isEqualToString: password_confirm.text] || [password_confirm.text length] == 0 || [password_register.text length] == 0){
        hasError = YES;
        errMessage = @"Passwords doesn\'t match!";
    }else if([full_name.text length] == 0) {
        hasError = YES;
        errMessage = @"Please fill in your name!";
    }
    
    if(hasError) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"justhearit"
                              message: errMessage
                              delegate: nil
                              cancelButtonTitle: @"OK"
                              otherButtonTitles: nil];
        [alert show];
        
        [registerButton setTitle:@"Register" forState:UIControlStateNormal];
        [registerButton setUserInteractionEnabled:YES];
        
    }else {
        [self registerWithData:email_register.text andPassword:password_register.text andPasswordConfirmation:password_confirm.text andFullName:full_name.text withCompleteBlock:^{}];
    }
    
}

- (void) registerWithData:(NSString *)email1 andPassword:(NSString *)password andPasswordConfirmation:(NSString *)password_confirmation andFullName:(NSString *)fullname withCompleteBlock:(void (^)(void))completeBlock {
    
    [[JusthearitClient sharedInstance]
     registration:email1
     andPassword:password
     andPasswordConfirmation:password_confirmation
     andFullName:fullname
     withSuccessBlock:^(NSArray *response) {
         
         [registerButton setTitle:@"Register" forState:UIControlStateNormal];
         [registerButton setUserInteractionEnabled:YES];
         [self hideRegisterElements];
         [self loginWithSession:[Session initWithLogin:email1 andPassword:password] withCompleteBlock:^{}];
         
         completeBlock();
     } withErrorBlock:^(NSError *error) {
         
         [registerButton setTitle:@"Register" forState:UIControlStateNormal];
         [registerButton setUserInteractionEnabled:YES];
         
         // The `description` method of the class the error is mapped to is used to construct the value of the localizedDescription
         Log(@"Loaded this error: %@", [error localizedDescription]);
         
         // You can access the model object used to construct the `NSError` via the `userInfo`
         RKErrorMessage *errorMessage =  [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
         
         NSString *capitalized = [[[errorMessage.errorMessage substringToIndex:1] uppercaseString] stringByAppendingString:[errorMessage.errorMessage substringFromIndex:1]];
         
         UIAlertView *alert = [[UIAlertView alloc]
                               initWithTitle: @"justhearit"
                               message: capitalized
                               delegate: nil
                               cancelButtonTitle: @"OK"
                               otherButtonTitles: nil];
         [alert show];
         
         completeBlock();
     }];
}

- (void) resendConfirmation:(NSString *)email1 withCompleteBlock:(void (^)(void))completeBlock {
    [[JusthearitClient sharedInstance]
     resendConfirmation:email1
     withSuccessBlock:^(NSArray *response) {
         [email changeText:@""];
         completeBlock();
     } withErrorBlock:^(NSError *error) {
         completeBlock();
     }];
}

#pragma mark login functions

//- (void) loginWithFacebook
//{
//    [self loginWithSession:[Session initWithfbAuthToken:FBSession.activeSession.accessTokenData.accessToken] withCompleteBlock:^{
//        [facebookButton setTitle:@"Sign In" forState:UIControlStateNormal];
//        facebookButton.userInteractionEnabled = YES;
//    }];
//}

- (void) loginWithEmail:(NSString*) email andPassword:(NSString*)password {
    [animatedSpinner show:YES];
    [self loginWithSession:[Session initWithLogin:email andPassword:password] withCompleteBlock:^{}];
}

-(void) loginWithSession:(Session *) session withCompleteBlock:(void (^)(void))completeBlock {
    
    __weak typeof(self) weakSelf = self;
    
    [[JusthearitClient sharedInstance] login:session withSuccessBlock:^(NSArray *response) {
        Session *responseSession = _.first(response);
        
        if ([responseSession.status isEqualToString:@"ok"] && !responseSession.token.isEmpty) {
            BOOL sessionChanged = [weakSelf sessionChanged:responseSession.token];
            [SETTINGS login:responseSession];
            [[FayeManager sharedInstance] connect];
            if (sessionChanged) [weakSelf.delegate sessionChanged:self];
            
            [weakSelf.self showLogoutElements];
            [weakSelf.self dismissViewController:self];
            
        } else if ([responseSession.status isEqualToString:@"error"] && !responseSession.message.isEmpty) {
            
            Log(@"error code %@", responseSession.code);
            NSString *alertBtnText;
            UIAlertView *alert;
            
            if([responseSession.code isEqualToString:@"confirm"]) {
                alertBtnText = @"Resend activation email";
                
                alert = [[UIAlertView alloc]
                         initWithTitle: @"justhearit"
                         message: responseSession.message
                         delegate: self
                         cancelButtonTitle: nil
                         otherButtonTitles:alertBtnText, nil];
                [alert show];
                
                [password changeText:@""];
                
            }else {
                alertBtnText = @"Ok";
                
                alert = [[UIAlertView alloc]
                         initWithTitle: @"justhearit"
                         message: responseSession.message
                         delegate: nil
                         cancelButtonTitle: alertBtnText
                         otherButtonTitles: nil];
                
                [password changeText:@""];
                [self showForgotPassword:YES];
            }
            
        }
        completeBlock();
    } withErrorBlock:^(NSError *error) {
        Log(@"error %@", error);
        completeBlock();
    }];
}

- (BOOL) sessionChanged:(NSString*)responseToken {
    return ![SETTINGS.token isEqualToString: responseToken];
}

#pragma mark UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [alertView cancelButtonIndex]) {
        [self resendConfirmation:email.text withCompleteBlock:^{}];
    }
}

#pragma mark Interface automation

- (void) setRegisterElementsHidden:(BOOL)hidden {
    
    if (!hidden) {
        email_register.alpha = 0.0f;
        password_register.alpha = 0.0f;
        password_confirm.alpha = 0.0f;
        full_name.alpha = 0.0f;
        cancelButton.alpha = 0.0f;
        registerButton.alpha = 0.0f;
    }
    
    email_register.hidden = hidden;
    password_register.hidden = hidden;
    password_confirm.hidden = hidden;
    full_name.hidden = hidden;
    cancelButton.hidden = hidden;
    registerButton.hidden = hidden;
    
    if (!hidden) {
        
        [UIView animateWithDuration:0.2 animations:^{
            email_register.alpha = 1;}];
        
        [UIView animateWithDuration:0.2
                              delay:0.2
                            options:UIViewAnimationOptionCurveLinear
                         animations:^ {
                             password_register.alpha = 1;
                         }
                         completion:^(BOOL finished) {}];
        
        [UIView animateWithDuration:0.2
                              delay:0.4
                            options:UIViewAnimationOptionCurveLinear
                         animations:^ {
                             password_confirm.alpha = 1;
                         }
                         completion:^(BOOL finished) {}];
        [UIView animateWithDuration:0.2
                              delay:0.6
                            options:UIViewAnimationOptionCurveLinear
                         animations:^ {
                             full_name.alpha = 1;
                         }
                         completion:^(BOOL finished) {}];
        [UIView animateWithDuration:0.2
                              delay:0.8
                            options:UIViewAnimationOptionCurveLinear
                         animations:^ {
                             cancelButton.alpha = 1;
                         }
                         completion:^(BOOL finished) {}];
        [UIView animateWithDuration:0.2
                              delay:0.8
                            options:UIViewAnimationOptionCurveLinear
                         animations:^ {
                             registerButton.alpha = 1;
                         }
                         completion:^(BOOL finished) {}];
        
    }
}

- (void) setOtherElementsHidden:(BOOL)hidden {
    
    orLine.hidden = hidden;
    facebookButton.hidden = hidden;
    password.hidden = hidden;
    email.hidden = hidden;
    createAccountButton.hidden = hidden;
}

- (void) setLoginElementsHidden:(BOOL)hidden {
    orLine.hidden = hidden;
    facebookButton.hidden = hidden;
    password.hidden = hidden;
    createAccountButton.hidden = hidden;
    
    [UIView animateWithDuration:0.9 animations:^{
        createAccountButton.alpha = 1;
    }];
    
    if (hidden) {
        email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Your Name" attributes:@{NSForegroundColorAttributeName: RGBA(255,255,255,0.7)}];
        [email changeText:SETTINGS.fullName];
        email.returnKeyType = UIReturnKeyDone;
    } else {
        email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"email" attributes:@{NSForegroundColorAttributeName: RGBA(255,255,255,0.7)}];
        [email changeText:@""];
        email.returnKeyType = UIReturnKeyDefault;
    }
}

- (void) setLogoutElementsHidden:(BOOL)hidden {
    signOutButton.hidden = hidden;
    sliderView.hidden = hidden;
    lteSwitch.hidden = hidden;
    lteSwitchLabel.hidden = hidden;
    OfflineSwitch.hidden=hidden;
    OfflineSwitchLabel.hidden=hidden;
}

- (void) showForgotPassword:(BOOL)show {
    
    if (forgotPasswordButton.alpha == show) return;
    
    CGFloat yOffset = orLine.frame.size.height;
    if (!show) yOffset = -yOffset;
    
    CGRect orLineFrame = orLine.frame;
    orLineFrame.origin.y += yOffset;
    
    CGRect facebookButtonFrame = facebookButton.frame;
    facebookButtonFrame.origin.y += yOffset;
    
    CGRect createAcountButtonFrame = createAccountButton.frame;
    createAcountButtonFrame.origin.y += yOffset;
    
    [UIView animateWithDuration:0.3 animations:^{
        orLine.frame = orLineFrame;
        facebookButton.frame = facebookButtonFrame;
        createAccountButton.frame = createAcountButtonFrame;
        forgotPasswordButton.alpha = show;
    }];
    
}

- (void) showRegisterElements {
    [self setRegisterElementsHidden:NO];
    [self setOtherElementsHidden:YES];
}

- (void) hideRegisterElements {
    [self setRegisterElementsHidden:YES];
    [self setOtherElementsHidden:NO];
}

- (void) showLoginElements {
    [self setLoginElementsHidden:NO];
    [self setLogoutElementsHidden:YES];
    [self setRegisterElementsHidden:YES];
}

- (void) showLogoutElements {
    [self setLoginElementsHidden:YES];
    [self setLogoutElementsHidden:NO];
    [self setRegisterElementsHidden:YES];
}


@end

//
// Created by andrei st on 1/7/14.
// Copyright (c) 2014 HearIt Inc. All rights reserved.
//


#import "NSArray+Utils.h"


@implementation NSArray (Utils)
- (id)next:(id)el {
    int index = (int)[self indexOfObject:el];
    if (index >= 0) {
        if (index + 1 < self.count) {
            return [self objectAtIndex:index+1];
        }
    }
    return nil;
}

- (id)prev:(id)el {
    int index = (int)[self indexOfObject:el];
    if (index >= 0) {
        if (index - 1 >= 0) {
            return [self objectAtIndex:index-1];
        }
    }
    return nil;
}


@end

//
//  NWReachabilityManger.h
//  Just Hear It
//
//  Created by Goiceanu Ciprian on 17/11/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Reachability;

@interface NWReachabilityManger : NSObject

@property (strong, nonatomic) Reachability *reachability;

#pragma mark -
#pragma mark Shared Manager
+ (NWReachabilityManger *)sharedManager;

#pragma mark -
#pragma mark Class Methods
+ (BOOL)isReachable;
+ (BOOL)isUnreachable;
+ (BOOL)isReachableViaWWAN;
+ (BOOL)isReachableViaWiFi;

@end

#import "PlaylistsViewController.h"
#import "SearchViewController.h"
#import "PlaylistAddSongs.h"
#import "PlaylistMove.h"
#import "PlaylistIsDefault.h"
#import "LocalPlaylist.h"
#import "NSArray+Utils.h"
#import "Scrollable.h"
#import "PlaylistBadgeChangeNotification.h"
#import "PlaylistChangeNotification.h"
#import "SongCache.h"
#import "CachedLocalPlaylist.h"
#import "CachedPlaylist+Utils.h"
#import "CachedLocalPlaylist+Utils.h"
#import "SearchViewController.h"
#import "Pagination.h"
#import "NWReachabilityManger.h"
#import "CachedLocalPlaylistSong.h"
#import "Player.h"
#import <QuartzCore/QuartzCore.h>
#import "Bluuur.h"
#import "SongCacheDownloadManager.h"
#import "MPCoachMarks.h"
@implementation PlaylistsViewController

int iPL, iLogin;

@synthesize playlistsTable, loginButton, addPlaylistButton, searchButton, playlists, animatedSpinner, playlistsMode, tableScroll, movingPlaylist, playlistCellForActionSheet, background, playlistNotification, cachedLocalPlaylists, lastScrollPosition,animator,dashed;

- (void)awakeFromNib {
    playlistsMode = playlistsViewControllerNormal;
    playlists = [Playlists sharedInstance];
    cachedLocalPlaylists = [[NSMutableArray alloc] init];
    
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"LocalData.sqlite"];
    
    if(IS_DEVICE == IS_IPAD) {
        animatedSpinner = [Spinner initWithParentView:self.view withFixedWidth:LEFTPANEL_WIDTH_IPAD withHeight:[Utils currentHeight]];
    }else {
        animatedSpinner = [Spinner initWithParentView:self.view withOffsetX:-40];
    }
    
    //Log(@"awake from nib");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    playlistsMode = playlistsViewControllerNormal;
    
    tableScroll = [[Scrollable alloc] init];
    
    if(IS_DEVICE == IS_IPAD) {
        playlistsTable.frame = CGRectMake(playlistsTable.frame.origin.x, 55, playlistsTable.frame.size.width, playlistsTable.frame.size.height);
    }else {
        playlistsTable.frame = CGRectMake(playlistsTable.frame.origin.x, 44, playlistsTable.frame.size.width, playlistsTable.frame.size.height);
    }
    if(IS_DEVICE==IS_IPAD)
    {
       dashed = [[UIView alloc] initWithFrame:CGRectMake(0, 5, LEFTPANEL_WIDTH_IPAD-5,addPlaylistButton.frame.size.height-10)];
    }
    else{
   dashed = [[UIView alloc] initWithFrame:CGRectMake(5, 5, [Utils screenWidth]-90,addPlaylistButton.frame.size.height-10)];
    }
    
    dashed.backgroundColor = [UIColor clearColor];
    CAShapeLayer *border = [CAShapeLayer layer];
    border.strokeColor = RGBA(255,255,255,1).CGColor;
    border.fillColor = nil;
    border.lineWidth = 1.5f;
    border.lineDashPattern = @[@4, @2];
    border.path = [UIBezierPath bezierPathWithRoundedRect:dashed.bounds cornerRadius:5.f].CGPath;
    dashed.autoresizingMask = UIViewContentModeLeft;
    [dashed.layer addSublayer:border];
    dashed.alpha=0;
    [addPlaylistButton addSubview:dashed];
    
    lastScrollPosition = 0;
    
    //Log(@"last scroll position view did load: %f", lastScrollPosition);
    
    tableScroll.scroll = playlistsTable;
    
    [tableScroll.scroll setContentInset:UIEdgeInsetsMake(5.0f, 0, 0, 0)];
    
    FAKFontAwesome *cogIcon = [FAKFontAwesome cogIconWithSize:20];
    [cogIcon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    cogIcon.iconFontSize = 19;
    
    UIImage *cogIconImage = [cogIcon imageWithSize:CGSizeMake(20, 20)];
    
    [loginButton setImage:cogIconImage forState:UIControlStateNormal];
    
    /*if(IS_DEVICE == IS_IPAD) {
     background.image = [UIImage imageNamed:@"background_landscape_ipad.png"];
     }*/
    
    [self observeObject:playlists property:@"collection" withSelector:@selector(playlistsChanged)];
    [self observeObject:playlists property:@"temporaryCollection" withSelector:@selector(playlistsChanged)];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadPlaylists) name:Ev.playlistsShouldReload object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotOfflineSet) name:Ev.gotOffline object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotOnlineSet) name:Ev.gotOnline object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CoachPH2:)
                                                 name:@"CoachPH2"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CoachPH3:)
                                                 name:@"CoachPH3"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CoachPH4:)
                                                 name:@"CoachPH4"
                                               object:nil];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadPlaylists) name:@"spinner:show" object:nil];
    //__block __weak id playlistsNotifObserver;
    
    
    /*playlistsNotifObserver = [[NSNotificationCenter defaultCenter]
     addObserverForName:Ev.playlistsNotification
     object:nil
     queue:[NSOperationQueue mainQueue]
     usingBlock:^(NSNotification *note){
     
     [[NSNotificationCenter defaultCenter] removeObserver:playlistsNotifObserver];
     
     }];*/
    //Log(@"ia vezi: %hhd",playlistNotification);
    //if(playlistNotification == NO) {
    if(IS_DEVICE == IS_IPHONE) {
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playlistsNotification:) name:Ev.playlistsNotification object:nil];
    }
    //}
    
    //UIStoryboard * storyboard = self.storyboard;
    //NSString * storyboardName = [storyboard valueForKey:@"name"];
    
    
}
- (void) CoachPH2:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
   /* if(iPL==0)
    {
    [self performSelector:@selector(coachadel) withObject:nil afterDelay:0.1 ];
        iPL++;
    } NUSH CE E INCA*/
    
    
    CGRect coachmark2 = CGRectMake(0, [Utils screenHeight]-38, 0, 0);
  
    // Setup coach marks
   NSArray *coachMarks = @[
                   
                   @{
                       @"rect": [NSValue valueWithCGRect:coachmark2],
                       @"caption": @"tap anywhere here to sign up or sign in",
                       @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                       @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                       @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                       @"showArrow":[NSNumber numberWithBool:YES]
                       }
                   
                   
                   ];
    CGRect cmFrame=self.view.bounds;
    cmFrame.size.height-=40;
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:cmFrame coachMarks:coachMarks];
    coachMarksView.TUTnmbr=2;
<<<<<<< HEAD
   // [self.view addSubview:coachMarksView];
   // [coachMarksView start];
=======
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
>>>>>>> 94c154b3c0bdd90a3bbf2a9e9126f9733a6be368
    
    
    
    
    
    
}
- (void) CoachPH3:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    /*iLogin=1;
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
    */
    //alert(@"OK");
   
    //[self performSegueWithIdentifier:@"loginSegue" sender:self];
<<<<<<< HEAD
    
    
    
    
=======
>>>>>>> 94c154b3c0bdd90a3bbf2a9e9126f9733a6be368
    
    
    
    
}
- (void) CoachPH4:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    /*iLogin=1;
     [self performSegueWithIdentifier:@"loginSegue" sender:self];
     */
    //alert(@"OK");
 
    
    
    
    CGRect coachmark1 = CGRectMake(0, 0, 500, 500);
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Drag searches into folowing to be notified when new songs are added",
                                @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            
                            ];
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
   // [self.view addSubview:coachMarksView];
   // [coachMarksView start];
    
    
}
- (void) CoachPH5:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
// unless you use this method for observation of other notifications
    // as well.
    /*iLogin=1;
     [self performSegueWithIdentifier:@"loginSegue" sender:self];
     */
    //alert(@"OK");
    alert(@"OK");
   
    
}
- (void) CoachPH4:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    /*iLogin=1;
     [self performSegueWithIdentifier:@"loginSegue" sender:self];
     */
    //alert(@"OK");
 
    
    
    
    CGRect coachmark1 = CGRectMake(0, 0, 500, 500);
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Drag searches into folowing to be notified when new songs are added",
                                @"shape": [NSNumber numberWithInteger:SHAPE_SQUARE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            
                            ];
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
    
    
}
- (void) CoachPH5:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
// unless you use this method for observation of other notifications
    // as well.
    /*iLogin=1;
     [self performSegueWithIdentifier:@"loginSegue" sender:self];
     */
    //alert(@"OK");
    alert(@"OK");
   
    
    
    
}
-(void) coachadel{
   /* CGRect coachmark1 = loginButton.frame;
    if(IS_DEVICE==IS_IPHONE)
    {
    coachmark1.origin.x+=45;
    coachmark1.size.width-=33;
    }
    
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"tap here to access settings",
                                @"shape": [NSNumber numberWithInteger:SHAPE_CIRCLE],
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"alignment":[NSNumber numberWithInteger:LABEL_ALIGNMENT_LEFT],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            
                            ];
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:window.bounds coachMarks:coachMarks];
    [window addSubview:coachMarksView];
    [coachMarksView start];
    */

}
-(void)viewWillAppear:(BOOL)animated {
    //Log(@"appear");
    
    [super viewWillAppear:animated];
    if(IS_DEVICE == IS_IPAD) {
        playlistNotification = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playlistsNotification:) name:Ev.playlistsNotification object:nil];
    }
}
-(void)viewWillDisappear:(BOOL)animated {
    //Log(@"disappear");
    [super viewWillDisappear:animated];
    if(IS_DEVICE == IS_IPAD) {
        playlistNotification = NO;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:Ev.playlistsNotification object:nil];
    }
}

- (void) playlistsNotification:(NSNotification *)notification {
    //Log(@"update playlist badge");
    PlaylistBadgeChangeNotification *badgeChangeNotification = (PlaylistBadgeChangeNotification *)notification.userInfo;
    
    [[Playlists sharedInstance] handlePlaylistNotification:badgeChangeNotification];
    
    //Log(@"playlists notif: %@", badgeChangeNotification.song_ids);
    
    //NSArray *keys = [NSArray arrayWithObjects:@"playlistId", @"song_ids", nil];
    //NSArray *objects = [NSArray arrayWithObjects:badgeChangeNotification.playlistId, badgeChangeNotification.song_ids, nil];
    
    //NSDictionary *dictionary = [NSDictionary dictionaryWithObject:badgeChangeNotification.playlistId forKey:@"playlistId"];
    /*NSDictionary *dictionary = @{
     @"playlistId" : badgeChangeNotification.playlistId,
     @"song_ids" : badgeChangeNotification.song_ids
     };*/
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:Ev.shouldUpdateNewSongs object:nil userInfo:dictionary];
    
    [playlistsTable reloadData];
    
}
- (void) playlistsChangeNotification:(NSNotification *)notification {
    PlaylistChangeNotification *playlistChangeNotification = (PlaylistChangeNotification *) notification.userInfo;
    
    Log(@"event: %@", playlistChangeNotification.event);
    if([playlistChangeNotification.event isEqualToString:@"delete"]) {
        
        PlaylistCell *foundCell = [[PlaylistCell alloc] init];
        for (int section = 0; section < [playlistsTable numberOfSections]; section++) {
            for (int row = 0; row < [playlistsTable numberOfRowsInSection:section]; row++) {
                NSIndexPath* cellPath = [NSIndexPath indexPathForRow:row inSection:section];
                PlaylistCell* cell = (PlaylistCell *)[playlistsTable cellForRowAtIndexPath:cellPath];
                //do stuff with 'cell'
                
                if([cell.playlist.playlistId isEqualToString:playlistChangeNotification.playlistId]) {
                    foundCell = cell;
                }
            }
        }
        
        if (foundCell.playlist == playlists.openedPlaylist) {
            NSMutableArray *allPlaylists = playlists.allPlaylists;
            
            Playlist *toSelect = [allPlaylists next:foundCell.playlist];
            if (!toSelect) toSelect = [allPlaylists prev:foundCell.playlist];
            if (toSelect) [self openPlaylist:toSelect andShowMain:NO];
        }
        
        if (foundCell.playlist.isPlayingOrPaused) {
            
            if ([[self.sidePanelController centerPanel] isKindOfClass:[MainViewController class]]) {
                MainViewController *mainViewController = (MainViewController *)[self.sidePanelController centerPanel];
                [mainViewController stopMusic];
            }
        }
        
        NSArray *deletableSongsLC = [[NSArray alloc] init];
        if(playlists.openedPlaylist.isMusicFeed) {
            CachedLocalPlaylist *cachedPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:foundCell.playlist.playlistId];
            deletableSongsLC = [SongCache songsOnlyInCachedMusicFeed:cachedPlaylist];
        }
        
        [CachedPlaylist removeFromCache:[CachedPlaylist findByPlaylistId:foundCell.playlist.playlistId]];
        [CachedLocalPlaylist removeFromCache:[CachedLocalPlaylist findByPlaylistId:foundCell.playlist.playlistId]];
        
        [playlists removePlaylist:foundCell.playlist];
        [playlistsTable beginUpdates];
        [playlistsTable deleteRowsAtIndexPaths:@[[playlistsTable indexPathForCell:foundCell]] withRowAnimation:UITableViewRowAnimationFade];
        [playlistsTable endUpdates];
        
        MainViewController *mainViewController = (MainViewController *)[self.sidePanelController centerPanel];
        [mainViewController removeRowsWhenPlaylistDelete:deletableSongsLC andPlaylist:playlists.openedPlaylist];
        
        [playlistsTable reloadData];
        [self setOpacityForCells];
    }
    
    if([playlistChangeNotification.event isEqualToString:@"new_playlist"]) {
        
        Playlist *newPlaylist = [[Playlist alloc] init];
        newPlaylist.playlistId = playlistChangeNotification.playlistId;
        
        [[JusthearitClient sharedInstance] loadPlaylist:newPlaylist withSuccessBlock:^(NSArray *array) {
            
            Playlist *newPlaylistData = _.first(array);
            
            Log(@"playlist: %@", newPlaylistData);
            
            [playlists.collection addObject:newPlaylistData];
            [playlists refreshPlaylistGroups];
            
            if (playlists.myPlaylistsGroup.collapsed) {
                playlists.myPlaylistsGroup.collapsed = NO;
                [playlistsTable beginUpdates];
                [playlistsTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
                [playlistsTable endUpdates];
            }else {
                [playlistsTable reloadData];
                
            }
            
            [self setOpacityForCells];
            
            [CachedLocalPlaylist findOrCreateByPlaylist:newPlaylistData withBlock:^(CachedLocalPlaylist *cachedLocalPlaylist) {
            }];
            
        } withErrorBlock:^(NSError *error) {}];
        
    }
    
    if([playlistChangeNotification.event isEqualToString:@"position"]) {
        
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:playlistChangeNotification.playlistId];
        
        if(existingPlaylist) {
            Log(@"existing playlist position before: %@", existingPlaylist.position);
            
            existingPlaylist.position = playlistChangeNotification.position;
            [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
            Log(@"existing playlist position after: %@", existingPlaylist.position);
        }
        
        [playlists updatePosition:existingPlaylist.playlistId andOrder:[NSNumber numberWithInteger:[playlistChangeNotification.position integerValue]]];
        [playlists refreshPlaylistGroups];
        
        [playlistsTable reloadData];
        [self setOpacityForCells];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark ActionSheed Delegate

- (void)actionSheetCancel:(UIActionSheet *)actionSheet {
    playlistCellForActionSheet = nil;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    PlaylistCell *cell = playlistCellForActionSheet;
    
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([buttonTitle isEqualToString:@"Rename"]) {
        [Analytics event:@"playlists_view_controller":@"hit_rename_playlist_from_menu"];
        [self performSegueWithIdentifier:@"addToSegue" sender:cell];
    } else if ([buttonTitle isEqualToString:@"Share"]) {
        [Analytics event:@"playlists_view_controller":@"hit_share_playlist_from_menu"];
        //share
        [self presentViewController:[Utils sharingViewControllerForPlaylist:cell.playlist] animated:YES completion:nil];
    } else if ([buttonTitle isEqualToString:@"Cache"]) {
        //cache
        [Analytics event:@"playlists_view_controller":@"hit_cache_playlist_from_menu"];
        cell.playlist.cache;
    } else if ([buttonTitle isEqualToString:@"Delete"]) {
        //delete
        [Analytics event:@"playlists_view_controller":@"hit_delete_playlist_from_menu"];
        
        if (cell.playlist == playlists.openedPlaylist) {
            NSMutableArray *allPlaylists = playlists.allPlaylists;
            
            Playlist *toSelect = [allPlaylists next:cell.playlist];
            if (!toSelect) toSelect = [allPlaylists prev:cell.playlist];
            if (toSelect) [self openPlaylist:toSelect andShowMain:NO];
        }
        
        if (cell.playlist.isPlayingOrPaused) {
            
            if ([[self.sidePanelController centerPanel] isKindOfClass:[MainViewController class]]) {
                MainViewController *mainViewController = (MainViewController *)[self.sidePanelController centerPanel];
                [mainViewController stopMusic];
            }
        }
        
        if (!cell.playlist.isLocalPlaylist) {
            [[JusthearitClient sharedInstance] deletePlaylist:cell.playlist withSuccessBlock:^(NSArray *array) {} withErrorBlock:^(NSError *error) {}];
        }
        
        NSArray *deletableSongsLC = [[NSArray alloc] init];
        if(playlists.openedPlaylist.isMusicFeed) {
            CachedLocalPlaylist *cachedPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:cell.playlist.playlistId];
            deletableSongsLC = [SongCache songsOnlyInCachedMusicFeed:cachedPlaylist];
        }
        
        [CachedPlaylist removeFromCache:[CachedPlaylist findByPlaylistId:cell.playlist.playlistId]];
        [CachedLocalPlaylist removeFromCache:[CachedLocalPlaylist findByPlaylistId:cell.playlist.playlistId]];
        
        [playlists removePlaylist:cell.playlist];
        [playlistsTable beginUpdates];
        [playlistsTable deleteRowsAtIndexPaths:@[[playlistsTable indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationFade];
        [playlistsTable endUpdates];
        
        MainViewController *mainViewController = (MainViewController *)[self.sidePanelController centerPanel];
        [mainViewController removeRowsWhenPlaylistDelete:deletableSongsLC andPlaylist:playlists.openedPlaylist];
        
    } else if ([buttonTitle isEqualToString:@"Delete from cache"]) {
        [Analytics event:@"playlists_view_controller":@"hit_delete_from_cache_playlist_from_menu"];
        [CachedPlaylist removeFromCache:[CachedPlaylist findByPlaylistId:cell.playlist.playlistId]];
         [[SongCacheDownloadManager sharedInstance]clearQueue ];
        if (playlists.offlineMode) {
            [playlists removePlaylist:cell.playlist];
            [playlistsTable beginUpdates];
            [playlistsTable deleteRowsAtIndexPaths:@[[playlistsTable indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationFade];
            [playlistsTable endUpdates];
        }
        
    } else if ([buttonTitle isEqualToString:@"Make Public"]) {
        cell.playlist.private = @NO;
        [playlistsTable reloadData];
        [self setOpacityForCells];
        
        [[JusthearitClient sharedInstance] updatePlaylist:cell.playlist withSuccessBlock:^(NSArray *array) {} withErrorBlock:^(NSError *error) {}];
    } else if ([buttonTitle isEqualToString:@"Make Private"]) {
        cell.playlist.private = @YES;
        [playlistsTable reloadData];
        [self setOpacityForCells];
        [[JusthearitClient sharedInstance] updatePlaylist:cell.playlist withSuccessBlock:^(NSArray *array) {} withErrorBlock:^(NSError *error) {}];
    }
}

#pragma mark PlaylistCellDelegate

- (void)playlistCellDidSwipeLeft:(PlaylistCell *)cell {
    [[NSNotificationCenter defaultCenter] postNotificationName:PlaylistCellEnclosingTableViewDidBeginScrollingNotification object:nil userInfo:cell];
    
    BOOL canShowOptions = NO;
    
    if (SETTINGS.isLoggedIn) {
        //if we're logged in
        if (!(cell.playlist.cachedPlaylist)) {
            //dont show options on cached history playlist
            canShowOptions = YES;
        }
    } else {
        if (playlists.offlineMode) {
            if (!(cell.playlist.cachedPlaylist)) {
                //dont show options on cached history playlist
                canShowOptions = YES;
            }
        }
    }
    
    if (canShowOptions) [cell toggleOptionsButton];
}

- (void)playlistCellDidHitOptions:(PlaylistCell *)cell {
    playlistCellForActionSheet = cell;
    
    NSMutableArray *actionSheetButtonTitles = [@[] mutableCopy];
    
    if (playlists.offlineMode) {
        [actionSheetButtonTitles addObject:@"Delete from cache"];
    } else {
        if (cell.playlist.playlistGroupObject.isMyPlaylistsGroup) {
            [actionSheetButtonTitles addObject:@"Rename"];
            if (cell.playlist.private.boolValue) {
                [actionSheetButtonTitles addObject:@"Make Public"];
            } else {
                [actionSheetButtonTitles addObject:@"Make Private"];
            }
        }
        if (!cell.playlist.isSystem && !cell.playlist.isLocalPlaylist && !cell.playlist.isOfflinePlaylist) [actionSheetButtonTitles addObject:@"Share"];
        CachedPlaylist *cachedPlaylist = [CachedPlaylist findByPlaylistId:cell.playlist.playlistId];
        
        if (cachedPlaylist) {
            [actionSheetButtonTitles addObject:@"Delete from cache"];
        } else {
            [actionSheetButtonTitles addObject:@"Cache"];
        }
        
        if (!cell.playlist.isSystem) [actionSheetButtonTitles addObject:@"Delete"];
    }
    
    [actionSheetButtonTitles addObject:@"Cancel"];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:cell.playlist.name
                                  delegate:self
                                  cancelButtonTitle:nil
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:nil];
    
    for (uint i = 0; i < actionSheetButtonTitles.count; i++) {
        NSString *title = actionSheetButtonTitles[i];
        [actionSheet addButtonWithTitle:title];
        if ([title isEqualToString:@"Cancel"]) {
            [actionSheet setCancelButtonIndex:i];
        } else if ([title isEqualToString:@"Delete"] || [title isEqualToString:@"Delete from cache"]) {
            [actionSheet setDestructiveButtonIndex:i];
        }
    }
    
    [actionSheet showInView:self.view];
}

#pragma mark FMMoveTableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (playlistsMode == playlistsViewControllerAddTo) {
        return 1;
    } else if (playlists.offlineMode || !SETTINGS.isLoggedIn) {
        return 1;
    } else {
        return playlists.playlistGroups.count;
    }
}

- (NSInteger)tableView:(FMMoveTableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (playlistsMode == playlistsViewControllerAddTo) {
        return [playlists myPlaylistsGroup].collection.count;
    } else {
        NSInteger numberOfRows = [playlists numberOfPlaylistsInGroupIndex:section];
        
        if ([tableView movingIndexPath] && [[tableView movingIndexPath] section] != [[tableView initialIndexPathForMovingRow] section]){
            if (section == [[tableView movingIndexPath] section]) {
                numberOfRows++;
            } else if (section == [[tableView initialIndexPathForMovingRow] section]) {
                numberOfRows--;
            }
        }
        
        return numberOfRows;
    }
}

- (UITableViewCell *)tableView:(FMMoveTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"playlistCell";
    PlaylistCell *cell = (PlaylistCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setBackgroundColor:[UIColor clearColor]];
    if ([tableView indexPathIsMovingIndexPath:indexPath]) {
        [cell prepareForMove];
    } else {
        
        Playlist *playlist = nil;
        
        if (playlistsMode == playlistsViewControllerAddTo) {
            playlist = [playlists myPlaylistsGroup].collection[indexPath.row];
        } else {
            playlist = [playlists playlistAtIndexPath:indexPath];
        }
        
        cell.titleLabel.text = playlist.name;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        NSString *icon = @"icon_normal_playlist.png";
        
        if (playlist.private.boolValue) {
            icon = @"icon_playlist_private.png";
        }
        //NSLog(@"playlist mode %d", playlistsMode);
        //NSLog(@"is add to: %d", playlistsViewControllerAddTo);
        //Log(@"scroll position: %f", playlistsTable.contentOffset.y);
        //Log(@"playlist name: %@", playlist.name);
        //Log(@"playlist group: %@", playlist.playlistGroup);
        
        
        if (playlistsMode == playlistsViewControllerAddTo) {
            icon = @"icon_plus.png";
        } else if (playlist.isLocalPlaylist) {
            
            Log(@"is local shared playlist: %hhd", playlist.isLocalSharedPlaylist);
            
            if (playlist.isLocalSearchPlaylist) {
                icon = @"icon_magnifier36.png";
            } else if (playlist.isLocalSearchPlaylistsWith) {
                icon = @"icon_playlists_with.png";
            } else if (playlist.isLocalSharedPlaylist) {
                icon = @"icon_playlist_shared.png";
            }
            
        } else {
            if (playlist.isMusicFeed) {
                icon = @"icon_music_feed.png";
            } else if (playlist.isMyUploads) {
                icon = @"icon_my_uploads.png";
            } else if (playlist.isRecentlyPlayed) {
                icon = @"icon_recently_played.png";
            } else if (playlist.isSharedPlaylist) {
                icon = @"icon_playlist_shared.png";
            } else if (playlist.isSharedSearchPlaylist) {
                icon = @"icon_search_shared.png";
            }else if(playlist.isMyPlaylist&&playlist.playlistType.intValue!=playlistTypeShared) {
                icon = @"icon_normal_playlist.png";
            }else if (playlist.isInCache) {
                icon = @"icon_playlist_shared.png";
            }else if (playlist.playlistType.intValue==playlistTypeShared) {
                icon = @"icon_playlist_shared.png";
            }
            
        }
        
        //Log(@"icon playlist: %@", icon);
        
        if (playlist.isPlayingOrPaused && playlistsMode != playlistsViewControllerAddTo) {
            icon = [icon stringByReplacingOccurrencesOfString:@".png" withString:@"_active.png"];
        }
        
        [cell.icon setImage:[UIImage imageNamed:icon]];
        
        UIView *bgColorView = [PlaylistCell makeSelectionView];
        bgColorView.frame = [cell selectionFrame];
        UIView *cellBackground = [[UIView alloc] initWithFrame:cell.frame];
        [cellBackground addSubview:bgColorView];
        [cell setSelectedBackgroundView:cellBackground];
        
        cell.playlist = playlist;
        cell.playlistCellDelegate = self;
        
        if (playlist.badgeCount.intValue > 0) {
            [cell repositionBadgeWithText:[NSString stringWithFormat:@"+%d", playlist.badgeCount.intValue]];
        } else {
            [cell repositionBadgeWithText:[NSString stringWithFormat:@""]];
        }
        
        [cell markSelected:[playlists.openedPlaylist equalsToPlaylist:playlist]];
        
        cell.shouldIndentWhileEditing = NO;
        cell.showsReorderControl = NO;
        
        [cell setBackgroundColor:[UIColor clearColor]];
        
    }
    //[cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}

- (BOOL)moveTableView:(FMMoveTableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if (playlistsMode == playlistsViewControllerAddTo) {
        return NO;
    } else {
        return SETTINGS.isLoggedIn;
    }
}
- (void) gotOfflineSet
{
    loginButton.hidden = YES;
    addPlaylistButton.hidden = YES;
}
- (void) gotOnlineSet
{
    addPlaylistButton.hidden=NO;
    loginButton.hidden = NO;
}
- (void)moveTableView:(FMMoveTableView *)tableView moveRowFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
    Log(@"to index Path: %d", toIndexPath.row);
    
    [playlists movePlaylistFromIndexPath:fromIndexPath toIndexPath:toIndexPath];
    
    if (toIndexPath.section == fromIndexPath.section) {
        if(toIndexPath.row != fromIndexPath.row) {
            PlaylistMove *playlistMove = [[PlaylistMove alloc] init];
            playlistMove.playlistId = movingPlaylist.playlistId;
            playlistMove.position = [NSNumber numberWithInt:toIndexPath.row];
            [[JusthearitClient sharedInstance] movePlaylist:playlistMove withSuccessBlock:^(NSArray *array) {
                
                Playlist *responsePlaylist = array.firstObject;
                Log(@"playlist name id: %@ %@ %@", responsePlaylist.name, responsePlaylist.playlistId, responsePlaylist.position);
                
                NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
                CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:responsePlaylist.playlistId];
                
                if(existingPlaylist) {
                    
                    existingPlaylist.position = [NSString stringWithFormat:@"%@", responsePlaylist.position];
                    [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
                    
                }
                
            } withErrorBlock:^(NSError *error) {}];
            // && toIndexPath.row == 0
            if(toIndexPath.section == 0){
                
                Playlist *playlistIsDefault = [playlists.defaultPlaylistsGroup.collection objectAtIndex:0];
                PlaylistIsDefault *pls = [[PlaylistIsDefault alloc] init];
                
                pls.playlistId = playlistIsDefault.playlistId;
                pls.isDefault = @YES;
                [[JusthearitClient sharedInstance] updatePlaylistIsDefault:pls withSuccessBlock:^(NSArray *array) {
                    
                    Playlist *responsePlaylist = array.firstObject;
                    //Log(@"playlist response: %@", reponsePlaylist.isDefault);
                    
                    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
                    CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:responsePlaylist.playlistId];
                    
                    NSArray *allPlaylists = [CachedLocalPlaylist MR_findAll];
                    
                    for(CachedLocalPlaylist *p in allPlaylists) {
                        
                        p.isDefault = [NSNumber numberWithBool:NO];
                        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
                        
                    }
                    //Log(@"existing playlist: %@", existingPlaylist);
                    if(existingPlaylist) {
                        
                        existingPlaylist.position = [NSString stringWithFormat:@"%@", responsePlaylist.position];
                        existingPlaylist.isDefault = responsePlaylist.isDefault;
                        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
                        
                    }
                    
                } withErrorBlock:^(NSError *error) {
                    
                }];
                
            }
        }
    } else if (movingPlaylist.isLocalSearchPlaylist && toIndexPath.section == 1) {
        LocalPlaylist *moving = movingPlaylist;
        moving.position = @(toIndexPath.row);
        
        [[JusthearitClient sharedInstance] createSharedSearchFromPlaylist:moving withSuccessBlock:^(NSArray *array) {
            [self localPlaylistDroppedInFollowingGroup:moving withResponse:array.firstObject toIndexPath:toIndexPath];
        } withErrorBlock:^(NSError *error) {}];
    } else if (movingPlaylist.isLocalSharedPlaylist && toIndexPath.section == 1 && movingPlaylist.userId && ![SETTINGS.userId isEqualToNumber:movingPlaylist.userId]) {
        LocalPlaylist *moving = movingPlaylist;
        moving.position = @(toIndexPath.row);
        
        [[JusthearitClient sharedInstance] createSharedPlaylistFromPlaylist:moving withSuccessBlock:^(NSArray *array) {
            [self localPlaylistDroppedInFollowingGroup:moving withResponse:array.firstObject toIndexPath:toIndexPath];
        } withErrorBlock:^(NSError *error) {}];
    }
    
    movingPlaylist = nil;
    
}

- (void)moveTableViewFinished:(FMMoveTableView *)tableView {
    
    //    Playlist *blank = playlists.followingPlaylistsGroup.blankPlaylist;
    //    if (blank) {
    //        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[playlists.followingPlaylistsGroup.collection indexOfObject:blank] inSection:1];
    //        [playlists.followingPlaylistsGroup.collection removeObject:blank];
    //        [playlistsTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    //    }
}

- (void) localPlaylistDroppedInFollowingGroup:(Playlist *)movingPlaylist withResponse:(Playlist*)responsePlaylist toIndexPath:(NSIndexPath *)toIndexPath{
    if (!responsePlaylist) return;
    
    responsePlaylist.playState = movingPlaylist.playState;
    if (playlists.openedPlaylist == movingPlaylist) {
        playlists.openedPlaylist = responsePlaylist;
        //[playlists.openedPlaylist.pagination incrementCurrentPage];
    }
    
    [playlists.temporaryCollection removeObject:movingPlaylist];
    [playlists.collection addObject:responsePlaylist];
    if (playlists.followingPlaylistsGroup.collection.count == 1) {
        [playlists.followingPlaylistsGroup.collection replaceObjectAtIndex:0 withObject:responsePlaylist];
        [playlistsTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        [playlists.followingPlaylistsGroup.collection replaceObjectAtIndex:toIndexPath.row withObject:responsePlaylist];
        [playlistsTable reloadRowsAtIndexPaths:@[toIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    responsePlaylist.songs = movingPlaylist.songs;
    
    Playlist *playlist = [playlists playlistAtIndexPath:toIndexPath];
    
    responsePlaylist.isDroppedInFollowing = YES;
    
    [self openPlaylist:responsePlaylist andShowMain:NO];
}

- (void)moveTableView:(FMMoveTableView *)tableView willMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    movingPlaylist = [playlists playlistAtIndexPath:indexPath];
    
    if (movingPlaylist.isLocalPlaylist && indexPath.section == 0) {
        
        //       if (playlists.followingPlaylistsGroup.collection.count == 0) {
        //           [playlists.followingPlaylistsGroup.collection addObject:[Playlist blankPlaylist]];
        //           NSIndexPath *blankIndex = [NSIndexPath indexPathForRow:0 inSection:1];
        //           [playlistsTable insertRowsAtIndexPaths:@[blankIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
        //       }
        
        
        if (playlists.followingPlaylistsGroup.collapsed) {
            playlists.followingPlaylistsGroup.collapsed = NO;
            [playlistsTable beginUpdates];
            [playlistsTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
            [playlistsTable endUpdates];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) return 0;
    else return PLAYLISTS_HEADER_HEIGHT - 10;
}

#pragma mark -
#pragma mark Table view delegate

- (CGFloat)tableView:(FMMoveTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (playlistsMode == playlistsViewControllerAddTo) {
        return PLAYLISTS_CELL_HEIGHT;
    }
    
    if ([playlists playlistIsLastInSection:indexPath]) {
        return PLAYLISTS_CELL_HEIGHT + PLAYLISTS_CELL_PADDING_BOTTOM;
    }
    
    return PLAYLISTS_CELL_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PlaylistHeader *header = [PlaylistHeader initWithPlaylistGroup:playlists.playlistGroups[section] andWidth:tableView.frame.size.width];
    header.delegate = self;
    return header;
}

- (NSIndexPath *)moveTableView:(FMMoveTableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    
    if (proposedDestinationIndexPath.section != sourceIndexPath.section) {
        if (movingPlaylist.isLocalPlaylist && !movingPlaylist.isLocalSearchPlaylistsWith) {
            Log(@"proposed section %d", proposedDestinationIndexPath.section);
            if (proposedDestinationIndexPath.section <= 1) {
                return proposedDestinationIndexPath;
            } else {
                Log(@"returning source %d", sourceIndexPath.section);
                return sourceIndexPath;
            }
        }
    } else {
        return proposedDestinationIndexPath;
    }
    
    return sourceIndexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[NSNotificationCenter defaultCenter] postNotificationName:PlaylistCellEnclosingTableViewDidBeginScrollingNotification object:nil];
    
    if (playlistsMode == playlistsViewControllerAddTo) {
        //Log(@"DROPPED");
        [playlistsTable deselectRowAtIndexPath:indexPath animated:YES];
        [self addSongsToPlaylist:[playlists myPlaylistsGroup].collection[indexPath.row]];
        
        if(IS_DEVICE == IS_IPHONE) {
            
            [self.sidePanelController showCenterPanelAnimated:YES];
            
        }
        
    } else {
        
        Playlist *playlist = [playlists playlistAtIndexPath:indexPath];
        
        if(IS_DEVICE == IS_IPAD) {
            [self openPlaylist:playlist andShowMain:NO];
        }else {
            [self openPlaylist:playlist andShowMain:YES];
        }
        
    }
}

- (void) openPlaylist:(Playlist *)playlist andShowMain:(BOOL)showMain {
    
    [self loadPlaylistSongsFromPlaylist:playlist andShowMain:showMain];
    
    [playlists openPlaylist:playlist];
    [playlistsTable reloadData];
    
    //Log(@"has notification: %hhd", playlist.hasNotification);
    
    if(playlist.isDroppedInFollowing) {
        [CachedLocalPlaylist findOrCreateByPlaylist:playlist withBlock:^(CachedLocalPlaylist *cachedLocalPlaylist) {
        }];
    }
    
    
    [self setOpacityForCells];
}

- (void) scrollToBottom {
    
    [(MainViewController*) self.sidePanelController.centerPanel scrollToBottom];
    
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
}

#pragma mark -
#pragma mark Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"loginSegue"]) {
        
        LoginViewController *lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewC"];
        lvc.delegate = self;
       
        if(iLogin==1)
        {
            lvc.myInteger=1;
        }
        else
        {
            lvc.myInteger=0;
        }
        iLogin=0;
        
        
       // [self presentViewController:modalVC animated:YES completion:nil];

        
        
        
        
        
        
        
        
        
        
        UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
        MLWBluuurView *beView = [[MLWBluuurView alloc] initWithEffect:blurEffect];
        beView.blurRadius=10;
        beView.frame = self.view.bounds;
        
        lvc.view.frame = self.view.bounds;
        lvc.view.backgroundColor = [UIColor clearColor];
        [lvc.view insertSubview:beView atIndex:0];
        lvc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        
        [self presentViewController:lvc animated:YES completion:nil];
    }
    else if ([segue.identifier isEqualToString:@"addToSegue"]) {
        NewPlaylistViewController *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPlaylistC"];
        
        nvc.delegate = self;
        
        
        
        
        
        self.animator = [[ZFModalTransitionAnimator alloc] initWithModalViewController:nvc];
        self.animator.dragable = YES;
        self.animator.bounces = YES;
        self.animator.behindViewAlpha = 1;
        self.animator.behindViewScale = 1;
        self.animator.transitionDuration = 0.7f;
        
        
        
        
        nvc.transitioningDelegate = self.animator;
        
        
        
        
        UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
        MLWBluuurView *beView = [[MLWBluuurView alloc] initWithEffect:blurEffect];
        beView.blurRadius=20;
        beView.frame = self.view.bounds;
        
        nvc.view.frame = self.view.bounds;
        nvc.view.backgroundColor = [UIColor clearColor];
        [nvc.view insertSubview:beView atIndex:0];
        nvc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        
        [self presentViewController:nvc animated:YES completion:nil];
        
        if (sender == _addToSongs) {
            nvc.songs = _addToSongs;
        } else if ([sender class] == [PlaylistCell class]) {
            PlaylistCell *cell = sender;
            nvc.renamePlaylist = cell.playlist;
        }
    } else if([segue.identifier isEqualToString:@"searchSegue"]) {
        SearchViewController * svc = (SearchViewController*) segue.destinationViewController;
        svc.delegate = self;
    }
}

#pragma mark -
#pragma mark New Playlist VC delegate

- (void)newPlaylistViewControllerDidFinish:(NewPlaylistViewController *)controller withCreatePlaylist:(Playlist *)playlist {
    [self dismissViewControllerAnimated:YES completion:^{}];
    [self createPlaylist:playlist];
    
}

- (void)newPlaylistViewControllerDidFinish:(NewPlaylistViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^{}];
    [self setNormalMode];
}

- (void)newPlaylistViewControllerDidFinish:(NewPlaylistViewController *)controller withRenamePlaylist:(Playlist *)playlist {
    [self dismissViewControllerAnimated:YES completion:^{}];
    [self renamePlaylist:playlist];
}


#pragma mark -
#pragma mark Login View controller delegate

- (void)loginViewControllerDidFinish:(LoginViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)sessionChanged:(LoginViewController *)controller {
    /* cachedLocalPlaylists = [[NSMutableArray alloc] initWithArray:[CachedLocalPlaylist MR_findAll]];
     if(cachedLocalPlaylists.count>0)
     {
     for(int i=0;i<playlists.allPlaylists.count;i++)
     {
     [CachedLocalPlaylist removeFromCache:cachedLocalPlaylists[i]];
     }
     }
     if(SETTINGS.isLoggedIn)
     {
     [playlists loadPlaylistsWithBlock:^(void) {
     [animatedSpinner show:NO];
     //Log(@"load open url: %@", ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists);
     
     if (ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists) {
     [playlists handleOpenUrl:ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists];
     ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists = nil;
     } else {
     if(playlists.allPlaylists.count>0)
     {
     Playlist *firstplaylist = playlists.allPlaylists[0];
     id currentCenter = self.sidePanelController.centerPanel;
     if (firstplaylist) {
     [playlists openPlaylist:firstplaylist];
     
     if ([currentCenter isKindOfClass:[MainViewController class]]) {
     [(MainViewController*) self.sidePanelController.centerPanel loadPlaylistSongs:firstplaylist fromScratch:YES];
     }
     
     } else {
     if ([currentCenter isKindOfClass:[MainViewController class]]) {
     [(MainViewController*) self.sidePanelController.centerPanel loadPlaylistSongs:nil fromScratch:YES];
     }
     
     }
     }
     }
     
     [self playlistsChanged];
     }];
     }*/

    [self loadPlaylists];
}



- (void) loadPlaylists {
    

    
    if(SETTINGS.isLoggedIn && [NWReachabilityManger isReachable]&&SETTINGS.OfflineOnly==NO) {
       // Log(@"1");
        // [playlists loadLocalCachedPlaylists];
      /*         [[JusthearitClient sharedInstance] loadPlaylists:^(NSArray *response) {
            
            Playlist *playlistToCompare = [[Playlist alloc] init];
            for (Playlist *playlistFound in response) {
                playlistToCompare = [playlists findByPlaylistId:playlistFound.playlistId];
                
                
                if ([playlistToCompare.updatedAt compare:playlistFound.updatedAt] != NSOrderedSame) {
                    
                    [playlists updateHasNotification:playlistToCompare];
                    
                    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
                    CachedLocalPlaylist *existingPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:playlistToCompare.playlistId];
                    
                    if(existingPlaylist) {
                        
                        existingPlaylist.updatedAt = playlistFound.updatedAt;
                        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
                        
                    }
                }
                
            }
            
        } withErrorBlock:^(NSError *error) {}];*/
        
        
        playlists.offlineMode = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.navigationBarShouldReload object:nil userInfo:nil];
        
        if (ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists) {
            [playlists handleOpenUrl:ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists];
            ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists = nil;
        } else {
            [animatedSpinner show:YES];
            [playlists loadPlaylistsWithBlock:^(void) {
               
                [animatedSpinner show:NO];
                
                if (ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists) {
                    [playlists handleOpenUrl:ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists];
                    ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists = nil;
                } else {
                    Playlist *defaultPlaylist = playlists.defaultPlaylist;
                    id currentCenter = self.sidePanelController.centerPanel;
                    NSLog(@"<<<<<<<<<<< Is Music Feed: %hhd >>>>>>>>>>>>>",[defaultPlaylist isMusicFeed]);
                    if (defaultPlaylist) {
                        [playlists openPlaylist:defaultPlaylist];
                        
                        if ([currentCenter isKindOfClass:[MainViewController class]]) {
                            [(MainViewController*) self.sidePanelController.centerPanel loadPlaylistSongs:defaultPlaylist fromScratch:YES];
                        
                        }
                    } else {
                        if ([currentCenter isKindOfClass:[MainViewController class]]) {
                            [(MainViewController*) self.sidePanelController.centerPanel loadPlaylistSongs:nil fromScratch:YES];
                        }
                    }
                }
                [self playlistsChanged];
            }];
            
            // [self performSelector:@selector(openpl) withObject:nil afterDelay:.6];
            
        }
    }
    else if(SETTINGS.OfflineOnly==YES)
    {
       // Log(@"2");
   
        Log(@"PLMPLMPLMPLM");
        [animatedSpinner show:NO];
        
        
        [playlists loadPlaylistsWithBlock:^void{
            
            
        }];
        if(playlists.allPlaylists.count>0)
        {
            
            id currentCenter = self.sidePanelController.centerPanel;
            Playlist *firstPlaylist;
            firstPlaylist= playlists.allPlaylists[0];
            [playlists openPlaylist:firstPlaylist];
            //NUSH INCA [playlists openPlaylist:firstPlaylist];
            
            if ([currentCenter isKindOfClass:[MainViewController class]]) {
                [(MainViewController*) self.sidePanelController.centerPanel loadPlaylistSongs:firstPlaylist fromScratch:YES];
            }
            
            
            
        }
        [self playlistsChanged];
        
        
        
        
        //[animatedSpinner show:NO];
        
    }
    else {
       // alert(@"3");

        
        [animatedSpinner show:YES];
        [playlists loadPlaylistsWithBlock:^(void) {
            [animatedSpinner show:NO];
            //Log(@"load open url: %@", ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists);
            
            if (ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists) {
                [playlists handleOpenUrl:ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists];
                ApplicationDelegate.applicationShouldLoadResourceAfterLoadingPlaylists = nil;
            } else {
                
                
                Playlist *firstPlaylist;
                if(playlists.allPlaylists.count>0)
                {
                    firstPlaylist= playlists.allPlaylists[0];
                }
                id currentCenter = self.sidePanelController.centerPanel;
                if(playlists.allPlaylists.count>0)
                {
                    [playlists openPlaylist:firstPlaylist];
                    if ([currentCenter isKindOfClass:[MainViewController class]]) {
                        [(MainViewController*) self.sidePanelController.centerPanel loadPlaylistSongs:firstPlaylist fromScratch:YES];
                    }
                    
                }
                
            }
            
            [self playlistsChanged];
        }];
        
    }
}



#pragma mark -
#pragma mark User interface actions

- (IBAction) newPlaylistHit {
    if (SETTINGS.isLoggedIn)
        [self performSegueWithIdentifier:@"addToSegue" sender:_addToSongs];
    else
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"CoachPH10"
         object:self];
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
    
}

- (IBAction)openLoginViewController {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"CoachPH10"
     object:self];
    [self performSegueWithIdentifier:@"loginSegue" sender:self];
}
- (void)searchViewControllerDidFinish:(SearchViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)openAfterSearch:(MainViewController *)mainVC andPlaylist:(id)playlist{
    
    [self.sidePanelController showCenterPanelAnimated:NO];
    [(MainViewController*)self.sidePanelController.centerPanel loadPlaylistSongs:playlist fromScratch:YES];
    
}
- (IBAction)openSearchViewController {
    
    [self performSegueWithIdentifier:@"searchSegue" sender:self];
    /*if ([[self.sidePanelController centerPanel] isKindOfClass:[SearchViewController class]]) {
     [self.sidePanelController showCenterPanelAnimated:YES];
     } else {
     SearchViewController* searchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"searchViewController"];
     self.sidePanelController.delegate = searchViewController;
     [self.sidePanelController setCenterPanel:searchViewController];
     }*/
}

- (IBAction) openSongsViewController {
    [self loadPlaylistSongsFromPlaylist:playlists.openedPlaylist andShowMain:YES];
}

- (void) loadPlaylistSongsFromPlaylist:(Playlist *)playlist andShowMain:(BOOL)showMain {
    
    if ([[self.sidePanelController centerPanel] isKindOfClass:[MainViewController class]]) {
        MainViewController *mainViewController = (MainViewController *)[self.sidePanelController centerPanel];
        if (mainViewController.playerView.viewState == STATE_OPEN) [mainViewController.playerView setStateClosedAnimated];
        if (showMain) [self.sidePanelController showCenterPanelAnimated:YES];
    } else {
        //if we delete a playlist while the search view controller is on, the playlist drawer will close
        MainViewController *mainViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
        self.sidePanelController.delegate = mainViewController;
        [self.sidePanelController setCenterPanel:mainViewController];
    }
    //Log(@"islocal: %hhd", playlists.openedPlaylist.isLocalSharedPlaylist);
    if ( (playlist && playlist != playlists.openedPlaylist ) || playlist.isDroppedInFollowing) {
        playlists.openedPlaylist = playlist;
        [(MainViewController*) self.sidePanelController.centerPanel loadPlaylistSongs:playlist fromScratch:YES];
    }
    
}

#pragma mark -
#pragma mark ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self setOpacityForCells];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [[NSNotificationCenter defaultCenter] postNotificationName:PlaylistCellEnclosingTableViewDidBeginScrollingNotification object:nil];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    lastScrollPosition = playlistsTable.contentOffset.y;
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate {
    lastScrollPosition = playlistsTable.contentOffset.y;
}

- (void) setOpacityForCells {
    NSArray *visibleCells = [playlistsTable visibleCells];
    
    PlaylistCell *firstCell = [visibleCells firstObject];
    
    for (PlaylistCell *visibleCell in visibleCells) {
        if (visibleCell != firstCell) {
            visibleCell.alpha = 1;
        }
    }
    
    if ([playlistsTable indexPathForCell:firstCell].section == 0) {
        return;
    }
    
    CGFloat delta = firstCell.frame.origin.y - playlistsTable.contentOffset.y;
    
    NSInteger max = 5;
    NSInteger min = -10;
    
    NSInteger coef = abs(min);
    
    
    if (delta > min && delta < max) {
        firstCell.alpha = (delta+coef)/15;
    } else if (delta <= min) {
        firstCell.alpha = 0;
        
    } else if (delta > max) {
        firstCell.alpha = 1;
    }
}

#pragma mark -
#pragma mark PlaylistHeaderDelegate

- (void)playlistHeaderHit:(PlaylistHeader *)playlistHeader {
    NSInteger section = [playlists.playlistGroups indexOfObject:playlistHeader.playlistGroup];
    if (section >= 0) {
        [playlistsTable beginUpdates];
        [playlistsTable reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationAutomatic];
        [playlistsTable endUpdates];
    }
}

#pragma mark -
#pragma mark General functions

- (void) playlistsChanged {
    
    [playlistsTable reloadData];
    [self setOpacityForCells];
    if(SETTINGS.OfflineOnly)
    {
        [self setPlaylistsTableFull];
        searchButton.hidden = YES;
        loginButton.hidden = YES;
        addPlaylistButton.hidden = YES;
        
    }
    else if (playlists.offlineMode) {
        [self setPlaylistsTableFull];
        searchButton.hidden = YES;
        loginButton.hidden = YES;
        addPlaylistButton.hidden = YES;
    } else {
        [self setPlaylistsTableNormal];
        searchButton.hidden = NO;
        loginButton.hidden = NO;
        addPlaylistButton.hidden = NO;
    }
}

- (void) setAddToModeWithSongs:(NSArray*)songs {
    playlistsMode = playlistsViewControllerAddTo;
    [self playlistsChanged];
    searchButton.hidden = YES;
    loginButton.hidden = YES;
    [self setPlaylistsTableFull];
    _addToSongs = songs;
}

- (void) setPlaylistsTableNormal {
    CGFloat height = playlistsTable.frame.size.height;
    
    if (playlistsTable.frame.origin.y == 0)
        height = playlistsTable.frame.size.height - TABLE_TOP_OFFSET;
    
    playlistsTable.frame = CGRectMake(0, TABLE_TOP_OFFSET, [Utils screenWidth], 920.0f);
    
    [playlistsTable setContentOffset:CGPointMake(0, lastScrollPosition) animated:NO];
}

- (void) setPlaylistsTableFull {
    CGRect playlistsTableFrame = playlistsTable.frame;
    
    playlistsTableFrame.size.height = playlistsTable.frame.size.height + playlistsTable.frame.origin.y;
    
    CGFloat y;
    if(IS_DEVICE == IS_IPAD) {
        y = 55.0f;
    } else {
        y = 0.0f;
    }
    
    playlistsTableFrame.origin.y = y;
    playlistsTable.frame = playlistsTableFrame;
}

- (void)setNormalMode {
    playlistsMode = playlistsViewControllerNormal;
    [self playlistsChanged];
    searchButton.hidden = NO;
    loginButton.hidden = NO;
    addPlaylistButton.hidden = NO;
    [self setPlaylistsTableNormal];
    _addToSongs = nil;
    
    //Log(@"index of opened p: %lu", (unsigned long)[[playlists collection] indexOfObject:playlists.openedPlaylist]);
    /*Log(@"playlist group: %@", playlists.openedPlaylist.playlistGroup);
     
     int section = 0;
     
     if(playlists.openedPlaylist.playlistGroup == [NSNumber numberWithInt:3]) {
     section = 1;
     }else if(playlists.openedPlaylist.playlistGroup == [NSNumber numberWithInt:1]){
     section = 2;
     }
     
     NSMutableArray *playlistsTmp = [[NSMutableArray alloc] init];
     playlistsTmp = [playlists playlistsForGroupKey:playlists.openedPlaylist.playlistGroup.intValue];
     
     int indexOfObject = [playlistsTmp indexOfObject:playlists.openedPlaylist];
     Log(@"index of object: %d", indexOfObject);
     
     NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfObject inSection:section];
     
     [playlistsTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];*/
    
}

- (void)createPlaylist:(Playlist *)playlist {
    [self setNormalMode];
    [animatedSpinner show:YES];
    
    [[JusthearitClient sharedInstance] createPlaylist:playlist withSuccessBlock:^(NSArray *array) {
        Playlist *playlist = array.firstObject;
        [playlists.collection addObject:playlist];
        [playlists refreshPlaylistGroups];
        
        if (playlists.myPlaylistsGroup.collapsed) {
            playlists.myPlaylistsGroup.collapsed = NO;
            [playlistsTable beginUpdates];
            [playlistsTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
            [playlistsTable endUpdates];
        }else {
            [playlistsTable reloadData];
        }
        
        [CachedLocalPlaylist findOrCreateByPlaylist:playlist withBlock:^(CachedLocalPlaylist *cachedLocalPlaylist) {
        }];
        
        [animatedSpinner show:NO];
        [self scrollTableToBottom];
    } withErrorBlock:^(NSError *error) {
        [animatedSpinner show:NO];
    }];
}

- (void) renamePlaylist:(Playlist *)playlist {
    [playlistsTable reloadData];
    [[JusthearitClient sharedInstance] updatePlaylist:playlist withSuccessBlock:^(NSArray *array) {
        
    } withErrorBlock:^(NSError *error) {
        
    }];
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    CachedLocalPlaylist *cachedLocalPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:playlist.playlistId inContext:localContext];
    
    if(cachedLocalPlaylist) {
        cachedLocalPlaylist.name = playlist.name;
        //[song MR_deleteInContext:localContext];
        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
    }
}

- (void) addSongsToPlaylist:(Playlist *)playlist {
    //    [animatedSpinner show:YES];
    
    PlaylistAddSongs *p = [[PlaylistAddSongs alloc] init];
    p.playlistId = playlist.playlistId;
    p.songs = [_addToSongs mutableCopy];
    
    [self setNormalMode];
    
    [[JusthearitClient sharedInstance] addSongsToPlaylist:p withSuccessBlock:^(NSArray *array) {
    } withErrorBlock:^(NSError *error) {
    }];
    
    //Log(@"has songs in cache: %hhd", [playlist hasSongsInCache]);
    
    if([playlist hasSongsInCache]) {
        
        PlaylistSong *playlistSong = [p.songs objectAtIndex:0];
        
        /*[[JusthearitClient sharedInstance] loadSong:playlistSong withSuccessBlock:^(NSArray *array) {
         
         Log(@"response: %@", array);
         
         } withErrorBlock:^(NSError *error) {
         
         }];*/
        
        //Log(@"playlist id: %@", playlistSong.playlistId);
        //Log(@"songs to cache: %@",[p.songs objectAtIndex:0]);
        
        SongCache *cache = [SongCache initWithSong:playlistSong];
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        //Log(@"playlist id: %@", p.playlistId);
        CachedLocalPlaylist *localPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistId" withValue:p.playlistId inContext:localContext];
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"(playlistId = %@)", localPlaylist.playlistId];
        
        NSArray *alls = [[NSArray alloc] init];
        alls = [CachedLocalPlaylistSong MR_findAllWithPredicate:predicate1];
        
        NSSortDescriptor *orderDescriptor;
        orderDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:NO];
        
        alls = [alls sortedArrayUsingDescriptors:@[orderDescriptor]];
        //Log(@"alls: %@", [[alls firstObject] order]);
        
        NSNumber *maxOrder = [CachedLocalPlaylistSong MR_aggregateOperation:@"max:" onAttribute:@"order" withPredicate:predicate1];
        
        [cache saveToCachedPlaylistNoDownload:localPlaylist andOrder:[NSNumber numberWithInt:[maxOrder integerValue] + 1]];
        
        [[[[Player sharedInstance] currentPlayingPlaylist] songs] addObject:playlistSong];
        
    }
}

- (void) scrollTableToBottom {
    
    NSInteger numberOfSections = [self numberOfSectionsInTableView:playlistsTable];
    NSInteger numberOfRowsInLastSection = [self tableView:playlistsTable numberOfRowsInSection:numberOfSections-1];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(numberOfRowsInLastSection - 1) inSection:(numberOfSections - 1)];
    
    [playlistsTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}

- (void) draggingSongOverStartedWithGesture:(UILongPressGestureRecognizer *)gesture andContainer:(UIView *)container{
    [tableScroll scrollBeganWithView:container];
}

- (void) draggingSongOverStoppedWithGesture:(UILongPressGestureRecognizer *)gesture andContainer:(UIView *)container {
    [tableScroll scrollingEnded];
  
        
    
    dashed.alpha=0;
    addPlaylistButton.alpha = 0.8;
    
    [addPlaylistButton setTitle:@"New Playlist" forState:UIControlStateNormal];
}


- (void) draggingSongOverWithGesture:(UILongPressGestureRecognizer *)gesture andContainer:(UIView *)container {
    
    CGPoint touchPoint = [gesture locationInView:self.view];
    
    CGPoint touchPointTable = [gesture locationInView:playlistsTable];
    NSIndexPath *indexPath = [playlistsTable indexPathForRowAtPoint:touchPointTable];
    
    PlaylistCell *cell = (PlaylistCell *)[playlistsTable cellForRowAtIndexPath:indexPath];
    
    _.arrayEach(playlistsTable.visibleCells, ^(PlaylistCell *cell){
        [cell markSelected:NO];
    });
    
    [cell markSelected:YES];
   
    
    
  

    [addPlaylistButton setTitle:@"Drop here to create playlist" forState:UIControlStateNormal];
    dashed.alpha=1;
    CGRect temp = self.addPlaylistButton.frame;
    temp.size.width=[Utils currentWidth];
    self.addPlaylistButton.frame = temp;
    if (touchPoint.y > addPlaylistButton.frame.origin.y) {
        //if we're under the table, we use all the space for the drop zone, not just the button's frame
        addPlaylistButton.alpha = 1;
        
        tableScroll.scrollingPaused = YES;
    } else {
        tableScroll.scrollingPaused = NO;
        addPlaylistButton.alpha = 0.5;
        [tableScroll scrollingWithView:container];
    }
}

- (void)droppedSongOverWithGesture:(UILongPressGestureRecognizer *)gesture andContainer:(UIView *)container;{
    CGPoint touchPoint = [gesture locationInView:self.view];
    CGPoint touchPointTable = [gesture locationInView:playlistsTable];
    NSIndexPath *indexPath = [playlistsTable indexPathForRowAtPoint:touchPointTable];
    //if(indexPath == nil) indexPath = addToIndexPath;
    //Log(@"debug dropped song: %hhd %@ %f", (CGRectContainsPoint(playlistsTable.frame, touchPoint)), indexPath, touchPoint.y);
    //Log(@"playlist index: %ld", (long)addToIndexPath.row);
    if (CGRectContainsPoint(playlistsTable.frame, touchPoint) && indexPath) {
        //Log(@"dropped song");
        [self tableView:playlistsTable didSelectRowAtIndexPath:indexPath];
    } else if (touchPoint.y > addPlaylistButton.frame.origin.y) {
        //if we're under the table, we use all the space for the drop zone, not just the button's frame
        [self newPlaylistHit];
    } else {
        playlistsMode = playlistsViewControllerNormal;
        [self playlistsChanged];
        searchButton.hidden = NO;
        loginButton.hidden = NO;
        //[self setPlaylistsTableNormal];
        _addToSongs = nil;
        //Log(@"left panel height: %f", self.sidePanelController.leftPanel.view.frame.size.height);
    }
}

- (void) repositionBottomButtons {
    if (IS_DEVICE==IS_IPHONE) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
    addPlaylistButton.frame = CGRectMake(addPlaylistButton.frame.origin.x, addPlaylistButton.frame.origin.y - PLAYER_DRAWER_HEIGHT_X, addPlaylistButton.frame.size.width, addPlaylistButton.frame.size.height);
        }
        else
        {
    addPlaylistButton.frame = CGRectMake(addPlaylistButton.frame.origin.x, addPlaylistButton.frame.origin.y - PLAYER_DRAWER_HEIGHT, addPlaylistButton.frame.size.width, addPlaylistButton.frame.size.height);
        }
    }
    loginButton.frame = CGRectMake(loginButton.frame.origin.x, loginButton.frame.origin.y - PLAYER_DRAWER_HEIGHT, loginButton.frame.size.width, loginButton.frame.size.height);
    tableScroll.scroll.frame = CGRectMake(tableScroll.scroll.frame.origin.x, tableScroll.scroll.frame.origin.y, tableScroll.scroll.frame.size.width, tableScroll.scroll.frame.size.height - PLAYER_DRAWER_HEIGHT);
    
}
- (void) repositionNegativeBottomButtons {
    
    addPlaylistButton.frame = CGRectMake(addPlaylistButton.frame.origin.x, addPlaylistButton.frame.origin.y + PLAYER_DRAWER_HEIGHT, addPlaylistButton.frame.size.width, addPlaylistButton.frame.size.height);
    loginButton.frame = CGRectMake(loginButton.frame.origin.x, loginButton.frame.origin.y + PLAYER_DRAWER_HEIGHT, loginButton.frame.size.width, loginButton.frame.size.height);
    tableScroll.scroll.frame = CGRectMake(tableScroll.scroll.frame.origin.x, tableScroll.scroll.frame.origin.y, tableScroll.scroll.frame.size.width, tableScroll.scroll.frame.size.height + PLAYER_DRAWER_HEIGHT);
    
}

- (void) viewDidLayoutSubviews {
    
    if( IS_DEVICE == IS_IPHONE && ![Utils isVersionLessThan_8_0] ) {
        if (playlistsMode == playlistsViewControllerAddTo) {
            playlistsTable.frame = CGRectMake(playlistsTable.frame.origin.x, playlistsTable.frame.origin.y, playlistsTable.frame.size.width, [Utils currentHeight] - ( [Utils currentHeight] - addPlaylistButton.frame.origin.y ) );
        }else {
            playlistsTable.frame = CGRectMake(playlistsTable.frame.origin.x, playlistsTable.frame.origin.y, playlistsTable.frame.size.width, [Utils currentHeight] - 55.0f - ( [Utils currentHeight] - addPlaylistButton.frame.origin.y ) );
        }
    }else {
        playlistsTable.frame = CGRectMake(playlistsTable.frame.origin.x, playlistsTable.frame.origin.y, playlistsTable.frame.size.width, [Utils currentHeight] - 55.0f - ( [Utils currentHeight] - addPlaylistButton.frame.origin.y ) );
    }
    
}

@end

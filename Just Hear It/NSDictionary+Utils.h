//
// Created by andreis on 11/5/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@interface NSDictionary (Utils)

- (NSInteger) intValueForKey:(NSString *)key;
- (BOOL) boolValueForKey:(NSString *)key;

- (NSString *)stringValueForKey:(NSString *)key;

- (id) nullToNil:(id) object;

@end
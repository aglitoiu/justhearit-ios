//
// Created by andreis on 10/4/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface HandleView : UIView

@property (nonatomic, strong) UIView *handleNormalView;
@property (nonatomic, strong) UIView *handleDraggingView;
@property (nonatomic, assign) NSUInteger headerHeight;

- (void)initializeWithHeaderHeight:(NSUInteger)headerHeight;

- (void)setHandleProperties:(UIImageView *)imageView;

- (void)showNormalView;

- (void)showDraggingView;

- (void)fadeOut:(UIView *)view;

- (void)fadeIn:(UIView *)view;


@end
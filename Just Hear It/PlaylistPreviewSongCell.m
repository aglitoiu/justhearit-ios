//
//  PlaylistPreviewSongCell.m
//  Just Hear It
//
//  Created by andrei st on 12/20/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import "PlaylistPreviewSongCell.h"

@implementation PlaylistPreviewSongCell

- (void) awakeFromNib {
    CGFloat x = PLAYLISTS_PREVIEW_ICON_PADDING_LEFT*2 + PLAYLISTS_PREVIEW_ICON_WIDTH;

    _songNameLabel = [[UILabel alloc] init];
    
    CGFloat width;
    
    if(IS_DEVICE == IS_IPAD) {
        width = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - 50.0f;
    }else {
        width = 270.0f;
    }
    _songNameLabel.frame = CGRectMake(x, 0, width, PLAYLISTS_PREVIEW_SONG_CELL_HEIGHT);
    _songNameLabel.adjustsFontSizeToFitWidth = NO;
    _songNameLabel.font = [UIFont boldSystemFontOfSize:14];
    _songNameLabel.textColor = [UIColor whiteColor];
    _songNameLabel.alpha = 0.6;
    [self.contentView addSubview:_songNameLabel];
    [super awakeFromNib];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    CGFloat x = PLAYLISTS_PREVIEW_ICON_PADDING_LEFT*2 + PLAYLISTS_PREVIEW_ICON_WIDTH;
    
    CGFloat width;
    
    if(IS_DEVICE == IS_IPAD) {
        width = [Utils currentWidth] - LEFTPANEL_WIDTH_IPAD - 50.0f;
    }else {
        width = 270.0f;
    }
    
    _songNameLabel.frame = CGRectMake(x, 0, width, PLAYLISTS_PREVIEW_SONG_CELL_HEIGHT);
}

- (void)takeShotIn:(UIView *)snapshotImageView { }
@end

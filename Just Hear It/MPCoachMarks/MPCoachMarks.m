//
//  MPCoachMarks.m
//  Example
//
//  Created by marcelo.perretta@gmail.com on 7/8/15.
//  Copyright (c) 2015 MAWAPE. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MPCoachMarks.h"
#import "MainViewController.h"
#import "Bluuur.h"
#import "PlaylistsViewController.h"
static const CGFloat kAnimationDuration = 0.3f;
static const CGFloat kCutoutRadius = 5.0f;
static const CGFloat kMaxLblWidth = 230.0f;
static const CGFloat kLblSpacing = 35.0f;
static const CGFloat kLabelMargin = 5.0f;
static const CGFloat kMaskAlpha = 0.75f;
static const BOOL kEnableContinueLabel = NO;
static const BOOL kEnableSkipButton = NO;
NSString *const kSkipButtonText = @"Skip";
NSString *const kContinueLabelText = @"Tap to continue";
int iPLM=0;

@implementation MPCoachMarks {
    CAShapeLayer *mask;
    NSUInteger markIndex;
    UIView *currentView;
}

#pragma mark - Properties

@synthesize delegate;
@synthesize coachMarks;
@synthesize lblCaption;
@synthesize lblContinue;
@synthesize btnSkipCoach;
@synthesize maskColor = _maskColor;
@synthesize animationDuration;
@synthesize cutoutRadius;
@synthesize maxLblWidth;
@synthesize lblSpacing;
@synthesize enableContinueLabel;
@synthesize enableSkipButton;
@synthesize continueLabelText;
@synthesize skipButtonText;
@synthesize arrowImage;
@synthesize continueLocation;
@synthesize TUTnmbr;
#pragma mark - Methods

- (id)initWithFrame:(CGRect)frame coachMarks:(NSArray *)marks {
    self = [super initWithFrame:frame];
    if (self) {
        // Save the coach marks
        self.coachMarks = marks;
        
        // Setup
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Setup
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Setup
        [self setup];
    }
    return self;
}

- (void)setup {
    // Default
    self.animationDuration = kAnimationDuration;
    self.cutoutRadius = kCutoutRadius;
    self.maxLblWidth = kMaxLblWidth;
    self.lblSpacing = kLblSpacing;
    self.enableContinueLabel = kEnableContinueLabel;
    self.enableSkipButton = kEnableSkipButton;
    self.continueLabelText = kContinueLabelText;
    self.skipButtonText = kSkipButtonText;
    
    CALayer *blurLayer = [CALayer layer];
    
    CIFilter *blur = [CIFilter filterWithName:@"CIGaussianBlur"];
     blurLayer.backgroundFilters = [NSArray arrayWithObject:blur];
    [blur setDefaults];
    // Shape layer mask
    mask = [CAShapeLayer layer];
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
    NSArray *transparentRects = [[NSArray alloc] initWithObjects:[NSValue valueWithCGRect:CGRectMake(0, 0, 1000, 100)],[NSValue valueWithCGRect:CGRectMake(0, 200, 100, 200)], nil];
    //MLWBluuurView *beView = [[MLWBluuurView alloc] initWithFrame:CGRectMake(0,0,200,400) backgroundColor:[UIColor whiteColor] andTransparentRects:transparentRects];
    //UIView *plm = [[UIView alloc]initWithFrame:self.bounds];
    //plm.layer.mask = mask;
    //[plm addSubview:beView];
    //beView.blurRadius=50;
    //beView.frame = self.bounds;
   // [self addSubview:plm];
    

    [mask setFillRule:kCAFillRuleEvenOdd];
    [mask setFillColor:[[UIColor colorWithHue:0.0f saturation:0.0f brightness:0.0f alpha:kMaskAlpha] CGColor]];
    [self.layer addSublayer:mask];
    //[mask addSublayer:blurLayer];
    
    
    // Capture touches
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidTap:)];
    [self addGestureRecognizer:tapGestureRecognizer];
    
    UISwipeGestureRecognizer *swipeGestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(userDidSwipeRight:)];
    UISwipeGestureRecognizer *swipeGestureRecognizerUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(userDidSwipeUp:)];
    UISwipeGestureRecognizer *swipeGestureRecognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(userDidSwipeLeft:)];
    swipeGestureRecognizerRight.direction=UISwipeGestureRecognizerDirectionRight;
    swipeGestureRecognizerUp.direction=UISwipeGestureRecognizerDirectionUp;
    swipeGestureRecognizerLeft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:swipeGestureRecognizerRight];
     [self addGestureRecognizer:swipeGestureRecognizerUp];
     [self addGestureRecognizer:swipeGestureRecognizerLeft];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CoachPH10:)
                                                 name:@"CoachPH10"
                                               object:nil];
    if(IS_IPAD)
    {
        iPLM=1;
    }
    
    
    // Captions
    self.lblCaption = [[UILabel alloc] initWithFrame:(CGRect){{0.0f, 0.0f}, {self.maxLblWidth, 0.0f}}];
    self.lblCaption.backgroundColor = [UIColor clearColor];
    self.lblCaption.textColor = [UIColor whiteColor];
    self.lblCaption.font = [UIFont systemFontOfSize:16.0f];
    self.lblCaption.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblCaption.numberOfLines = 0;
    self.lblCaption.textAlignment = NSTextAlignmentCenter;
    self.lblCaption.alpha = 0.0f;
    [self addSubview:self.lblCaption];
    
    //Location Position
    self.continueLocation = LOCATION_BOTTOM;
    
    // Hide until unvoked
    self.hidden = YES;
}
- (void) CoachPH10:(NSNotification *) notification{
    [self goToCoachMarkIndexed:(markIndex+1)];
    iPLM++;
    [SETTINGS setTutorialDisabled:[NSNumber numberWithBool:YES]];
}

#pragma mark - Cutout modify

- (void)setCutoutToRect:(CGRect)rect withShape:(MaskShape)shape{
    // Define shape
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRect:self.bounds];
    UIBezierPath *cutoutPath;
    
    if (shape == SHAPE_CIRCLE)
        cutoutPath = [UIBezierPath bezierPathWithOvalInRect:rect];
    else if (shape == SHAPE_SQUARE)
        cutoutPath = [UIBezierPath bezierPathWithRect:rect];
    else
        cutoutPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cutoutRadius];
    
    
    [maskPath appendPath:cutoutPath];
    
    // Set the new path
    mask.path = maskPath.CGPath;
}

- (void)animateCutoutToRect:(CGRect)rect withShape:(MaskShape)shape{
    // Define shape
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRect:self.bounds];
    UIBezierPath *cutoutPath;
    
    if (shape == SHAPE_CIRCLE)
        cutoutPath = [UIBezierPath bezierPathWithOvalInRect:rect];
    else if (shape == SHAPE_SQUARE)
        cutoutPath = [UIBezierPath bezierPathWithRect:rect];
    else
        cutoutPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cutoutRadius];
    
    
    [maskPath appendPath:cutoutPath];
    
    // Animate it
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"path"];
    anim.delegate = self;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    anim.duration = self.animationDuration;
    anim.removedOnCompletion = NO;
    anim.fillMode = kCAFillModeForwards;
    anim.fromValue = (__bridge id)(mask.path);
    anim.toValue = (__bridge id)(maskPath.CGPath);
    [mask addAnimation:anim forKey:@"path"];
    mask.path = maskPath.CGPath;
}

#pragma mark - Mask color

- (void)setMaskColor:(UIColor *)maskColor {
    _maskColor = maskColor;
    [mask setFillColor:[maskColor CGColor]];
}

#pragma mark - Touch handler

- (void)userDidTap:(UITapGestureRecognizer *)recognizer {
    CGPoint tappedPoint = [recognizer locationInView:self];
    CGFloat xCoordinate = tappedPoint.x;
    CGFloat yCoordinate = tappedPoint.y;
    
    
    if(TUTnmbr==1)
    {
        if(iPLM==1)
        {
            if([Utils IS_IPHONE_X])
            {
                if(xCoordinate>=7&&xCoordinate<=47&&yCoordinate>=30&&yCoordinate<=70)
                {
                    [self goToCoachMarkIndexed:(markIndex+1)];
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"CoachPH2"
                     object:self];
                    iPLM++;
            }
            }
            else{
                if(xCoordinate>=5&&xCoordinate<=45&&yCoordinate>=5&&yCoordinate<=45)
                {
                    [self goToCoachMarkIndexed:(markIndex+1)];
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"CoachPH2"
                     object:self];
                    iPLM++;
            }
           
            }
        }
        else if(iPLM==2)
        {
            [self goToCoachMarkIndexed:(markIndex+1)];
            
            iPLM++;
        }
    }
    else if(TUTnmbr==2)
    {
    
    
    if(iPLM==1)
    {
     [self goToCoachMarkIndexed:(markIndex+1)];
        iPLM++;
    }
    
    else if(iPLM==2)
    {
        /* if(xCoordinate>=0&&xCoordinate<=300&&yCoordinate>=[Utils screenHeight]-40&&yCoordinate<=[Utils screenHeight])
         {
        [self goToCoachMarkIndexed:(markIndex+1)];
        
             //[[NSNotificationCenter defaultCenter]
         //postNotificationName:@"CoachPH3"
         //object:self];
             //PlaylistsViewController *pvc=[[PlaylistsViewController alloc] init];
            // [pvc newPlaylistHit];
        
        iPLM++;
         }*/
    }
    
    
    }
    else{
       [self goToCoachMarkIndexed:(markIndex+1)];
    }


    


}

- (void)userDidSwipeRight:(UISwipeGestureRecognizer *)recognizer {

    if(TUTnmbr==1&&iPLM==1)
    {
        [self goToCoachMarkIndexed:(markIndex+1)];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"CoachPH2"
         object:self];
        iPLM++;
    }

    
    //[self goToCoachMarkIndexed:self.coachMarks.count];
    
    
    
    
}
- (void)userDidSwipeLeft:(UISwipeGestureRecognizer *)recognizer {
    
    if(TUTnmbr==4&&iPLM==1)
    {
        [self goToCoachMarkIndexed:(markIndex+1)];
     
        iPLM++;
    }
    
    
   // [self goToCoachMarkIndexed:self.coachMarks.count];
    
    
    
    
}
-(void)userDidSwipeUp:(UISwipeGestureRecognizer *)recognizer{
    
    if(TUTnmbr==3&&iPLM==1)
    {
      
        [self goToCoachMarkIndexed:(markIndex+1)];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"CoachPH5"
         object:self];
        iPLM++;
    }
}

#pragma mark - Navigation

- (void)start {
    // Fade in self
    self.alpha = 0.0f;
    self.hidden = NO;
    [UIView animateWithDuration:self.animationDuration
                     animations:^{
                         self.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                         // Go to the first coach mark
                         [self goToCoachMarkIndexed:0];
                     }];
}

- (void)skipCoach {
    [self goToCoachMarkIndexed:self.coachMarks.count];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.delegate coachMarksViewDidClicked:self atIndex:markIndex];
    [self cleanup];
}

- (UIImage*)fetchImage:(NSString*)name {
    // Check for iOS 8
    if ([UIImage respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)]) {
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        return [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
    } else {
        return [UIImage imageNamed:name];
    }
}

- (void)goToCoachMarkIndexed:(NSUInteger)index {
    // Out of bounds
    if (index >= self.coachMarks.count) {
        
        [self cleanup];
        return;
    }
    
    // Current index
    markIndex = index;
    
    // Coach mark definition
    NSDictionary *markDef = [self.coachMarks objectAtIndex:index];
    NSString *markCaption = [markDef objectForKey:@"caption"];
    CGRect markRect = [[markDef objectForKey:@"rect"] CGRectValue];
    
    MaskShape shape = DEFAULT;
    if([[markDef allKeys] containsObject:@"shape"])
        shape = [[markDef objectForKey:@"shape"] integerValue];
    
    
    //Label Position
    LabelAligment labelAlignment = [[markDef objectForKey:@"alignment"] integerValue];
    LabelPosition labelPosition = [[markDef objectForKey:@"position"] integerValue];
    if([markDef objectForKey:@"cutoutRadius"]) {
        self.cutoutRadius = [[markDef objectForKey:@"cutoutRadius"] floatValue];
    } else {
        self.cutoutRadius = kCutoutRadius;
    }
    
    if ([self.delegate respondsToSelector:@selector(coachMarksViewDidClicked:atIndex:)]) {
        [currentView removeFromSuperview];
        currentView = [[UIView alloc] initWithFrame:markRect];
        currentView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(handleSingleTap:)];
        [currentView addGestureRecognizer:singleFingerTap];
        [self addSubview:currentView];
    }
    
    
    
    [self.arrowImage removeFromSuperview];
    BOOL showArrow = NO;
    if( [markDef objectForKey:@"showArrow"])
        showArrow = [[markDef objectForKey:@"showArrow"] boolValue];
    
    
    // Calculate the caption position and size
    self.lblCaption.alpha = 0.0f;
    self.lblCaption.frame = (CGRect){{0.0f, 0.0f}, {self.maxLblWidth, 0.0f}};
    self.lblCaption.text = markCaption;
    [self.lblCaption sizeToFit];
    CGFloat y;
    CGFloat x;
    
    
    //Label Aligment and Position
    switch (labelAlignment) {
        case LABEL_ALIGNMENT_RIGHT:
            x = floorf(self.bounds.size.width - self.lblCaption.frame.size.width - kLabelMargin);
            break;
        case LABEL_ALIGNMENT_LEFT:
            x = kLabelMargin;
            break;
        default:
            x = floorf((self.bounds.size.width - self.lblCaption.frame.size.width) / 2.0f);
            break;
    }
    
    switch (labelPosition) {
        case LABEL_POSITION_TOP:
        {
            y = markRect.origin.y - self.lblCaption.frame.size.height - kLabelMargin;
            if(showArrow) {
                if(TUTnmbr==4&&iPLM==1)
                {
                self.arrowImage = [[UIImageView alloc] initWithImage:[self fetchImage:@"arrow-swipe-left"]];
                }
                else if(TUTnmbr==3&&iPLM==1)
                {
                self.arrowImage = [[UIImageView alloc] initWithImage:[self fetchImage:@"arrow-swipe-up"]];
                }
                else{
                self.arrowImage = [[UIImageView alloc] initWithImage:[self fetchImage:@"arrow-down"]];
                }
                CGRect imageViewFrame = self.arrowImage.frame;
                imageViewFrame.origin.x = x;
                imageViewFrame.origin.y = y;
                if((TUTnmbr==4&&iPLM==1)||(TUTnmbr==3&&iPLM==1))
                {
                    if(TUTnmbr==3&&iPLM==1)
                    {
                        imageViewFrame.origin.y-=110;
                    }
                    

                    imageViewFrame.size.height=imageViewFrame.size.height/2;
                    imageViewFrame.size.width=imageViewFrame.size.width/2;
                }
                self.arrowImage.frame = imageViewFrame;
                y -= (self.arrowImage.frame.size.height + kLabelMargin);
                [self addSubview:self.arrowImage];
            }
        }
            break;
        case LABEL_POSITION_LEFT:
        {
            y = markRect.origin.y + markRect.size.height/2 - self.lblCaption.frame.size.height/2;
            x = self.bounds.size.width - self.lblCaption.frame.size.width - kLabelMargin - markRect.size.width;
            if(showArrow) {
                self.arrowImage = [[UIImageView alloc] initWithImage:[self fetchImage:@"arrow-right"]];
                CGRect imageViewFrame = self.arrowImage.frame;
                imageViewFrame.origin.x = self.bounds.size.width - self.arrowImage.frame.size.width - kLabelMargin - markRect.size.width;
                imageViewFrame.origin.y = y + self.lblCaption.frame.size.height/2 - imageViewFrame.size.height/2;
                self.arrowImage.frame = imageViewFrame;
                x -= (self.arrowImage.frame.size.width + kLabelMargin);
                [self addSubview:self.arrowImage];
            }
        }
            break;
        case LABEL_POSITION_RIGHT:
        {
            y = markRect.origin.y + markRect.size.height/2 - self.lblCaption.frame.size.height/2;
            x = markRect.origin.x + markRect.size.width + kLabelMargin;
            if(showArrow) {
                
            }
        }
            break;
        case LABEL_POSITION_RIGHT_BOTTOM:
        {
            y = markRect.origin.y + markRect.size.height + self.lblSpacing;
            CGFloat bottomY = y + self.lblCaption.frame.size.height + self.lblSpacing;
            if (bottomY > self.bounds.size.height) {
                y = markRect.origin.y - self.lblSpacing - self.lblCaption.frame.size.height;
            }
            x = markRect.origin.x + markRect.size.width + kLabelMargin;
            if(showArrow) {
                self.arrowImage = [[UIImageView alloc] initWithImage:[self fetchImage:@"arrow-top"]];
                CGRect imageViewFrame = self.arrowImage.frame;
                imageViewFrame.origin.x = x - markRect.size.width/2 - imageViewFrame.size.width/2;
                imageViewFrame.origin.y = y - kLabelMargin; //self.lblCaption.frame.size.height/2
                y += imageViewFrame.size.height/2;
                self.arrowImage.frame = imageViewFrame;
                [self addSubview:self.arrowImage];
            }
        }
            break;
        default: {
            y = markRect.origin.y + markRect.size.height + self.lblSpacing;
            CGFloat bottomY = y + self.lblCaption.frame.size.height + self.lblSpacing;
            if (bottomY > self.bounds.size.height) {
                y = markRect.origin.y - self.lblSpacing - self.lblCaption.frame.size.height;
            }
            if(showArrow) {
                if(TUTnmbr==1&&iPLM==1)
                {
                    
                    self.arrowImage = [[UIImageView alloc] initWithImage:[self fetchImage:@"arrow-swipe-right"]];
                }
                else{
                self.arrowImage = [[UIImageView alloc] initWithImage:[self fetchImage:@"arrow-top"]];
                }
                CGRect imageViewFrame = self.arrowImage.frame;
                
                imageViewFrame.origin.x = x;
                imageViewFrame.origin.y = y;
                if(TUTnmbr==1&&iPLM==1)
                {
                    imageViewFrame.size.height=imageViewFrame.size.height/2;
                    imageViewFrame.size.width=imageViewFrame.size.width/2;
                }
                self.arrowImage.frame = imageViewFrame;
                y += (self.arrowImage.frame.size.height + kLabelMargin);
                [self addSubview:self.arrowImage];
            }
        }
            break;
    }
    
    // Animate the caption label
    self.lblCaption.frame = (CGRect){{x, y}, self.lblCaption.frame.size};
    
    [UIView animateWithDuration:0.3f animations:^{
        self.lblCaption.alpha = 1.0f;
    }];
    
    // Delegate (coachMarksView:willNavigateTo:atIndex:)
    if ([self.delegate respondsToSelector:@selector(coachMarksView:willNavigateToIndex:)]) {
        [self.delegate coachMarksView:self willNavigateToIndex:markIndex];
    }
    
    // If first mark, set the cutout to the center of first mark
    if (markIndex == 0) {
        CGPoint center = CGPointMake(floorf(markRect.origin.x + (markRect.size.width / 2.0f)), floorf(markRect.origin.y + (markRect.size.height / 2.0f)));
        CGRect centerZero = (CGRect){center, CGSizeZero};
        [self setCutoutToRect:centerZero withShape:shape];
    }
    
    // Animate the cutout
    [self animateCutoutToRect:markRect withShape:shape];
    
    CGFloat lblContinueWidth = self.enableSkipButton ? (70.0/100.0) * self.bounds.size.width : self.bounds.size.width;
    CGFloat btnSkipWidth = self.bounds.size.width - lblContinueWidth;
    
    // Show continue lbl if first mark
    if (self.enableContinueLabel) {
        if (markIndex == 0) {
            lblContinue = [[UILabel alloc] initWithFrame:(CGRect){{0, [self yOriginForContinueLabel]}, {lblContinueWidth, 30.0f}}];
            lblContinue.font = [UIFont boldSystemFontOfSize:13.0f];
            lblContinue.textAlignment = NSTextAlignmentCenter;
            lblContinue.text = self.continueLabelText;
            lblContinue.alpha = 0.0f;
            lblContinue.backgroundColor = [UIColor whiteColor];
            [self addSubview:lblContinue];
            [UIView animateWithDuration:0.3f delay:1.0f options:0 animations:^{
                lblContinue.alpha = 1.0f;
            } completion:nil];
        } else if (markIndex > 0 && lblContinue != nil) {
            // Otherwise, remove the lbl
            [lblContinue removeFromSuperview];
            lblContinue = nil;
        }
    }
    
    if (self.enableSkipButton) {
        btnSkipCoach = [[UIButton alloc] initWithFrame:(CGRect){{lblContinueWidth, [self yOriginForContinueLabel]}, {btnSkipWidth, 30.0f}}];
        [btnSkipCoach addTarget:self action:@selector(skipCoach) forControlEvents:UIControlEventTouchUpInside];
        [btnSkipCoach setTitle:self.skipButtonText forState:UIControlStateNormal];
        btnSkipCoach.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        btnSkipCoach.alpha = 0.0f;
        btnSkipCoach.tintColor = [UIColor whiteColor];
        [self addSubview:btnSkipCoach];
        [UIView animateWithDuration:0.3f delay:1.0f options:0 animations:^{
            btnSkipCoach.alpha = 1.0f;
        } completion:nil];
    }
}

- (CGFloat)yOriginForContinueLabel {
    switch (self.continueLocation) {
        case LOCATION_TOP:
            return 20.0f;
        case LOCATION_CENTER:
            return self.bounds.size.height / 2 - 15.0f;
        default:
            return self.bounds.size.height - 30.0f;
    }
}

#pragma mark - Cleanup

- (void)cleanup {
    // Delegate (coachMarksViewWillCleanup:)
    if ([self.delegate respondsToSelector:@selector(coachMarksViewWillCleanup:)]) {
        [self.delegate coachMarksViewWillCleanup:self];
    }
    
    // Fade out self
    [UIView animateWithDuration:self.animationDuration
                     animations:^{
                         self.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         // Remove self
                         [self removeFromSuperview];
                         
                         // Delegate (coachMarksViewDidCleanup:)
                         if ([self.delegate respondsToSelector:@selector(coachMarksViewDidCleanup:)]) {
                             [self.delegate coachMarksViewDidCleanup:self];
                         }
                     }];
}

#pragma mark - Animation delegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    // Delegate (coachMarksView:didNavigateTo:atIndex:)
    if ([self.delegate respondsToSelector:@selector(coachMarksView:didNavigateToIndex:)]) {
        [self.delegate coachMarksView:self didNavigateToIndex:markIndex];
    }
}


@end


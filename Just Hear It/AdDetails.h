#import <Foundation/Foundation.h>

enum AdDetailsType {
    JusthearitAdTypeNormal = 0,
    JusthearitAdTypePreroll = 1
};

@interface AdDetails : NSObject

@property(nonatomic, assign) NSInteger adType;
@property(nonatomic, assign) NSInteger seconds;

+ (AdDetails *) prerollAdWithSeconds:(NSInteger)seconds;
+ (AdDetails *) normalAdWithSeconds:(NSInteger)seconds;
+ (AdDetails *) adWithType:(NSInteger)adType andSeconds:(NSInteger)seconds;

@end
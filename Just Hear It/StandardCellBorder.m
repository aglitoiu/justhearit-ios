//
// Created by andreis on 10/26/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "StandardCellBorder.h"


@implementation StandardCellBorder

@synthesize selectedView, borderBottom, borderLeft;

- (void) initWithParent:(UIView *)parent {
    selectedView = [[UIImageView alloc] initWithImage:[Utils imageFromColor:RGBA(0, 0, 0, 0.4)]];
    CGRect f = CGRectMake(0, 0, parent.frame.size.width, parent.frame.size.height-1);
    selectedView.frame = f;
    selectedView.autoresizingMask = UIViewAutoresizingAll;
    [parent addSubview:selectedView];
    [parent sendSubviewToBack:selectedView];

    //Log(@"border bottom width: %f", parent.frame.size.width);
    //border bottom
    borderBottom = [[UIImageView alloc] initWithImage:[Utils imageFromColor:RGBA(255,255,255,.1)]];
    borderBottom.frame = CGRectMake(0, parent.frame.size.height-1, parent.frame.size.width, 1);
    borderBottom.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    [parent addSubview:borderBottom];

    //border left
    borderLeft = [[UIImageView alloc] initWithImage:[Utils imageFromColor:HexColor(0x10abfd)]];
    borderLeft.frame = CGRectMake(0, 0, 6, parent.frame.size.height-1);
    borderLeft.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight;
    [parent addSubview:borderLeft];
    borderLeft.alpha = 0;
}

- (void) markSelected:(BOOL)state {
    selectedView.alpha = state;
}

- (void) markPlaying:(BOOL)state {
    borderLeft.alpha = state;
}

@end
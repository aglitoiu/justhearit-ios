
#import <Foundation/Foundation.h>

@interface Scrollable : NSObject

@property (nonatomic, assign) NSInteger autoscrollDistance;
@property (nonatomic, strong) NSTimer *autoscrollTimer;
@property (nonatomic, assign) NSInteger autoscrollThreshold;
@property (nonatomic, strong) UIScrollView *scroll;
@property (nonatomic, assign) BOOL scrollingPaused;

- (void)scrollTimerFired:(NSTimer *)timer;

- (void)getAutoscrollDistance;

- (void)stopAutoscroll;

- (void) scrollBeganWithView:(UIView *)view;

- (float)autoscrollDistanceWithProximity:(float)proximity;

- (void)autoscrollForImageView:(UIView *)view atLocation:(CGPoint)location;

- (void)scrollingEnded;

- (void)scrollingWithView:(UIView *)view;


@end
#import "Player.h"
#import "PlaylistSong.h"
#import "Playlist.h"
#import "AudioPlayer.h"

@implementation Player

@synthesize playingQueue, shuffledQueue, queue, shuffleMode, repeatMode, nextSongsInPlaylist, upNextQueue, isinUpNext;
@synthesize currentPlayingSong = _currentPlayingSong;
@synthesize currentPlayingPlaylist = _currentPlayingPlaylist;
@synthesize queueDelegate;

+ (id)sharedInstance {
    static Player *__sharedInstance;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        __sharedInstance = [[Player alloc] init];
    });

    return __sharedInstance;
}

- (id) init {
    if (self=[super init]) {
        queue = [[NSMutableArray alloc] init];
        shuffledQueue = [[NSMutableArray alloc] init];
        playingQueue = queue;
        shuffleMode = ShuffleModeOff;
        repeatMode = RepeatModeOff;
        nextSongsInPlaylist = [[NSMutableArray alloc] init];
        upNextQueue = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)setCurrentPlayingSong:(PlaylistSong *)song {
    if (_currentPlayingSong) {
        _currentPlayingSong.playState = playStateStopped;
    }
    _currentPlayingSong = song;
    _currentPlayingSong.playState = playStatePlaying;
    _currentPlayingSong.isNewPlaylistSong = @(NO);
    [self refreshNextSongsInPlaylist];
}

- (void)setCurrentPlayingPlaylist:(Playlist *)playlist {
    if (_currentPlayingPlaylist) {
        _currentPlayingPlaylist.playState = playStateStopped;
    }
    
    _currentPlayingPlaylist = playlist;
    _currentPlayingPlaylist.playState = playStatePlaying;
}

- (void) playSong:(PlaylistSong *)song fromPlaylist:(Playlist *)playlist {
    //NSString *plm=[NSString stringWithFormat:@"%@", SETTINGS.firstPlay];
    //alert(@"OKL");
   
    Log(@"current playing songs: %@", playlist.songs);
    
    if (playlist) {
        [upNextQueue removeAllObjects];
        queue = playlist.songs;
        
        if (shuffleMode == ShuffleModeOff) {
            playingQueue = queue;
        } else {
            playingQueue = shuffledQueue;
        }

        self.currentPlayingPlaylist = playlist;
    }
    
    self.currentPlayingSong = song;

    if (shuffleMode == ShuffleModeOn) {
        [self reloadShuffledQueue];
    }

    [[AudioPlayer sharedInstance] playSong:song withUserAction:YES];
}

- (void) reloadShuffledQueue {
    [shuffledQueue removeAllObjects];
    [shuffledQueue addObjectsFromArray:_.shuffle(queue)];
    [shuffledQueue removeObject:_currentPlayingSong];
    [shuffledQueue insertObject:_currentPlayingSong atIndex:0];
}

- (PlaylistSong *) nextSong {
    PlaylistSong *currentSong = nil;
   

    if (_playingSongBeforeQueue) {
   
        if (upNextQueue.count == 0) {
                       currentSong = _playingSongBeforeQueue;
            _playingSongBeforeQueue = nil;
                    }
    }

    if (upNextQueue.count > 0) {
       
        self.currentPlayingSong = upNextQueue.firstObject;
        return _currentPlayingSong;
        }
    

    if (!currentSong)
    {
                currentSong = _currentPlayingSong;}

    NSInteger index = [playingQueue indexOfObject:currentSong] + 1;

    if (repeatMode == RepeatModeOff) {
        if (playingQueue.count > 0 && index < playingQueue.count && index >= 0) {
                                  self.currentPlayingSong = [playingQueue objectAtIndex:index];
            return _currentPlayingSong;
        }
    } else if (repeatMode == RepeatModeAll) {
        if (playingQueue.count > 0 && index >= 0) {
            if (index < playingQueue.count) {
                self.currentPlayingSong = [playingQueue objectAtIndex:index];
            } else {
                self.currentPlayingSong = [playingQueue objectAtIndex:0];
            }
            return _currentPlayingSong;
        }
    } else if (repeatMode == RepeatModeOne) {
        return _currentPlayingSong;
    }


    return nil;
}

- (PlaylistSong *) prevSong {
    NSInteger index = [playingQueue indexOfObject:_currentPlayingSong] - 1;

    if (playingQueue.count > 0 && index < playingQueue.count && index >= 0) {
        self.currentPlayingSong = [playingQueue objectAtIndex:index];
        return _currentPlayingSong;
    }

    return nil;
}

- (BOOL) hasNextSong {
   
    if(upNextQueue.count==1)
    {
        isinUpNext=YES;
    }
    
   if (upNextQueue.count > 0) {
     
    return YES;
        
    }
    NSUInteger index;
    if(isinUpNext==YES)
    {
        index=0;
    }
    else
    {
        index = [playingQueue indexOfObject:_currentPlayingSong];
    }
    
       if (playingQueue.count > 0) {
        if (repeatMode == RepeatModeAll || repeatMode == RepeatModeOne) {
        
            return YES;
           
        } else {
            
                       return index < (playingQueue.count - 1) ;
        }
    }
      return NO;
  
}

- (BOOL) hasPrevSong {
    NSUInteger index = [playingQueue indexOfObject:_currentPlayingSong];

    if (playingQueue.count > 0) {
        if (repeatMode == RepeatModeAll || repeatMode == RepeatModeOne) {
            return YES;
        } else {
            return index > 0;
        }
    }
    return NO;
}


- (NSInteger) toggleShuffle {
    if (shuffleMode == ShuffleModeOff) {
        shuffleMode = ShuffleModeOn;
        [self reloadShuffledQueue];
        playingQueue = shuffledQueue;
    } else {
        shuffleMode = ShuffleModeOff;
        playingQueue = queue;
    }

    [self refreshNextSongsInPlaylist];
    return shuffleMode;
}

- (NSInteger) toggleRepeat {
    if (repeatMode == RepeatModeOff)
        repeatMode = RepeatModeAll;
    else if (repeatMode == RepeatModeAll)
        repeatMode = RepeatModeOne;
    else
        repeatMode = RepeatModeOff;

    [self refreshNextSongsInPlaylist];
    return repeatMode;
}

- (void) refreshNextSongsInPlaylist {
    if ([upNextQueue containsObject:_currentPlayingSong]) {
        [upNextQueue removeObject:_currentPlayingSong];
    } else {
        [nextSongsInPlaylist removeAllObjects];
        [nextSongsInPlaylist addObjectsFromArray:[self getNextSongsInPlaylist]];
    }
    
    

    if ([queueDelegate respondsToSelector:@selector(nextPlaylistSongsChanged:)]) {
        [queueDelegate nextPlaylistSongsChanged:nextSongsInPlaylist];
    }
}

- (NSArray *) getNextSongsInPlaylist {
    if (repeatMode == RepeatModeOne) {
        return @[_currentPlayingSong];
    } else if (repeatMode == RepeatModeOff) {
        return [self nextPlaylistSongsAfterSong:_currentPlayingSong];
    } else if (repeatMode == RepeatModeAll) {
        if (playingQueue.count > 0) {
            NSMutableArray *nextSongs = [[NSMutableArray alloc] init];
            [nextSongs addObjectsFromArray:[self nextPlaylistSongsAfterSong:_currentPlayingSong]];
            while(nextSongs.count < 50) {
                [nextSongs addObjectsFromArray:playingQueue];
            }
            return nextSongs;
        }
    }
    return @[];
}

- (NSArray *) nextPlaylistSongsAfterSong:(PlaylistSong *) song {
    NSInteger index = [playingQueue indexOfObject:song];
    if (playingQueue.count > 0 && index >= 0) {
        if (index + 1 < playingQueue.count) {
            return _.tail(playingQueue, playingQueue.count-index-1);
        }
    }
    return @[];
}

- (void)stopMusic {
    [queue removeAllObjects];
    [shuffledQueue removeAllObjects];
    [nextSongsInPlaylist removeAllObjects];
    [upNextQueue removeAllObjects];
}

- (BOOL)hasUpNextSongs {
    return upNextQueue.count > 0;
}

- (void)addUpNextSong:(PlaylistSong *)playlistSong {
    if (![upNextQueue containsObject:playlistSong]) {
        if (!_playingSongBeforeQueue) _playingSongBeforeQueue = _currentPlayingSong;
        [upNextQueue addObject:playlistSong];
        [[Player sharedInstance] refreshNextSongsInPlaylist];
    }
}
- (void)addQueueSong:(PlaylistSong *)playlistSong {
    if (![nextSongsInPlaylist containsObject:playlistSong]) {
        //if (!_playingSongBeforeQueue) _playingSongBeforeQueue = _currentPlayingSong;
        [nextSongsInPlaylist addObject:playlistSong];
        [[Player sharedInstance] refreshNextSongsInPlaylist];
    }
}
@end

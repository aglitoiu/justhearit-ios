
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import <FBSDKCoreKit/FBSDKAccessToken.h>

#import "RKLog.h"
#import "UIDevice+IdentifierAddition.h"
#import "AppDelegate.h"
#import "NWReachabilityManger.h"
//#import "TestFlight.h"
#import "MagicalRecord.h"
#import "MagicalRecord+Setup.h"
#import "NSTimer+Blocks.h"
#import "JusthearitClient.h"
#import "FayeManager.h"
#import "CachedPlaylist.h"
#import "CachedLocalPlaylist.h"
#import "CachedPlaylistSong.h"
#import "CachedPlaylist+Utils.h"
#import "CachedLocalPlaylist+Utils.h"
#import "SongCacheDownloadManager.h"
#import "GAI.h"
#import "Playlists.h"
#import "MasterViewController.h"


@implementation AppDelegate
BOOL alreadyOnline=YES;
NSString *const FBSessionStateChangedNotification = @"com.justhearit.Just-Hear-It:FBSessionStateChangedNotification";

@synthesize reachabilityTimer, online, applicationShouldLoadResourceAfterLoadingPlaylists;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
#ifdef DEBUG
    //RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    //RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
#else
    
#ifdef TEST_MODE
    //[TestFlight takeOff:@"9e20a23e-01ff-4d1d-8090-d5216adde712"];
    //    [TestFlight takeOff:@"367d59d8-3185-412d-9727-5eb6747aed47"];
#endif
    
#endif
    Log(@"appdelegate didfinishlaunching");
    
    // Instantiate Shared Manager
    [NWReachabilityManger sharedManager];
    [MainViewController sharedInstance];
    
    online = YES;
    
    //we use this in the masterVC to load the url after playlists load, but only when we first launch the app
    //at a second launch, didfinish wont get called, and the url will be taken over by openUrl...
    //in masterVC this should be made nil after its used
    applicationShouldLoadResourceAfterLoadingPlaylists = nil;
    

    
    NSURL *url = (NSURL *)[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey];
    
    if (url) {
        applicationShouldLoadResourceAfterLoadingPlaylists = url.relativePath;
    }
    
    [[SongCacheDownloadManager sharedInstance] resumeDownloads];
    
    [FBSDKSettings setAppID:FB_APP_ID];
    //    [FBSettings setDefaultAppID:FB_APP_ID];
    
    SETTINGS.udid = UDID;
    [SETTINGS setDefaultMaxCacheGb];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged) name:Ev.reachabilityChanged object:nil];
    
    [[FayeManager sharedInstance] verifySessionAndConnect];
    
#ifndef DEBUG
#ifndef TEST_MODE
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    // [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-40464219-2"];
    
#endif
#endif
    
    ACAccountStore *accountStore;
    ACAccountType *accountTypeFB;
    if ((accountStore = [[ACAccountStore alloc] init]) &&
        (accountTypeFB = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook] ) ){
        
        NSArray *fbAccounts = [accountStore accountsWithAccountType:accountTypeFB];
        id account;
        if (fbAccounts && [fbAccounts count] > 0 &&
            (account = [fbAccounts objectAtIndex:0])){
            
            [accountStore renewCredentialsForAccount:account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
                //we don't actually need to inspect renewResult or error.
                if (error){
                    
                }
            }];
        }
    }
    
    UIStoryboard *storyboard;
    if(IS_DEVICE == IS_IPAD) {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    }else {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    
    MasterViewController *masterViewController = [storyboard instantiateInitialViewController];
    
    if( ( IS_DEVICE == IS_IPAD && [Utils isVersionLessThan_8_0] ) || IS_DEVICE != IS_IPAD) {
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController = masterViewController;
        [self.window makeKeyAndVisible];
    }
    if(!SETTINGS.tutorialDisabled)
    {
    if(IS_DEVICE==IS_IPHONE){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"startTUT"
         object:self];
    }
    else{
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"CoachPH2"
         object:self];
       
    }
    }
    //if (!SETTINGS.tutorialDisabled.boolValue) [masterViewController performSegueWithIdentifier:@"tourSegue" sender:nil];
        //[masterViewController performSegueWithIdentifier:@"tourSegue" sender:nil];
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}

- (void) reachabilityChanged {
    
    Log(@"reachability has changed");


    //BOOL currentOnline = [Utils isReachable];
    BOOL currentOnline = [NWReachabilityManger isReachable];
    
  
    Log(@"online or not? %hhd", currentOnline);
    
    if (alreadyOnline==YES&&currentOnline==NO&&SETTINGS.OfflineOnly==NO) {
 
  

      [[NSNotificationCenter defaultCenter] postNotificationName:Ev.gotOffline object:nil userInfo:nil];
    }
    else if(alreadyOnline==NO&&currentOnline==YES&&SETTINGS.OfflineOnly==NO)
    {

        [[NSNotificationCenter defaultCenter] postNotificationName:Ev.gotOnline object:nil userInfo:nil];
    }
    alreadyOnline=currentOnline;
    
    
}

- (void) performReachabilityChangeActions {
   // [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playlistsShouldReload object:nil userInfo:nil];
    //[[NSNotificationCenter defaultCenter] postNotificationName:Ev.navigationBarShouldReload object:nil userInfo:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    //Log(@"check reachable");
    //BOOL currentOnline = [Utils isReachable];
   /* BOOL currentOnline = [NWReachabilityManger isReachable];
    if (online != currentOnline) {
        online = currentOnline;
        [self performReachabilityChangeActions];
    }*/
    
    [[FayeManager sharedInstance] verifySessionAndConnect];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    Log(@"got active");
    
    //BOOL currentOnline = [Utils isReachable];
    
    /*BOOL currentOnline = [NWReachabilityManger isReachable];
    
    if (online != currentOnline) {
        online = currentOnline;
        [self performReachabilityChangeActions];
    }*/
    
    [[FayeManager sharedInstance] verifySessionAndConnect];
    
    [FBSDKAppEvents activateApp];
    //    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //    [FBSession.activeSession close];
    [MagicalRecord cleanUp];
    [FayeManager.sharedInstance disconnect];
}

//- (void)sessionStateChanged:(FBSession *)session
//                      state:(FBSessionState) state
//                      error:(NSError *)error
//{
//    switch (state) {
//        case FBSessionStateOpen:
//            if (!error) {
//                // We have a valid session
//                Log(@"User session found");
//            }
//            break;
//        case FBSessionStateClosed:
//        case FBSessionStateClosedLoginFailed:
//            [FBSession.activeSession closeAndClearTokenInformation];
//            break;
//        default:
//            break;
//    }

//    [[NSNotificationCenter defaultCenter]
//            postNotificationName:FBSessionStateChangedNotification
//                          object:session];
//
//    if (error) {
//        if ([FBErrorUtility shouldNotifyUserForError:error]) {
//            NSString *errorMessage = [FBErrorUtility userMessageForError:error];
//
//            UIAlertView *alertView = [[UIAlertView alloc]
//                    initWithTitle:@"Error"
//                          message:errorMessage
//                         delegate:nil
//                cancelButtonTitle:@"OK"
//                otherButtonTitles:nil];
//            [alertView show];
//        }
//    }
//}

//- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
//    //    [FBSession.activeSession initWithAppID:FB_APP_ID permissions:FB_PERMISSIONS urlSchemeSuffix:FB_APP_ID_URL tokenCacheStrategy:nil];
//
//
//
//    return [FBSession openActiveSessionWithReadPermissions:FB_PERMISSIONS
//                                              allowLoginUI:allowLoginUI
//                                         completionHandler:^(FBSession *session,
//                                                 FBSessionState state,
//                                                 NSError *error) {
//                                             [self sessionStateChanged:session
//                                                                 state:state
//                                                                 error:error];
//                                         }];
//}

/*
 * If we have a valid session at the time of openURL call, we handle
 * Facebook transitions by passing the url argument to handleOpenURL
 */
//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
//    Log(@"applicationShouldLoadResourceAfterLoadingPlaylists: %@", applicationShouldLoadResourceAfterLoadingPlaylists);
//    if (!applicationShouldLoadResourceAfterLoadingPlaylists) {
//        //means we already had the app launched and we can inject the resource in the playlists
//        Log(@"handle open url");
//        if ([[Playlists sharedInstance] handleOpenUrl:url.relativePath]) {
//            return YES;
//        }
//    }
//
//    // attempt to extract a token from the url
//    return [FBSession.activeSession handleOpenURL:url];
//}
//
- (void) closeSession
{
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    
    //    [FBSession.activeSession closeAndClearTokenInformation];
}
//
//- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
//    return [FBSession.activeSession handleOpenURL:url];
//}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    Log(@"applicationShouldLoadResourceAfterLoadingPlaylists: %@", applicationShouldLoadResourceAfterLoadingPlaylists);
    if (!applicationShouldLoadResourceAfterLoadingPlaylists) {
        //means we already had the app launched and we can inject the resource in the playlists
        Log(@"handle open url");
        if ([[Playlists sharedInstance] handleOpenUrl:url.relativePath]) {
            return YES;
        }
    }
    
    // attempt to extract a token from the url
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation ];
    // Add any custom logic here.
    return handled;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    Log(@"applicationShouldLoadResourceAfterLoadingPlaylists: %@", applicationShouldLoadResourceAfterLoadingPlaylists);
    if (!applicationShouldLoadResourceAfterLoadingPlaylists) {
        //means we already had the app launched and we can inject the resource in the playlists
        Log(@"handle open url");
        if ([[Playlists sharedInstance] handleOpenUrl:url.relativePath]) {
            return YES;
        }
    }
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                          openURL:url
                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                       annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

@end

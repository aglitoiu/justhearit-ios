#import <Foundation/Foundation.h>

@interface Autocomplete : NSObject

@property(nonatomic, strong) NSString *query;
@property(nonatomic, strong) NSMutableArray *suggestions;

@end
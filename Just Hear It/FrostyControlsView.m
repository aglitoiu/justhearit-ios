#import "FrostyControlsView.h"
#import "NSArray+OCTotallyLazy.h"
#import "PlaylistSong.h"
#import "CachedPlaylistSong.h"
#import "MainViewController.h"
#import "MasterViewController.h"

@implementation FrostyControlsView
int downed;
@synthesize songMetadataView, songActionsView, playButton, prevButton, nextButton, shuffleButton, repeatButton, scrubBarView, artistNameMarquee, songNameMarquee, scrubbing;
@synthesize shareButton, addToButton, discoverButton, buttonsContainer, dropToPlayNext;

enum SongActionButtonTags {
    songActionButtonAddToTag = 1,
    songActionButtonDiscoverTag = 2,
    songActionButtonShareTag = 3
};

- (void) setup {
    
    NSLog(@"<<<<<< SETUP >>>>>>");
    if (IS_DEVICE==IS_IPHONE) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
            self.frame = CGRectMake(0, 0, [Utils currentWidth], FROSTY_PLAYER_HEIGHT_X);
        }
        else{
            self.frame = CGRectMake(0, 0, [Utils currentWidth], FROSTY_PLAYER_HEIGHT);
        }
    }
    else{
        self.frame = CGRectMake(0, 0, [Utils currentWidth], FROSTY_PLAYER_HEIGHT);
    }
        
    
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"drawer_handle.png"]];
    backgroundImage.tag = 111;
    backgroundImage.frame = self.frame;
    
    scrubbing = NO;
    
    [self addSubview:backgroundImage];
    [self setupButtons];
    [self setupScrubBar];
    [self setupMetadata];
    [self setupSongActions];
    [self setupDropToPlayNext];
    [self resetInterfaceWithPlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSongStartedBuffering:) name:Ev.songStartedBuffering object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSongStartedPlay:) name:Ev.songStartedPlay object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(songEnded:) name:Ev.songPaused object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSongResumed:) name:Ev.songResumed object:nil];
}

- (void)setupDropToPlayNext {
    
    CGFloat width;
    if(IS_DEVICE == IS_IPAD) {
        width = [Utils currentWidth];
    }else {
        width = [Utils screenWidth];
    }
    
    dropToPlayNext = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 64)];
    UILabel *dropToPlayNextLabel = [[UILabel alloc] initWithFrame:dropToPlayNext.frame];
    dropToPlayNextLabel.text = @"Drop here to play next";
    //dropToPlayNextLabel.text = @"Drop here to queue";
    dropToPlayNextLabel.textAlignment = NSTextAlignmentCenter;
    dropToPlayNextLabel.font = [UIFont boldSystemFontOfSize:16];
    dropToPlayNextLabel.textColor = [UIColor whiteColor];
    dropToPlayNextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
    
    UIView *dashed = [[UIView alloc] initWithFrame:CGRectMake(5, 5, width-10, 64-10)];
    
    dashed.backgroundColor = [UIColor clearColor];
    CAShapeLayer *border = [CAShapeLayer layer];
    border.strokeColor = RGBA(255,255,255,1).CGColor;
    border.fillColor = nil;
    border.lineWidth = 1.5f;
    border.lineDashPattern = @[@4, @2];
    border.path = [UIBezierPath bezierPathWithRoundedRect:dashed.bounds cornerRadius:5.f].CGPath;
    dashed.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
    [dashed.layer addSublayer:border];
    
    
    [dropToPlayNext addSubview:dashed];
    
    [dropToPlayNext addSubview:dropToPlayNextLabel];
    dropToPlayNext.alpha = 0;
    
    [self addSubview:dropToPlayNext];
}

- (void) setupSongActions {
    
    songActionsView = [[UIView alloc] init];
    
    CGRect frame = songMetadataView.frame;
    frame.origin.y += songMetadataView.frame.size.height + 15;
    frame.size.height = 45;
    
    frame.origin.x = ( [Utils currentWidth] - 320 ) / 2 - 7.0f;
    
    songActionsView.frame = frame;
    
    shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setImage:[UIImage imageNamed:@"icon_share.png"] forState:UIControlStateNormal];
    [shareButton setTitle:@"Share" forState:UIControlStateNormal];
    shareButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [shareButton setTitleColor:RGBA(255,255,255,0.6) forState:UIControlStateNormal];
    shareButton.frame = CGRectMake(0, 0, 320/3, 40);
    shareButton.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
    shareButton.tag = songActionButtonShareTag;
    
    addToButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addToButton setImage:[UIImage imageNamed:@"icon_normal_playlist.png"] forState:UIControlStateNormal];
    [addToButton setTitle:@"Add To..." forState:UIControlStateNormal];
    addToButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [addToButton setTitleColor:RGBA(255,255,255,0.6) forState:UIControlStateNormal];
    addToButton.frame = CGRectMake(320/3, 0, 320/3, 40);
    addToButton.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
    addToButton.tag = songActionButtonAddToTag;
    
    discoverButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [discoverButton setImage:[UIImage imageNamed:@"icon_playlists_with.png"] forState:UIControlStateNormal];
    [discoverButton setTitle:@"Discover" forState:UIControlStateNormal];
    discoverButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [discoverButton setTitleColor:RGBA(255,255,255,0.6) forState:UIControlStateNormal];
    discoverButton.frame = CGRectMake(320*2/3, 0, 320/3, 40);
    discoverButton.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
    discoverButton.tag = songActionButtonDiscoverTag;
    
    [songActionsView addSubview:addToButton];
    [songActionsView addSubview:shareButton];
    [songActionsView addSubview:discoverButton];
    
    [addToButton addTarget:self action:@selector(songActionButtonHit:) forControlEvents:UIControlEventTouchUpInside];
    [discoverButton addTarget:self action:@selector(songActionButtonHit:) forControlEvents:UIControlEventTouchUpInside];
    [shareButton addTarget:self action:@selector(songActionButtonHit:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:songActionsView];
}

- (void) setupMetadata {
    songMetadataView = [[UIView alloc] init];
    
    CGRect frame = scrubBarView.frame;
    frame.origin.y += scrubBarView.frame.size.height;
    frame.size.height = 35;
    songMetadataView.frame = frame;
    
    songNameMarquee = [[MarqueeLabel alloc] initWithFrame:CGRectMake(15, 0, self.frame.size.width-(2*15), 20) rate:30.0f andFadeLength:10.0f];
    songNameMarquee.tag = 102;
    songNameMarquee.marqueeType = MLLeftRight;
    songNameMarquee.numberOfLines = 1;
    songNameMarquee.opaque = NO;
    songNameMarquee.enabled = YES;
    songNameMarquee.shadowOffset = CGSizeMake(0.0, -1.0);
    songNameMarquee.font = [UIFont boldSystemFontOfSize:16];
    songNameMarquee.textAlignment = NSTextAlignmentCenter;
    songNameMarquee.textColor = RGB(255, 255, 255);
    songNameMarquee.backgroundColor = [UIColor clearColor];
    songNameMarquee.text = @"";
    songNameMarquee.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    
    artistNameMarquee = [[MarqueeLabel alloc] initWithFrame:CGRectMake(15, 25, self.frame.size.width-(2*15), 15) rate:30.0f andFadeLength:10.0f];
    artistNameMarquee.tag = 103;
    artistNameMarquee.marqueeType = MLLeftRight;
    artistNameMarquee.numberOfLines = 1;
    artistNameMarquee.opaque = NO;
    artistNameMarquee.enabled = YES;
    artistNameMarquee.shadowOffset = CGSizeMake(0.0, -1.0);
    artistNameMarquee.font = [UIFont boldSystemFontOfSize:14];
    artistNameMarquee.textAlignment = NSTextAlignmentCenter;
    artistNameMarquee.textColor = RGBA(255, 255, 255, .6);
    artistNameMarquee.backgroundColor = [UIColor clearColor];
    artistNameMarquee.text = @"";
    artistNameMarquee.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    
    [songMetadataView addSubview:songNameMarquee];
    [songMetadataView addSubview:artistNameMarquee];
    
    [self addSubview:songMetadataView];
}

- (void) setupScrubBar {
    scrubBarView = [[ScrubBarView alloc] init];
    if (IS_DEVICE==IS_IPHONE) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f){
     scrubBarView.frame = CGRectMake(0, PLAYER_DRAWER_HEIGHT_X + 10, [Utils currentWidth], SCRUB_BAR_HEIGHT);
        }
        else{
      scrubBarView.frame = CGRectMake(0, PLAYER_DRAWER_HEIGHT + 10, [Utils currentWidth], SCRUB_BAR_HEIGHT);
        }
    }
    else{
     scrubBarView.frame = CGRectMake(0, PLAYER_DRAWER_HEIGHT + 10, [Utils currentWidth], SCRUB_BAR_HEIGHT);
    }
    [self addSubview:scrubBarView];
   
   
    
    [scrubBarView setup];
    scrubBarView.delegate = self;
    
}

- (void) setupButtons {
    playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    prevButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shuffleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    repeatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    NSArray *buttonList = @[repeatButton, prevButton, playButton, nextButton, shuffleButton];
    
    CGFloat butW = 64.0f;
    
    if(IS_DEVICE == IS_IPAD) {
        buttonsContainer = [[UIView alloc] initWithFrame:CGRectMake( ([Utils currentWidth] - [buttonList count] * butW ) / 2, 0, [buttonList count] * butW, 64)];
    }else {
        buttonsContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, butW*5, 64)];
        buttonsContainer.center=CGPointMake([Utils screenWidth]/2 , buttonsContainer.center.y);

    }
    
    _.arrayEach(buttonList, ^(UIButton *but){
        [buttonsContainer addSubview:but];
        but.contentMode = UIViewContentModeCenter;
    });
    
    [self setButtonFrames:buttonList width:butW];
    [self showThreeButtonsAnimated:NO];
    
    [playButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    [prevButton setImage:[UIImage imageNamed:@"icon_prev.png"] forState:UIControlStateNormal];
    [nextButton setImage:[UIImage imageNamed:@"icon_next.png"] forState:UIControlStateNormal];
    [shuffleButton setImage:[UIImage imageNamed:@"icon_shuffle.png"] forState:UIControlStateNormal];
    [repeatButton setImage:[UIImage imageNamed:@"icon_repeat.png"] forState:UIControlStateNormal];
    
    [playButton addTarget:self action:@selector(playPauseButtonHit) forControlEvents:UIControlEventTouchUpInside];
    [prevButton addTarget:self action:@selector(prevButtonHit) forControlEvents:UIControlEventTouchUpInside];
    [nextButton addTarget:self action:@selector(nextButtonHit) forControlEvents:UIControlEventTouchUpInside];
    [shuffleButton addTarget:self action:@selector(shuffleButtonHit) forControlEvents:UIControlEventTouchUpInside];
    [repeatButton addTarget:self action:@selector(repeatButtonHit) forControlEvents:UIControlEventTouchUpInside];
    
    [self setShuffleButtonState:[[Player sharedInstance] shuffleMode]];
    [self setRepeatButtonState:[[Player sharedInstance] repeatMode]];
    
    [self addSubview:buttonsContainer];
}

- (void) showAllButtonsAnimated:(BOOL) animated {
    if (animated) {
        scrubBarView.slider.userInteractionEnabled=YES;
        scrubBarView.userInteractionEnabled=YES;
        shuffleButton.hidden = NO;
        repeatButton.hidden = NO;
        [UIView animateWithDuration:.2 animations:^{
            [self setButtonFrames:@[repeatButton, prevButton, playButton, nextButton, shuffleButton] width:64];
            [self setShuffleButtonState:[[Player sharedInstance] shuffleMode]];
            [self setRepeatButtonState:[[Player sharedInstance] repeatMode]];
            if (IS_DEVICE==IS_IPHONE) {
                CGSize screenSize = [[UIScreen mainScreen] bounds].size;
                if (screenSize.height == 812.0f){
                    if(downed==0)
                    {
                    CGRect butfrm=buttonsContainer.frame;
                    butfrm.origin.y=butfrm.origin.y+20;
                    buttonsContainer.frame=butfrm;
                    downed=1;
                    }
                    
                }
                
            }
        }];
    } else {
        [self setButtonFrames:@[repeatButton, prevButton, playButton, nextButton, shuffleButton] width:64];
        shuffleButton.hidden = NO;
        repeatButton.hidden = NO;
        [self setShuffleButtonState:[[Player sharedInstance] shuffleMode]];
        [self setRepeatButtonState:[[Player sharedInstance] repeatMode]];
    }
}

- (void) showThreeButtonsAnimated:(BOOL) animated {
    if (animated) {
        scrubBarView.slider.userInteractionEnabled=NO;
        
        scrubBarView.userInteractionEnabled=NO;
        [UIView animateWithDuration:.2 animations:^{
            [self setButtonFrames: @[prevButton, playButton, nextButton] width:106];
            shuffleButton.alpha = 0;
            repeatButton.alpha = 0;
            if (IS_DEVICE==IS_IPHONE) {
                CGSize screenSize = [[UIScreen mainScreen] bounds].size;
                if (screenSize.height == 812.0f){
                    if(downed==1){
                        CGRect butfrm=buttonsContainer.frame;
                        butfrm.origin.y=butfrm.origin.y-20;
                        buttonsContainer.frame=butfrm;
                        downed=0;
                    }
                    
                }
            }
        } completion:^(BOOL finished) {
            shuffleButton.hidden = YES;
            repeatButton.hidden = YES;
        }];
    } else {
        [self setButtonFrames:@[prevButton, playButton, nextButton] width:106];
        shuffleButton.alpha = 0;
        repeatButton.alpha = 0;
        shuffleButton.hidden = YES;
        repeatButton.hidden = YES;
    }
}

- (void) setButtonFrames:(NSArray*)buttons width:(NSInteger) width {
    NSInteger index = 0;
    for (UIButton* button in buttons) {
        button.frame = CGRectMake(index*width, 0, width, 64);
        index++;
    }
}

- (void)handleSongStartedBuffering:(NSNotification *)notification {
    [playButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];
    [self updateMetadata];
}

- (void)handleSongStartedPlay:(NSNotification *)notification {
    [playButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];
    [self updateMetadata];
}

- (void) updateMetadata {
    
    if (!scrubbing) {
        
        PlaylistSong *song = [[Player sharedInstance] currentPlayingSong];
        songNameMarquee.text = song.name;
        songNameMarquee.textAlignment = NSTextAlignmentCenter;
        
        artistNameMarquee.text = song.artist;
        artistNameMarquee.textAlignment = NSTextAlignmentCenter;
        
        //scrubBarView.audioLoaded.trackImage
        
        CachedPlaylistSong *existingSong = [CachedPlaylistSong MR_findFirstByAttribute:@"songId" withValue:song.songId];
        //existingSong.cachedPlaylists;
        NSLog(@"<<<<<<<<< is in CACHE >>>>>>> %@, %hhd, %@",existingSong.name, existingSong.downloaded.boolValue, song.playlistId);
        
        if(existingSong) {
            
            scrubBarView.isCache = YES;
            
            CGFloat inset = 7+17.5f;
            
            UIImage *trackSlider = [Utils image:[[UIImage imageNamed:@"scrub_bar_background.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, inset, 0, inset)] byApplyingAlpha:0.4f];
            UIImage *backgroundSlider = [Utils image:[[UIImage imageNamed:@"scrub_bar_background.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, inset, 0, inset)] byApplyingAlpha:0.2f];
            
            [scrubBarView.audioLoaded setProgressImage:backgroundSlider];
            [scrubBarView.audioLoaded setTrackImage:trackSlider];
            
        }
        
    }
}

- (void) handleSongResumed:(NSNotification *)notification {
    [playButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];
}

- (void) resetInterfaceWithPlayer {
    [self updateMetadata];
    if ([[[Player sharedInstance] currentPlayingSong] isPlaying]) {
        [playButton setImage:[UIImage imageNamed:@"icon_pause.png"] forState:UIControlStateNormal];
    } else {
        [playButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
    }
}

- (void) playPauseButtonHit {
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.playPauseButtonHit object:nil userInfo:nil];
}

- (void) nextButtonHit {
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.nextButtonHit object:nil userInfo:nil];
}

- (void) prevButtonHit {
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.prevButtonHit object:nil userInfo:nil];
}

- (void) shuffleButtonHit {
    [self setShuffleButtonState:[[Player sharedInstance] toggleShuffle]];
}

- (void) songActionButtonHit:(id) sender {
    if ([sender tag] == songActionButtonAddToTag) {
        if(IS_DEVICE==IS_IPAD)
        {
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[[Player sharedInstance] currentPlayingSong] forKey:@"AddTo"];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"shouldLaunchSegue"
             object:self userInfo:dictionary];
        }
        [_mainViewController addToWithPlaylistSongs:@[[[Player sharedInstance] currentPlayingSong]]];
    } else if ([sender tag] == songActionButtonDiscoverTag) {
        {
            //  [_masterViewController performSegueWithIdentifier:@"discoverSegue" sender:[[Player sharedInstance] currentPlayingSong]];
            if(IS_DEVICE==IS_IPAD)
            {
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[[Player sharedInstance] currentPlayingSong] forKey:@"Discover"];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"shouldLaunchSegue"
                 object:self userInfo:dictionary];
            }
            else
            {
                [_mainViewController performSegueWithIdentifier:@"discoverSegue" sender:[[Player sharedInstance] currentPlayingSong]];
            }
            
        }
    } else if ([sender tag] == songActionButtonShareTag) {
        if(IS_DEVICE==IS_IPAD)
        {
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[[Player sharedInstance] currentPlayingSong] forKey:@"Share"];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"shouldLaunchSegue"
             object:self userInfo:dictionary];
        }
        else
        {
            [_mainViewController presentViewController:[Utils sharingViewControllerForSong:[[Player sharedInstance] currentPlayingSong]] animated:YES completion:nil];
        }
        
        // UIActivityViewController *activityViewController =
        
        /*UIViewController *activityViewController = [Utils sharingViewControllerForSong:[[Player sharedInstance] currentPlayingSong]];
         self.popover = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
         self.popover.delegate = self;
         [self.popover presentPopoverFromRect:CGRectMake(self.frame.size.width/2, self.frame.size.width/2, 100, 100) inView:self permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];*/
    }
}

- (void) setShuffleButtonState:(NSInteger)state {
    if (state == ShuffleModeOff) {
        shuffleButton.alpha = 0.3;
    } else {
        shuffleButton.alpha = 1;
    }
}

- (void) setRepeatButtonState:(NSInteger)state {
    if (state == RepeatModeAll) {
        repeatButton.alpha = 1;
        [repeatButton setImage:[UIImage imageNamed:@"icon_repeat.png"] forState:UIControlStateNormal];
    } else if (state == RepeatModeOne) {
        repeatButton.alpha = 1;
        [repeatButton setImage:[UIImage imageNamed:@"icon_repeat_one.png"] forState:UIControlStateNormal];
    } else if (state == RepeatModeOff) {
        repeatButton.alpha = 0.3;
        [repeatButton setImage:[UIImage imageNamed:@"icon_repeat.png"] forState:UIControlStateNormal];
    }
}

- (void) repeatButtonHit {
    [self setRepeatButtonState:[[Player sharedInstance] toggleRepeat]];
}

- (void) songEnded:(NSNotification *)notification {
    [playButton setImage:[UIImage imageNamed:@"icon_play.png"] forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark ScrubBarViewDelegate

- (void)scrubBarSpeedChanged:(CGFloat)speed {
    if (speed == 1.0f) {
        songNameMarquee.text = @"Hi-Speed Scrubbing";
    } else if (speed == 0.5f) {
        songNameMarquee.text = @"Half-Speed Scrubbing";
    } else if (speed == 0.25f) {
        songNameMarquee.text = @"Quarter-Speed Scrubbing";
    } else if (speed == 0.1f) {
        songNameMarquee.text = @"Fine Scrubbing";
    }
    
    artistNameMarquee.text = @"Slide your finger to adjust the scrubbing rate";
    songNameMarquee.textAlignment = NSTextAlignmentCenter;
    artistNameMarquee.textAlignment = NSTextAlignmentCenter;
}

- (void)scrubBarDragStarted {
    scrubbing = YES;
   
    [self scrubBarSpeedChanged:1.0f];
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.isScrubbing object:nil];
    
}

- (void)scrubBarDragEnded {
    scrubbing = NO;
    [self updateMetadata];
  [[NSNotificationCenter defaultCenter] postNotificationName:@"ended" object:nil];
   

}


- (void)showDropHereToPlayNext {
    [UIView animateWithDuration:0.3 animations:^{
        buttonsContainer.alpha = 0;
        dropToPlayNext.alpha = 0.7;
    }];
}

- (void) highlightDropHereToPlayNext:(BOOL)highlight {
    float alpha = 0.7;
    if (highlight) alpha = 1;
    [UIView animateWithDuration:0.3 animations:^{
        dropToPlayNext.alpha = alpha;
    }];
}

- (void)showControls {
    [UIView animateWithDuration:0.3 animations:^{
        buttonsContainer.alpha = 1;
        dropToPlayNext.alpha = 0;
    }];
}

- (void) reposition {
    NSArray *buttonList = @[repeatButton, prevButton, playButton, nextButton, shuffleButton];
    CGFloat butW = 64.0f;
    
    
    //dropToPlayNext.frame = CGRectMake(dropToPlayNext.frame.origin.x, dropToPlayNext.frame.origin.y, [Utils currentWidth], dropToPlayNext.frame.size.height);
    
    scrubBarView.frame = CGRectMake( ( [Utils currentWidth] - scrubBarView.frame.size.width ) / 2, scrubBarView.frame.origin.y, scrubBarView.frame.size.width, scrubBarView.frame.size.height);
    
    Log(@"songmetadata x: %f", ( songMetadataView.frame.size.width ) );
    songMetadataView.frame = CGRectMake(songMetadataView.frame.origin.x, songMetadataView.frame.origin.y, [Utils currentWidth], songMetadataView.frame.size.height);
    
    buttonsContainer.frame = CGRectMake( ([Utils currentWidth] - [buttonList count] * butW ) / 2, 0, [buttonList count] * butW, 64);
    CGRect frame = songMetadataView.frame;
    frame.origin.y += songMetadataView.frame.size.height + 15;
    frame.size.height = 45;
    
    if(IS_DEVICE == IS_IPAD) {
        frame.origin.x = ( [Utils currentWidth] - 320 ) / 2 - 7.0f;
    }
    
    songActionsView.frame = frame;
    
    for (UIView *subview in [self subviews]) {
        if (subview.tag == 111) {
            subview.frame = CGRectMake( subview.frame.origin.x, subview.frame.origin.y, [Utils currentWidth], self.frame.size.height);
        }
    }
    
    [dropToPlayNext removeFromSuperview];
    [self setupDropToPlayNext];
}

@end

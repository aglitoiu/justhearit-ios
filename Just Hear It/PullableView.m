
#import "PullableView.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation PullableView

@synthesize closedCenter;
@synthesize openedCenter;
@synthesize hiddenCenter;
@synthesize dragRecognizer;
@synthesize tapRecognizer;
@synthesize animate;
@synthesize animationDuration;
@synthesize delegate;
@synthesize headerHeight;
@synthesize viewState;
@synthesize isscrubbing;
@synthesize isDragging;

- (void)initDragWithHeaderHeight:(NSInteger)height {
    headerHeight = height;

    [self.handleView initializeWithHeaderHeight:headerHeight];

    animate = YES;
    animationDuration = 0.2;

    toggleOnTap = NO;

    dragRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleDrag:)];
    dragRecognizer.minimumNumberOfTouches = 1;
    dragRecognizer.maximumNumberOfTouches = 1;

    [self.handleView addGestureRecognizer:dragRecognizer];
    [dragRecognizer release];
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    
    [self.handleView addGestureRecognizer:tapRecognizer];
    [tapRecognizer release];

    
    /* UITapGestureRecognizer *tapGestureOnView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnView:)];
    tapGestureOnView.numberOfTapsRequired = 1; */
    //tapGestureOnView.numberOfTouchesRequired = 0;
    //[self.handleView addGestureRecognizer:tapGestureOnView];
    
    /* UISwipeGestureRecognizer* swipeUpGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUpFrom:)];
    swipeUpGestureRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    
    [self.handleView addGestureRecognizer:swipeUpGestureRecognizer]; */
    
    /* UISwipeGestureRecognizer* swipeDownGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUpFrom:)];
    swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.handleView addGestureRecognizer:swipeDownGestureRecognizer]; */
    
    viewState = STATE_HIDDEN;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setScrubbingState:) name:Ev.isScrubbing object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setScrubbingStateOff:) name:@"ended" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"dis"
                                               object:nil];
}
- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    dragRecognizer.enabled=NO;
    tapRecognizer.enabled=NO;
}

-(void)handleSwipeUpFrom:(UISwipeGestureRecognizer *)sender
{
    //Log(@"swipe up from");
    [self recursivelyEnable:YES panGesturesInSuperview:self.handleView];
}

- (void) setScrubbingState: (NSNotification *)n {
    Log(@"scrubbing state: %hhd", [n.userInfo[@"state"] isEqual: @YES]);
    
    dragRecognizer.enabled=NO;
    tapRecognizer.enabled=NO;
        isscrubbing = YES;
    
    
    Log(@"is scrubbing: %hhd", isscrubbing);
}
- (void) setScrubbingStateOff: (NSNotification *)n {
    Log(@"scrubbing state: %hhd", [n.userInfo[@"state"] isEqual: @YES]);
    dragRecognizer.enabled=YES;
    tapRecognizer.enabled=YES;
    
    isscrubbing = NO;
    
    
    Log(@"is scrubbing: %hhd", isscrubbing);
}

- (void) startDrag {

    isDragging = YES;
    
    if ([delegate respondsToSelector:@selector(pullableViewStartedDrag:fromState:)]) {
        [delegate pullableViewStartedDrag:self fromState:self.viewState];
    }

    startPos = self.center;
    //Log(@"start pos: %@", NSStringFromCGPoint(startPos));
    
    // Determines if the view can be pulled in the x or y axis
    verticalAxis = closedCenter.x == openedCenter.x;

    // Finds the minimum and maximum points in the axis
    if (verticalAxis) {
        minPos = closedCenter.y < openedCenter.y ? closedCenter : openedCenter;
        maxPos = closedCenter.y > openedCenter.y ? closedCenter : openedCenter;
    } else {
        minPos = closedCenter.x < openedCenter.x ? closedCenter : openedCenter;
        maxPos = closedCenter.x > openedCenter.x ? closedCenter : openedCenter;
    }

//    [self.superview bringSubviewToFront:self];
}

- (void) stopDrag:(UIPanGestureRecognizer*) sender {
    // Gets the velocity of the gesture in the axis, so it can be
    // determined to which endpoint the state should be set.
    
    isDragging = NO;
    
    CGPoint vectorVelocity = [sender velocityInView:self.superview];
    CGFloat axisVelocity = verticalAxis ? vectorVelocity.y : vectorVelocity.x;

    CGPoint target = axisVelocity < 0 ? minPos : maxPos;
    BOOL op = CGPointEqualToPoint(target, openedCenter);
    
    [self setState:op ? STATE_OPEN : STATE_CLOSED animated:animate];
}

- (void) dragging:(UIPanGestureRecognizer*) sender {
    
    isDragging = YES;
    
    CGPoint translate = [sender translationInView:self.superview];
    
    CGPoint newPos;

    // Moves the view, keeping it constrained between openedCenter and closedCenter
    if (verticalAxis) {

        //Log(@"view state: %ld", (long)viewState);
        
        if(viewState == 1) {
            startPos = openedCenter;
        }else {
            startPos = closedCenter;
        }
        
        newPos = CGPointMake(startPos.x, startPos.y + translate.y);

        if (newPos.y < minPos.y) {
            newPos.y = minPos.y;
            translate = CGPointMake(0, newPos.y - startPos.y);
        }

        if (newPos.y > maxPos.y) {
            newPos.y = maxPos.y;
            translate = CGPointMake(0, newPos.y - startPos.y);
        }

    } else {

        newPos = CGPointMake(startPos.x + translate.x, startPos.y);

        if (newPos.x < minPos.x) {
            newPos.x = minPos.x;
            translate = CGPointMake(newPos.x - startPos.x, 0);
        }

        if (newPos.x > maxPos.x) {
            newPos.x = maxPos.x;
            translate = CGPointMake(newPos.x - startPos.x, 0);
        }
    }
    
    //Log(@"translate: %@", NSStringFromCGPoint(translate));
    //Log(@"new center: %@", NSStringFromCGPoint(newPos));
    
    [sender setTranslation:translate inView:self.superview];
    
    
    
    self.center = newPos;

    if ([delegate respondsToSelector:@selector(pullableView:movingToYOffset:)]) {
        [delegate pullableView:self movingToYOffset:self.center.y - self.frame.size.height/2];
    }
}

- (void)handleDrag:(UIPanGestureRecognizer *)sender {
    
    //CGPoint touchPoint = CGPointMake([sender locationInView:self.superview].x,[sender locationInView:self.superview].y);
    //CGPoint distance = [sender translationInView:self];
    //Log(@"point of drag, %@, %d, %d", NSStringFromCGPoint(touchPoint), ( [Utils currentWidth] - 320 ) / 2 , (( [Utils currentWidth] - 320) / 2 ) + 320 );
    CGPoint velocity = [sender velocityInView:self]; // get velocity of pan/swipe in the view in which the gesture recognizer was added
    //float usersSwipeSpeed = abs(velocity.x); // use this if you need to move an object at a speed that matches the users swipe speed
    //Log(@"swipe speed: %@", NSStringFromCGPoint(velocity));
    /* if(velocity.y > 20 ) {
        return;
    } */
    //Log(@"is scrubbing: %hhd", isscrubbing);
    //Log(@"state: %d", [sender state]);
    
    //Log(@"velocity y: %f, velocity x: %f", velocity.y - 100.0f, velocity.x);
    
    //dragRecognizer.enabled = YES;
    
    if(isscrubbing) {
        
        return;
        
    }
        else {
               if ([sender state] == UIGestureRecognizerStateBegan) {
                
            [self startDrag];

        } else if ([sender state] == UIGestureRecognizerStateChanged) {

        [self dragging:sender];

        } else if ([sender state] == UIGestureRecognizerStateEnded) {

            [self stopDrag:sender];
            
        }
            
        
    }
    
}

- (void)recursivelyEnable:(BOOL)enable panGesturesInSuperview:(UIView *)superview
{
    for(UIPanGestureRecognizer *recognizer in superview.gestureRecognizers)
    {
        if ([recognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            
            //Log(@"recognizer: %@", recognizer);
            
            if([superview isKindOfClass:[UIScrollView class]])[(UIScrollView *)superview setScrollEnabled:enable];
            else [recognizer setEnabled:enable];
            
        }
    }
    if(superview.superview)[self recursivelyEnable:enable panGesturesInSuperview:superview.superview];
}

- (void)handleTapOnView:(UITapGestureRecognizer *)sender {
    //Log(@"tapped that ass");
    [self recursivelyEnable:YES panGesturesInSuperview:self.handleView];
    
}

- (void)handleTap:(UITapGestureRecognizer *)sender {
    
    //CGPoint touchPoint = CGPointMake([sender locationInView:self.superview].x,[sender locationInView:self.superview].y);
    
    /*if ( ( touchPoint.y >= PLAYER_DRAWER_HEIGHT &&
          touchPoint.y <= PLAYER_DRAWER_HEIGHT + 50 + SCRUB_BAR_HEIGHT ) &&
        ( touchPoint.x >= ([Utils currentWidth] - 320) / 2 &&
         touchPoint.x <= (([Utils currentWidth] - 320) / 2 ) + 320 ) )
    {
        
        return;
        
    }*/
    
    //dragRecognizer.enabled = YES;
    //Log(@"tapped that ass");
    
    if ([sender state] == UIGestureRecognizerStateEnded) {
        [self setState:viewState == STATE_OPEN ? STATE_CLOSED : STATE_OPEN animated:animate];
    }
}

- (void)setToggleOnTap:(BOOL)tap {
    toggleOnTap = tap;
    tapRecognizer.enabled = tap;
}

- (BOOL)toggleOnTap {
    return toggleOnTap;
}

- (void)setState:(int)state animated:(BOOL)anim {
    int currentState = (int)viewState;
    viewState = state;

    if (anim) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    }

    if ([delegate respondsToSelector:@selector(pullableView:willChangeStateFromState:toState:)]) {
        [delegate pullableView:self willChangeStateFromState:currentState toState:viewState];
    }

    [self applyState:viewState];

    if (anim) {

        // For the duration of the animation, no further interaction with the view is permitted
      //  dragRecognizer.enabled = NO;
       // tapRecognizer.enabled = NO;

        [UIView commitAnimations];

    } else {

       if ([delegate respondsToSelector:@selector(pullableView:didChangeState:)]) {
            [delegate pullableView:self didChangeState:viewState];
        }
    }
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (finished) {
        UISelectionFeedbackGenerator *selfeedbacGenerator = [[UISelectionFeedbackGenerator alloc] init];
        [selfeedbacGenerator prepare];
        [selfeedbacGenerator selectionChanged];
        AudioServicesPlaySystemSound(1519);
        // Restores interaction after the animation is over
       // dragRecognizer.enabled = YES;
       // tapRecognizer.enabled = toggleOnTap;

        if ([delegate respondsToSelector:@selector(pullableView:didChangeState:)]) {
            [delegate pullableView:self didChangeState:viewState];
            
            
            //
        }
    }
}



- (void) closeView {
    self.center = self.closedCenter;
    if ([delegate respondsToSelector:@selector(pullableView:movingToYOffset:)]) {
        [delegate pullableView:self movingToYOffset:self.center.y - self.frame.size.height/2];

    }
}

- (void) openView {
    self.center = self.openedCenter;
    [[NSNotificationCenter defaultCenter] postNotificationName:Ev.shouldHideSpinner object:nil userInfo:nil];
    //Log(@" opened center %@", NSStringFromCGPoint(self.openedCenter));
    
    if ([delegate respondsToSelector:@selector(pullableView:didChangeState:)]) {
        [delegate pullableView:self didChangeState:viewState];
    }
    if ([delegate respondsToSelector:@selector(pullableView:movingToYOffset:)]) {
        [delegate pullableView:self movingToYOffset:self.center.y - self.frame.size.height/2];
    }
    
}

- (void) hideView {
    self.center = self.hiddenCenter;
    
    //Log(@" center hidden: %@",NSStringFromCGPoint(self.hiddenCenter));
    
    if ([delegate respondsToSelector:@selector(pullableView:movingToYOffset:)]) {
        [delegate pullableView:self movingToYOffset:self.center.y - self.frame.size.height/2];
    }
}

- (void) applyState:(int) state {
    switch (state) {
        case STATE_OPEN:
            [self openView];
            break;

        case STATE_CLOSED:
            [self closeView];
            break;

        case STATE_HIDDEN:
            [self hideView];
            break;

        default:
            break;
    }
}

- (void) enableInteraction {
    self.handleView.userInteractionEnabled = YES;
    for (UIGestureRecognizer * rec in self.handleView.gestureRecognizers) {
        [rec setEnabled:true];
    }
}

- (void) setStateOpenAnimated {
    [self setState:STATE_OPEN animated:YES];
}

- (void) setStateClosedAnimated{
    [self setState:STATE_CLOSED animated:YES];
}


- (void) disableInteraction {
    self.handleView.userInteractionEnabled = NO;
    for (UIGestureRecognizer * rec in self.handleView.gestureRecognizers) {
        [rec setEnabled:false];
    }
}



@end

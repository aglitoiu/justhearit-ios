//
//  TopHeaderView.h
//  Just Hear It
//
//  Created by andrei st on 10/15/13.
//  Copyright (c) 2013 HearIt Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationBarView : UIView

@property(nonatomic, strong) IBOutlet UIImageView *logoView;
@property(nonatomic, strong) IBOutlet UIButton *btnMNU;
@property(nonatomic, strong) IBOutlet UIButton *btnFNGR;
@property(nonatomic, strong) UIImageView *handle;
@property(nonatomic, strong) UIImageView *handleShadow;
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, strong) UILabel *subTitleLabel;

- (void)setup;

- (void) showLogo;

- (void)showTitle:(NSString *)title :(NSString *) subtitle;
-(void) refreshTitle;


- (void)showRedHandle;
- (void)showHalfRedHandle;

- (void)showNormalHandle;
@end

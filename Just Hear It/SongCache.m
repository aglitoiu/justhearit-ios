#import "AFNetworking/AFRKHTTPRequestOperation.h"
#import "NSArray+OCTotallyLazy.h"
#import "PlaylistSong.h"
#import "SongCache.h"
#import "CachedPlaylistSong.h"
#import "CachedLocalPlaylistSong.h"
#import "FileManager.h"
#import "SongCacheDownloadManager.h"
#import "CachedPlaylist.h"
#import "CachedPlaylist+Utils.h"
#import "CachedLocalPlaylist.h"
#import "CachedLocalPlaylist+Utils.h"
#import "NSSet+OCTotallyLazy.h"


@implementation SongCache

@synthesize song;

+ (SongCache *) initWithSong:(PlaylistSong *)song {
    SongCache *cache = [[SongCache alloc] init];
    cache.song = song;
    return cache;
}



+ (NSArray *) songsOnlyInCachedPlaylist:(CachedPlaylist *)cachedPlaylist {
    return _.filter(cachedPlaylist.cachedPlaylistSongs.asArray, ^BOOL(CachedPlaylistSong *song){
        //songs that are downloaded and only exist in the history playlist

        __block BOOL existInOtherPlaylists = NO;

        [song.cachedPlaylists.asArray foreach:^(CachedPlaylist *p) {
            if (!([p.playlistId isEqualToString:cachedPlaylist.playlistId])) {
                existInOtherPlaylists = YES;
            }
        }];

        return song.downloaded.boolValue && !existInOtherPlaylists;
    });
}

+ (NSArray *) songsOnlyInCachedLocalPlaylist:(CachedLocalPlaylist *)cachedLocalPlaylist {
    
    Log(@"songs only in cached local playlist: %@", cachedLocalPlaylist.cachedLocalPlaylistSongs.asArray);
    //NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    NSArray *alls = [CachedLocalPlaylistSong MR_findByAttribute:@"playlistId" withValue:cachedLocalPlaylist.playlistId];
    
    Log(@"songs in cached local playlist: %@", alls);
    
    return _.filter(cachedLocalPlaylist.cachedLocalPlaylistSongs.asArray, ^BOOL(CachedLocalPlaylistSong *song){
        
        __block BOOL toDelete = NO;
        Log(@"to be deleted %d", [song.playlistId isEqualToString:cachedLocalPlaylist.playlistId]);
        if([song.playlistId isEqualToString:cachedLocalPlaylist.playlistId]) {
            toDelete = YES;
        }
        
        return toDelete;
    });
}

+ (NSArray *) songsOnlyInCachedMusicFeed:(CachedLocalPlaylist *)cachedLocalPlaylist {
    
    CachedLocalPlaylist *musicFeedPlaylist = [CachedLocalPlaylist MR_findFirstByAttribute:@"playlistType" withValue:@"5"];
    NSArray *cachedPlaylistSongs = [CachedLocalPlaylistSong MR_findByAttribute:@"playlistId" withValue:cachedLocalPlaylist.playlistId];
    
   Log(@"music feed playlist id: %@", musicFeedPlaylist.playlistId);
    
    return _.filter(musicFeedPlaylist.cachedLocalPlaylistSongs.asArray, ^BOOL(CachedLocalPlaylistSong *song){
        
        __block BOOL toDelete = NO;
     //   Log(@"number of songs to check: %d", [cachedLocalPlaylist.cachedLocalPlaylistSongs.asArray count]);
        for (CachedLocalPlaylistSong *songC in cachedPlaylistSongs) {

       //     Log(@"mf songid : %@, cp songid: %@", song.songId, songC.songId);
            
            if([song.songId isEqualToString:songC.songId]) {
                toDelete = YES;
         //       Log(@"mf playlistid: %@, cp playlistid: %@", songC.playlistId, song.playlistId);
                break;
            }
        }
        
        return toDelete;
    });
}

+ (NSArray *) songsNotCompleted {
    return [CachedPlaylistSong MR_findByAttribute:@"downloaded" withValue:@(NO)];
}

+ (void) deleteSongFromCache:(CachedPlaylistSong *)song {
    [FileManager deleteFileAtPath:[FileManager filePathForSongId:song.songId]];
    [SongCache deleteSongFromCoreData:song];
}

+ (void) deleteSongFromCoreData:(CachedPlaylistSong *)song {
   
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    [song MR_deleteEntityInContext:localContext];
    [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
}

+ (void) deleteSongFromCoreDataNoDownload:(CachedLocalPlaylistSong *)song {
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    [song MR_deleteEntityInContext:localContext];
    [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
}

- (void) saveToCachedPlaylist:(CachedPlaylist *)playlist{
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    
        //   NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"(playlistId = %d)", song.playlistId];
    //[CachedPlaylistSong MR_deleteAllMatchingPredicate:predicate1];
   // [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
    CachedPlaylistSong *existingSong = [CachedPlaylistSong MR_findFirstByAttribute:@"songId" withValue:song.songId];
    //[[SongCacheDownloadManager sharedInstance] killQueue];


    if (existingSong) {
        //alert(@"Existing");
        //if song is already in coredata
                if (![playlist songAlreadyInPlaylist:existingSong]) {
            
            Log(@"songalreadyinplayilist");
            //otherwise, check if the song is already in the playlist
            [playlist addCachedPlaylistSongsObject:existingSong];
            [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
        }

                   Log(@"!downloaded, pushing song");
            [[SongCacheDownloadManager sharedInstance] pushSong:existingSong];
        

    } else {
        //if the song is not in coredata, just add it and make a ref to whatever playlist was sent to
        Log(@"creating new core data song");

        CachedPlaylistSong *cachedPlaylistSong = [CachedPlaylistSong MR_createEntityInContext:localContext];

        cachedPlaylistSong.name     = song.name;
        cachedPlaylistSong.artist   = song.artist;
        cachedPlaylistSong.album    = song.album;
        cachedPlaylistSong.songId   = song.songId;
        cachedPlaylistSong.url      = song.url;
        cachedPlaylistSong.lastListenedAt = [NSDate date];
        cachedPlaylistSong.downloaded = @(NO);
        cachedPlaylistSong.nodownload = @(NO);
        cachedPlaylistSong.position=song.position;
        cachedPlaylistSong.order=song.order;
        

        [playlist addCachedPlaylistSongsObject:cachedPlaylistSong];
        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];

        [[SongCacheDownloadManager sharedInstance] pushSong:cachedPlaylistSong];
    }
}

- (void) updateNewPlaylistSong {
    
    /*
    
    CachedLocalPlaylistSong *existingSong = [CachedLocalPlaylistSong MR_findFirstByAttribute:@"songId" withValue:song.songId];
    Log(@"existing song: %@", existingSong);
    if(existingSong) {
        existingSong.isNewPlaylistSong = @"0";
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            //Log(@"saved");
        }];
    }*/
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    NSArray *existingSongs = [CachedLocalPlaylistSong MR_findByAttribute:@"songId" withValue:song.songId];
   
    for(CachedLocalPlaylistSong *existingSong in existingSongs) {
        existingSong.isNewPlaylistSong = @"0";
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            //Log(@"saved");
        }];

    }
    
}

- (void) saveToCachedPlaylistNoDownload:(CachedLocalPlaylist *)playlist andOrder:(NSNumber *)order{
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];

    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"(playlistId = %d) AND (songId = %d) AND (playlistSongId = %d)", song.playlistId, song.songId, song.playlistSongId];
    
    CachedLocalPlaylistSong *existingSong = [CachedLocalPlaylistSong MR_findFirstWithPredicate:predicate1];
    
    //CachedPlaylistSong *existingSong = [CachedPlaylistSong MR_findFirstByAttribute:@"songId" withValue:song.songId];

    
    if (existingSong) {
        //if song is already in coredata
        
        //otherwise, check if the song is already in the playlist
        [playlist addCachedLocalPlaylistSongsObject:existingSong];
        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
        
        /*if (!existingSong.downloaded.boolValue)
            [[SongCacheDownloadManager sharedInstance] pushSong:existingSong];*/
        
    } else {
        //if the song is not in coredata, just add it and make a ref to whatever playlist was sent to
        Log(@"creating new core data song");
        
        CachedLocalPlaylistSong *cachedPlaylistSong = [CachedLocalPlaylistSong MR_createEntityInContext:localContext];
        
        cachedPlaylistSong.playlistId     = song.playlistId;
        cachedPlaylistSong.name     = song.name;
        cachedPlaylistSong.artist   = song.artist;
        cachedPlaylistSong.album    = song.album;
        cachedPlaylistSong.songId   = song.songId;
        cachedPlaylistSong.url      = song.url;
        cachedPlaylistSong.lastListenedAt = [NSDate date];
        cachedPlaylistSong.createdAt = song.createdAt;
        cachedPlaylistSong.downloaded = @(NO);
        cachedPlaylistSong.nodownload = @(YES);
        cachedPlaylistSong.order = order;
        //Log(@"token: %@", song.token);
        cachedPlaylistSong.token = song.token;
        cachedPlaylistSong.playlistSongId = song.playlistSongId;
        
        Log(@"song position for cache: %@", song.position);
        //Log(@"song position in cache: %@", [NSString stringWithFormat:@"%ld",(long)order]);
        
        cachedPlaylistSong.position = song.position;
        cachedPlaylistSong.isNewPlaylistSong = [song.isNewPlaylistSong stringValue];
        
        cachedPlaylistSong.shareUrl = song.shareUrl;
        
        [playlist addCachedLocalPlaylistSongsObject:cachedPlaylistSong];
        //[localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
        
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            //Log(@"saved");
        }];
        
        //[[SongCacheDownloadManager sharedInstance] pushSong:cachedPlaylistSong];
    }
}




+ (NSMutableArray *)cachedPlaylistSongsAsPlaylistSongs:(NSArray *)cachedPlaylistSongs {

    return _.array(cachedPlaylistSongs).filter(^BOOL(CachedPlaylistSong *cachedPlaylistSong){
        return cachedPlaylistSong.downloaded.boolValue;
    }).map(^(CachedPlaylistSong *cachedPlaylistSong){
        PlaylistSong *song = [[PlaylistSong alloc] init];
        song.name = cachedPlaylistSong.name;
        song.artist = cachedPlaylistSong.artist;
        song.album = cachedPlaylistSong.album;
        song.songId = cachedPlaylistSong.songId;
        song.position=cachedPlaylistSong.position;
        song.order=cachedPlaylistSong.order;
        song.createdAt=cachedPlaylistSong.createdAt;
        return song;
    }).unwrap.mutableCopy;

}

+ (NSMutableArray *)cachedPlaylistSongsAsPlaylistSongsNoDownload:(NSArray *)cachedPlaylistSongs {
    
    return _.array(cachedPlaylistSongs).map(^(CachedLocalPlaylistSong *cachedPlaylistSong){
        PlaylistSong *song = [[PlaylistSong alloc] init];
        song.name = cachedPlaylistSong.name;
        song.artist = cachedPlaylistSong.artist;
        song.album = cachedPlaylistSong.album;
        song.songId = cachedPlaylistSong.songId;
        song.playlistId = cachedPlaylistSong.playlistId;
        song.url = cachedPlaylistSong.url;
        song.createdAt = cachedPlaylistSong.createdAt;
        song.isNewPlaylistSong = [NSNumber numberWithInt:[cachedPlaylistSong.isNewPlaylistSong intValue]];
        //Log(@"is new playlist song %@", song.isNewPlaylistSong)
        song.order = [NSNumber numberWithInteger: [cachedPlaylistSong.order integerValue]];
        Log(@"init song position in db: %@", cachedPlaylistSong.position);
        //Log(@"init song position: %@", [NSNumber numberWithInteger: [cachedPlaylistSong.order integerValue]]);
        //Log(@"song position when load cached playlist: %@", cachedPlaylistSong.position);
        song.position = cachedPlaylistSong.position;
        song.playlistSongId = cachedPlaylistSong.playlistSongId;
        song.shareUrl = cachedPlaylistSong.shareUrl;
        song.token = cachedPlaylistSong.token;
        return song;
    }).unwrap.mutableCopy;
    
}

- (BOOL) fileExists {
    return [[NSFileManager defaultManager] fileExistsAtPath:[FileManager filePathForSongId:song.songId]];
}

- (CachedPlaylistSong *) coreDataSong {
    return [CachedPlaylistSong MR_findFirstByAttribute:@"songId" withValue:song.songId];
}

- (BOOL)isCached {
    return self.fileExists && self.coreDataSong.downloaded.boolValue;
}

- (BOOL) instaPlayAvailable {
    return [[NSFileManager defaultManager] fileExistsAtPath:[FileManager filePathForInstaPlaySongId:song.songId]];
}

@end

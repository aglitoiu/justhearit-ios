#import "TourPageViewController.h"

@implementation TourPageViewController

- (void)viewDidLoad {
    self.imageView.image = [UIImage imageNamed:self.imageFile];
    self.descriptionLabel.text = self.titleText;
}

@end
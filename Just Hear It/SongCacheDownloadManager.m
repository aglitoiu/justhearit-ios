#import "AFNetworking/AFRKHTTPRequestOperation.h"
#import "NSArray+OCTotallyLazy.h"
#import "SongCacheDownloadManager.h"
#import "PlaylistSong.h"
#import "SongCache.h"
#import "FileManager.h"
#import "CachedPlaylistSong.h"
#import "CachedPlaylist.h"
#import "CachedPlaylist+Utils.h"


@implementation SongCacheDownloadManager
int iss;
@synthesize queue, isDownloading, operation, timer;

+ (id)sharedInstance {
    static SongCacheDownloadManager *__sharedInstance;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        __sharedInstance = [[SongCacheDownloadManager alloc] init];
        __sharedInstance.isDownloading = NO;
        [FileManager createCacheFolder];
    });

    return __sharedInstance;
}

- (id) init {
    if (self=[super init]) {
        queue = [[NSMutableArray alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:Ev.reachabilityChanged object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(StartDownloadingAfterReachCh:) name:Ev.gotOnline object:nil];
    }
    return self;
}

- (void)reachabilityChanged:(NSNotification *)notification {
    //[self downloadNextSongIfPossible];
}

- (BOOL) songInQueue:(CachedPlaylistSong *)song {
    __block BOOL exists = NO;

    [queue foreach:^(CachedPlaylistSong *s) {
        if ([s.songId isEqualToString:song.songId]) exists = YES;
    }];

    return exists;
}
-(void) clearQueue{
    isDownloading=NO;
    [queue removeAllObjects];
}
- (void) clearDownload{
  //  isDownloading=NO;
}

- (void)pushSong:(CachedPlaylistSong *)song {
   // alert(song.name);
    if(![self songInQueue:song]) {
        [queue addObject:song];
        if (!isDownloading) {
           
       
            [self cleanupSongs];
            if (queue.count == 0) return;
            Log(@"when song was pushed, manager was not downloading -> starting download NOW");
            if ([self canDownloadReachable]) {
                
                [self downloadSong:queue.firstObject];
            }
            else {
                isDownloading=NO;
            }
        }
    }
}
-(void) StartDownloadingAfterOffline{
    if(!isDownloading)
    {
        if([self canDownloadReachable]){
    [self downloadSong:queue.firstObject];
        }
    
    
}
}
-(void) StartDownloadingAfterReachCh: (NSNotification*)notification {
    if(!isDownloading)
    {
        if([self canDownloadReachable]){
            [self downloadSong:queue.firstObject];
        }
        
        
    }
}


    


- (void) downloadSong:(CachedPlaylistSong *)song {
    [self downloadSong:song toFilePath:[FileManager filePathForSongId:song.songId]];
    
}

- (void) downloadSong:(CachedPlaylistSong *)song toFilePath:(NSString *)path {
    [self cleanupSongs];

    if (queue.count == 0) return;
  
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:song.url]];
    operation = [[AFRKHTTPRequestOperation alloc] initWithRequest:request];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [self downloadStarted];
    //alert(song.name);
    if(!song.nodownload.boolValue) {
    [operation setCompletionBlockWithSuccess:^(AFRKHTTPRequestOperation *operation, id responseObject) {
      

        
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        song.downloaded = @(YES);
        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {}];
        [self songDownloadFinished:song withSuccess:YES];
        
    } failure:^(AFRKHTTPRequestOperation *operation, NSError *error) {
        
        Log(@"song download ERROR");
        [self songDownloadFinished:song withSuccess:NO];
        
    }];

    [operation start];
        
    }
}

- (void)downloadStarted {
    
    isDownloading = YES;
    [self startTimer];
}

- (void)startTimer{
    timer = [NSTimer scheduledTimerWithTimeInterval:60.0
                                               target:self
                                             selector:@selector(checkWifi:)
                                             userInfo:nil
                                              repeats:YES ];
}

- (void)checkWifi:(id)sender {
    
    if (![self canDownloadReachable]) {
        
        [operation cancel];
        
        [timer invalidate];
        timer = nil;
        
    }
    
}

- (void)songDownloadFinished:(CachedPlaylistSong *)song withSuccess:(BOOL)success {

    if (success) {
        [queue removeObject:song];
    } else {
        if ([Utils isReachable]) {
            //if error but reachable, must be a server error, we wont try again
            [queue removeObject:song];
        }
    }

    [self downloadNextSongIfPossible];

}

- (void) downloadNextSongIfPossible {
    if ([self canDownloadReachable]) {
        if (queue.count > 0) {
            Log(@"we downloaded a song and there are still more songs to download. getting the next one");
            [self downloadSong:queue.firstObject];
            
        } else {
            Log(@"we downloaded a song but it was the last one. will STOP downloading");
            isDownloading = NO;
        }
    } else {
        Log(@"we will stop downloading because we're not reachable enough");
        isDownloading = NO;
    }
}

- (BOOL) canDownloadReachable {
    if (SETTINGS.OfflineOnly==NO&& SETTINGS.wifiOnlySync && [Utils isReachableOverWifi]) return YES;
    if (SETTINGS.OfflineOnly==NO&&!SETTINGS.wifiOnlySync && [Utils isReachable]) return YES;
    return NO;
}

- (void)cleanupSongs {

    __block unsigned long long int currentCacheSize = [FileManager cacheFolderSize];
    int maxCacheSize = [SETTINGS.maxCacheGb floatValue]*1000;

    [self cleanupIncompleteSongs];

if    (currentCacheSize > maxCacheSize) {
    NSArray *ch=[CachedPlaylist MR_findAll];
    [ch foreach:^(CachedPlaylist *playlist ){
        
        NSArray *songsOnlyInHistory = [SongCache songsOnlyInCachedPlaylist:playlist];
        [songsOnlyInHistory foreach:^(CachedPlaylistSong *song) {
            if (currentCacheSize > maxCacheSize) {
                Log(@"=== not enough space, deleting song with id %@", song.songId);
                [SongCache deleteSongFromCache:song];
                currentCacheSize = [FileManager cacheFolderSize];
                [queue removeAllObjects];
                
            }
        }];
        if(playlist.cachedPlaylistSongs.count==0)
        {
            [CachedPlaylist  removeFromCache:playlist];
            
    
        }

    }];
    
    

        if ([FileManager cacheFolderSize] > maxCacheSize){
            //if we just can't delete any more files, clear the queue
            [queue removeAllObjects];
        }
    }
}

- (void) cleanupIncompleteSongs {
    //a song can be in coredata with downloaded false. if we are not downloading, we should delete all the songs that have littered files

    if (isDownloading) return;

    NSArray *songsNotCompleted = [SongCache songsNotCompleted];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

    [songsNotCompleted foreach:^(CachedPlaylistSong *song) {
        if(!song.nodownload) {
            NSString *path = [paths[0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@", CACHE_FOLDER, song.songId]];
            [FileManager deleteFileAtPath:path];
        }
    }];
    
}

- (void)resumeDownloads {
    if (queue.isEmpty) {
        NSArray *songsNotCompleted = [SongCache songsNotCompleted];
        [songsNotCompleted foreach:^(CachedPlaylistSong *song) {
            if(!song.nodownload.boolValue) {
                [self pushSong:song];
            }
        }];
    }
}
@end

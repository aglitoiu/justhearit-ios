//
// Created by andrei st on 10/15/13.
// Copyright (c) 2013 HearIt Inc. All rights reserved.
//


#import "UIImageView+Utils.h"

@implementation UIImageView (Utils)

+ (UIImageView *)imageNamed:(NSString *)name {
    return [[UIImageView alloc] initWithImage:[UIImage imageNamed:name]];
}

@end
#import "PlaylistGroup.h"
#import "Playlist.h"

@implementation PlaylistGroup

static NSArray* PlaylistGroups;

@synthesize collapsed, key, name;

- (id) init {
    if (self=[super init]) {
        collapsed = NO;
    }
    return self;
}

+ (PlaylistGroup *)initWithKey:(NSDictionary *)key andCollection:(NSMutableArray *)collection {
    PlaylistGroup *playlistGroup = [[PlaylistGroup alloc] init];
    playlistGroup.key = [key objectForKey:@"key"];
    playlistGroup.name = [key objectForKey:@"name"];
    playlistGroup.groupId = [key objectForKey:@"id"];

    _.arrayEach(collection, ^(Playlist* playlist) {
        playlist.playlistGroupObject = playlistGroup;
    });

    playlistGroup.collection = collection;
    return playlistGroup;
}

+ (NSArray *) PlaylistGroups {
    if (!PlaylistGroups)
        PlaylistGroups = @[
                @{
                        @"key": @"default",
                        @"name": @"Default Playlists",
                        @"id": @2
                },
                @{
                        @"key": @"following",
                        @"name": @"Following",
                        @"id": @3
                },
                @{
                        @"key": @"user",
                        @"name": @"My Playlists",
                        @"id": @1
                }];
    return PlaylistGroups;
}

- (BOOL)isDefaultPlaylistsGroup {
    return [key isEqualToString:@"default"];
}

- (BOOL) isMyPlaylistsGroup {
    return [key isEqualToString:@"user"];
}

- (BOOL)isFollowingPlaylistsGroup {
    return [key isEqualToString:@"following"];
}

//- (Playlist *)blankPlaylist {
//    return _.filter(_collection, ^BOOL(Playlist *playlist){
//        return playlist.blank;
//    }).firstObject;
//}
@end

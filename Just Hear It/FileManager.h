#import <Foundation/Foundation.h>

#define CACHE_FOLDER @"/cache"
#define INSTAPLAY_FOLDER @"/instaplay"

@interface FileManager : NSObject


+ (void)createCacheFolder;

+ (NSString *)cacheFolderPath;

+ (NSArray *)filesInCacheFolder;

+ (void) createInstaPlayFolder;
+ (NSString *)instaPlayFolderPath;

+ (unsigned long long int)cacheFolderSize;

+ (unsigned long long int)fileSizeForPath:(NSString *)path;

+ (NSString *)filePathForSongId:(NSString *)songId;

+ (NSString *)filePathForInstaPlaySongId:(NSString *)songId;

+ (void)deleteFileAtPath:(NSString *)path;

@end
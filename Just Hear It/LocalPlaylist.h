#import <Foundation/Foundation.h>
#import "Playlist.h"

@class SearchParams;

@interface LocalPlaylist : Playlist

+ (LocalPlaylist *) localSearchPlaylistWithQuery:(NSString *)query;

+ (LocalPlaylist *)localPlaylistWithSongToken:(NSString *)token;

+ (LocalPlaylist *) localSearchPlaylistsWithQuery:(NSString *)query;

+ (LocalPlaylist *)localSharedPlaylistWithPlaylist:(Playlist *)playlist;

@property(nonatomic, strong) SearchParams *searchParams;

@end
//
//  CachedPlaylistSong.m
//  Just Hear It
//
//  Created by andrei st on 1/16/14.
//  Copyright (c) 2014 HearIt Inc. All rights reserved.
//

#import "CachedPlaylistSong.h"
#import "CachedPlaylist.h"


@implementation CachedPlaylistSong

@dynamic album;
@dynamic artist;
@dynamic fileSize;
@dynamic lastListenedAt;
@dynamic name;
@dynamic songId;
@dynamic url;
@dynamic downloaded;
@dynamic cachedPlaylists;
@dynamic nodownload;
@dynamic isNewPlaylistSong;
@dynamic playlistId;
@dynamic createdAt;
@dynamic order;
@dynamic position;
@dynamic playlistSongId;
@dynamic shareUrl;

@end

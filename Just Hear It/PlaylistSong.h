//
// Created by andrei st on 10/18/13.
// Copyright (c) 2013 HearIt Inc. All rights reserved.
//


#import <Foundation/Foundation.h>

@class Playlist;

@interface PlaylistSong : NSObject

@property(nonatomic, strong) NSString *songId;
@property(nonatomic, strong) NSString *playlistSongId;
@property(nonatomic, strong) NSString *playlistId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *artist;
@property(nonatomic, strong) NSString *album;
@property(nonatomic, strong) NSString *readUrl;
@property(nonatomic, strong) NSString *shareUrl;
@property(nonatomic, strong) NSString *encryptedUrl;
@property(nonatomic, strong) NSString *url;
@property(nonatomic, strong) NSString *token;
@property(nonatomic, strong) NSNumber *fileSize;
@property(nonatomic, strong) NSString *cover;
@property(nonatomic, strong) NSString *thumb;
@property(nonatomic, strong) NSNumber *year;
@property(nonatomic, strong) NSString *genre;
@property(nonatomic, strong) NSNumber *duration;
@property(nonatomic, strong) NSNumber *position;
@property(nonatomic, strong) NSNumber *encrypted;
@property(nonatomic, strong) NSString *playlistSongUpdateId;
@property(nonatomic, strong) NSNumber *isNewPlaylistSong;
@property(nonatomic, strong) NSDate *createdAt;

@property(nonatomic, assign) NSNumber * order;

@property(nonatomic, assign) NSInteger playState;

@property(nonatomic, strong) Playlist *playlist;
@property(nonatomic, strong) Playlist *playlistOrigin;

- (BOOL) isPlayingOrPaused;

- (BOOL)isPlaying;

- (BOOL)isPaused;

- (BOOL)hasProperty:(NSString *)prop;

- (BOOL)isReorderable;

- (BOOL)isEncrypted;

- (BOOL) isNew;
@end
#import "GVUserDefaults+Settings.h"
#import "Session.h"

@implementation GVUserDefaults (Settings)

@dynamic token;
@dynamic udid;
@dynamic userId;
@dynamic maxCacheGb;
@dynamic wifiOnlySync;
@dynamic OfflineOnlySync;
@dynamic fullName;
@dynamic welcomeMessageDisabled;
@dynamic tutorialDisabled;
@dynamic notfirstPlay;
@dynamic notfirstLogin;
@dynamic notfirstSongDrag;

-(void)setnotfirstSongDrag{
    SETTINGS.notfirstSongDrag= [NSNumber numberWithBool:true];
}
-(void)setnotfirstLogin{
    SETTINGS.notfirstLogin= [NSNumber numberWithBool:true];
}
-(void)setnotfirstPlay{
    SETTINGS.notfirstPlay= [NSNumber numberWithBool:true];
}
- (void) setDefaultMaxCacheGb {
    if (SETTINGS.maxCacheGb == nil) {
        SETTINGS.maxCacheGb = [NSNumber numberWithFloat:[self defaultMaxCacheGb]];
    }
}

- (float) defaultMaxCacheGb {
    float freeSpace = [Utils getFreeDiskspace];
    if (freeSpace > 1) return 1;
    return freeSpace;
}
- (BOOL)OfflineOnly {
    return SETTINGS.OfflineOnlySync!= nil && [SETTINGS.OfflineOnlySync boolValue];
}
- (BOOL)wifiOnly {
    return SETTINGS.wifiOnlySync != nil && [SETTINGS.wifiOnlySync boolValue];
}

- (void)setWifiOnly:(BOOL)value {
    SETTINGS.wifiOnlySync = [NSNumber numberWithBool:value];
}
- (void)setOfflineOnly:(BOOL)value {
    SETTINGS.OfflineOnlySync = [NSNumber numberWithBool:value];
}

- (BOOL)isLoggedIn {
    return !(self.token == nil || self.token.isEmpty);
}

- (void)logout {
    self.token = nil;
    self.userId = nil;
    self.fullName = nil;
}

- (void)login:(Session *)session {
    self.token = session.token;

    
    self.userId = session.userId;
    self.fullName = session.fullName;
}


@end

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "Callables.h"
#import "Coercions.h"
#import "EasyEnumerable.h"
#import "Enumerable.h"
#import "EmptyEnumerator.h"
#import "EnumerateEnumerator.h"
#import "FilterEnumerator.h"
#import "FlattenEnumerator.h"
#import "GroupedEnumerator.h"
#import "MapEnumerator.h"
#import "MemoisedEnumerator.h"
#import "MergeEnumerator.h"
#import "NSEnumerator+OCTotallyLazy.h"
#import "PairEnumerator.h"
#import "PartitionEnumerator.h"
#import "RepeatEnumerator.h"
#import "SingleValueEnumerator.h"
#import "TakeWhileEnumerator.h"
#import "Flattenable.h"
#import "Foldable.h"
#import "Function1.h"
#import "Function2.h"
#import "Functions.h"
#import "Group.h"
#import "Mappable.h"
#import "MemoisedSequence.h"
#import "None.h"
#import "NoSuchElementException.h"
#import "NSArray+OCTotallyLazy.h"
#import "NSDictionary+OCTotallyLazy.h"
#import "NSSet+OCTotallyLazy.h"
#import "Numbers.h"
#import "OCTotallyLazy.h"
#import "Option.h"
#import "Pair.h"
#import "Predicates.h"
#import "Queue.h"
#import "Range.h"
#import "Sequence.h"
#import "Some.h"
#import "Types.h"

FOUNDATION_EXPORT double OCTotallyLazyVersionNumber;
FOUNDATION_EXPORT const unsigned char OCTotallyLazyVersionString[];

